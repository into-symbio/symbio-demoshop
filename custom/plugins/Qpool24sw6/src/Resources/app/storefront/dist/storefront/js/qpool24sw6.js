$(document).ready(function(){
    $(".header--service-menu").click(function(){
        $(this).next(".header-service-menu-list").toggleClass("active");
    });

    $("html").click(function(e) {
        if ($(e.target).closest('.nav--entry.service').length == 0)
            $(".header-service-menu-list").removeClass("active");
    });

    $(".search-toggle-btn").click(function(){
        $(".header-main .header-search-col").toggle();
    });

    $('.product-detail-quantity-select').on('change', function() {
        var total = this.value * parseFloat($(".content--totalhiddenprice").text());
        $(".content--totalprice").text(total.toLocaleString('de-DE', { style: 'currency', currency: 'EUR' }, {minimumFractionDigits: 2, maximumFractionDigits: 2}));
    });

    $(window).scroll(function(){
        var sticky = $('.header-main'),
            scroll = $(window).scrollTop();
		if($(window).width() < 768) {
			if (scroll >= 250) {
				sticky.addClass('fixed');
			}
			if (scroll < 5) {
				sticky.removeClass('fixed');
			}			
		} else {			
			if (scroll > 100) {
				sticky.addClass('fixed');
			} else {
				sticky.removeClass('fixed');
			}
		}
    });
    

});


jQuery(".view-more-listings").on('click', function(){
    var shownumber = 2;
    for (let i = 0; i < shownumber; i++) {
        var total = jQuery(".prac-for-sale-block.show").length;
        jQuery(".prac-for-sale-block.show").each(function(index){

            if(index == total - 1){
                jQuery(this).next().addClass('show');
            }
        })
    }
});

