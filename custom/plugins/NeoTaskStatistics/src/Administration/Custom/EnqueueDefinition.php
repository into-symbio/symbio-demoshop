<?php declare(strict_types=1);
/*
 * (c) Neofonie GmbH
 *
 * This computer program is the sole property of Neofonie GmbH
 * (http://www.neofonie.de) and is protected under the German Copyright Act
 * (paragraph 69a UrhG). All rights are reserved. Making copies,
 * duplicating, modifying, using or distributing this computer program
 * in any form, without prior written consent of Neofonie, is
 * prohibited. Violation of copyright is punishable under the
 * German Copyright Act (paragraph 106 UrhG). Removing this copyright
 * statement is also a violation.
 */

namespace NeoTaskStatistics\Administration\Custom;

use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class EnqueueDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'enqueue';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getCollectionClass(): string
    {
        return EnqueueCollection::class;
    }

    public function getEntityClass(): string
    {
        return EnqueueEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new StringField('id', 'id'))
                ->addFlags(new PrimaryKey(), new Required()),
            (new StringField('queue', 'queue'))
                ->addFlags(new Required()),
            (new IntField('count', 'count'))
                ->addFlags(new Required()),
        ]);
    }

    protected function defaultFields(): array
    {
        return [];
    }
}
