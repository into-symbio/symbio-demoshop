<?php declare(strict_types=1);
/*
 * (c) Neofonie GmbH
 *
 * This computer program is the sole property of Neofonie GmbH
 * (http://www.neofonie.de) and is protected under the German Copyright Act
 * (paragraph 69a UrhG). All rights are reserved. Making copies,
 * duplicating, modifying, using or distributing this computer program
 * in any form, without prior written consent of Neofonie, is
 * prohibited. Violation of copyright is punishable under the
 * German Copyright Act (paragraph 106 UrhG). Removing this copyright
 * statement is also a violation.
 */

namespace NeoTaskStatistics\Administration\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use NeoTaskStatistics\Administration\Custom\EnqueueCollection;
use NeoTaskStatistics\Administration\Custom\EnqueueEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\EntityHydrator;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Read\EntityReaderInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntityAggregatorInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearcherInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\VersionManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use function ceil;
use function count;

class EnqueueRepository extends EntityRepository
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var EntityHydrator
     */
    private $entityHydrator;

    /**
     * @var float
     */
    private $shopwareVersion;

    public function __construct(
        EntityDefinition $definition,
        EntityReaderInterface $reader,
        VersionManager $versionManager,
        EntitySearcherInterface $searcher,
        EntityAggregatorInterface $aggregator,
        EventDispatcherInterface $eventDispatcher,
        Connection $connection,
        EntityHydrator $entityHydrator,
        string $shopwareVersion
    ) {
        $this->connection = $connection;
        $this->entityHydrator = $entityHydrator;
        $this->shopwareVersion = (float) $shopwareVersion;

        parent::__construct(
            $definition,
            $reader,
            $versionManager,
            $searcher,
            $aggregator,
            $eventDispatcher
        );
    }

    public function search(Criteria $criteria, Context $context): EntitySearchResult
    {
        if ($criteria->getAggregation('count') === null) {
            return parent::search($criteria, $context);
        }

        $query = $this->createSearchQuery($criteria);

        if ($this->shopwareVersion >= 6.4) {
            return $this->getSearchResult($query, $context, $criteria);
        }

        return $this->getSeachResultDeprecated($query, $context, $criteria);
    }

    private function createSearchQuery(Criteria $criteria): QueryBuilder
    {
        $query = $this->connection->createQueryBuilder();
        $subQuery = clone $query;
        $subQuery
            ->select('enq.id')
            ->from('enqueue', 'enq')
            ->where($subQuery->expr()->eq('enq.queue', 'enqueue.queue'))
            ->setMaxResults(1);

        $query
            ->select('queue as `enqueue.queue`')
            ->addSelect('COUNT(queue) AS `enqueue.count`')
            ->addSelect('(' . $subQuery->getSQL() . ') AS `enqueue.id`')
            ->from('enqueue')
            ->groupBy('queue');

        $selects = $query->getQueryPart('select');
        $selects[0] = 'SQL_CALC_FOUND_ROWS '.$selects[0];
        $query->select($selects);

        $this->addLimitAndOffset($criteria, $query);

        $this->addSorting($criteria, $query);

        return $query;
    }

    private function addSorting(Criteria $criteria, QueryBuilder $query): void
    {
        if (count($criteria->getSorting())) {
            foreach ($criteria->getSorting() as $sorting) {
                $field = $sorting->getField();
                if ($field === 'enqueue.count') {
                    $field = 'COUNT(queue)';
                }
                $query->addOrderBy($field, $sorting->getDirection());
            }
        }
    }

    private function addLimitAndOffset(Criteria $criteria, QueryBuilder $query): void
    {
        if ($criteria->getOffset() !== null) {
            $query->setFirstResult($criteria->getOffset());
        }
        if ($criteria->getLimit() !== null) {
            $query->setMaxResults($criteria->getLimit());
        }
    }

    private function getSearchResult(
        QueryBuilder $query,
        Context $context,
        Criteria $criteria
    ): EntitySearchResult
    {
        $entities = $this->entityHydrator->hydrate(
            new EnqueueCollection(),
            EnqueueEntity::class,
            $this->getDefinition(),
            $this->connection->fetchAllAssociative($query->getSQL()),
            $this->getDefinition()->getEntityName(),
            $context
        );

        return new EntitySearchResult(
            $this->getDefinition()->getEntityName(),
            (int)$this->connection->fetchOne('SELECT FOUND_ROWS()'),
            $entities,
            null,
            $criteria,
            $context,
        );
    }

    /** @deprecated since Shopware 6.4 */
    private function getSeachResultDeprecated(
        QueryBuilder $query,
        Context $context,
        Criteria $criteria
    ): EntitySearchResult {
        $entities = $this->entityHydrator->hydrate(
            new EnqueueCollection(),
            EnqueueEntity::class,
            $this->getDefinition(),
            $this->connection->fetchAll($query->getSQL()),
            $this->getDefinition()->getEntityName(),
            $context
        );

        return new EntitySearchResult(
            (int)$this->connection->fetchColumn('SELECT FOUND_ROWS()'),
            $entities,
            null,
            $criteria,
            $context,
            !$criteria->getLimit() ? 1 : (int) ceil(($criteria->getOffset() ?? 0 + 1) / $criteria->getLimit()),
            $criteria->getLimit()
        );
    }

}
