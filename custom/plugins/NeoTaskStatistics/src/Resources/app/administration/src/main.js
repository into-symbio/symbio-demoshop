/*
 * (c) Neofonie GmbH
 *
 * This computer program is the sole property of Neofonie GmbH
 * (http://www.neofonie.de) and is protected under the German Copyright Act
 * (paragraph 69a UrhG). All rights are reserved. Making copies,
 * duplicating, modifying, using or distributing this computer program
 * in any form, without prior written consent of Neofonie, is
 * prohibited. Violation of copyright is punishable under the
 * German Copyright Act (paragraph 106 UrhG). Removing this copyright
 * statement is also a violation.
 */

import './module/task-statistics-scheduled-tasks';
import './module/task-statistics-index';
import './module/task-statistics-message-queue-stats';
import './module/task-statistics-queue-per-channel';
import './core/filter/scheduled-task-class.filter.js';
import './core/filter/format-datetime.filter.js';
import './component/neo-datepicker'
