/*
 * (c) Neofonie GmbH
 *
 * This computer program is the sole property of Neofonie GmbH
 * (http://www.neofonie.de) and is protected under the German Copyright Act
 * (paragraph 69a UrhG). All rights are reserved. Making copies,
 * duplicating, modifying, using or distributing this computer program
 * in any form, without prior written consent of Neofonie, is
 * prohibited. Violation of copyright is punishable under the
 * German Copyright Act (paragraph 106 UrhG). Removing this copyright
 * statement is also a violation.
 */

const { Component } = Shopware;
const { Mixin } = Shopware;
const { Criteria } = Shopware.Data;

import template from './neo_task_statistics_message_queue_stats.html.twig';

Component.register('task-statistics-message-queue-stats', {
    template,

    inject: ['repositoryFactory', 'context'],

    mixins: [
        Mixin.getByName('listing')
    ],

    data: function () {
       return {
           total: 0,
           items: null,
           repository: null,
           isLoading: false,
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    computed: {
        columns() {
            return [
                { property: 'name', label: this.$tc('task-statistics-message-queue-stats.grid.column.name') },
                { property: 'size', label: this.$tc('task-statistics-message-queue-stats.grid.column.size') },
            ];
        },

        entityRepository() {
            return this.repositoryFactory.create('message_queue_stats');
        },
    },

    methods: {
        getList() {
            this.isLoading = true;
            this.entityRepository = this.repositoryFactory.create('message_queue_stats');
            let criteria = new Criteria()

            this.entityRepository
                .search(criteria, Shopware.Context.api)
                .then(result => {
                    this.items = result;
                    this.total = result.total;
                    this.isLoading = false;
                });
        },
    }
});
