/*
 * (c) Neofonie GmbH
 *
 * This computer program is the sole property of Neofonie GmbH
 * (http://www.neofonie.de) and is protected under the German Copyright Act
 * (paragraph 69a UrhG). All rights are reserved. Making copies,
 * duplicating, modifying, using or distributing this computer program
 * in any form, without prior written consent of Neofonie, is
 * prohibited. Violation of copyright is punishable under the
 * German Copyright Act (paragraph 106 UrhG). Removing this copyright
 * statement is also a violation.
 */

const { Component } = Shopware;
const { Mixin } = Shopware;
const { Criteria } = Shopware.Data;

import template from './neo_task_statistics_scheduled_tasks.html.twig';
import './neo_task_statistics_scheduled_tasks.scss';

Component.register('task-statistics-scheduled-tasks', {
    template,

    inject: ['repositoryFactory', 'context'],

    mixins: [
        Mixin.getByName('listing'),
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder')
    ],

    data() {
        return {
            items: null,
            repository: null,
            isLoading: false,
            showDeleteModal: false,
            total: 0,
            statusStyle: {
                'scheduled': 'success',
                'queued': 'info',
                'running': 'warning',
                'failed': 'danger',
                'inactive': 'done',
            },
            intervals: {
                0: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.0'),
                20: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.20'),
                120: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.120'),
                300: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.300'),
                600: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.600'),
                900: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.900'),
                1800: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.1800'),
                3600: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.3600'),
                7200: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.7200'),
                14400: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.14400'),
                28800: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.28800'),
                43200: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.43200'),
                86400: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.86400'),
                172800: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.172800'),
                604800: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.604800'),
                2592000: this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.2592000'),
                'seconds' : this.$tc('task-statistics-scheduled-tasks.grid.column.intervalDetail.seconds')
            }
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    computed: {
        columns() {
            return [
                {
                    property: 'name',
                    label: this.$tc('task-statistics-scheduled-tasks.grid.column.name'),
                    allowResize: true
                },
                {
                    property: 'scheduledTaskClass',
                    label: this.$tc('task-statistics-scheduled-tasks.grid.column.class'),
                    allowResize: true,
                    sortable: false
                },
                {
                    property: 'runInterval',
                    label: this.$tc('task-statistics-scheduled-tasks.grid.column.interval'),
                    allowResize: true
                },
                {
                    property: 'lastExecutionTime',
                    label: this.$tc('task-statistics-scheduled-tasks.grid.column.lastExecutionTime'),
                    allowResize: true
                },
                {
                    property: 'nextExecutionTime',
                    label: this.$tc('task-statistics-scheduled-tasks.grid.column.nextExecutionTime'),
                    allowResize: true
                },
                {
                    property: 'status',
                    label: this.$tc('task-statistics-scheduled-tasks.grid.column.status'),
                    allowResize: true
                },
                {
                    property: 'deadMessages[0].exceptionMessage',
                    label: this.$tc('task-statistics-scheduled-tasks.grid.column.error'),
                    allowResize: true
                }
            ];
        },

        entityRepository() {
            return this.repositoryFactory.create('scheduled_task');
        },
    },

    methods: {
        getList() {
            this.isLoading = true;
            const criteria = new Criteria(this.page, this.limit);
            criteria.addAssociation('deadMessages');
            if (this.term != '') {
                criteria.setTerm(this.term);
            }

            return this.entityRepository
                .search(criteria, Shopware.Context.api)
                .then((result) => {
                    this.items = result;
                    this.total = result.total;
                    this.isLoading = false;
                });
        },

        getInterval(interval) {
            return this.intervals[interval] || (interval + " " + this.intervals.seconds);
        },

        onDelete(id) {
            this.showDeleteModal = id;
        },

        onCloseDeleteModal() {
            this.showDeleteModal = false;
        },

        onConfirmDelete(id) {
            this.showDeleteModal = false;
            return this.entityRepository.delete(id, Shopware.Context.api).then(() => {
                this.getList();
            });
        },

        getStatusStyle(status) {
            if (this.statusStyle[status]) {
                return this.statusStyle[status]
            }

            return '';
        },

        isItemDeletable(item) {
            return !(item.scheduledTaskClass.lastIndexOf('Shopware', 0) === 0);
        }
    }
});
