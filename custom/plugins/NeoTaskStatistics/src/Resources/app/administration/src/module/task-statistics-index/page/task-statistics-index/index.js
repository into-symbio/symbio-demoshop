/*
 * (c) Neofonie GmbH
 *
 * This computer program is the sole property of Neofonie GmbH
 * (http://www.neofonie.de) and is protected under the German Copyright Act
 * (paragraph 69a UrhG). All rights are reserved. Making copies,
 * duplicating, modifying, using or distributing this computer program
 * in any form, without prior written consent of Neofonie, is
 * prohibited. Violation of copyright is punishable under the
 * German Copyright Act (paragraph 106 UrhG). Removing this copyright
 * statement is also a violation.
 */

const { Component } = Shopware;

import template from './task-statistics-index.html.twig';

Component.register('task-statistics-index', {
    template,

    inject: ['systemConfigApiService'],

    data() {
        return {
            actualConfigData: {
                'NeoTaskStatistics.config.showScheduledTasks': false,
                'NeoTaskStatistics.config.showMessageQueueStats': false,
                'NeoTaskStatistics.config.showQueuePerChannel': false,
            },
            isLoading: false,
        }
    },

    created() {
        return this.systemConfigApiService.getValues('NeoTaskStatistics.config', null)
            .then(values => {
                this.actualConfigData = values;
            })
            .finally(() => {
                this.isLoading = false;
            });
    }
});
