/*
 * (c) Neofonie GmbH
 *
 * This computer program is the sole property of Neofonie GmbH
 * (http://www.neofonie.de) and is protected under the German Copyright Act
 * (paragraph 69a UrhG). All rights are reserved. Making copies,
 * duplicating, modifying, using or distributing this computer program
 * in any form, without prior written consent of Neofonie, is
 * prohibited. Violation of copyright is punishable under the
 * German Copyright Act (paragraph 106 UrhG). Removing this copyright
 * statement is also a violation.
 */

import './page/task-statistics-scheduled-tasks';
import './page/task-statistics-scheduled-tasks-detail';

import deDE from "./snippet/de-DE";
import enGB from "./snippet/en-GB";

Shopware.Module.register('task-statistics-scheduled-tasks', {
    type: 'plugin',
    name: 'task-statistics-scheduled-tasks',
    title: 'task-statistics-scheduled-tasks.general.mainMenuItemGeneral',
    description: 'task-statistics-scheduled-tasks.general.description',
    version: '1.0.0',
    targetVersion: '1.0.0',
    color: '#333',
    icon: 'default-action-settings',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        scheduledtasks: {
            component: 'task-statistics-scheduled-tasks',
            path: 'scheduled-tasks',
            icon: 'default-text-emoji',
            meta: {
                parentPath: 'task.statistics.index.index'
            }
        },
        detail: {
            component: 'task-statistics-scheduled-tasks-detail',
            path: 'detail/:id',
            meta: {
                parentPath: 'task.statistics.scheduled.tasks.scheduledtasks'
            }
        },
    }
});
