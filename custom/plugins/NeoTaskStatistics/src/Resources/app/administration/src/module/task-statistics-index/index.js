/*
 * (c) Neofonie GmbH
 *
 * This computer program is the sole property of Neofonie GmbH
 * (http://www.neofonie.de) and is protected under the German Copyright Act
 * (paragraph 69a UrhG). All rights are reserved. Making copies,
 * duplicating, modifying, using or distributing this computer program
 * in any form, without prior written consent of Neofonie, is
 * prohibited. Violation of copyright is punishable under the
 * German Copyright Act (paragraph 106 UrhG). Removing this copyright
 * statement is also a violation.
 */

import './page/task-statistics-index';

import deDE from "./snippet/de-DE";
import enGB from "./snippet/en-GB";

Shopware.Module.register('task-statistics-index', {
    type: 'plugin',
    name: 'task-statistics-index',
    title: 'task-statistics-index.general.mainMenuItemGeneral',
    description: 'task-statistics-index.general.descriptionTextmodule',
    version: '1.0.0',
    targetVersion: '1.0.0',
    color: '#333',
    icon: 'default-text-emoji',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        index: {
            component: 'task-statistics-index',
            path: 'index',
            icon: 'default-text-emoji',
            meta: {
                parentPath: 'sw.settings.index'
            }
        },
    },
    settingsItem: {
        name: 'task-statistics-index',
        group: 'plugins',
        label:  'task-statistics-index.general.mainMenuItemGeneral',
        to: 'task.statistics.index.index',
        icon: 'default-text-emoji'
    }
});
