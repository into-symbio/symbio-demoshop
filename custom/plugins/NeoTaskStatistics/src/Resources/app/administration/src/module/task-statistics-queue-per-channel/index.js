/*
 * (c) Neofonie GmbH
 *
 * This computer program is the sole property of Neofonie GmbH
 * (http://www.neofonie.de) and is protected under the German Copyright Act
 * (paragraph 69a UrhG). All rights are reserved. Making copies,
 * duplicating, modifying, using or distributing this computer program
 * in any form, without prior written consent of Neofonie, is
 * prohibited. Violation of copyright is punishable under the
 * German Copyright Act (paragraph 106 UrhG). Removing this copyright
 * statement is also a violation.
 */

import './page/task-statistics-queue-per-channel';
import deDE from "./snippet/de-DE";
import enGB from "./snippet/en-GB";

Shopware.Module.register('task-statistics-queue-per-channel', {
    type: 'plugin',
    name: 'task-statistics-queue-per-channel',
    title: 'task-statistics-queue-per-channel.general.mainMenuItemGeneral',
    description: 'task-statistics-queue-per-channel.general.description',
    version: '1.0.0',
    targetVersion: '1.0.0',
    color: '#333',
    icon: 'default-action-settings',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        queueperchannel: {
            component: 'task-statistics-queue-per-channel',
            path: 'queue-per-channel',
            icon: 'default-text-emoji',
            meta: {
                parentPath: 'task.statistics.index.index'
            }
        },
    }
});
