# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.3.2] - 2023-07-10
### Change logger output to warning, when a services is locked.
### The sync.lock file is getting moved directly to the path inside the CronJob folder.

## [3.3.1] - 2023-06-28
### Added service for a sync lock. For the sync lock, the times at which the sync lock should be activated and deactivated can be stored via a cron tab
### Videos can now also be embedded in the image slider on the product detail page. The videos are stored in the into_symbio_connector_product_addons table.

## [3.3.0] - 2023-06-08
### Changed
- Changed the Logo of this plugin
- Implemented handling of 409 error in OrderDownStreamService by checking the API response status code and extracting the required information to populate the `into_symbio_connector_order_addons` table.
- Updated the OrderDownStreamService to include a call to `validateCreatedApiOrder` method, ensuring the `into_symbio_connector_order_addons` table is updated even when a 409 error occurs.


## [3.2.0 - RC2] - 2022-12-27
### Added
- Magnalister - add logic for Otto orders
### Changed
- DeliveryImport - fill order tracking code with 'handling_unit', add carrier to custom field 'migration_Shopware6_order_attribute6'

## [3.2.0 - RC1] - 2022-12-23
### Fixed
- SyncMessageHandler - fix broken messages ending up in dead_message table and not generating a log error message
### Added
- ProductSync - add logic to handle Symbio video links, will be stored in new JSON field `videos in SymbioProductAddons-Extension

## [3.1.0 - RC1] - 2022-12-21
### Fixed
- Data definitions: change all Symbio provided ids to BIGINT(8) format
- A bulk of PHPSTAN level 9 indicated minor errors, also added some PHPSTAN ignore directives
### Changed
- MediaUpdateService - change logic to check whether media exists before attempting to create
- ProductHydrator - use first media file as cover, disregard for other article media
- OrderDownstream - use new SymbioApi order-placement endpoint
- ProductUpdate - keep link to Symbio objects when deactivating products and articles (this way, re-activated products/articles will not be duplicated)

## [3.0.4] - 2022-06-29
### Merged 2.4.6

## [3.0.3] - 2022-06-27
### Changed
- ProductSync: Die arrays sellingArticles und attributes in der API-Response werden jetzt sortiert. Das Symbio-API lieferte die Daten in zufälligen Sortierungen, was zu jeweils abweichenden Hash-Werten bei ansonsten identischen Produktdaten führte. 

## [3.0.2] - 2022-06-20
### Fixed
- ArticlePlate: typo in ArticlePlate customTariffNumber

## [3.0.1] - 2022-06-08
### Fixed
- ProductUpdate: use name from productPlate (instead of first variant) for products (i.e. Shopware parent product)
- Snippets: fix typo
- ProductDashboard: allow for Windows specific mime types for CSV

## [3.0.0 RC] - 2022-05-27
### Fixed
- A number of PHPSTAN identified errors up to level 9
- ProductSyncService: Do not attempt to update media anymore - a change e.g. in media URI is not a conclusive indicator for a changed media. Attempting to update media has led to an abundance of unused media.

### Added
- Aufträge - Magnalister (Bestellnummern für Ebay- und Amazon-Aufträge)
- Aufträge - Steuerung erneute Übertragung
- Produkte - Änderungen Importprozess: ProductSync führt standardmaßig keine Produktaktualisierungen mehr aus, Symbio-Dashboard, Ausweis zu aktualisierender Produkte per CSV-Download, CSV-Upload zur Auswahl zu aktualisierender Produkte
- Produkte - Übernahme "unit" für Attribute

## [2.4.6] - 2022-06-29
### Fixed
- ProductUpdate: Fix an SQL duplicate key error when trying to add an existing configurator setting for a product. This happens in case an article with the same configurator setting has existed previously, and a new article with the same setting is created.
### Changed
- Add unique index for product_id/product_version_id in product_addons table
- Allow for null value for media text fields in API response

## [2.4.5] - 2022-05-20
### Fixed
- CustomerDownstream: Fix country not correctly set when updating customers

## [2.4.4] - 2022-04-26
### Fixed
- CategorySync, ProductSync: fix API timeout 

## [2.4.3] - 2022-03-22
### Fixed
- CustomerDownstream: fix error - sometimes country was not transmitted correctly for non-DE customer

## [2.4.2] - 2022-03-02
### Fixed
- OrderDownstream: fixed an error when order address was matched to customer address not yet downloaded to Symbio

## [2.4.1] - 2022-03-01
### Changed
MessageHandler: catch throwable level (instead of exception)
### Fixed
- CustomerDownstream: in exceptional cases, main address might be not set in customer addons

## [2.4.0] - 2022-02-28
### Changed
- DownstreamOrder: use addressIds if address can be assigned to existing customer address

## [2.3.8] - 2022-02-21
### Fixed
- ProductUpdateService: fix product creation for products with 1+ dimensional variants

## [2.3.7] - 2022-02-18
### Changed
- SyncMessageHandler: wrap update in try/catch to avoid dead messages and improve failure analysis

## [2.3.6] - 2022-01-24
### Changed
- DownstreamOrder: instead of addressIds pass actual AddressDTO with order (note addresses might have been edited in admin for order only and not saved to customer)

## [2.3.5] - 2022-01-11
### Added
- SymbioClient: added an API test function to plugin config

## [2.3.4] - 2022-01-10
### Fixed
- StockSyncService: request stock only if symbioArticleUid not null

## [2.3.3] - 2021-12-02
### Fixed
- DownstreamOrder: fixed issue caused by Magnalister adding identical shipping addresses to same customer

## [2.3.2] - 2021-11-29
### Changed
- set all new articles as closeout articles (see Slack MT 26.11.)
- remove event subscriber for orders, instead use time-driven order collector for all orders

## [2.3.1] - 2021-11-22
### Changed
- UpdatedMessageHandler: catch non-recoverable errors and route to log, to bypass dead_message table

## [2.3.0] - 2021-11-22
### Added
- Magnalister order collector to run as a scheduled-task or command
### Fixed
- CustomerAddress: add a field 'type' to address addons to differentiate between main and shipping addresses, should they be identical

## [2.2.1] - 2021-11-19
### Fixed
- OrderEvent: avoid loop by writing addons directly

## [2.2.0] - 2021-11-18
### Changed
- OrderEventSubscriber: no OrderDownstreamMessage for magnalister orders

## [2.1.1] - 2021-11-17
### Fixed
- MediaUpload: delete media entity in case duplicate file encountered (should never happen)
- MediaUpload: convert file extension to lowercase for identification of mimetype
- DownstreamOrder: add firstname/lastname to checksum for billing/shipping address identification

## [2.1.0] - 2021-11-16
### Changed
- MediaUpload: Catch Shopware duplicateFileException, generate unique filenames

## [2.0.9] - 2021-11-12
### Changed
- DownstreamCustomer - improve handling for 404
- SymbioClient - set 5 sec connect-timeout, 20 sec timeout

## [2.0.8] - 2021-11-05
### Fixed
- ProductUpdate: fix bug when updating article (missing symbio data in addons)

## [2.0.7] - 2021-11-04
### Fixed
- ProductUpdate: fix error when trying to import new article for existing product

## [2.0.6] - 2021-11-04
### Changed
- DeliveryImport: add exception for product not found for line item

## [2.0.5] - 2021-10-28
### Fixed
- OrderDownstream: wrong calculation of price when sy_vpe and SC_PE fields are set

## [2.0.4] - 2021-10-26
### Fixed
- UpdateProduct: use upsert instead of update when updating variant product, as variants may have been added
### Changed
- Improve error handling for invalid media url
- Improve error message in CustomerDownstream
- DownstreamOrder: correct price by custom fields sy_vpe and SC_PE

## [2.0.3] - 2021-10-15
### Changed
- DownstreamOrder: switch field additionalOrderPayload back to string format

## [2.0.2] - 2021-10-14
### Added
- debug & error level logging for SymbioClient
### Changed
- DownstreamOrder: narrowed the window for accidental duplicate order transmission
- OrderPlate: changed format of additionalOrderPayload to proper JSON (instead of string as in API doc)

## [2.0.1] - 2021-10-14
### Fixed
- SymbioClient: bug in exception handling

## [2.0.0] - 2021-10-12
### Added
- downstream order: customer_comment is transmitted in OrderPlacementDto:remark
- Shopware promotion discounts are extended for a field ERP-Konditionsschlüssel
- downstream order: Konditionsschlüssel and actual discounts are transmitted in OrderPlacementDto:additionalOrderPayload
- downstream order: shipping costs are transmitted in OrderPlacementDto:additionalOrderPayload
- downstream order: fill orderOrigin with (migrated) custom field from customer group
- sync stock: calculate stock based on article custom field sy_vpe
- downstream order: add shopware order number to error message in dead_message table
- delivery import: calculate delivered items based on article custom field sy_vpe
### Changed
- downstream order: transmit net prices (line-items, discounts and shipping), even for customers with tax
- sync stock: use articleUid instead of sku as unique key for syncing stocks

## [1.2.2] - 2021-08-31
### Added
- add externalPaymentReference to downstream order
### Changed
- VAT id no longer mandatory
- add personal name fields to Symbio API name field in addition to company

## [1.2.1] - 2021-08-12
### Added
- Admin Manual

## [1.2.0] - 2021-08-09
### Added
- Import new API field articleSalesId and provide for downstream orders
### Changed
- Import new API field distributorSKU into product addons

## [1.1.0] - 2021-07-12
### Changed
- Adapt to renamed variables in API article structs
- Add new field distributorSKU (with undefined meaning) to PriceStruct.php

## [1.0.0] - 2021-07-08
Initial fully functional release.
### Changed
- Simplify configuration of excluded channels

