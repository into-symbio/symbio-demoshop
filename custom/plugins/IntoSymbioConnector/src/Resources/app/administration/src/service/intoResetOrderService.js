const ApiService = Shopware.Classes.ApiService;
const { Application } = Shopware;

class ApiClient extends ApiService {
    constructor(httpClient, loginService, apiEndpoint = 'into-symbio-connector') {
        super(httpClient, loginService, apiEndpoint);
    }

    reset(orderId) {
        return this.httpClient
            .post(`_action/${this.getApiBasePath()}/reset-order/${orderId}`,
                {},
                {
                    headers: this.getBasicHeaders(),
                })
            .then((response) => {
                return ApiService.handleResponse(response);
            });
    }
}

Application.addServiceProvider('intoResetOrder', (container) => {
    const initContainer = Application.getContainer('init');
    return new ApiClient(initContainer.httpClient, container.loginService);
});
