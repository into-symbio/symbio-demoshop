import template from './sw-product-detail-base.html.twig';
import deDE from "../../snippet/de-DE.json";

Shopware.Component.override('sw-product-detail-base', {
    template: template,

    snippet: {
        'de-DE': deDE
    }
});
