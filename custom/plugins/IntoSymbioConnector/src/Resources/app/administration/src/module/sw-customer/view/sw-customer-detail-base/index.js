import template from './sw-customer-detail-base.html.twig';
import deDE from "../../../../snippet/de-DE.json";

Shopware.Component.override('sw-customer-detail-base', {
    template: template,

    snippet: {
        'de-DE': deDE
    }
});
