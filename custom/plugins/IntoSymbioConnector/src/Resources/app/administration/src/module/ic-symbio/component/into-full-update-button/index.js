const {Component, Mixin} = Shopware;
import template from './into-full-update-button.html.twig';

Component.register('into-full-update-button', {
    template,

    inject: ['intoDashboard'],

    mixins: [
        Mixin.getByName('notification')
    ],

    data() {
        return {
            isLoading: false,
            isSaveSuccessful: false,
        };
    },

    methods: {
        saveFinish() {
            this.isSaveSuccessful = false;
        },

        fullUpdate() {

            this.isLoading = true;
            this.intoDashboard.fullUpdate().then((res) => {
                if (res.success) {
                    this.isSaveSuccessful = true;
                    this.createNotificationSuccess({
                        title: this.$tc('into-symbio-connector.dashboard.fullUpdateButtonTitle'),
                        message: this.$tc('into-symbio-connector.dashboard.fullUpdateButtonSuccess')
                    });
                } else {
                    this.createNotificationError({
                        title: this.$tc('into-symbio-connector.dashboard.fullUpdateButtonTitle'),
                        message: this.$tc('into-symbio-connector.dashboard.fullUpdateButtonError')
                    });
                }

                this.isLoading = false;
            });
        }
    }
})
