import './page/sw-category-detail-base';
import './page/sw-product-detail-base';

import './module/ic-symbio';
import './module/sw-customer/view/sw-customer-detail-base';
import './module/sw-media/component/sidebar/sw-media-quickinfo';
import './module/sw-order/view/sw-order-detail-base';
import './module/sw-promotion/component/sw-promotion-discount-component';
import './module/sw-promotion/view/sw-promotion-detail-discounts';

import './service/intoApiTestService';
import './service/intoDashboardService';
import './service/intoResetOrderService';

import './component/into-api-test-button/';
import './component/into-reset-order-button/';
