import template from './into-symbio-dashboard.html.twig';

const {Component, Mixin} = Shopware;
const {Criteria} = Shopware.Data;

Component.register('into-symbio-dashboard', {
    template,

    inject: ['intoDashboard'],
    
    mixins: [
        Mixin.getByName('notification')
    ],
    
    data() {
        return {
            isLoading: false,
            isSaveSuccessful: false,
            selectedCsvFile: false,
            features: {
                uploadFileSizeLimit: 52428800,
                fileTypes: ['text/csv', 'text/x-csv', 'text/plain', 'application/csv', 'application/x-csv', 'application/vnd.ms-excel'],
            },
        };
    },

    methods: {
        saveFinish() {
            this.isSaveSuccessful = false;
        },

        processCsv() {
            this.isLoading = true;
            this.intoDashboard.selected(this.selectedCsvFile ).then((res) => {
                if (res.success) {
                    this.isSaveSuccessful = true;
                    this.createNotificationSuccess({
                        title: this.$tc('into-symbio-connector.dashboard.selectedButtonTitle'),
                        message: this.$tc('into-symbio-connector.dashboard.selectedButtonSuccess')
                    });
                    setTimeout(() => {
                        location.reload();
                    }, 3000)
                } else {
                    this.createNotificationError({
                        title: this.$tc('into-symbio-connector.dashboard.selectedButtonTitle'),
                        message: this.$tc('into-symbio-connector.dashboard.selectedButtonError')
                    });
                }

                this.isLoading = false;
            });
        }
    }
});