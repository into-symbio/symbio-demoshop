import { DiscountTypes, DiscountScopes } from '../../../../../../../../../../../../vendor/shopware/administration/Resources/app/administration/src/module/sw-promotion/helper/promotion.helper';
import template from './sw-promotion-detail-discounts.html.twig';

const { Component } = Shopware;
const utils = Shopware.Utils;

Shopware.Component.override('sw-promotion-detail-discounts', {
    template,

    inject: ['repositoryFactory', 'acl'],

    data() {
        return {
            deleteDiscountId: null,
            repository: null
        };
    },

    computed: {
        promotion() {
            return Shopware.State.get('swPromotionDetail').promotion;
        },

        isLoading: {
            get() {
                return Shopware.State.get('swPromotionDetail').isLoading;
            },
            set(isLoading) {
                Shopware.State.commit('swPromotionDetail/setIsLoading', isLoading);
            }
        },

        discounts() {
            return Shopware.State.get('swPromotionDetail').promotion &&
                Shopware.State.get('swPromotionDetail').promotion.discounts;
        }

    },

    methods: {
        onAddDiscount() {
            const promotionDiscountRepository = this.repositoryFactory.create(
                this.discounts.entity,
                this.discounts.source
            );
            const newDiscount = promotionDiscountRepository.create(Shopware.Context.api);
            newDiscount.id = utils.createId();
            newDiscount.promotionId = this.promotion.id;
            newDiscount.scope = DiscountScopes.CART;
            newDiscount.type = DiscountTypes.PERCENTAGE;
            newDiscount.value = 0.01;
            newDiscount.considerAdvancedRules = false;
            newDiscount.sorterKey = 'PRICE_ASC';
            newDiscount.applierKey = 'ALL';
            newDiscount.usageKey = 'ALL';

            const promotionDiscountAddonsRepository =
                this.repositoryFactory.create('into_symbio_connector_promotion_discount_addons');
            const newAddons = promotionDiscountAddonsRepository.create(Shopware.Context.api);
            newAddons.promotionDiscountId = newDiscount.id;
            newAddons.promotionDiscountKey = '';
            newDiscount.extensions.intoSymbioConnectorAddons = newAddons;

            this.discounts.push(newDiscount);
        },

        deleteDiscount(discount) {
            if (discount.isNew()) {
                this.discounts.remove(discount.id);
                return;
            }

            this.isLoading = true;
            const promotionDiscountRepository = this.repositoryFactory.create(
                this.discounts.entity,
                this.discounts.source
            );

            promotionDiscountRepository.delete(discount.id, this.discounts.context).then(() => {
                this.discounts.remove(discount.id);
                this.isLoading = false;
            });
        }
    }
});
