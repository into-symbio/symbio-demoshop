const { Component, Mixin } = Shopware;
import template from './into-api-test-button.html.twig';

Component.register('into-api-test-button', {
    template,

    props: ['label'],
    inject: ['intoApiTest'],

    mixins: [
        Mixin.getByName('notification')
    ],

    data() {
        return {
            isLoading: false,
            isSaveSuccessful: false,
        };
    },

    methods: {
        saveFinish() {
            this.isSaveSuccessful = false;
        },

        check() {
            this.isLoading = true;
            this.intoApiTest.check().then((res) => {
                if (res.success) {
                    this.isSaveSuccessful = true;
                    this.createNotificationSuccess({
                        title: this.$tc('into-api-test-button.title'),
                        message: this.$tc('into-api-test-button.success')
                    });
                } else {
                    this.createNotificationError({
                        title: this.$tc('into-api-test-button.title'),
                        message: this.$tc('into-api-test-button.error')
                    });
                }

                this.isLoading = false;
            });
        }
    }
})
