const ApiService = Shopware.Classes.ApiService;
const {Application} = Shopware;

class ApiClient extends ApiService {
    constructor(httpClient, loginService, apiEndpoint = 'into-symbio-connector/dashboard') {
        super(httpClient, loginService, apiEndpoint);
    }

    fullUpdate() {
        return this.httpClient
            .post(`_action/${this.getApiBasePath()}/full-update`,
                {},
                {
                    headers: this.getBasicHeaders(),
                })
            .then((response) => {
                return ApiService.handleResponse(response);
            });
    }

    checkOnly() {
        return this.httpClient
            .post(`_action/${this.getApiBasePath()}/check-only`,
                {},
                {
                    headers: this.getBasicHeaders(),
                })
            .then((response) => {
                return ApiService.handleResponse(response);
            });
    }

    selected(file) {
        const formData = new FormData();
        formData.append('file', file);
        let headers = this.getBasicHeaders();
        headers['Content-Type'] = 'multipart/form-data';

        return this.httpClient
            .post(`_action/${this.getApiBasePath()}/selected`,
                formData,
                {
                    headers: headers,
                })
            .then((response) => {
                return ApiService.handleResponse(response);
            });
    }

    csvList() {
        return this.httpClient
            .post(`_action/${this.getApiBasePath()}/csv-list`,
                {},
                {
                    headers: this.getBasicHeaders(),
                })
            .then((response) => {
                return ApiService.handleResponse(response);
            });
    }

    csvDownload(path) {
        return this.httpClient
            .get(
                `_action/${this.getApiBasePath()}/csv-download/${path}`,
                {
                    params: {},
                    responseType: 'blob',
                    headers: this.getBasicHeaders(),
                },
            );
    }

    csvDelete(path) {
        return this.httpClient
            .post(`_action/${this.getApiBasePath()}/csv-delete/${path}`,
                {},
                {
                    headers: this.getBasicHeaders(),
                })
            .then((response) => {
                return ApiService.handleResponse(response);
            });
    }
}

Application.addServiceProvider('intoDashboard', (container) => {
    const initContainer = Application.getContainer('init');
    return new ApiClient(initContainer.httpClient, container.loginService);
});
