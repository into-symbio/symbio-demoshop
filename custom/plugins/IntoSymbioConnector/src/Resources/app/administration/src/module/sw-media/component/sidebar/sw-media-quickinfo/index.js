import template from './sw-media-quickinfo.html.twig';
import deDE from "../../../../../snippet/de-DE.json";

Shopware.Component.override('sw-media-quickinfo', {
    template: template,

    snippet: {
        'de-DE': deDE
    }
});
