import './component/into-check-only-button';
import './component/into-csv-list';
import './component/into-full-update-button';
import './page/into-symbio-dashboard';

const {Module} = Shopware;

Module.register('into-symbio-dashboard', {
    type: 'plugin',
    name: 'SymbioConnector',
    title: 'into-symbio-connector.dashboard.moduleTitle',
    description: 'into-symbio-connector.dashboard.moduleDescription',
    color: '#b3922b',

    routes: {
        overview: {
            component: 'into-symbio-dashboard',
            path: 'overview'
        }
    },

    navigation: [
        {
            id: 'into-symbio-dashboard',
            label: 'into-symbio-connector.dashboard.moduleLabel',
            color: '#0080ff',
            path: 'into.symbio.dashboard.overview',
            parent: 'sw-catalogue',
            position: 20
        },
    ]
});
