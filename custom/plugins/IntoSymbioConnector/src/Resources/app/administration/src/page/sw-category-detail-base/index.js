import template from './sw-category-detail-base.html.twig';
import deDE from "../../snippet/de-DE.json";

Shopware.Component.override('sw-category-detail-base', {
    template: template,

    snippet: {
        'de-DE': deDE
    }
});
