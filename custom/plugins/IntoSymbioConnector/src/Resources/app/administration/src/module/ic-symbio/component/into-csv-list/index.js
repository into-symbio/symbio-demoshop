import template from './into-csv-list.html.twig';

const {Component, Mixin} = Shopware;

Component.register('into-csv-list', {
    template,

    inject: ['intoDashboard'],

    mixins: [
        Mixin.getByName('notification')
    ],

    data() {
        return {
            downloads: [],
            isLoading: false
        }
    },

    computed: {
        downloadColumns() {
            return this.getDownloadColumns();
        },
    },

    created() {
        this.getList();
    },

    methods: {
        getDownloadColumns() {
            return [{
                property: 'datetime',
                label: 'into-symbio-connector.dashboard.download.datetimeLabel',
                allowResize: true,
                primary: true,
            }, {
                property: 'path',
                label: 'into-symbio-connector.dashboard.download.pathLabel',
                allowResize: true,
            }];
        },

        async getList() {
            this.isLoading = true;
            try {
                this.downloads = await this.intoDashboard.csvList();
                this.isLoading = false;
            } catch {
            }
            this.isLoading = false;
        },

        onDelete(path) {
            this.isLoading = true;
            this.intoDashboard.csvDelete(path).then((res) => {
                if (res.success) {
                    this.isSaveSuccessful = true;
                    this.createNotificationSuccess({
                        title: this.$tc('into-symbio-connector.dashboard.download.deleteTitle'),
                        message: this.$tc('into-symbio-connector.dashboard.download.deleteSuccess')
                    });
                } else {
                    this.createNotificationError({
                        title: this.$tc('into-symbio-connector.dashboard.download.deleteTitle'),
                        message: this.$tc('into-symbio-connector.dashboard.download.deleteError')
                    });
                }
                this.isLoading = false;
                this.getList().then();
            });
        },

        onDownload(path) {
            this.intoDashboard.csvDownload(path)
                .then((response) => {
                    if (response.data) {
                        const filename = response.headers['content-disposition'].split('filename=')[1];
                        const link = document.createElement('a');
                        link.href = URL.createObjectURL(response.data);
                        link.download = filename;
                        link.dispatchEvent(new MouseEvent('click'));
                        link.remove();
                    }
                    this.getList().then();
                });
        }
    }
});