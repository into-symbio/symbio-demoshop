const {Component, Mixin} = Shopware;
import template from './into-check-only-button.html.twig';

Component.register('into-check-only-button', {
    template,

    inject: ['intoDashboard'],

    mixins: [
        Mixin.getByName('notification')
    ],

    data() {
        return {
            isLoading: false,
            isSaveSuccessful: false,
        };
    },

    methods: {
        saveFinish() {
            this.isSaveSuccessful = false;
        },

        checkOnly() {
            this.isLoading = true;
            this.intoDashboard.checkOnly().then((res) => {
                if (res.success) {
                    this.isSaveSuccessful = true;
                    this.createNotificationSuccess({
                        title: this.$tc('into-symbio-connector.dashboard.checkOnlyButtonTitle'),
                        message: this.$tc('into-symbio-connector.dashboard.checkOnlyButtonSuccess')
                    });
                } else {
                    this.createNotificationError({
                        title: this.$tc('into-symbio-connector.dashboard.checkOnlyButtonTitle'),
                        message: this.$tc('into-symbio-connector.dashboard.checkOnlyButtonError')
                    });
                }

                this.isLoading = false;
            });
        }
    }
})
