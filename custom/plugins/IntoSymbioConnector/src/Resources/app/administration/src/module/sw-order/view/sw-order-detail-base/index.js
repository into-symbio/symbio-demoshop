import template from './sw-order-detail-base.html.twig';
import deDE from "../../../../snippet/de-DE.json";

Shopware.Component.override('sw-order-detail-base', {
    template: template,

    snippet: {
        'de-DE': deDE
    }
});
