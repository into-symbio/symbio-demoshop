const {Component, Mixin} = Shopware;
import template from './into-reset-order-button.html.twig';
import './into-reset-order-button.scss';

Component.register('into-reset-order-button', {
    template,

    props: ['orderId'],
    inject: ['intoResetOrder'],

    mixins: [
        Mixin.getByName('notification')
    ],

    data() {
        return {
            isLoading: false,
            isSaveSuccessful: false,
        };
    },

    methods: {
        saveFinish() {
            this.isSaveSuccessful = false;
        },

        reset(orderId) {
            this.isLoading = true;
            this.intoResetOrder.reset(orderId).then((res) => {
                if (res.success) {
                    this.isSaveSuccessful = true;
                    this.createNotificationSuccess({
                        title: this.$tc('into-reset-order-button.title'),
                        message: this.$tc('into-reset-order-button.success')
                    });
                    setTimeout(() => {
                        location.reload();
                    }, 5000)
                } else {
                    this.createNotificationError({
                        title: this.$tc('into-reset-order-button.title'),
                        message: this.$tc('into-reset-order-button.error')
                    });
                }

                this.isLoading = false;
            });
        }
    }
})
