<?php declare(strict_types=1);

namespace IntoSymbioConnector\Administration;

use IntoSymbioConnector\Api\Client\AbstractClient;
use IntoSymbioConnector\Api\Client\SymbioClientFactory;
use IntoSymbioConnector\Service\Common\IntoConfigService;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"api"})
 */
class TestApiController extends AbstractController
{
    private IntoConfigService $intoConfigService;
    private SymbioClientFactory $symbioClientFactory;

    public function __construct(IntoConfigService $intoConfigService, SymbioClientFactory $symbioClientFactory)
    {
        $this->intoConfigService = $intoConfigService;
        $this->symbioClientFactory = $symbioClientFactory;
    }

    /**
     * @Route("/api/_action/into-symbio-connector/test-api", name="api.action.into_symbio_connector.test_api", methods={"POST"})
     */
    public function testApi(): Response
    {
        $credentials = $this->intoConfigService->getApiCredentials();
        try {
            $client = $this->symbioClientFactory->createSymbioClient($credentials);
        } catch (\Throwable $throwable) {
            return new TestApiFailureResponse();
        }

        return new TestApiSuccessResponse();
    }
}
