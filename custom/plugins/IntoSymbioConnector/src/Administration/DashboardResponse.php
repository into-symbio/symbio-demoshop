<?php declare(strict_types=1);

namespace IntoSymbioConnector\Administration;

use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\SalesChannel\StoreApiResponse;

class DashboardResponse extends StoreApiResponse
{
    public function __construct(ArrayStruct $response)
    {
        parent::__construct($response);
    }
}
