<?php declare(strict_types=1);

namespace IntoSymbioConnector\Administration;

use IntoSymbioConnector\MessageQueue\Message\StartProductSyncMessage;
use IntoSymbioConnector\Service\Sync\ProductSyncService;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\Framework\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"api"})
 */
class DashboardController extends AbstractController
{
    const DOWNLOAD = 'touched_products/download/';
    const UPLOAD = 'touched_products/upload/';

    public MessageBusInterface $messageBus;

    public FilesystemInterface $filesystem;

    public function __construct(MessageBusInterface $messageBus, FilesystemInterface $filesystem)
    {
        $this->messageBus = $messageBus;
        $this->filesystem = $filesystem;
    }

    /**
     * @Route("/api/_action/into-symbio-connector/dashboard/full-update", name="api.action.into_symbio_connector.dashboard.full_update", methods={"POST"})
     */
    public function fullUpdate(Request $request, Context $context): Response
    {
        $this->messageBus->dispatch((new StartProductSyncMessage(ProductSyncService::MODE_UPDATE)));

        return new DashboardResponse(new ArrayStruct(['success' => true]));
    }

    /**
     * @Route("/api/_action/into-symbio-connector/dashboard/check-only", name="api.action.into_symbio_connector.dashboard.check_only", methods={"POST"})
     */
    public function checkOnly(Request $request, Context $context): Response
    {
        $this->messageBus->dispatch((new StartProductSyncMessage(ProductSyncService::MODE_CHECK)));

        return new DashboardResponse(new ArrayStruct(['success' => true]));
    }

    /**
     * @Route("/api/_action/into-symbio-connector/dashboard/selected", name="api.action.into_symbio_connector.dashboard.selected", methods={"POST"})
     */
    public function selected(Request $request, Context $context): Response
    {
        $file = $_FILES['file'] ?? null;
        if (!is_array($file)) {
            return new DashboardResponse(new ArrayStruct(['success' => false]));
        }

        $content = file_get_contents($file['tmp_name']);
        if ($content === false) {
            return new DashboardResponse(new ArrayStruct(['success' => false]));
        }

        $path = $file['name'];
        if ($this->filesystem->has(self::UPLOAD . $path)) {
            $path = pathinfo($path, PATHINFO_FILENAME) . '_' . Uuid::randomHex() . '.' .
                pathinfo($path, PATHINFO_EXTENSION);
        }

        $this->filesystem->write(self::UPLOAD . $path, $content);
        unlink($file['tmp_name']);

        $this->messageBus->dispatch((new StartProductSyncMessage(ProductSyncService::MODE_TOUCHED, $path)));

        return new DashboardResponse(new ArrayStruct(['success' => true]));
    }

    /**
     * @Route("/api/_action/into-symbio-connector/dashboard/csv-list", name="api.action.into_symbio_connector.dashboard.csv_list", methods={"POST"})
     */
    public function csvList(Request $request, Context $context): Response
    {
        $files = $this->filesystem->listContents(self::DOWNLOAD, false);

        $data = [];
        foreach ($files as $file) {
            if ($file['type'] === 'file' && strtolower($file['extension']) === 'csv') {
                $timestamp = new \DateTime();
                $timestamp->setTimestamp($file['timestamp']);
                $timestamp->setTimezone(new \DateTimeZone('Europe/Berlin'));
                $data[] = [
                    'datetime' => $timestamp->format('Y-m-d H:i:s'),
                    'path' => $file['basename'],
                ];
            }
        }
        usort($data, function ($a, $b) {
            return $a['datetime'] <=> $b['datetime'];
        });

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/_action/into-symbio-connector/dashboard/csv-delete/{path}", name="api.action.into_symbio_connector.dashboard.csv_delete", methods={"POST"})
     */
    public function csvDelete(string $path, Request $request, Context $context): Response
    {
        try {
            $success = $this->filesystem->delete(self::DOWNLOAD . $path);
        } catch (FileNotFoundException $exception) {
            $success = false;
        }

        return new DashboardResponse(new ArrayStruct(['success' => $success]));
    }

    /**
     * @Route("/api/_action/into-symbio-connector/dashboard/csv-download/{path}", name="api.action.into_symbio_connector.dashboard.csv_download", methods={"GET"})
     */
    public function csvDownload(string $path, Request $request, Context $context): Response
    {
        try {
            $file = $this->filesystem->readAndDelete(self::DOWNLOAD . $path);
            if (!is_string($file)) {
                return new DashboardResponse(new ArrayStruct(['success' => false]));
            }
        } catch (FileNotFoundException $exception) {
            return new DashboardResponse(new ArrayStruct(['success' => false]));
        }

        $response = new Response($file);

        $disposition = HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_ATTACHMENT, $path);

        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }
}
