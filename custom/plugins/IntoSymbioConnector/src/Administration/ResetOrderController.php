<?php declare(strict_types=1);

namespace IntoSymbioConnector\Administration;

use IntoSymbioConnector\DataAbstractionLayer\Entity\OrderAddonsEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\Framework\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"api"})
 */
class ResetOrderController extends AbstractController
{
    private EntityRepositoryInterface $orderAddonsRepository;

    public function __construct(EntityRepositoryInterface $orderAddonsRepository)
    {
        $this->orderAddonsRepository = $orderAddonsRepository;
    }

    /**
     * @Route("/api/_action/into-symbio-connector/reset-order/{orderId}", name="api.action.into_symbio_connector.reset_order", methods={"POST"})
     */
    public function resetOrder(string $orderId, Request $request, Context $context): Response
    {
        if (!Uuid::isValid($orderId)) {
            throw new \Exception('ResetOrderController - invalid orderId given.');
        }

        $context = Context::createDefaultContext();
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('orderId', $orderId));
        $orderAddonsEntity = $this->orderAddonsRepository->search($criteria, $context)->first();
        if (!$orderAddonsEntity instanceof OrderAddonsEntity) {
            return new ResetOrderResponse(new ArrayStruct(['success' => true]));
        }

        if ($orderAddonsEntity->getSymbioOrderId() !== null) {
            return new ResetOrderResponse(new ArrayStruct(['success' => false]));
        }

        $this->orderAddonsRepository->delete([
            ['id' => $orderAddonsEntity->getId(),],
        ], $context);

        return new ResetOrderResponse(new ArrayStruct(['success' => true]));
    }
}
