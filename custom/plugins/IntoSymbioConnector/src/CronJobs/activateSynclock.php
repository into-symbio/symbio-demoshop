<?php

namespace IntoSymbioConnector\CronJobs;

use IntoSymbioConnector\Exception\LockSyncException;

require_once __DIR__ . '/../../../../../vendor/autoload.php';


function lockSync(): void
{
    $lockFilePath = __DIR__ . '/sync.lock';
    $result = file_put_contents( 'sync.lock','Sync is locked');
    rename('sync.lock', $lockFilePath);

    if ($result === false) {
        throw new LockSyncException('IntoSymbioConnector: LockSyncException - Failed to write lock file');
    }
}

try {
    lockSync();
} catch (LockSyncException $e) {
}