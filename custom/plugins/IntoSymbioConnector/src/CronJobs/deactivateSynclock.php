<?php

require_once __DIR__ . '/../../../../../vendor/autoload.php';

function unlockSync(): void
{
    $lockFilePath = __DIR__ . '/sync.lock';
    if (file_exists($lockFilePath)) {
        unlink($lockFilePath);
    }
}

unlockSync();