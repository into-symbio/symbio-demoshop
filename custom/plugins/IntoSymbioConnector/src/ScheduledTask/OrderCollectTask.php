<?php declare(strict_types=1);

namespace IntoSymbioConnector\ScheduledTask;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

class OrderCollectTask extends ScheduledTask
{
    private const NAME = 'into.symbio_connector.order_downstream';
    private const INTERVAL = 300;

    public static function getTaskName(): string
    {
        return self::NAME;
    }

    public static function getDefaultInterval(): int
    {
        return self::INTERVAL;
    }
}
