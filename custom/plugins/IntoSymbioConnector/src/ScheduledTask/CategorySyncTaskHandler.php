<?php declare(strict_types=1);


namespace IntoSymbioConnector\ScheduledTask;


use IntoSymbioConnector\Service\Sync\AbstractSyncService;
use IntoSymbioConnector\Service\Sync\SyncLockService;
use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;

class CategorySyncTaskHandler extends ScheduledTaskHandler
{
    protected AbstractSyncService $syncService;
    private SyncLockService $syncLock;
    private LoggerInterface $logger;


    public function __construct(EntityRepositoryInterface $scheduledTaskRepository, AbstractSyncService $syncService, SyncLockService $syncLock, LoggerInterface $logger)
    {
        $this->syncService = $syncService;
        $this->syncLock = $syncLock;
        $this->logger = $logger;
        parent::__construct($scheduledTaskRepository);
    }

    public static function getHandledMessages(): iterable
    {

        return [CategorySyncTask::class];
    }

    public function run(): void
    {
        if ($this->syncLock->isSyncLocked()) {
            $this->logger->warning('IntoSymbioConnector: CategorySyncTaskHandler is currently locked.');
            return;
        }
        $context = Context::createDefaultContext();
        $this->syncService->run($context);
    }
}