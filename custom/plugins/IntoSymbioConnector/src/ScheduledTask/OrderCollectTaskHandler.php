<?php declare(strict_types=1);

namespace IntoSymbioConnector\ScheduledTask;

use IntoSymbioConnector\Service\Collect\OrderCollectService;
use IntoSymbioConnector\Service\Sync\SyncLockService;
use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;

class OrderCollectTaskHandler extends ScheduledTaskHandler
{
    private OrderCollectService $orderCollectService;
    private SyncLockService $syncLock;
    private LoggerInterface $logger;


    public function __construct(
        EntityRepositoryInterface $scheduledTaskRepository,
        OrderCollectService $orderCollectService,
        SyncLockService $syncLock,
        LoggerInterface $logger
    ) {
        $this->orderCollectService = $orderCollectService;
        $this->syncLock = $syncLock;
        $this->logger = $logger;
        parent::__construct($scheduledTaskRepository);
    }

    public static function getHandledMessages(): iterable
    {
        return [OrderCollectTask::class];
    }

    public function run(): void
    {
        if ($this->syncLock->isSyncLocked()) {
            $this->logger->warning('IntoSymbioConnector: OrderSyncTaskHandler is currently locked.');
            return;
        }
        $context = Context::createDefaultContext();
        $this->orderCollectService->collect($context);
    }
}
