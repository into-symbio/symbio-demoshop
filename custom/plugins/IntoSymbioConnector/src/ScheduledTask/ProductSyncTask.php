<?php declare(strict_types=1);


namespace IntoSymbioConnector\ScheduledTask;


use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

class ProductSyncTask extends ScheduledTask
{
    private const NAME = 'into.symbio_connector.product_sync';
    private const INTERVALL = 3600;

    public static function getTaskName(): string
    {

        return self::NAME;
    }

    public static function getDefaultInterval(): int
    {

        return self::INTERVALL;
    }
}
