<?php declare(strict_types=1);


namespace IntoSymbioConnector\ScheduledTask;


use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

class DeliveryImportTask extends ScheduledTask
{
    private const NAME = 'into.symbio_connector.delivery_import';
    private const INTERVALL = 300;

    public static function getTaskName(): string
    {

        return self::NAME;
    }

    public static function getDefaultInterval(): int
    {

        return self::INTERVALL;
    }
}
