<?php declare(strict_types=1);


namespace IntoSymbioConnector\ScheduledTask;


use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

class CategorySyncTask extends ScheduledTask
{
    private const NAME = 'into.symbio_connector.category_sync';
    private const INTERVALL = 7200;

    public static function getTaskName(): string
    {

        return self::NAME;
    }

    public static function getDefaultInterval(): int
    {

        return self::INTERVALL;
    }
}
