<?php declare(strict_types=1);

namespace IntoSymbioConnector\Magnalister;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception\TableNotFoundException;

class MagnalisterOrderRepository
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getExternalMarketPlaceData(?string $ordersId): ?array
    {
        if ($ordersId === null) {
            return null;
        }

        $result = false;
        try {
            $result = $this->connection->createQueryBuilder()
                ->select(['platform', 'special', 'data'])
                ->from('magnalister_orders')
                ->where('orders_id = :ordersId')
                ->setParameter(':ordersId', $ordersId)
                ->execute()
                ->fetch(\PDO::FETCH_ASSOC);
        } catch (TableNotFoundException $e) {
            // ignore Magnalister not installed/active
        }

        if (!is_array($result)) {
            return null;
        }

        if ($result['platform'] === 'otto') {
            $reference = '';
            $data = \json_decode($result['data'], true);
            if (is_array($data)) {
                $reference = $data['OttoOrderNumber'];
            }

            return [
                'platform' => 'otto',
                'reference' => $reference,
            ];
        }

        if ($result['platform'] === 'amazon') {

            return [
                'platform' => 'amazon',
                'reference' => $result['special'],
            ];
        }

        if ($result['platform'] === 'ebay') {
            $reference = '';
            $data = \json_decode($result['data'], true);
            if (is_array($data)) {
                $reference = $data['ExtendedOrderID'];
            }

            return [
                'platform' => 'ebay',
                'reference' => $reference,
            ];
        }

        return null;
    }
}