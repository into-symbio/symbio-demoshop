<?php

namespace IntoSymbioConnector\Subscriber;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Storefront\Page\Product\ProductPageLoadedEvent;

class ProductPageLoadedSubscriber implements EventSubscriberInterface
{

    private EntityRepositoryInterface $videoRepository;

    public function __construct(EntityRepositoryInterface $videoRepository)
    {
        $this->videoRepository = $videoRepository;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ProductPageLoadedEvent::class => 'onProductDetailPageLoaded',
        ];
    }

    public function onProductDetailPageLoaded(ProductPageLoadedEvent $event): void
    {
        $context = $event->getContext();
        $product = $event->getPage()->getProduct();

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('productId', $product->getId()));

        $data = $this->videoRepository->search($criteria, $context);

        $product->addExtension('mediaVideos', $data);
    }
}
