<?php declare(strict_types=1);

namespace IntoSymbioConnector\Subscriber;

use IntoSymbioConnector\Exception\CustomerDownstreamException;
use IntoSymbioConnector\MessageQueue\Message\CustomerUpdatedMessage;
use IntoSymbioConnector\Struct\DownstreamMessageStruct;
use Shopware\Core\Checkout\Customer\CustomerEvents;
use Shopware\Core\Framework\Api\Context\SalesChannelApiSource;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class CustomerEventsSubscriber implements EventSubscriberInterface
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CustomerEvents::CUSTOMER_WRITTEN_EVENT => 'onCustomerWritten',
        ];
    }

    public function onCustomerWritten(EntityWrittenEvent $event): void
    {
        foreach ($event->getWriteResults() as $result) {
            if ($result->getEntityName() !== 'customer') {
                continue;
            }

            $downstream = new DownstreamMessageStruct();
            $downstream->setEvent($event->getName());
            $downstream->setEntity($result->getEntityName());
            $primaryKey = $result->getPrimaryKey();
            if (is_array($primaryKey)) {
                $primaryKey = array_shift($primaryKey);
            }
            $downstream->setId($primaryKey);
            $downstream->setOperation($result->getOperation());
            if ($event->getContext()->getSource() instanceof SalesChannelApiSource) {
                $downstream->setSalesChannelId($event->getContext()->getSource()->getSalesChannelId());
            }

            $payload = \json_encode($downstream);
            if ($payload === false) {
                throw new CustomerDownstreamException(
                    sprintf('IntoSymbioConnector - error creating downstream message for customer uuid %s', $primaryKey)
                );
            }
            $managerMessage = new CustomerUpdatedMessage();
            $managerMessage->setPayload($payload);

            $this->messageBus->dispatch($managerMessage);
        }
    }
}
