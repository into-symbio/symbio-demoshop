<?php declare(strict_types=1);

namespace IntoSymbioConnector\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1633359991PromotionDiscountAddons extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1633359991;
    }

    /**
     * @param Connection $connection
     * @throws Exception
     */
    public function update(Connection $connection): void
    {
        $connection->executeStatement(
            '
            CREATE TABLE IF NOT EXISTS `into_symbio_connector_promotion_discount_addons` (
              `id` BINARY(16) NOT NULL,
              `promotion_discount_id` BINARY(16) NULL,
              `symbio_promotion_discount_key` VARCHAR(255) NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`id`),
              CONSTRAINT `fk.into_symbio_connector.promotion_discount_id` FOREIGN KEY (`promotion_discount_id`)
                  REFERENCES `promotion_discount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            )
            ENGINE=InnoDB
            DEFAULT CHARSET=utf8mb4
            COLLATE=utf8mb4_unicode_ci;
        '
        );
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
