<?php declare(strict_types=1);

namespace IntoSymbioConnector\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1656416836_unique_index_product_addons extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1656416836;
    }

    public function update(Connection $connection): void
    {
        try {
            $connection->executeStatement(
                'CREATE UNIQUE INDEX `unique.into_symbio_connector.product_id` 
                ON into_symbio_connector_product_addons (product_id,product_version_id);'
            );
        } catch (Exception $e) {
            // ignore
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
