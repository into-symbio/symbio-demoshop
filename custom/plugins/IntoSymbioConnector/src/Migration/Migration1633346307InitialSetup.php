<?php declare(strict_types=1);

namespace IntoSymbioConnector\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1633346307InitialSetup extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1633346307;
    }

    public function update(Connection $connection): void
    {
        $connection->executeStatement(
            '
            CREATE TABLE IF NOT EXISTS `into_symbio_connector_category_addons` (
              `id` BINARY(16) NOT NULL,
              `category_id` BINARY(16) NULL,
              `category_version_id` BINARY(16) NULL,
              `symbio_id` BIGINT(8) NULL,
              `hash` VARCHAR(255) NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `unique.into_symbio_connector.symbio_id` (`symbio_id`),
              CONSTRAINT `fk.into_symbio_connector.category_id` FOREIGN KEY (`category_id`, `category_version_id`)
                  REFERENCES `category` (`id`, `version_id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        '
        );

        $connection->executeStatement(
            '
            CREATE TABLE IF NOT EXISTS `into_symbio_connector_product_addons` (
              `id` BINARY(16) NOT NULL,
              `product_id` BINARY(16) NULL,
              `product_version_id` BINARY(16) NULL,
              `symbio_product_id` BIGINT(8) NULL,
              `symbio_article_id` BIGINT(8) NULL,
              `symbio_article_uid` VARCHAR(255) NULL,
              `symbio_article_sku` VARCHAR(255) NULL,
              `symbio_article_sales_id` BIGINT(8) NULL,
              `symbio_category_id` BIGINT(8) NULL,
              `symbio_distributor_sku` VARCHAR(255) NULL,
              `hash` VARCHAR(255) NULL,
              `videos` JSON NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `unique.into_symbio_connector.symbio_product_id` (`symbio_product_id`),
              UNIQUE KEY `unique.into_symbio_connector.symbio_article_id` (`symbio_article_id`),
              INDEX `unique.into_symbio_connector.symbio_article_sku` (`symbio_article_sku`),
              INDEX `index.into_symbio_connector.symbio_category_id` (`symbio_category_id`),
              INDEX `unique.into_symbio_connector.symbio_distributor_sku` (`symbio_distributor_sku`),
              INDEX `unique.into_symbio_connector.symbio_article_sales_id` (`symbio_article_sales_id`),
              CONSTRAINT `fk.into_symbio_connector.product_id` FOREIGN KEY (`product_id`, `product_version_id`)
                  REFERENCES `product` (`id`, `version_id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        '
        );

        $connection->executeStatement(
            '
            CREATE TABLE IF NOT EXISTS `into_symbio_connector_media_addons` (
              `id` BINARY(16) NOT NULL,
              `media_id` BINARY(16) NULL,
              `symbio_id` BIGINT(8) NULL,
              `hash` VARCHAR(255) NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `unique.into_symbio_connector.symbio_id` (`symbio_id`),
              CONSTRAINT `fk.into_symbio_connector.media_id` FOREIGN KEY (`media_id`)
                  REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        '
        );

        $connection->executeStatement(
            '
            CREATE TABLE IF NOT EXISTS `into_symbio_connector_order_addons` (
              `id` BINARY(16) NOT NULL,
              `order_id` BINARY(16) NULL,
              `order_version_id` BINARY(16) NULL,
              `symbio_order_id` BIGINT(8) NULL,
              `symbio_order_status` VARCHAR(255) NULL,
              `symbio_order_placement` JSON NULL,
              `symbio_order_summary` JSON NULL,
              `symbio_order_timestamp` DATETIME(3) NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `unique.into_symbio_connector.symbio_order_id` (`symbio_order_id`),
              CONSTRAINT `fk.into_symbio_connector.order_id` FOREIGN KEY (`order_id`, `order_version_id`)
                  REFERENCES `order` (`id`, `version_id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        '
        );

        $connection->executeStatement(
            '
            CREATE TABLE IF NOT EXISTS `into_symbio_connector_customer_addons` (
              `id` BINARY(16) NOT NULL,
              `customer_id` BINARY(16) NULL,
              `symbio_recipient_id` BIGINT(8) NULL,
              `symbio_recipient_identification` VARCHAR(255) NULL,
              `main_address_id` BINARY(16) NULL,
              `hash` VARCHAR(255) NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `unique.into_symbio_connector.symbio_recipient_id` (`symbio_recipient_id`),
              CONSTRAINT `fk.into_symbio_connector.customer_id` FOREIGN KEY (`customer_id`)
                  REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.into_symbio_connector.main_address_id` FOREIGN KEY (`main_address_id`)
                  REFERENCES `customer_address` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        '
        );

        $connection->executeStatement(
            '
            CREATE TABLE IF NOT EXISTS `into_symbio_connector_customer_address_addons` (
              `id` BINARY(16) NOT NULL,
              `customer_address_id` BINARY(16) NULL,
              `symbio_address_id` BIGINT(8) NULL,
              `symbio_recipient_id` BIGINT(8) NULL,
              `type` VARCHAR(255) NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `unique.into_symbio_connector.symbio_address_id` (`symbio_address_id`),
              INDEX `index.into_symbio_connector.symbio_recipient_id` (`symbio_recipient_id`),
              CONSTRAINT `fk.into_symbio_connector.customer_address_id` FOREIGN KEY (`customer_address_id`)
                  REFERENCES `customer_address` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        '
        );

        $connection->executeStatement(
            '
            CREATE TABLE IF NOT EXISTS `into_symbio_connector_order_delivery_position_addons` (
              `id` BINARY(16) NOT NULL,
              `order_delivery_position_id` BINARY(16) NULL,
              `order_delivery_position_version_id` BINARY(16) NULL,
              `quantity` INT(11) NULL,
              `carrier` VARCHAR(255) NULL,
              `handling_unit` VARCHAR(255) NULL,
              `tracking_code` VARCHAR(255) NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`id`),
              CONSTRAINT `fk.into_symbio_connector.order_delivery_position_id` FOREIGN KEY (`order_delivery_position_id`)
                  REFERENCES `order_delivery_position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        '
        );
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
