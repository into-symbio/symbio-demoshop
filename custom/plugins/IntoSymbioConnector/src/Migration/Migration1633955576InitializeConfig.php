<?php declare(strict_types=1);

namespace IntoSymbioConnector\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;

class Migration1633955576InitializeConfig extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1633955576;
    }

    public function update(Connection $connection): void
    {
        $sql = <<<SQL
            INSERT IGNORE INTO `system_config`
                SET `id` = '0B62DDA22A434D15A200D3E165351C16',
                    `configuration_key` = 'IntoSymbioConnector.config.triggerOrderStates',
                    `configuration_value` = '{\"_value\": [\"048cc2d38a584f9a8adb3f550b133289\"]}',
                    `created_at` = '2021-10-11 12:30:00.0000';
SQL;
        $connection->executeStatement($sql);

        $sql = <<<SQL
            INSERT IGNORE INTO `system_config`
                SET `id` = '62019F5BBA94406C87E1D8D048141148',
                    `configuration_key` = 'IntoSymbioConnector.config.triggerPaymentStates',
                    `configuration_value` = '{\"_value\": [\"725764972533472c8ef44d0b5c8c3d00\"]}',
                    `created_at` = '2021-10-11 12:30:00.0000';
SQL;
        $connection->executeStatement($sql);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
