<?php declare(strict_types=1);


namespace IntoSymbioConnector\Service\Common;


use IntoSymbioConnector\Api\Authentication\OAuthCredentials;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class IntoConfigService
{
    private SystemConfigService $systemConfigService;

    public function __construct(SystemConfigService $systemConfigService)
    {
        $this->systemConfigService = $systemConfigService;
    }

    public function getApiCredentials(): OAuthCredentials
    {
        $credentials = new OAuthCredentials();
        $credentials->setBaseUrl($this->getString('baseUrl'));
        $credentials->setClientId($this->getString('clientId'));
        $credentials->setUsername($this->getString('username'));
        $credentials->setPassword($this->getString('password'));
        $credentials->setClientSecret($this->getString('clientSecret'));

        return $credentials;
    }

    /**
     * @param string $key
     * @return array|bool|float|int|string|null
     */
    public function get(string $key)
    {

        return $this->systemConfigService->get('IntoSymbioConnector.config.' . $key);
    }

    public function getString(string $key): string
    {

        return $this->systemConfigService->getString('IntoSymbioConnector.config.' . $key);
    }

    public function getArray(string $key): array
    {
        $result = $this->systemConfigService->get('IntoSymbioConnector.config.' . $key) ?? [];
        if (!is_array($result)) {
            $result = [$result];
        }

        return $result;
    }
}
