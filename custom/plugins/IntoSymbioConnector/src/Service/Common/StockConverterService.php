<?php declare(strict_types=1);

namespace IntoSymbioConnector\Service\Common;

use IntoSymbioConnector\Exception\StockConverterException;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;

class StockConverterService implements StockConverterServiceInterface
{
    private EntityRepositoryInterface $productRepository;

    public function __construct(EntityRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function convertStock2Shopware(ProductEntity $productEntity, int $symbioStock, Context $context): int
    {
        return (int)floor($symbioStock / $this->getVpeFromProduct($productEntity, $context));
    }

    public function convertStock2Symbio(?ProductEntity $productEntity, int $shopwareStock, Context $context): int
    {
        if ($productEntity === null) {
            throw new StockConverterException('Missing product entity.');
        }

        return $shopwareStock * $this->getVpeFromProduct($productEntity, $context);
    }

    public function convertPrice2Symbio(?ProductEntity $productEntity, float $shopwarePrice, Context $context): float
    {
        if ($productEntity === null) {
            throw new StockConverterException('Missing product entity.');
        }

        return (float)$this->getPeFromProduct($productEntity, $context) *
            $shopwarePrice /
            (float)$this->getVpeFromProduct($productEntity, $context);
    }

    private function getVpeFromProduct(ProductEntity $productEntity, Context $context): int
    {
        return $this->getCustomFactorFromProduct('migration_Shopware6_product_sy_vpe', $productEntity, $context);
    }

    private function getPeFromProduct(ProductEntity $productEntity, Context $context): int
    {
        return $this->getCustomFactorFromProduct('migration_shopware6_product_SC_PE', $productEntity, $context);
    }

    private function getCustomFactorFromProduct(string $field, ProductEntity $productEntity, Context $context): int
    {
        $customFields = $productEntity->getCustomFields();
        if ($customFields === null && is_string($productEntity->getParentId())) {
            $criteria = new Criteria();
            $criteria->addFilter(new EqualsFilter('id', $productEntity->getParentId()));
            $parentProductEntity = $this->productRepository->search($criteria, $context)->first();
            if ($parentProductEntity instanceof ProductEntity) {
                $customFields = $parentProductEntity->getCustomFields();
            }
        }
        if (is_array($customFields) &&
            isset($customFields[$field]) &&
            $customFields[$field] >= 1) {
            return intval($customFields[$field]);
        }

        return 1;
    }
}
