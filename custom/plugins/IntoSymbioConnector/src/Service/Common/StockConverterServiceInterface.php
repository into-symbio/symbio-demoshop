<?php declare(strict_types=1);

namespace IntoSymbioConnector\Service\Common;

use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;

interface StockConverterServiceInterface
{
        public function convertStock2Shopware(ProductEntity $productEntity, int $symbioStock, Context $context): int;
        public function convertStock2Symbio(?ProductEntity $productEntity, int $shopwareStock, Context $context): int;
        public function convertPrice2Symbio(?ProductEntity $productEntity, float $shopwarePrice, Context $context): float;
}
