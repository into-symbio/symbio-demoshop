<?php declare(strict_types=1);

namespace IntoSymbioConnector\Service\Sync;

use IntoSymbioConnector\Exception\CsvFileInvalidException;
use IntoSymbioConnector\Exception\CsvFileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\Serializer\Serializer;

class TouchedProductsService
{
    private Serializer $serializer;

    private FilesystemInterface $filesystem;

    public function __construct(Serializer $serializer, FilesystemInterface $filesystem)
    {
        $this->serializer = $serializer;
        $this->filesystem = $filesystem;
    }

    public function readCsv(string $path): SymbioObjectsCollection
    {
        $content = $this->filesystem->read('/touched_products/upload/' . $path);
        if ($content === false) {
            throw new CsvFileNotFoundException(sprintf('File %s not found.', $path));
        }

        $data = $this->serializer->decode($content, 'csv');
        if (!is_array($data) || empty($data)) {
            throw new CsvFileInvalidException(sprintf('File %s is invalid.', $path));
        }

        $result = new SymbioObjectsCollection();
        foreach ($data as $row) {
            if ($row['Aktualisieren [0|1]'] === "1") {
                $result->add($row, 'symbioProductId');
            }
        }

        return $result;
    }

    public function writeCsv(SymbioObjectsCollection $touchedProductCollection): void
    {
        if ($touchedProductCollection->count() <= 0) {

            return;
        }
        $timestamp = new \DateTime();
        $timestamp = $timestamp->setTimezone(new \DateTimeZone('Europe/Berlin'));
        $this->filesystem->put(
            '/touched_products/download/symbio_produktabgleich_' . $timestamp->format('Y-m-d_His') . '.csv',
            $this->serializer->encode($this->getData($touchedProductCollection), 'csv')
        );
    }

    private function getData(SymbioObjectsCollection $touchedProductCollection): array
    {
        $data = [];
        /** @var array $cover */
        foreach ($touchedProductCollection as $cover) {
            $data[] = [
                'Aktualisieren [0|1]' => 1,
                'hash' => $cover['newHash'],
                'productNumber' => $cover['productNumber'],
                'symbioProductId' => $cover['id'],
                'symbioCategoryId' => $cover['categoryId'] ?? '',
                'created' => $cover['created'],
                'updated' => $cover['updated'] ?? '',
            ];
        }

        return $data;
    }
}