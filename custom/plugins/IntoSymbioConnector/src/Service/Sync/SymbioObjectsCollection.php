<?php declare(strict_types=1);


namespace IntoSymbioConnector\Service\Sync;


class SymbioObjectsCollection implements \IteratorAggregate
{
    private array $elements = [];

    public function add(array $element, string $idKey = 'id'): void
    {
        $this->elements[$element[$idKey]] = $element;
    }

    /**
     * @param int|string $key
     * @return array|null
     */
    public function get($key): ?array
    {
        if (array_key_exists($key, $this->elements)) {

            return $this->elements[$key];
        }

        return null;
    }

    /**
     * @param int|string $key
     * @return bool
     */
    public function has($key): bool
    {
        return \array_key_exists($key, $this->elements);
    }

    /**
     * @param int|string $key
     * @return array|null
     */
    public function pull($key): ?array
    {
        if (array_key_exists($key, $this->elements)) {
            $element =  $this->elements[$key];
            unset($this->elements[$key]);

            return $element;
        }

        return null;
    }

    public function count(): int
    {
        return \count($this->elements);
    }

    public function getIterator(): \Generator
    {
        yield from $this->elements;
    }
}
