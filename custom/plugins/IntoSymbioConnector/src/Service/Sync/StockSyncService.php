<?php declare(strict_types=1);


namespace IntoSymbioConnector\Service\Sync;


use IntoSymbioConnector\Api\Client\SymbioClientFactory;
use IntoSymbioConnector\Api\Exception\ApiSymbioException;
use IntoSymbioConnector\Api\RequestUri;
use IntoSymbioConnector\DataAbstractionLayer\Entity\ProductAddonsEntity;
use IntoSymbioConnector\Service\Common\IntoConfigService;
use IntoSymbioConnector\Service\Common\StockConverterServiceInterface;
use Psr\Log\LoggerInterface;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class StockSyncService extends AbstractSyncService
{
    private EntityRepositoryInterface $productRepository;

    private StockConverterServiceInterface $stockConverter;
    private SyncLockService $syncLock;


    public function __construct(
        MessageBusInterface $messageBus,
        SymbioClientFactory $symbioClientFactory,
        IntoConfigService $intoConfigService,
        LoggerInterface $logger,
        EntityRepositoryInterface $productRepository,
        StockConverterServiceInterface $stockConverter,
        SyncLockService $syncLock
    ) {
        $this->productRepository = $productRepository;
        $this->stockConverter = $stockConverter;
        $this->syncLock = $syncLock;

        parent::__construct($messageBus, $symbioClientFactory, $intoConfigService, $logger);
    }

    public function run(Context $context): int
    {
        if ($this->syncLock->isSyncLocked()) {
            $this->logger->warning('IntoSymbioConnector: StockSyncService is currently locked.');
            return 0;
        }
        $this->logger->info('IntoSymbioConnector - sync stock starting.');

        $products = $this->getSymbioProducts($context);
        $this->logger->info('IntoSymbioConnector - ' . count($products) . ' articles found for synchronization.');

        $articleUids = $this->getArticleUidsFromProducts($products);
        $stock = $this->getSymbioStock($articleUids);
        $this->updateStock($stock, $products, $context);

        $this->logger->info('IntoSymbioConnector - stock synced for ' . count($stock) . ' articles.');

        return 0;
    }

    private function getSymbioProducts(Context $context): array
    {
        $criteria = new Criteria();
        $criteria->addAssociation('intoSymbioConnectorAddons');
        $criteria->addFilter(
            new NotFilter(
                MultiFilter::CONNECTION_AND, [
                    new EqualsFilter('intoSymbioConnectorAddons.symbioArticleUid', null),
                ]
            )
        );

        return $this->productRepository->search($criteria, $context)->getElements();
    }

    /**
     * @param ProductEntity[] $products
     * @return string[]
     */
    private function getArticleUidsFromProducts(array $products): array
    {
        $articleUids = [];
        foreach ($products as $productEntity) {
            /** @var ProductAddonsEntity $addons */
            $addons = $productEntity->getExtension('intoSymbioConnectorAddons');
            if (is_string($addons->getSymbioArticleUid())) {
                $articleUids[] = $addons->getSymbioArticleUid();
            }
        }

        return $articleUids;
    }

    private function getSymbioStock(array $articleUids): array
    {
        $credentials = $this->intoConfigService->getApiCredentials();
        $client = $this->symbioClientFactory->createSymbioClient($credentials);
        $response = $client->sendPostRequest(
            RequestUri::CHECK_AVAILABILITY_OF_ARTICLES, ['articleUids' => $articleUids]
        );
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new ApiSymbioException(
                [
                    'error' => 'IntoSymbioConnector - Error retrieving stock from Symbio API',
                    'error_description' => json_encode($response->getHeaders()),
                ], $response->getStatusCode()
            );
        }
        $stock = $client->decodeJsonResponse($response);
        if ($stock === null or empty($stock)) {
            return [];
        }

        /** @phpstan-ignore-next-line */
        return array_combine(array_column($stock, 'uid'), array_values($stock));
    }

    private function updateStock(array $stock, array $products, Context $context): void
    {
        $data = [];
        /** @var ProductEntity $productEntity */
        foreach ($products as $productEntity) {
            /** @var ProductAddonsEntity $addon */
            $addon = $productEntity->getExtension('intoSymbioConnectorAddons');
            $uid = $addon->getSymbioArticleUid();
            if ($uid === null || !array_key_exists($uid, $stock)) {
                continue;
            }
            $convertedStock =
                $this->stockConverter->convertStock2Shopware($productEntity, $stock[$uid]['stock'], $context);
            if ($productEntity->getStock() === $convertedStock) {
                continue;
            }
            $data[] = [
                'id' => $productEntity->getId(),
                'stock' => $convertedStock,
                'intoSymbioConnectorAddons' => [
                    'id' => $addon->getId(),
                ],
            ];
        }
        $this->productRepository->upsert($data, $context);
    }
}
