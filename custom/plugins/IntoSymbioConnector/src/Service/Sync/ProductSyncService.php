<?php declare(strict_types=1);


namespace IntoSymbioConnector\Service\Sync;


use IntoSymbioConnector\Api\Client\SymbioClient;
use IntoSymbioConnector\Api\Client\SymbioClientFactory;
use IntoSymbioConnector\Api\RequestUri;
use IntoSymbioConnector\DataAbstractionLayer\Entity\ProductAddonsEntity;
use IntoSymbioConnector\Exception\SymbioProductNotFoundException;
use IntoSymbioConnector\MessageQueue\Message\MediaSyncMessage;
use IntoSymbioConnector\MessageQueue\Message\ProductSyncMessage;
use IntoSymbioConnector\Service\Common\IntoConfigService;
use IntoSymbioConnector\Struct\AbstractPlate;
use IntoSymbioConnector\Struct\ArticlePlate;
use IntoSymbioConnector\Struct\MediaPlate;
use IntoSymbioConnector\Struct\ProductPlate;
use IntoSymbioConnector\Struct\ProductPlatesCollection;
use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class ProductSyncService extends AbstractSyncService
{
    const MODE_STACK = 0; // this is the 'regular' mode - new products created, removed products deactivated
    const MODE_UPDATE = 1; // MODE_STACK plus updates of existing products
    const MODE_CHECK = 2; // like MODE_STACK but ignore new and removed products and report changed products to CSV
    const MODE_TOUCHED = 3; // process a list of products to be updated from a CSV

    private EntityRepositoryInterface $productAddonsRepository;

    private EntityRepositoryInterface $mediaAddonsRepository;

    private TouchedProductsService $touchedProductsService;
    private SyncLockService $syncLock;


    public function __construct(
        MessageBusInterface $messageBus,
        SymbioClientFactory $symbioClientFactory,
        IntoConfigService $intoConfigService,
        LoggerInterface $logger,
        EntityRepositoryInterface $productAddonsRepository,
        EntityRepositoryInterface $mediaAddonsRepository,
        TouchedProductsService $touchedProductsService,
        SyncLockService $syncLock
    ) {
        $this->productAddonsRepository = $productAddonsRepository;
        $this->mediaAddonsRepository = $mediaAddonsRepository;
        $this->touchedProductsService = $touchedProductsService;

        parent::__construct($messageBus, $symbioClientFactory, $intoConfigService, $logger);
        $this->syncLock = $syncLock;
    }

    public function run(Context $context, int $mode = self::MODE_STACK): int
    {
        if ($this->syncLock->isSyncLocked()) {
            $this->logger->warning('IntoSymbioConnector: ProductSyncService is currently locked.');
            return 0;
        }

        $this->logger->info(sprintf('IntoSymbioConnector - sync products starting in mode %s.', $mode));

        $productTree = $this->buildProductTree();

        $existingMedia = $this->fetchExistingMedia($context);
        $existingProducts = $this->fetchExistingProducts($context);

        $productPlates = $this->processMedia($productTree, $existingMedia);
        $this->processProducts($mode, $productPlates, $existingProducts);
        $this->processDeletedProducts($mode, $existingProducts);

        return 0;
    }

    public function processTouchedProducts(string $path, Context $context): void
    {
        $this->logger->info('IntoSymbioConnector - sync products starting in mode touched.');

        $touchedProducts = $this->touchedProductsService->readCsv($path);
        $productTree = $this->buildProductTreeFromTouched($touchedProducts);

        $existingMedia = $this->fetchExistingMedia($context);
        $existingProducts = $this->fetchExistingProducts($context);

        $productPlates = $this->processMedia($productTree, $existingMedia);
        $this->processProducts(self::MODE_UPDATE, $productPlates, $existingProducts);
    }

    /**
     * Pass 0: create ProductPlatesCollection and process media
     *
     * @param array|null $productTree
     * @param SymbioObjectsCollection $existingMedia
     * @return ProductPlatesCollection
     */
    private function processMedia(?array $productTree, SymbioObjectsCollection $existingMedia): ProductPlatesCollection
    {
        $productPlates = new ProductPlatesCollection();

        foreach ($productTree ?? [] as $product) {
            $productPlate = new ProductPlate();
            $productPlate->fromArray($product);

            if (isset($product['media'])) {
                $productPlate->setDirty($this->createMediaFromSet($product['media'], $existingMedia));
            }

            foreach ($product['sellingArticles'] as $article) {
                $articlePlate = new ArticlePlate();
                $articlePlate->fromArray($article);

                if (isset($article['media'])) {
                    $articlePlate->setDirty($this->createMediaFromSet($article['media'], $existingMedia));
                }

                $productPlate->addArticle($articlePlate);
            }

            $productPlates->add($productPlate);
        }

        return $productPlates;
    }

    private function processProducts(
        int $mode,
        ProductPlatesCollection $productPlates,
        SymbioObjectsCollection $existingProducts
    ): void {
        $touchedProducts = new SymbioObjectsCollection();

        /** @var ProductPlate $productPlate */
        foreach ($productPlates as $productPlate) {
            $this->processProduct($mode, $productPlate, $existingProducts, $touchedProducts);
        }

        if ($mode === self::MODE_CHECK) {
            $this->touchedProductsService->writeCsv($touchedProducts);
        }
    }

    private function processProduct(
        int $mode,
        ProductPlate $productPlate,
        SymbioObjectsCollection $existingProducts,
        SymbioObjectsCollection $touchedProducts
    ): void {
        $productId = $productPlate->getProductId();
        if (!is_int($productId)) {
            return;
        }
        $cover = $existingProducts->pull($productId);
        if ($cover === null) {
            if ($mode !== self::MODE_CHECK) {
                $productPlate->setMethod(AbstractPlate::CREATE);
                $this->dispatchProductMessage($productPlate);
            }
            return;
        }

        $newHash = $productPlate->getChecksum();
        if ($productPlate->isDirty() || (is_array($cover) && $cover['hash'] !== $newHash)) {
            if ($mode === self::MODE_UPDATE) {
                $productPlate->setEntityId($cover['entityId']);
                $productPlate->setAddonsId($cover['addonsId']);
                $productPlate->setMethod(AbstractPlate::UPDATE);
                $this->dispatchProductMessage($productPlate);
            }
            if (is_array($cover) && $mode === self::MODE_CHECK) {
                $cover['newHash'] = $newHash;
                $touchedProducts->add($cover);
            }
        }
    }

    private function processDeletedProducts(int $mode, SymbioObjectsCollection $existingProducts): void
    {
        if ($mode === self::MODE_CHECK) {
            return;
        }

        /** @var array $product */
        foreach ($existingProducts as $product) {
            $productPlate = new ProductPlate();
            $productPlate->setEntityId($product['entityId']);
            $productPlate->setAddonsId($product['addonsId']);
            $productPlate->setMethod(AbstractPlate::DELETE);
            $this->dispatchProductMessage($productPlate);
        }
    }

    private function createMediaFromSet(array $mediaSet, SymbioObjectsCollection $existing): bool
    {
        $dirty = false;

        foreach ($mediaSet as $media) {
            $mediaPlate = new MediaPlate();
            $mediaPlate->fromArray($media);

            if ($mediaPlate->getType() === MediaPlate::VIDEO) {
                continue;
            }

            $newMedium = $this->createMedia($mediaPlate, $existing);
            $dirty = $newMedium || $dirty;
        }

        return $dirty;
    }

    /**
     * @param MediaPlate $mediaPlate
     * @param SymbioObjectsCollection $existing
     * @return bool - true, if medium has to be created, false if existing medium
     */
    private function createMedia(MediaPlate $mediaPlate, SymbioObjectsCollection $existing): bool
    {
        $mediaId = $mediaPlate->getMediaId();
        if ($mediaId === null) {
            return false;
        }
        $cover = $existing->get($mediaId);
        if ($cover === null) {
            // new medium
            $mediaPlate->setTitle(($mediaPlate->getText() ?? 'Ohne Text') . ' ' . $mediaPlate->getMediaId());
            $mediaPlate->setMethod(AbstractPlate::CREATE);
            $this->dispatchMediaMessage($mediaPlate);
            return true;
        }

        return false;
    }

    private function createGetAllSellingProductsUrl(): string
    {
        return RequestUri::GET_ALL_SELLING_PRODUCTS .
            $this->intoConfigService->getString('channelId') .
            '?' . http_build_query([
                'accountId' => '',
                'countryIso2' => $this->intoConfigService->getString('country'),
                'currencyIso3' => $this->intoConfigService->getString('currency'),
            ]);
    }

    private function createGetSellingProductUrl(string $symbioProductId): string
    {
        return RequestUri::GET_SELLING_PRODUCT .
            $symbioProductId .
            '/channels/' . $this->intoConfigService->getString('channelId') .
            '?' . http_build_query([
                'accountId' => '',
                'countryIso2' => $this->intoConfigService->getString('country'),
                'currencyIso3' => $this->intoConfigService->getString('currency'),
                'loadArticles' => true,
            ]);
    }

    private function fetchExistingMedia(Context $context): SymbioObjectsCollection
    {
        $criteria = new Criteria();
        $criteria->addFilter(new NotFilter(
            MultiFilter::CONNECTION_AND, [
                new EqualsFilter('symbioId', null),
            ]
        ));
        /** @var Entity[] $elements */
        $elements = $this->mediaAddonsRepository->search($criteria, $context)->getElements();

        $existing = new SymbioObjectsCollection();
        foreach ($elements as $element) {
            $existing->add([
                'id' => $element->get('symbioId'),
                'hash' => $element->get('hash'),
                'entityId' => $element->get('mediaId'),
                'addonsId' => $element->get('id'),
            ]);
        }

        return $existing;
    }

    private function fetchExistingProducts(Context $context): SymbioObjectsCollection
    {
        $criteria = new Criteria();
        $criteria->addFilter(new NotFilter(
            MultiFilter::CONNECTION_AND, [
                new EqualsFilter('symbioProductId', null),
            ]
        ));
        $criteria->addAssociation('product');
        /** @var ProductAddonsEntity[] $elements */
        $elements = $this->productAddonsRepository->search($criteria, $context)->getElements();

        $existing = new SymbioObjectsCollection();
        foreach ($elements as $element) {
            $existing->add([
                'id' => $element->get('symbioProductId'),
                'hash' => $element->get('hash'),
                'entityId' => $element->get('productId'),
                'addonsId' => $element->get('id'),
                'categoryId' => $element->get('symbioCategoryId'),
                'productNumber' => $element->getProduct()?->getProductNumber(),
                'created' => $element->getCreatedAt()?->format('Y-m-d H:i:s'),
                'updated' => $element->getUpdatedAt()?->format('Y-m-d H:i:s'),
            ]);
        }

        return $existing;
    }

    private function buildProductTreeFromTouched(SymbioObjectsCollection $touchedProducts): array
    {
        $credentials = $this->intoConfigService->getApiCredentials();
        $client = $this->symbioClientFactory->createSymbioClient($credentials);

        $productTree = [];
        /** @var array $touchedProduct */
        foreach ($touchedProducts as $touchedProduct) {
            $productTree[] = $this->getProduct($client, $touchedProduct['symbioProductId']);
        }
        $this->normalizeProductTree($productTree);

        return $productTree;
    }

    private function buildProductTree(): array
    {
        $credentials = $this->intoConfigService->getApiCredentials();
        $client = $this->symbioClientFactory->createSymbioClient($credentials);
        $response = $client->sendGetRequest($this->createGetAllSellingProductsUrl(), null, 0);
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new \RuntimeException(sprintf('IntoSymbioConnector - no products found to sync, status code: %s',
                $response->getStatusCode()));
        }
        $productTree = $client->decodeJsonResponse($response);
        $this->normalizeProductTree($productTree);

        return $productTree;
    }

    private function getProduct(SymbioClient $client, string $symbioProductId): ?array
    {
        $response = $client->sendGetRequest($this->createGetSellingProductUrl($symbioProductId), null, 0);
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new SymbioProductNotFoundException(sprintf('Symbio product with id %s not found.', $symbioProductId));
        }
        return $client->decodeJsonResponse($response);
    }

    private function normalizeProductTree(array &$productTree): void
    {
        foreach ($productTree as &$product) {
            if (isset($product['sellingArticles'])) {
                usort($product['sellingArticles'], function ($a, $b) {
                    return $a['articleId'] <=> $b['articleId'];
                });
                foreach ($product['sellingArticles'] as &$article) {
                    if (isset($article['attributes'])) {
                        usort($article['attributes'], function ($a, $b) {
                            return $a['key'] <=> $b['key'];
                        });
                    }
                }
            }
        }
    }

    private function dispatchMediaMessage(MediaPlate $plate): void
    {
        $this->dispatchMessage(new MediaSyncMessage(), $plate);
    }

    private function dispatchProductMessage(ProductPlate $plate): void
    {
        $this->dispatchMessage(new ProductSyncMessage(), $plate);
    }
}
