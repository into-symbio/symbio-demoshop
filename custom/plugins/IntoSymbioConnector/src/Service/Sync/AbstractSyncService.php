<?php declare(strict_types=1);


namespace IntoSymbioConnector\Service\Sync;


use IntoSymbioConnector\Api\Client\SymbioClientFactory;
use IntoSymbioConnector\MessageQueue\Message\AbstractSyncMessage;
use IntoSymbioConnector\MessageQueue\Message\CategorySyncMessage;
use IntoSymbioConnector\Struct\AbstractPlate;
use IntoSymbioConnector\Service\Common\IntoConfigService;
use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Context;
use Symfony\Component\Messenger\MessageBusInterface;

abstract class AbstractSyncService
{
    protected MessageBusInterface $messageBus;

    protected SymbioClientFactory $symbioClientFactory;

    protected IntoConfigService $intoConfigService;

    protected LoggerInterface $logger;

    public function __construct(
        MessageBusInterface $messageBus,
        SymbioClientFactory $symbioClientFactory,
        IntoConfigService $intoConfigService,
        LoggerInterface $logger
    ) {
        $this->messageBus = $messageBus;
        $this->symbioClientFactory = $symbioClientFactory;
        $this->intoConfigService = $intoConfigService;
        $this->logger = $logger;
    }

    abstract public function run(Context $context): int;

    protected function dispatchMessage(AbstractSyncMessage $managerMessage, AbstractPlate $plate): void
    {
        $payload = \json_encode($plate);
        if (is_string($payload)) {
            $managerMessage->setPayload($payload);

            $this->messageBus->dispatch($managerMessage);
        }
    }
}
