<?php declare(strict_types=1);


namespace IntoSymbioConnector\Service\Sync;


use IntoSymbioConnector\Api\Client\SymbioClientFactory;
use IntoSymbioConnector\Api\RequestUri;
use IntoSymbioConnector\DataAbstractionLayer\Entity\ProductAddonsEntity;
use IntoSymbioConnector\MessageQueue\Message\CategorySyncMessage;
use IntoSymbioConnector\MessageQueue\Message\ProductSyncMessage;
use IntoSymbioConnector\Service\Common\IntoConfigService;
use IntoSymbioConnector\Struct\AbstractPlate;
use IntoSymbioConnector\Struct\CategoryPlate;
use IntoSymbioConnector\Struct\ProductPlate;
use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\Framework\Struct\ArrayEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class CategorySyncService extends AbstractSyncService
{
    private EntityRepositoryInterface $categoryAddonsRepository;

    private EntityRepositoryInterface $productAddonsRepository;
    private SyncLockService $syncLock;

    public function __construct(
        MessageBusInterface $messageBus,
        SymbioClientFactory $symbioClientFactory,
        IntoConfigService $intoConfigService,
        LoggerInterface $logger,
        EntityRepositoryInterface $categoryAddonsRepository,
        EntityRepositoryInterface $productAddonsRepository,
        SyncLockService $syncLock
    ) {
        $this->categoryAddonsRepository = $categoryAddonsRepository;
        $this->productAddonsRepository = $productAddonsRepository;
        parent::__construct($messageBus, $symbioClientFactory, $intoConfigService, $logger);
        $this->syncLock = $syncLock;
    }

    public function run(Context $context): int
    {
        if ($this->syncLock->isSyncLocked()) {
            $this->logger->info('IntoSymbioConnector: CategorySyncService is currently locked.');
            return 0;
        }
        $this->logger->info('IntoSymbioConnector - sync categories starting.');

        $credentials = $this->intoConfigService->getApiCredentials();
        $client = $this->symbioClientFactory->createSymbioClient($credentials);
        $response = $client->sendGetRequest(RequestUri::GET_LOCALIZED_CATEGORY_FOR_ACCOUNT, null, 0);
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error('IntoSymbioConnector - no categories found to sync.');

            return 1;
        }
        $categoryTree = $client->decodeJsonResponse($response);

        $existing = $this->fetchExistingCategories($context);

        $this->processCategory($categoryTree, null, $existing, true);
        $this->processTree($categoryTree, $existing);

        $this->processDeleted($existing, $context);

        return 0;
    }

    private function processCategory(
        ?array $category,
        ?int $sibling,
        SymbioObjectsCollection $existing,
        bool $root = false
    ): void {
        if ($category === null) {
            return;
        }
        $categoryPlate = new CategoryPlate();
        $categoryPlate->fromArray($category);
        $categoryPlate->setSiblingId($sibling);
        $categoryPlate->setRoot($root);

        $categoryId = $categoryPlate->getCategoryId();
        if (!is_int($categoryId)) {
            return;
        }
        $cover = $existing->pull($categoryId);
        if ($cover === null) {
            $categoryPlate->setMethod(AbstractPlate::CREATE);
            $this->dispatchCategoryMessage($categoryPlate);

            return;
        }

        if ($cover['hash'] !== $categoryPlate->getChecksum()) {
            $categoryPlate->setMethod(AbstractPlate::UPDATE);
            $categoryPlate->setEntityId($cover['entityId']);
            $categoryPlate->setAddonsId($cover['addonsId']);
            $this->dispatchCategoryMessage($categoryPlate);
        }
    }

    private function processTree(?array $tree, SymbioObjectsCollection $existing): void
    {
        if ($tree === null) {
            return;
        }
        if (!is_array($tree['children'])) {
            return;
        }

        $sibling = null;
        foreach ($tree['children'] as $category) {
            $this->processCategory($category, $sibling, $existing);
            $sibling = $category['id'];
        }

        foreach ($tree['children'] as $category) {
            $this->processTree($category, $existing);
        }
    }

    private function processDeleted(SymbioObjectsCollection $existing, Context $context): void
    {
        /** @var array $category */
        foreach ($existing as $category) {
            $this->processDeletedCategory($category, $context);
        }
    }

    private function processDeletedCategory(array $category, Context $context): void
    {
        $associated = $this->getAssociatedProducts($category['id'], $context);
        /** @var ProductAddonsEntity $productAddons */
        foreach ($associated as $productAddons) {
            $productPlate = new ProductPlate();
            $productPlate->setMethod(AbstractPlate::DELETE);
            $productPlate->setEntityId($productAddons->getProductId());
            $productPlate->setAddonsId($productAddons->getId());
            $this->dispatchProductMessage($productPlate);
        }

        $categoryPlate = new CategoryPlate();
        $categoryPlate->setMethod(AbstractPlate::DELETE);
        $categoryPlate->setEntityId($category['entityId']);
        $categoryPlate->setAddonsId($category['addonsId']);
        $this->dispatchCategoryMessage($categoryPlate);
    }

    private function getAssociatedProducts(int $categoryId, Context $context): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('symbioCategoryId', $categoryId));

        return $this->productAddonsRepository->search($criteria, $context)->getElements();
    }

    private function fetchExistingCategories(Context $context): SymbioObjectsCollection
    {
        $criteria = new Criteria();
        $criteria->addFilter(new NotFilter(
            MultiFilter::CONNECTION_AND, [
                new EqualsFilter('symbioId', null),
            ]
        ));

        $categories = $this->categoryAddonsRepository->search($criteria, $context)->getElements();

        $existing = new SymbioObjectsCollection();

        /** @var ArrayEntity $element */
        foreach ($categories as $element) {
            $existing->add([
                'id' => $element->get('symbioId'),
                'hash' => $element->get('hash'),
                'entityId' => $element->get('categoryId'),
                'addonsId' => $element->get('id'),
            ]);
        }

        return $existing;
    }

    private function dispatchCategoryMessage(CategoryPlate $plate): void
    {
        $this->dispatchMessage(new CategorySyncMessage(), $plate);
    }

    private function dispatchProductMessage(ProductPlate $plate): void
    {
        $this->dispatchMessage(new ProductSyncMessage(), $plate);
    }
}
