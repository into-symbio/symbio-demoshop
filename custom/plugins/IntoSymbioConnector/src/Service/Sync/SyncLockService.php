<?php declare(strict_types=1);

namespace IntoSymbioConnector\Service\Sync;


class SyncLockService
{
    public function isSyncLocked(): bool
    {
        return file_exists(__DIR__ . '/../../CronJobs/sync.lock');
    }
}