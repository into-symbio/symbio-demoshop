<?php declare(strict_types=1);

namespace IntoSymbioConnector\Service\Collect;

use IntoSymbioConnector\Exception\OrderCollectException;
use IntoSymbioConnector\MessageQueue\Message\OrderUpdatedMessage;
use IntoSymbioConnector\Service\Common\IntoConfigService;
use IntoSymbioConnector\Service\Sync\SyncLockService;
use IntoSymbioConnector\Struct\DownstreamMessageStruct;
use Monolog\Logger;
use Shopware\Core\Checkout\Order\Aggregate\OrderTransaction\OrderTransactionEntity;
use Shopware\Core\Checkout\Order\OrderCollection;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\EntityWriteResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\AndFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\RangeFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Symfony\Component\Messenger\MessageBusInterface;

class OrderCollectService
{
    const ORDER_TIME_CUSHION = 'PT1M';

    private MessageBusInterface $messageBus;

    private EntityRepositoryInterface $orderRepository;

    private EntityRepositoryInterface $orderAddonsRepository;

    private Logger $logger;

    private IntoConfigService $intoConfigService;
    private SyncLockService $syncLock;


    public function __construct(
        MessageBusInterface $messageBus,
        EntityRepositoryInterface $orderRepository,
        EntityRepositoryInterface $orderAddonsRepository,
        Logger $logger,
        IntoConfigService $intoConfigService,
        SyncLockService $syncLock,
    ) {
        $this->messageBus = $messageBus;
        $this->orderRepository = $orderRepository;
        $this->orderAddonsRepository = $orderAddonsRepository;
        $this->logger = $logger;
        $this->intoConfigService = $intoConfigService;
        $this->syncLock = $syncLock;
    }

    public function collect(Context $context): void
    {
        if ($this->syncLock->isSyncLocked()) {
            $this->logger->warning('IntoSymbioConnector: OrderCollectService is currently locked.');
            return;
        }

        if (!$this->intoConfigService->get('exportOrders')) {
            return;
        }

        $this->logger->info('IntoSymbioConnector - OrderCollectService starting.');

        $pendingOrderCollection = $this->getPendingOrders($context);
        $this->logger->info(
            'IntoSymbioConnector - OrderCollectService collecting ' . $pendingOrderCollection->count() . ' orders.'
        );

        foreach ($pendingOrderCollection->getElements() as $pendingOrderEntity) {
            $this->dispatchOrderMessage($pendingOrderEntity->getId(), $context);
        }

        $this->logger->info('IntoSymbioConnector - OrderCollectService terminating.');
    }

    private function getPendingOrders(Context $context): OrderCollection
    {
        $timeStamp = new \DateTime();
        $timeStamp->sub(new \DateInterval(self::ORDER_TIME_CUSHION));

        $criteria = new Criteria();
        $criteria->addAssociation('transactions');
        $criteria->addAssociation('intoSymbioConnectorAddons');
        $criteria->addFilter(
            new AndFilter([
                new RangeFilter('orderDateTime', [
                        RangeFilter::LT => $timeStamp->format(Defaults::STORAGE_DATE_TIME_FORMAT),
                        RangeFilter::GTE => $this->intoConfigService->get('orderExportStart'),
                    ]
                ),
                new EqualsAnyFilter('stateId', $this->intoConfigService->getArray('triggerOrderStates')),
                new EqualsFilter('intoSymbioConnectorAddons.id', null),
            ])
        );

        /** @var OrderCollection $orderCollection */
        $orderCollection = $this->orderRepository->search($criteria, $context);

        /**
         * @var string $orderKey
         * @var OrderEntity $orderEntity
         */
        foreach ($orderCollection->getElements() as $orderKey => $orderEntity) {
            $orderTransactionEntity = $orderEntity->getTransactions()?->first();
            if (!$orderTransactionEntity instanceof OrderTransactionEntity ||
                !in_array($orderTransactionEntity->getStateId(),
                    $this->intoConfigService->getArray('triggerPaymentStates'))
            ) {
                $orderCollection->remove($orderKey);
            }
        }

        return new OrderCollection($orderCollection->getElements());
    }

    private function dispatchOrderMessage(string $orderId, Context $context): void
    {
        $downstream = new DownstreamMessageStruct();
        $downstream->setEvent('createCollectedOrder');
        $downstream->setEntity('order');
        $downstream->setId($orderId);
        $downstream->setOperation(EntityWriteResult::OPERATION_INSERT);
        $downstream->setSalesChannelId($this->intoConfigService->getString('salesChannel'));

        $payload = json_encode($downstream);
        if ($payload === false) {
            throw new OrderCollectException(sprintf('Failure to create downstream message, order id: %s', $orderId));
        }

        $data = [
            'id' => Uuid::randomHex(),
            'orderId' => $orderId,
        ];
        $this->orderAddonsRepository->create([$data], $context);

        $managerMessage = new OrderUpdatedMessage();
        $managerMessage->setPayload($payload);
        $this->messageBus->dispatch(($managerMessage));
    }
}
