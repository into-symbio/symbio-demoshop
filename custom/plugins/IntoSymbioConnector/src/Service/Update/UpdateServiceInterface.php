<?php declare(strict_types=1);


namespace IntoSymbioConnector\Service\Update;


use IntoSymbioConnector\Struct\AbstractPlate;
use Shopware\Core\Framework\Context;

interface UpdateServiceInterface
{
    public function handle(AbstractPlate $plate, Context $context): void;
}
