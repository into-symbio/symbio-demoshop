<?php declare(strict_types=1);


namespace IntoSymbioConnector\Service\Update;


use IntoSymbioConnector\Exception\CategoryNotFoundException;
use IntoSymbioConnector\Struct\AbstractPlate;
use IntoSymbioConnector\Struct\CategoryPlate;
use IntoSymbioConnector\Service\Common\IntoConfigService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;

class CategoryUpdateService implements UpdateServiceInterface
{
    private IntoConfigService $intoConfigService;

    private EntityRepositoryInterface $categoryRepository;

    public function __construct(
        IntoConfigService $intoConfigService,
        EntityRepositoryInterface $categoryRepository
    ) {
        $this->intoConfigService = $intoConfigService;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param CategoryPlate $plate
     * @param Context $context
     * @throws CategoryNotFoundException
     */
    public function handle(AbstractPlate $plate, Context $context): void
    {
        if ($plate->isRoot()) {
            $this->upsertRoot($plate, $context);

            return;
        }
        switch ($plate->getMethod()) {
            case AbstractPlate::CREATE:
            case AbstractPlate::UPDATE:
                $this->upsert($plate, $context);
                break;
            case AbstractPlate::DELETE:
                $this->deactivate($plate, $context);
                break;
        }
    }

    private function upsert(CategoryPlate $categoryPlate, Context $context): void
    {
        $parentId = $this->getShopwareIdFromSymbioId($categoryPlate->getParentId(), $context);
        if (!is_string($parentId)) {
            throw new CategoryNotFoundException('IntoSymbioConnector - parent category not found, symbio id: ' .
                $categoryPlate->getCategoryId());
        }
        $data = $this->createBasicData($categoryPlate, $context);
        $data['parentId'] = $parentId;
        if (is_int($categoryPlate->getSiblingId())) {
            $afterCategoryId = $this->getShopwareIdFromSymbioId($categoryPlate->getSiblingId(), $context);
            if (!is_string($afterCategoryId)) {
                throw new CategoryNotFoundException('IntoSymbioConnector - sibling category not found, symbio id: ' .
                    $categoryPlate->getSiblingId());
            }
            $data['afterCategoryId'] = $afterCategoryId;
        }
        if ($categoryPlate->getMethod() === AbstractPlate::CREATE) {
            $data['active'] = false;
        }
        if ($categoryPlate->getMethod() === AbstractPlate::UPDATE) {
            $data['id'] = $categoryPlate->getEntityId();
            $data['intoSymbioConnectorAddons']['id'] = $categoryPlate->getAddonsId();
        }

        $this->categoryRepository->upsert([$data], $context);
    }

    private function deactivate(CategoryPlate $categoryPlate, Context $context): void
    {
        $data = [
            'id' => $categoryPlate->getEntityId(),
            'active' => false,
            'intoSymbioConnectorAddons' => [
                'id' => $categoryPlate->getAddonsId(),
                'symbioId' => null,
                'hash' => null,
            ],
        ];

        $this->categoryRepository->update([$data], $context);
    }

    private function upsertRoot(CategoryPlate $categoryPlate, Context $context): void
    {
        $rootId = $this->intoConfigService->getString('rootCategory');

        $data = $this->createBasicData($categoryPlate, $context);
        $data['id'] = $rootId;

        if ($categoryPlate->getMethod() === AbstractPlate::UPDATE) {
            $data['intoSymbioConnectorAddons']['id'] = $categoryPlate->getAddonsId();
        }

        $this->categoryRepository->upsert([$data], $context);
    }

    private function createBasicData(CategoryPlate $categoryPlate, Context $context): array
    {
        $data = [
            'translations' => [
                $context->getLanguageId() => [
                    'name' => $categoryPlate->getName(),
                ],
            ],
            'intoSymbioConnectorAddons' => [
                'symbioId' => $categoryPlate->getCategoryId(),
                'hash' => $categoryPlate->getChecksum(),
            ],
        ];
        if (is_string($categoryPlate->getDescription())) {
            $data['translations'][$context->getLanguageId()]['description'] = $categoryPlate->getDescription();
        }

        return $data;
    }

    private function getShopwareIdFromSymbioId(?int $id, Context $context): ?string
    {
        if ($id === null) {
            return null;
        }

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('intoSymbioConnectorAddons.symbioId', $id));
        $criteria->setLimit(1);

        return $this->categoryRepository->searchIds($criteria, $context)->firstId();
    }
}
