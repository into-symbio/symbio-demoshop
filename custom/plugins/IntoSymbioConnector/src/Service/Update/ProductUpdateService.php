<?php declare(strict_types=1);

namespace IntoSymbioConnector\Service\Update;

use IntoSymbioConnector\DataAbstractionLayer\Entity\ProductAddonsEntity;
use IntoSymbioConnector\Exception\InvalidDataException;
use IntoSymbioConnector\Exception\ProductNotFoundException;
use IntoSymbioConnector\Service\Hydrator\ProductHydrator;
use IntoSymbioConnector\Struct\AbstractPlate;
use IntoSymbioConnector\Struct\ArticlePlate;
use IntoSymbioConnector\Struct\ProductPlate;
use Shopware\Core\Content\Product\Aggregate\ProductMedia\ProductMediaCollection;
use Shopware\Core\Content\Product\Aggregate\ProductMedia\ProductMediaDefinition;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\DefinitionInstanceRegistry;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;

class ProductUpdateService implements UpdateServiceInterface
{
    private ProductHydrator $productHydrator;

    private DefinitionInstanceRegistry $definitionInstanceRegistry;

    public function __construct(
        ProductHydrator $productHydrator,
        DefinitionInstanceRegistry $definitionInstanceRegistry
    ) {
        $this->productHydrator = $productHydrator;
        $this->definitionInstanceRegistry = $definitionInstanceRegistry;
    }

    /**
     * @param ProductPlate $plate
     * @param Context $context
     * @throws InvalidDataException
     * @throws ProductNotFoundException
     */
    public function handle(AbstractPlate $plate, Context $context): void
    {
        switch ($plate->getMethod()) {
            case AbstractPlate::CREATE:
                $this->create($plate, $context);
                break;
            case AbstractPlate::UPDATE:
                $this->update($plate, $context);
                break;
            case AbstractPlate::DELETE:
                $this->deactivate($plate, $context);
                break;
        }
    }

    private function create(ProductPlate $productPlate, Context $context): void
    {
        if ($this->isVariantProduct($productPlate)) {
            $this->createVariantProduct($productPlate, $context);

            return;
        }
        $this->createSingleProduct($productPlate, $context);
    }

    private function createSingleProduct(ProductPlate $productPlate, Context $context): void
    {
        $data = $this->productHydrator->hydrateSingleProduct($productPlate, $context);
        $this->getRepository(ProductDefinition::ENTITY_NAME)->create([$data], $context);
    }

    private function createVariantProduct(ProductPlate $productPlate, Context $context): void
    {
        $data = $this->productHydrator->hydrateVariantProduct($productPlate, $context);
        $parentId = Uuid::randomHex();
        $data['id'] = $parentId;
        $data['children'] = [];
        foreach ($productPlate->getArticles() as $articlePlate) {
            $article = $this->productHydrator->hydrateVariantArticle($articlePlate, $context);
            $article['id'] = Uuid::randomHex();
            $article['parentId'] = $parentId;
            $data['children'][] = $article;
        }
        $this->getRepository(ProductDefinition::ENTITY_NAME)->create([$data], $context);
    }

    /**
     * @throws InvalidDataException
     * @throws ProductNotFoundException
     */
    private function update(ProductPlate $productPlate, Context $context): void
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('id', $productPlate->getEntityId()))
            ->addAssociations([
                'media',
                'children',
                'intoSymbioConnectorAddons',
                'children.media',
                'children.intoSymbioConnectorAddons',
                'configuratorSettings',
            ])
            ->setLimit(1);
        $product = $this->getRepository(ProductDefinition::ENTITY_NAME)->search($criteria, $context)->first();
        if (!$product instanceof ProductEntity) {
            throw new ProductNotFoundException(
                'IntoSymbioConnector - product not found, entity id: ' . $productPlate->getEntityId()
            );
        }

        $this->disconnectMediaFromProduct($product, $context);

        if ($this->isVariantProduct($productPlate)) {
            $this->updateVariantProduct($product, $productPlate, $context);
            $this->deactivateDeletedArticles($product, $productPlate, $context);

            return;
        }

        $this->updateSingleProduct($product, $productPlate, $context);
    }

    /**
     * @throws InvalidDataException
     */
    private function updateSingleProduct(ProductEntity $product, ProductPlate $productPlate, Context $context): void
    {
        if ($product->getChildren()?->count() > 0) {
            throw new InvalidDataException(
                'intoSymbioConnector - cannot change variant product to single product. Symbio productId: ' . $productPlate->getProductId()
            );
        }
        $data = $this->productHydrator->hydrateSingleProductUpdate($productPlate, $context);
        $data['id'] = $productPlate->getEntityId();
        $this->getRepository(ProductDefinition::ENTITY_NAME)->update([$data], $context);
    }

    /**
     * @throws InvalidDataException
     */
    private function updateVariantProduct(ProductEntity $product, ProductPlate $productPlate, Context $context): void
    {
        $data = [];
        if ($product->getChildren()?->count() < 1) {
            throw new InvalidDataException(
                'intoSymbioConnector - cannot change single product to variant product. Symbio productId: ' . $productPlate->getProductId()
            );
        }
        $parentData = $this->productHydrator->hydrateVariantProductUpdate($productPlate, $context);
        $parentData['id'] = $productPlate->getEntityId();
        $parentData['intoSymbioConnectorAddons']['id'] = $productPlate->getAddonsId();
        $parentData['configuratorSettings'] = [];

        foreach ($productPlate->getArticles() as $articlePlate) {
            $childData = $this->productHydrator->hydrateVariantArticleUpdate($articlePlate, $context);
            $child = $this->findArticleForArticlePlate($product, $articlePlate);

            // existing article / variant
            if ($child instanceof ProductEntity) {
                /** @var ProductAddonsEntity $addons */
                $addons = $child->getExtension('intoSymbioConnectorAddons');
                $childData['id'] = $child->getId();
                $childData['intoSymbioConnectorAddons']['id'] = $addons->get('id');
                $data[] = $childData;
                continue;
            }

            // new article / variant
            $childData['id'] = Uuid::randomHex();
            $childData['stock'] = 0;
            $childData['productNumber'] = $articlePlate->getProductId() . '.' . $articlePlate->getArticleId();
            $childData['parentId'] = $parentData['id'];
            $childData['intoSymbioConnectorAddons']['id'] = Uuid::randomHex();
            $childData['intoSymbioConnectorAddons']['symbioArticleId'] = $articlePlate->getArticleId();

            $parentData['children'][] = $childData;

            $parentData['configuratorSettings'] = array_merge(
                $parentData['configuratorSettings'],
                $this->getConfiguratorSettings($product, $articlePlate, $context)
            );
        }

        $data[] = $parentData;

        $this->getRepository(ProductDefinition::ENTITY_NAME)->upsert($data, $context);
    }

    private function getConfiguratorSettings(
        ProductEntity $productEntity,
        ArticlePlate $articlePlate,
        Context $context
    ): array {
        $settings = $this->productHydrator->getConfiguratorOptionIdsForArticle($articlePlate, $context, 'optionId');

        $existingOptionIds = array_flip($productEntity->getConfiguratorSettings()?->getOptionIds() ?? []);
        if (count($existingOptionIds) === 0) {
            return $settings;
        }

        foreach ($settings as &$setting) {
            if (array_key_exists($setting['optionId'], $existingOptionIds)) {
                $setting['id'] = $existingOptionIds[$setting['optionId']];
            }
        }

        return $settings;
    }

    private function findArticleForArticlePlate(ProductEntity $product, ArticlePlate $articlePlate): ?ProductEntity
    {
        foreach (($product->getChildren() ?? []) as $child) {
            /** @var ProductAddonsEntity|null $addons */
            $addons = $child->getExtension('intoSymbioConnectorAddons');
            if ($addons?->get('symbioArticleId') === $articlePlate->getArticleId()) {
                return $child;
            }
        }

        return null;
    }

    private function deactivate(ProductPlate $productPlate, Context $context): void
    {
        $this->productHydrator->fillVariantArticles($productPlate, $context);

        $data = array_map(function ($articlePlate) {
            return [
                'id' => $articlePlate->getEntityId(),
                'active' => false,
                'stock' => 0,
            ];
        }, $productPlate->getArticles());

        $data[] = [
            'id' => $productPlate->getEntityId(),
            'active' => false,
            'stock' => 0,
        ];

        $this->getRepository(ProductDefinition::ENTITY_NAME)->update($data, $context);
    }

    private function deactivateDeletedArticles(
        ProductEntity $productEntity,
        ProductPlate $productPlate,
        Context $context
    ): void {
        foreach (($productEntity->getChildren() ?? []) as $child) {
            /** @var ProductAddonsEntity|null $addons */
            $addons = $child->getExtension('intoSymbioConnectorAddons');
            $symbioArticleId = $addons?->get('symbioArticleId');
            if (!(is_int($symbioArticleId) && $symbioArticleId > 0)) {
                continue;
            }
            if ($this->getArticlePlateByArticleId($symbioArticleId, $productPlate) instanceof ArticlePlate) {
                continue;
            }
            $this->getRepository(ProductDefinition::ENTITY_NAME)->update([
                [
                    'id' => $child->getId(),
                    'active' => false,
                    'stock' => 0,
                ],
            ], $context);
        }
    }

    private function getArticlePlateByArticleId(int $symbioArticleId, ProductPlate $productPlate): ?ArticlePlate
    {
        foreach ($productPlate->getArticles() as $articlePlate) {
            if ($symbioArticleId === $articlePlate->getArticleId()) {
                return $articlePlate;
            }
        }

        return null;
    }

    private function disconnectMediaFromProduct(ProductEntity $productEntity, Context $context): void
    {
        $ids = [];
        if ($productEntity->getMedia() instanceof ProductMediaCollection) {
            $ids = $productEntity->getMedia()->getIds();
        }
        foreach ($productEntity->getChildren() ?? [] as $child) {
            if ($child->getMedia() instanceof ProductMediaCollection) {
                $ids = $ids + $child->getMedia()->getIds();
            }
        }
        $ids = array_map(function ($id) {
            return ['id' => $id];
        }, $ids);
        $this->getRepository(ProductMediaDefinition::ENTITY_NAME)->delete(array_values($ids), $context);
    }

    private function isVariantProduct(ProductPlate $productPlate): bool
    {
        if (count($productPlate->getArticles()) > 1) {
            return true;
        }
        if (count($productPlate->getArticles()) === 1) {
            $article = current($productPlate->getArticles());
            foreach ($article->getAttributeStructs() as $attribute) {
                if ($attribute->getType() === 'FEATURE') {
                    return true;
                }
            }
        }

        return false;
    }

    private function getRepository(string $entity): EntityRepositoryInterface
    {
        return $this->definitionInstanceRegistry->getRepository($entity);
    }
}
