<?php declare(strict_types=1);

namespace IntoSymbioConnector\Service\Update;

use IntoSymbioConnector\DataAbstractionLayer\Entity\MediaAddonsEntity;
use IntoSymbioConnector\Struct\AbstractPlate;
use IntoSymbioConnector\Struct\MediaPlate;
use IntoSymbioConnector\Service\Common\IntoConfigService;
use Psr\Log\LoggerInterface;
use Shopware\Core\Content\Media\Exception\DuplicatedMediaFileNameException;
use Shopware\Core\Content\Media\MediaService;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;

class MediaUpdateService implements UpdateServiceInterface
{
    private IntoConfigService $intoConfigService;

    private EntityRepositoryInterface $mediaRepository;

    private EntityRepository $mediaAddonsRepository;

    private MediaService $mediaService;

    private LoggerInterface $logger;

    public function __construct(
        IntoConfigService $intoConfigService,
        EntityRepositoryInterface $mediaRepository,
        EntityRepository $mediaAddonsRepository,
        MediaService $mediaService,
        LoggerInterface $logger
    ) {
        $this->intoConfigService = $intoConfigService;
        $this->mediaRepository = $mediaRepository;
        $this->mediaAddonsRepository = $mediaAddonsRepository;
        $this->mediaService = $mediaService;
        $this->logger = $logger;
    }

    /**
     * @param MediaPlate $plate
     * @param Context $context
     */
    public function handle(AbstractPlate $plate, Context $context): void
    {
        $this->create($plate, $context);
    }

    private function create(MediaPlate $mediaPlate, Context $context): void
    {
        // this can happen if media have been created during one and same import
        if ($this->mediaExists($mediaPlate, $context)) {
            return;
        }

        $mediaId = $this->createMedia($mediaPlate, $context);
        $uri = $mediaPlate->getUri();
        $content = file_get_contents($uri);
        if ($content === false) {
            $this->logger->error(sprintf('IntoSymbioConnector - image not found for uri: %s', $mediaPlate->getUri()));
            $this->mediaRepository->delete([
                [
                    'id' => $mediaId,
                ],
            ], $context);

            return;
        }

        try {
            $extension = strtolower(pathinfo($uri, PATHINFO_EXTENSION));
            $this->mediaService->saveFile(
                $content,
                $extension,
                $this->getContentTypeFromExtension($extension),
                'symbio_media_id_' . $mediaPlate->getMediaId() . '_' . $mediaId,
                $context,
                null,
                $mediaId
            );
        } catch (DuplicatedMediaFileNameException $duplicatedException) {
            $this->logger->error(
                sprintf('IntoSymbioConnector - media file already exists, filename: %s',
                    $duplicatedException->getFile())
            );
            $this->mediaRepository->delete([
                [
                    'id' => $mediaId,
                ],
            ], $context);
        }
    }

    private function mediaExists(MediaPlate $mediaPlate, Context $context): bool
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('symbioId', $mediaPlate->getMediaId()));
        $media = $this->mediaAddonsRepository->search($criteria, $context)->first();

        return ($media instanceof MediaAddonsEntity);
    }

    private function createMedia(MediaPlate $mediaPlate, Context $context): string
    {
        $mediaId = Uuid::randomHex();

        $this->mediaRepository->create([
            [
                'id' => $mediaId,
                'mediaFolderId' => $this->intoConfigService->getString('productMediaFolder'),
                'translations' => [
                    Defaults::LANGUAGE_SYSTEM => [
                        'title' => $mediaPlate->getTitle(),
                    ],
                ],
                'intoSymbioConnectorAddons' => [
                    'symbioId' => $mediaPlate->getMediaId(),
                    'hash' => $mediaPlate->getChecksum(),
                ],
            ],
        ], $context);

        return $mediaId;
    }

    private function getContentTypeFromExtension(?string $ext): string
    {
        if ($ext === null) {
            return 'image/jpg';
        }

        return match (strtolower($ext)) {
            'png' => 'image/png',
            'svg' => 'image/svg+xml',
            'pdf' => 'application/pdf',
            'mpeg', 'mpe', 'mpg' => 'video/mpeg',
            'ogg', 'ogv' => 'video/ogg',
            'zip' => 'application/zip',
            default => 'image/jpg',
        };
    }
}
