<?php declare(strict_types=1);

namespace IntoSymbioConnector\Service\Import;

use IntoSymbioConnector\DataAbstractionLayer\Entity\OrderDeliveryPositionAddonsCollection;
use IntoSymbioConnector\DataAbstractionLayer\Entity\OrderDeliveryPositionAddonsEntity;
use IntoSymbioConnector\Exception\DeliveryImportException;
use IntoSymbioConnector\Service\Common\StockConverterServiceInterface;
use IntoSymbioConnector\Service\Sync\SyncLockService;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Order\Aggregate\OrderDelivery\OrderDeliveryEntity;
use Shopware\Core\Checkout\Order\Aggregate\OrderDelivery\OrderDeliveryStates;
use Shopware\Core\Checkout\Order\Aggregate\OrderDeliveryPosition\OrderDeliveryPositionEntity;
use Shopware\Core\Checkout\Order\Aggregate\OrderLineItem\OrderLineItemEntity;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Checkout\Order\OrderStates;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\StateMachine\Aggregation\StateMachineTransition\StateMachineTransitionActions;
use Shopware\Core\System\StateMachine\Aggregation\StateMachineTransition\StateMachineTransitionEntity;
use Shopware\Core\System\StateMachine\StateMachineEntity;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Shopware\Core\System\StateMachine\Event\StateMachineTransitionEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;


class DeliveryImportService
{
    private FilesystemInterface $filesystem;

    private LoggerInterface $logger;

    private EntityRepositoryInterface $orderRepository;

    private EntityRepositoryInterface $stateMachineRepository;

    private StockConverterServiceInterface $stockConverterService;
    private EventDispatcherInterface $eventDispatcher;
    private EntityRepositoryInterface $stateMachineStateRepository;
    private SyncLockService $syncLock;



    public function __construct(
        FilesystemInterface             $filesystem,
        LoggerInterface                 $logger,
        EntityRepositoryInterface       $orderRepository,
        EntityRepositoryInterface       $stateMachineRepository,
        StockConverterServiceInterface  $stockConverterService,
        EventDispatcherInterface        $eventDispatcher,
        EntityRepositoryInterface       $stateMachineStateRepository,
        SyncLockService                 $syncLock,
    )
    {
        $this->filesystem = $filesystem;
        $this->logger = $logger;
        $this->orderRepository = $orderRepository;
        $this->stateMachineRepository = $stateMachineRepository;
        $this->stockConverterService = $stockConverterService;
        $this->eventDispatcher = $eventDispatcher;
        $this->stateMachineStateRepository = $stateMachineStateRepository;
        $this->syncLock = $syncLock;

    }

    /**
     * @param Context $context
     * @return int
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    public function run(Context $context): int
    {
        if ($this->syncLock->isSyncLocked()) {
            $this->logger->warning('IntoSymbioConnector: DeliveryImportService is currently locked.');
            return 0;
        }

        $csvHeaders = [
            'erp_auftragsnummer',
            'erp_position',
            'order_number',
            'position',
            'quantity',
            'carrier',
            'handling_unit',
            'tracking_code',
        ];
        $csvDecoder = new CsvEncoder(
            [
                'csv_delimiter' => ';',
            ]
        );
        $headerLine = implode(';', $csvHeaders) . PHP_EOL;
        $importFiles = $this->filesystem->listContents('/delivery');

        $returnCode = true;
        foreach ($importFiles as $importFile) {
            if ($importFile['type'] !== 'file' || $importFile['extension'] !== 'csv') {
                continue;
            }
            $content = $headerLine . $this->filesystem->read($importFile['path']);
            $this->filesystem->rename($importFile['path'], 'archive/' . $importFile['path']);
            $data = $csvDecoder->decode($content, 'csv');
            if (is_array($data)) {
                $returnCode = $returnCode && $this->processDelivery($data, $context);
            }
        }

        if ($returnCode === false) {
            return 4;
        }

        return 0;
    }

    /**
     * @param array $data
     * @param Context $context
     * @return bool
     */
    private function processDelivery(array $data, Context $context): bool
    {
        foreach ($data as $item) {
            try {
                $this->processDeliveryItem($item, $context);
            } catch (DeliveryImportException $deliveryImportException) {
                $this->logger->error($deliveryImportException->getMessage());

                return false;
            }
        }

        return true;
    }

    /**
     * @throws DeliveryImportException
     */
    private function processDeliveryItem(array $item, Context $context): void
    {
        if ($item['quantity'] <= 0) {
            return;
        }
        $orderEntity = $this->getOrderEntity($item['order_number'], $context);
        if (!$orderEntity instanceof OrderEntity) {
            throw new DeliveryImportException(
                'IntoSymbioConnector DeliverImport - Order not found, orderNumber: ' . $item['order_number']
            );
        }

        $this->amendOrder($orderEntity, $item, $context);
    }

    private function getOrderEntity(string $orderNumber, Context $context): ?OrderEntity
    {
        $criteria = new Criteria();
        $criteria->addAssociation('lineItems');
        $criteria->addAssociation('lineItems.orderDeliveryPositions');
        $criteria->addAssociation('lineItems.orderDeliveryPositions.intoSymbioConnectorAddons');
        $criteria->addAssociation('lineItems.product');
        $criteria->addAssociation('deliveries');
        $criteria->addFilter(new EqualsFilter('orderNumber', $orderNumber));

        return $this->orderRepository->search($criteria, $context)->first();
    }

    /**
     * @throws DeliveryImportException
     */
    private function amendOrder(OrderEntity $orderEntity, array $item, Context $context): void
    {
        $trackingCodes = $this->getOrderTrackingCodes($orderEntity);
        $trackingCodes[] = $item['handling_unit'];

        $orderLineItemEntity = $this->getMatchingLineItem($orderEntity, intval($item['position']));
        if (!$orderLineItemEntity instanceof OrderLineItemEntity) {
            throw new DeliveryImportException(
                'IntoSymbioConnector DeliverImport - Order position not found, orderNumber: ' .
                $orderEntity->getOrderNumber() . ' / position: ' . $item['position']
            );
        }

        $productEntity = $orderLineItemEntity->getProduct();
        if (!$productEntity instanceof ProductEntity) {
            throw new DeliveryImportException(
                'IntoSymbioConnector DeliverImport - Order line item product not found, orderNumber: ' .
                $orderEntity->getOrderNumber() . ' / position: ' . $item['position']
            );
        }

        $quantity = $this->stockConverterService->convertStock2Shopware(
            $productEntity,
            intval($item['quantity']),
            $context
        );

        $stateId = $this->getOrderDeliveryStateId(OrderDeliveryStates::STATE_PARTIALLY_SHIPPED, $context);
        $allItemsShipped = ($this->getPastItemsDelivered($orderEntity) + $quantity) >= $this->getTotalQuantity($orderEntity);

        if ($allItemsShipped) {
            $stateId = $this->getOrderDeliveryStateId(OrderDeliveryStates::STATE_SHIPPED, $context);
            $orderStateId = $this->getOrderStateId(OrderStates::STATE_COMPLETED, $context);
        } else {
            $stateId = $this->getOrderDeliveryStateId(OrderDeliveryStates::STATE_PARTIALLY_SHIPPED, $context);
            $orderStateId = $this->getOrderStateId(OrderStates::STATE_IN_PROGRESS, $context);
        }

        $data = [
            'id' => $orderEntity->getId(),
            'stateId' => $orderStateId,
            'customFields' => [
                'migration_Shopware6_order_attribute6' => $item['carrier'],
            ],
            'deliveries' => [
                [
                    'stateId' => $stateId,
                    'trackingCodes' => $trackingCodes,
                    'positions' => [
                        [
                            'intoSymbioConnectorAddons' => [
                                [
                                    'quantity' => $quantity,
                                    'carrier' => $item['carrier'],
                                    'handlingUnit' => $item['handling_unit'],
                                    'trackingCode' => $item['tracking_code'],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $orderDeliveryEntity = $orderEntity->getDeliveries()?->first();
        if ($orderDeliveryEntity instanceof OrderDeliveryEntity) {
            $data['deliveries'][0]['id'] = $orderDeliveryEntity->getId();


        }
        $orderDeliveryPositionEntity = $orderLineItemEntity->getOrderDeliveryPositions()?->first();
        if ($orderDeliveryPositionEntity instanceof OrderDeliveryPositionEntity) {
            $data['deliveries'][0]['positions'][0]['id'] = $orderDeliveryPositionEntity->getId();
        }

        $this->orderRepository->update([$data], $context);

        // 12.06.2023:
        $orderDeliveryEntity = $orderEntity->getDeliveries()?->first();
        $fromStateId = $orderDeliveryEntity->getStateId();
        $fromStateTechnicalName = OrderDeliveryStates::STATE_PARTIALLY_SHIPPED;
        $toStateTechnicalName = OrderDeliveryStates::STATE_SHIPPED;
        if (($this->getPastItemsDelivered($orderEntity) + $quantity) >= $this->getTotalQuantity($orderEntity)) {
            $toStateTechnicalName = OrderDeliveryStates::STATE_SHIPPED;
        }
        $this->dispatchStateChangeEvent($orderEntity, $fromStateId, $stateId, $context);
    }

    private function getOrderDeliveryStateId(string $technicalName, Context $context): ?string
    {
        $stateMachineEntity = $this->getStateMachineEntityOrderDelivery($context);
        foreach ($stateMachineEntity->getStates() ?? [] as $stateMachineStateEntity) {
            if ($stateMachineStateEntity->getTechnicalName() === $technicalName) {
                return $stateMachineStateEntity->getId();
            }
        }

        return null;
    }

    private function getStateMachineEntityOrderDelivery(Context $context): StateMachineEntity
    {
        $criteria = new Criteria();
        $criteria->addAssociation('states');
        $criteria->addFilter(new EqualsFilter('technicalName', OrderDeliveryStates::STATE_MACHINE));

        return $this->stateMachineRepository->search($criteria, $context)->first();
    }

    private function getPastItemsDelivered(OrderEntity $orderEntity): int
    {
        $delivered = 0;
        foreach ($orderEntity->getLineItems() ?? [] as $lineItem) {
            if ($lineItem->getType() !== LineItem::PRODUCT_LINE_ITEM_TYPE) {
                continue;
            }

            $firstPosition = $lineItem->getOrderDeliveryPositions()?->first();
            $addonsExtension = $firstPosition?->getExtension('intoSymbioConnectorAddons');
            if (!$addonsExtension instanceof OrderDeliveryPositionAddonsCollection) {
                continue;
            }
            /** @var OrderDeliveryPositionAddonsEntity $addons */
            foreach ($addonsExtension->getElements() as $addons) {
                $delivered += $addons->getQuantity();
            }
        }

        return $delivered;
    }

    private function getTotalQuantity(OrderEntity $orderEntity): int
    {
        $total = 0;
        foreach ($orderEntity->getLineItems() ?? [] as $lineItem) {
            if ($lineItem->getType() === LineItem::PRODUCT_LINE_ITEM_TYPE) {
                $total += $lineItem->getQuantity();
            }
        }

        return $total;
    }

    private function getOrderTrackingCodes(OrderEntity $orderEntity): array
    {
        $orderDeliveryEntity = $orderEntity->getDeliveries()?->first();
        if (!$orderDeliveryEntity instanceof OrderDeliveryEntity) {
            return [];
        }

        return $orderDeliveryEntity->getTrackingCodes();
    }

    private function getMatchingLineItem(OrderEntity $orderEntity, int $position): ?OrderLineItemEntity
    {
        foreach ($orderEntity->getLineItems() ?? [] as $lineItem) {
            if ($lineItem->getPosition() === $position) {
                return $lineItem;
            }
        }

        return null;
    }

    private function dispatchStateChangeEvent(OrderEntity $orderEntity, string $fromStateId, string $toStateId, Context $context): void
    {
        if (!$fromStateId || !$toStateId) {
            $this->logger->error('State not found', [
                'orderEntity' => $orderEntity,
                'fromStateId' => $fromStateId,
                'toStateId' => $toStateId,
                'context' => $context,
            ]);
            return;
        }

        $fromState = $this->stateMachineStateRepository->search(new Criteria([$fromStateId]), $context)->first();
        $toState = $this->stateMachineStateRepository->search(new Criteria([$toStateId]), $context)->first();


        if (!$fromState || !$toState) {
            $this->logger->info('Before dispatching StateMachineTransitionEvent', [
                'orderEntity' => $orderEntity,
                'fromStateId' => $fromStateId,
                'toStateId' => $toStateId,
                'context' => $context,
            ]);
            return;
        }
//        echo "FromStateId: $fromStateId \n";
//        echo "ToStateId: $toStateId \n";

        $transition = new StateMachineTransitionEntity();
        $transition->setFromStateId($fromStateId);
        $transition->setToStateId($toStateId);
        $transition->setActionName(StateMachineTransitionActions::ACTION_SHIP);


        $event = new StateMachineTransitionEvent('order_delivery', $this->getOrderDeliveryId($orderEntity), $fromState, $toState, $context);
        error_log('Event dispatched: ' . 'state_enter.order_delivery.state.shipped');
        $this->eventDispatcher->dispatch($event, 'state_enter.order_delivery.state.shipped');
        $this->logger->info('After dispatching StateMachineTransitionEvent');
    }

    private function getOrderDeliveryId(OrderEntity $orderEntity): ?string
    {
        $orderDeliveryEntity = $orderEntity->getDeliveries()?->first();
        if ($orderDeliveryEntity instanceof OrderDeliveryEntity) {
            return $orderDeliveryEntity->getId();
        }

        return null;
    }

    private function getOrderStateId(string $technicalName, Context $context): ?string
    {
        $stateMachineEntity = $this->getStateMachineEntityOrder($context);
        foreach ($stateMachineEntity->getStates() ?? [] as $stateMachineStateEntity) {
            if ($stateMachineStateEntity->getTechnicalName() === $technicalName) {
                return $stateMachineStateEntity->getId();
            }
        }
        return null;
    }

    private function getStateMachineEntityOrder(Context $context): StateMachineEntity
    {
        $criteria = new Criteria();
        $criteria->addAssociation('states');
        $criteria->addFilter(new EqualsFilter('technicalName', OrderStates::STATE_MACHINE));
        return $this->stateMachineRepository->search($criteria, $context)->first();
    }
}
