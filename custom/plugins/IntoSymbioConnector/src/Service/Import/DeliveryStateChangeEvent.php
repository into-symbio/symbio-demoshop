<?php declare(strict_types=1);

namespace IntoSymbioConnector\Service\Import;

use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Framework\Context;
use Symfony\Contracts\EventDispatcher\Event;

class DeliveryStateChangeEvent extends Event
{
    public const EVENT_NAME = 'into_symbio_connector.delivery.state_changed';

    private OrderEntity $orderEntity;
    private string $fromStateId;
    private string $toStateId;
    private Context $context;

    public function __construct(OrderEntity $orderEntity, string $fromStateId, string $toStateId, Context $context)
    {
        $this->orderEntity = $orderEntity;
        $this->fromStateId = $fromStateId;
        $this->toStateId = $toStateId;
        $this->context = $context;
    }

    public function getName(): string
    {
        return self::EVENT_NAME;
    }

    public function getOrderEntity(): OrderEntity
    {
        return $this->orderEntity;
    }

    public function getFromStateId(): string
    {
        return $this->fromStateId;
    }

    public function getToStateId(): string
    {
        return $this->toStateId;
    }

    public function getContext(): Context
    {
        return $this->context;
    }
}