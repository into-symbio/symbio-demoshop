<?php declare(strict_types=1);


namespace IntoSymbioConnector\Service\Hydrator;


use IntoSymbioConnector\DataAbstractionLayer\Entity\CategoryAddonsDefinition;
use IntoSymbioConnector\DataAbstractionLayer\Entity\CategoryAddonsEntity;
use IntoSymbioConnector\DataAbstractionLayer\Entity\MediaAddonsDefinition;
use IntoSymbioConnector\DataAbstractionLayer\Entity\MediaAddonsEntity;
use IntoSymbioConnector\DataAbstractionLayer\Entity\ProductAddonsEntity;
use IntoSymbioConnector\Exception\InvalidDataException;
use IntoSymbioConnector\Service\Common\IntoConfigService;
use IntoSymbioConnector\Struct\ArticlePlate;
use IntoSymbioConnector\Struct\PriceStruct;
use IntoSymbioConnector\Struct\ProductPlate;
use Shopware\Core\Content\Product\Aggregate\ProductVisibility\ProductVisibilityDefinition;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Property\Aggregate\PropertyGroupOption\PropertyGroupOptionDefinition;
use Shopware\Core\Content\Property\PropertyGroupDefinition;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\DefinitionInstanceRegistry;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\Currency\CurrencyDefinition;
use Shopware\Core\System\Tax\TaxDefinition;
use Shopware\Core\System\Tax\TaxEntity;

class ProductHydrator
{
    private IntoConfigService $intoConfigService;

    private DefinitionInstanceRegistry $definitionInstanceRegistry;

    public function __construct(
        IntoConfigService $intoConfigService,
        DefinitionInstanceRegistry $definitionInstanceRegistry
    ) {
        $this->intoConfigService = $intoConfigService;
        $this->definitionInstanceRegistry = $definitionInstanceRegistry;
    }

    public function hydrateSingleProduct(ProductPlate $productPlate, Context $context): array
    {
        $articlePlates = $productPlate->getArticles();
        $articlePlate = reset($articlePlates);
        if ($articlePlate === false) {
            throw new InvalidDataException(sprintf('IntoSymbioConnector - ProductHydrator, symbio product id: %s',
                $productPlate->getProductId()));
        }
        $data = $this->hydrateArticleBasics($articlePlate, $context);
        $data = array_merge($data, [
            'active' => false,
            'isCloseout' => true,
            'stock' => 0,
            'productNumber' => (string)$productPlate->getProductId(),
            'tax' => ['id' => $this->intoConfigService->getString('defaultTax'),],
            'visibilities' => [
                [
                    'salesChannelId' => $this->intoConfigService->getString('salesChannel'),
                    'visibility' => ProductVisibilityDefinition::VISIBILITY_ALL,
                ],
            ],
            'intoSymbioConnectorAddons' => [
                'symbioProductId' => $productPlate->getProductId(),
                'symbioArticleId' => $articlePlate->getArticleId(),
                'symbioArticleUid' => $articlePlate->getUid(),
                'symbioArticleSku' => $articlePlate->getSku(),
                'symbioCategoryId' => $productPlate->getCategoryId(),
                'symbioDistributorSku' => $this->getDistributorSku($articlePlate),
                'symbioArticleSalesId' => $this->getArticleSalesId($articlePlate),
                'hash' => $productPlate->getChecksum(),
                'videos' => $productPlate->getVideos(),
            ],
        ]);

        $categoryIds = $this->getCategoryIdsFromAddons([$productPlate->getCategoryId()], $context);
        if (count($categoryIds) > 0) {
            $data['categories'] = $categoryIds;
        }

        $productMediaIds = $this->getMediaIdsFromAddons($productPlate->getMediaIds(), $context);
        $productMediaIds = array_merge(
            $productMediaIds,
            $this->getMediaIdsFromAddons($articlePlate->getMediaIds(), $context)
        );
        if (count($productMediaIds) > 0) {
            $data['cover'] = array_shift($productMediaIds);
            $data['media'] = $productMediaIds;
        }

        return $data;
    }

    private function hydrateArticleBasics(ArticlePlate $articlePlate, Context $context): array
    {
        $data = [
            'ean' => $articlePlate->getGtin(),
            'translations' => [
                $context->getLanguageId() => [
                    'name' => $articlePlate->getName(),
                    'description' => $articlePlate->getDescription(),
                ],
            ],
            'price' => $this->hydratePrices($articlePlate, $context),
        ];

        $data = array_merge($data, $this->hydrateProperties($articlePlate, $context));

        $data['translations'][$context->getLanguageId()] = array_merge(
            $data['translations'][$context->getLanguageId()],
            $this->getPackunit($articlePlate)
        );

        return array_merge($data, $this->hydratePriceBasics($articlePlate));
    }

    private function hydratePrices(ArticlePlate $articlePlate, Context $context): array
    {
        $prices = [];

        foreach ($articlePlate->getPriceStructs() as $priceStruct) {
            $currencyId = $this->getCurrencyIdByIso($priceStruct->getCurrency(), $context);

            $net = $priceStruct->getPrice();
            if ($net === null) {
                throw new InvalidDataException(sprintf('Article with no price given. Symbio article id: %s',
                    $articlePlate->getArticleId()));
            }
            $data = [
                'currencyId' => $currencyId,
                'linked' => true,
                'net' => $net,
                'gross' => $this->calculateGrossPrice($net, $context),
            ];

            $prices[$currencyId] = $data;
        }

        return $prices;
    }

    private function getCurrencyIdByIso(?string $iso, Context $context): string
    {
        if ($iso === null) {
            throw new InvalidDataException('No currency provided.');
        }

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('isoCode', $iso));
        $id = $this->getRepository(CurrencyDefinition::ENTITY_NAME)
            ->searchIds($criteria, $context)
            ->firstId();
        if ($id === null) {
            throw new InvalidDataException('Invalid currency supplied: ' . $iso . ' is not defined.');
        }

        return $id;
    }

    private function getRepository(string $entity): EntityRepositoryInterface
    {
        return $this->definitionInstanceRegistry->getRepository($entity);
    }

    private function calculateGrossPrice(float $net, Context $context): float
    {
        $defaultTax = $this->getDefaultTaxEntity($context);
        if (!$defaultTax instanceof TaxEntity) {
            throw new InvalidDataException('No default tax entity provided.');
        }

        return ((0.01 * $defaultTax->getTaxRate()) + 1.0) * $net;
    }

    private function getDefaultTaxEntity(Context $context): ?TaxEntity
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('id', $this->intoConfigService->getString('defaultTax')));

        return $this->getRepository(TaxDefinition::ENTITY_NAME)->search($criteria, $context)->first();
    }

    private function hydrateProperties(ArticlePlate $articlePlate, Context $context): array
    {
        $properties = array_map(function ($attributeStruct) use ($context) {
            $groupId = $this->getPropertyGroupIdByKey($attributeStruct->getKey(), $context);
            $optionId = $this->getPropertyGroupOptionIdByValue($groupId,
                $attributeStruct->getValue(),
                $attributeStruct->getUnit(),
                $context);

            return ['id' => $optionId];
        }, $articlePlate->getAttributeStructs());

        if (count($properties) > 0) {
            return ['properties' => $properties];
        }

        return [];
    }

    private function getPropertyGroupIdByKey(?string $key, Context $context): string
    {
        if ($key === null) {
            throw new InvalidDataException(('Missing property group key.'));
        }

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('translations.name', $key));
        $criteria->setLimit(1);
        $propertyId = $this->getRepository(PropertyGroupDefinition::ENTITY_NAME)
            ->searchIds($criteria, $context)
            ->firstId();
        if (is_string($propertyId)) {
            return $propertyId;
        }

        return $this->createPropertyGroup($key, $context);
    }

    private function createPropertyGroup(string $key, Context $context): string
    {
        $id = Uuid::randomHex();
        $this->getRepository(PropertyGroupDefinition::ENTITY_NAME)
            ->create([
                [
                    'id' => $id,
                    'name' => $key,
                    'filterable' => true,
                ],
            ], $context);

        return $id;
    }

    private function getPropertyGroupOptionIdByValue(
        string $propertyGroupId,
        ?string $value,
        ?string $unit,
        Context $context
    ): string {
        if ($value === null) {
            throw new InvalidDataException(('Missing property option value.'));
        }
        if (!empty($unit)) {
            $value .= $unit;
        }
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('groupId', $propertyGroupId));
        $criteria->addFilter(new EqualsFilter('translations.name', $value));
        $criteria->setLimit(1);
        $valueId = $this->getRepository(PropertyGroupOptionDefinition::ENTITY_NAME)
            ->searchIds($criteria, $context)
            ->firstId();
        if (is_string($valueId)) {
            return $valueId;
        }

        return $this->createPropertyGroupOption($propertyGroupId, $value, $context);
    }

    private function createPropertyGroupOption(string $groupId, string $value, Context $context): string
    {
        $id = Uuid::randomHex();
        $this->getRepository(PropertyGroupOptionDefinition::ENTITY_NAME)
            ->create([
                [
                    'id' => $id,
                    'groupId' => $groupId,
                    'name' => $value,
                ],
            ], $context);

        return $id;
    }

    private function getPackunit(ArticlePlate $articlePlate): array
    {
        $priceStructs = $articlePlate->getPriceStructs();
        $priceStruct = reset($priceStructs);
        if ($priceStruct === false) {
            throw new InvalidDataException(sprintf('No price struct given, symbio article id: %s',
                $articlePlate->getArticleId()));
        }
        $packUnit = $priceStruct->getArticleUnit();
        if (is_string($packUnit)) {
            return ['packUnit' => $packUnit];
        }

        return [];
    }

    private function hydratePriceBasics(ArticlePlate $articlePlate): array
    {
        $data = [];
        if (is_int($articlePlate->getMinimumArticleQuantity())) {
            $data['minPurchase'] = $articlePlate->getMinimumArticleQuantity();
        }
        if (is_int($articlePlate->getArticleQuantityScale())) {
            $data['purchaseSteps'] = $articlePlate->getArticleQuantityScale();
        }

        return array_merge($data, $this->getPurchaseData($articlePlate));
    }

    private function getPurchaseData(ArticlePlate $articlePlate): array
    {
        $data = [];
        $priceStructs = $articlePlate->getPriceStructs();
        $priceStruct = reset($priceStructs);
        if ($priceStruct === false) {
            throw new InvalidDataException(sprintf('No price struct given, symbio article id: %s',
                $articlePlate->getArticleId()));
        }
        $purchaseUnit = $priceStruct->getArticleQuantity();
        if (is_int($purchaseUnit)) {
            $data['purchaseUnit'] = $purchaseUnit;
        }
        $data['referenceUnit'] = 1.0;

        return $data;
    }

    private function getCategoryIdsFromAddons(array $symbioIds, Context $context): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('symbioId', $symbioIds));
        /** @var CategoryAddonsEntity[] $elements */
        $elements = $this->getRepository(CategoryAddonsDefinition::ENTITY_NAME)
            ->search($criteria, $context)
            ->getElements();
        $data = [];
        foreach ($elements as $element) {
            $data[] = ['id' => $element->getCategoryId()];
        }

        return $data;
    }

    private function getMediaIdsFromAddons(array $symbioIds, Context $context): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('symbioId', $symbioIds));
        /** @var MediaAddonsEntity[] $elements */
        $elements = $this->getRepository(MediaAddonsDefinition::ENTITY_NAME)
            ->search($criteria, $context)
            ->getElements();
        $data = [];
        foreach ($elements as $element) {
            $data[] = ['mediaId' => $element->getMediaId()];
        }

        return $data;
    }

    public function hydrateSingleProductUpdate(ProductPlate $productPlate, Context $context): array
    {
        $articlePlate = $this->getFirstArticlePlate($productPlate);
        $data = $this->hydrateArticleBasics($articlePlate, $context);

        $data['intoSymbioConnectorAddons'] = [
            'id' => $productPlate->getAddonsId(),
            'symbioCategoryId' => $productPlate->getCategoryId(),
            'symbioDistributorSku' => $this->getDistributorSku($articlePlate),
            'symbioArticleSalesId' => $this->getArticleSalesId($articlePlate),
            'hash' => $productPlate->getChecksum(),
            'videos' => $productPlate->getVideos(),
        ];

        $categoryIds = $this->getCategoryIdsFromAddons([$productPlate->getCategoryId()], $context);
        if (count($categoryIds) > 0) {
            $data['categories'] = $categoryIds;
        }

        $productMediaIds = $this->getMediaIdsFromAddons($productPlate->getMediaIds(), $context);
        $productMediaIds = array_merge(
            $productMediaIds,
            $this->getMediaIdsFromAddons($articlePlate->getMediaIds(), $context)
        );
        if (count($productMediaIds) > 0) {
            $data['cover'] = array_shift($productMediaIds);
            $data['media'] = $productMediaIds;
        }

        return $data;
    }

    public function hydrateVariantProduct(ProductPlate $productPlate, Context $context): array
    {
        $data = $this->hydrateArticleBasics($this->getFirstArticlePlate($productPlate), $context);

        $data = array_merge($data, [
            'active' => false,
            'isCloseout' => true,
            'stock' => 0,
            'productNumber' => (string)$productPlate->getProductId(),
            'tax' => ['id' => $this->intoConfigService->getString('defaultTax'),],
            'translations' => [
                $context->getLanguageId() => [
                    'name' => $productPlate->getName(),
                    'description' => '',
                ],
            ],
            'visibilities' => [
                [
                    'salesChannelId' => $this->intoConfigService->getString('salesChannel'),
                    'visibility' => ProductVisibilityDefinition::VISIBILITY_ALL,
                ],
            ],
            'configuratorGroupConfig' => $this->getConfiguratorGroupConfig($productPlate, $context),
            'configuratorSettings' => $this->getConfiguratorSetIdsForProduct($productPlate, $context),
            'intoSymbioConnectorAddons' => [
                'symbioProductId' => $productPlate->getProductId(),
                'symbioCategoryId' => $productPlate->getCategoryId(),
                'hash' => $productPlate->getChecksum(),
                'videos' => $productPlate->getVideos(),
            ],
        ]);

        $categoryIds = $this->getCategoryIdsFromAddons([$productPlate->getCategoryId()], $context);
        if (count($categoryIds) > 0) {
            $data['categories'] = $categoryIds;
        }

        $productMediaIds = $this->getMediaIdsFromAddons($productPlate->getMediaIds(), $context);
        if (count($productMediaIds) > 0) {
            $data['cover'] = array_shift($productMediaIds);
            $data['media'] = $productMediaIds;
        }

        return $data;
    }

    private function getConfiguratorGroupConfig(ProductPlate $productPlate, Context $context): array
    {
        $data = [];
        $articlePlate = $this->getFirstArticlePlate($productPlate);
        foreach ($articlePlate->getAttributeStructs() as $attributeStruct) {
            if ($attributeStruct->getType() !== 'FEATURE') {
                continue;
            }
            $data[] = [
                'id' => $this->getPropertyGroupIdByKey($attributeStruct->getKey(), $context),
                'representation' => 'box',
                'expressionForListings' => false,
            ];
        }

        return $data;
    }

    private function getConfiguratorSetIdsForProduct(ProductPlate $productPlate, Context $context): array
    {
        $set = [];
        foreach ($productPlate->getArticles() as $articlePlate) {
            $set = array_merge($set, $this->getConfiguratorOptionIdsForArticle($articlePlate, $context, 'optionId'));
        }

        if (count($set) <= 0) {
            return [];
        }

        /** @phpstan-ignore-next-line */
        return array_values(array_combine(array_column($set, 'optionId'), $set));
    }

    public function getConfiguratorOptionIdsForArticle(
        ArticlePlate $articlePlate,
        Context $context,
        string $key = 'id'
    ): array {
        $ids = [];
        foreach ($articlePlate->getAttributeStructs() as $attributeStruct) {
            if ($attributeStruct->getType() !== 'FEATURE') {
                continue;
            }
            $groupId = $this->getPropertyGroupIdByKey($attributeStruct->getKey(), $context);
            $ids[] = [
                $key => $this->getPropertyGroupOptionIdByValue($groupId,
                    $attributeStruct->getValue(),
                    $attributeStruct->getUnit(),
                    $context),
            ];
        }

        return $ids;
    }

    public function hydrateVariantProductUpdate(ProductPlate $productPlate, Context $context): array
    {
        $data = $this->hydrateArticleBasics($this->getFirstArticlePlate($productPlate), $context);

        $data['translations'] = [
            $context->getLanguageId() => [
                'name' => $productPlate->getName(),
                'description' => '',
            ],
        ];

        $data = array_merge($data, [
            'intoSymbioConnectorAddons' => [
                'symbioCategoryId' => $productPlate->getCategoryId(),
                'hash' => $productPlate->getChecksum(),
                'videos' => $productPlate->getVideos(),
            ],
        ]);

        $categoryIds = $this->getCategoryIdsFromAddons([$productPlate->getCategoryId()], $context);
        if (count($categoryIds) > 0) {
            $data['categories'] = $categoryIds;
        }

        $productMediaIds = $this->getMediaIdsFromAddons($productPlate->getMediaIds(), $context);
        if (count($productMediaIds) > 0) {
            $data['cover'] = array_shift($productMediaIds);
            $data['media'] = $productMediaIds;
        }

        return $data;
    }

    public function hydrateVariantArticle(ArticlePlate $articlePlate, Context $context): array
    {
        $data = $this->hydrateArticleBasics($articlePlate, $context);

        $data = array_merge($data, [
            'stock' => 0,
            'productNumber' => $articlePlate->getProductId() . '.' . $articlePlate->getArticleId(),
            'options' => $this->getConfiguratorOptionIdsForArticle($articlePlate, $context),
            'intoSymbioConnectorAddons' => [
                'symbioArticleId' => $articlePlate->getArticleId(),
                'symbioArticleUid' => $articlePlate->getUid(),
                'symbioArticleSku' => $articlePlate->getSku(),
                'symbioDistributorSku' => $this->getDistributorSku($articlePlate),
                'symbioArticleSalesId' => $this->getArticleSalesId($articlePlate),
                'videos' => $articlePlate->getVideos(),
            ],
        ]);

        $productMediaIds = $this->getMediaIdsFromAddons($articlePlate->getMediaIds(), $context);
        if (count($productMediaIds) > 0) {
            $data['cover'] = array_shift($productMediaIds);
            $data['media'] = $productMediaIds;
        }

        return $data;
    }

    public function hydrateVariantArticleUpdate(ArticlePlate $articlePlate, Context $context): array
    {
        $data = $this->hydrateArticleBasics($articlePlate, $context);

        $data = array_merge($data, [
            'options' => $this->getConfiguratorOptionIdsForArticle($articlePlate, $context),
            'intoSymbioConnectorAddons' => [
                'symbioArticleId' => $articlePlate->getArticleId(),
                'symbioArticleUid' => $articlePlate->getUid(),
                'symbioArticleSku' => $articlePlate->getSku(),
                'symbioDistributorSku' => $this->getDistributorSku($articlePlate),
                'symbioArticleSalesId' => $this->getArticleSalesId($articlePlate),
                'videos' => $articlePlate->getVideos(),
            ],
        ]);

        $productMediaIds = $this->getMediaIdsFromAddons($articlePlate->getMediaIds(), $context);
        if (count($productMediaIds) > 0) {
            $data['cover'] = array_shift($productMediaIds);
            $data['media'] = $productMediaIds;
        }

        return $data;
    }

    public function fillVariantArticles(ProductPlate $productPlate, Context $context): void
    {
        $criteria = new Criteria();
        $criteria->addAssociation('intoSymbioConnectorAddons')
            ->addFilter(new EqualsFilter('parentId', $productPlate->getEntityId()))
            ->addFilter(
                new NotFilter(
                    MultiFilter::CONNECTION_AND,
                    [new EqualsFilter('intoSymbioConnectorAddons.symbioArticleId', null)],
                )
            );

        $elements = $this->getRepository(ProductDefinition::ENTITY_NAME)->search($criteria, $context)
            ->getElements();

        array_map(function ($element) use ($productPlate) {
            /** @var  ProductEntity $element */
            $articlePlate = new ArticlePlate();
            $articlePlate->setEntityId($element->getId());
            /** @var ProductAddonsEntity $addons */
            $addons = $element->getExtension('intoSymbioConnectorAddons');
            $articlePlate->setAddonsId($addons->getId());
            $productPlate->addArticle($articlePlate);
        }, $elements);
    }

    private function getArticleSalesId(ArticlePlate $articlePlate): ?int
    {
        $priceStruct = $articlePlate->getFirstPriceStruct();

        return ($priceStruct instanceof PriceStruct) ? $priceStruct->getArticleSalesId() : null;
    }

    private function getDistributorSku(ArticlePlate $articlePlate): ?string
    {
        $priceStruct = $articlePlate->getFirstPriceStruct();

        return ($priceStruct instanceof PriceStruct) ? $priceStruct->getDistributorSku() : '';
    }

    private function getFirstArticlePlate(ProductPlate $productPlate): ArticlePlate
    {
        $articlePates = $productPlate->getArticles();
        $articlePlate = reset($articlePates);
        if ($articlePlate === false) {
            throw new InvalidDataException(sprintf('Product without articles, symbio product id: %s',
                $productPlate->getProductId()));
        }

        return $articlePlate;
    }
}
