<?php declare(strict_types=1);


namespace IntoSymbioConnector\Service\Downstream;


use IntoSymbioConnector\Struct\DownstreamMessageStruct;

interface DownstreamServiceInterface
{
    public function handle(DownstreamMessageStruct $downstream): void;
}
