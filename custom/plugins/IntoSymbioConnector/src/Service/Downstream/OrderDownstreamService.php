<?php declare(strict_types=1);


namespace IntoSymbioConnector\Service\Downstream;

use IntoSymbioConnector\Api\Client\SymbioClientFactory;
use IntoSymbioConnector\Api\Exception\ApiOrderDownstreamException;
use IntoSymbioConnector\Api\Exception\ApiSymbioException;
use IntoSymbioConnector\Api\RequestUri;
use IntoSymbioConnector\DataAbstractionLayer\Entity\CustomerAddonsEntity;
use IntoSymbioConnector\DataAbstractionLayer\Entity\CustomerAddressAddonsEntity;
use IntoSymbioConnector\DataAbstractionLayer\Entity\OrderAddonsDefinition;
use IntoSymbioConnector\DataAbstractionLayer\Entity\OrderAddonsEntity;
use IntoSymbioConnector\DataAbstractionLayer\Entity\ProductAddonsEntity;
use IntoSymbioConnector\DataAbstractionLayer\Entity\PromotionDiscountAddonsEntity;
use IntoSymbioConnector\Exception\OrderDownstreamException;
use IntoSymbioConnector\Magnalister\MagnalisterOrderRepository;
use IntoSymbioConnector\Service\Common\IntoConfigService;
use IntoSymbioConnector\Service\Common\StockConverterServiceInterface;
use IntoSymbioConnector\Service\Sync\SyncLockService;
use IntoSymbioConnector\Struct\AddressPlate;
use IntoSymbioConnector\Struct\DownstreamMessageStruct;
use IntoSymbioConnector\Struct\OrderItemPlate;
use IntoSymbioConnector\Struct\OrderPlate;
use IntoSymbioConnector\Struct\OrderSummaryStruct;
use Psr\Log\LoggerInterface;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Order\Aggregate\OrderAddress\OrderAddressCollection;
use Shopware\Core\Checkout\Order\Aggregate\OrderAddress\OrderAddressEntity;
use Shopware\Core\Checkout\Order\Aggregate\OrderLineItem\OrderLineItemEntity;
use Shopware\Core\Checkout\Order\Aggregate\OrderTransaction\OrderTransactionEntity;
use Shopware\Core\Checkout\Order\OrderDefinition;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Checkout\Promotion\Aggregate\PromotionDiscount\PromotionDiscountDefinition;
use Shopware\Core\Checkout\Promotion\Aggregate\PromotionDiscount\PromotionDiscountEntity;
use Shopware\Core\Framework\Api\Context\SalesChannelApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\DefinitionInstanceRegistry;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Symfony\Component\HttpFoundation\Response;
use Shopware\Core\Framework\Uuid\Uuid;

class OrderDownstreamService implements DownstreamServiceInterface
{
    const ERP_CONDITION_KEY_SHIPPING = '88';

    private array $parameters;

    private IntoConfigService $intoConfigService;

    private SymbioClientFactory $symbioClientFactory;

    private DefinitionInstanceRegistry $definitionInstanceRegistry;

    private StockConverterServiceInterface $stockConverterService;

    private MagnalisterOrderRepository $magnalisterOrderRepository;

    private LoggerInterface $logger;
    private SyncLockService $syncLock;


    public function __construct(
        array                          $parameters,
        IntoConfigService              $intoConfigService,
        SymbioClientFactory            $symbioClientFactory,
        DefinitionInstanceRegistry     $definitionInstanceRegistry,
        StockConverterServiceInterface $stockConverterService,
        MagnalisterOrderRepository     $magnalisterOrderRepository,
        LoggerInterface                $logger,
        SyncLockService                $syncLock
    )
    {
        $this->parameters = array_replace_recursive(
            [
                'transaction_ids' => [
                    "swag_paypal_resource_id",
                ],
            ],
            $parameters
        );
        $this->intoConfigService = $intoConfigService;
        $this->symbioClientFactory = $symbioClientFactory;
        $this->definitionInstanceRegistry = $definitionInstanceRegistry;
        $this->stockConverterService = $stockConverterService;
        $this->magnalisterOrderRepository = $magnalisterOrderRepository;
        $this->logger = $logger;
        $this->syncLock = $syncLock;
    }

    public function handle(DownstreamMessageStruct $downstream): void
    {
        if ($this->syncLock->isSyncLocked()) {
            $this->logger->warning('IntoSymbioConnector: OrderDownstreamService is currently locked.');
            return;
        }

        $context = Context::createDefaultContext($this->getSalesChannelApiSource($downstream->getSalesChannelId()));

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('id', $downstream->getId()));
        $criteria->addAssociation('addresses.country');
        $criteria->addAssociation('currency');
        $criteria->addAssociation('lineItems.product');
        $criteria->addAssociation('orderCustomer.customer.group');
        $criteria->addAssociation('orderCustomer.customer.addresses');
        $criteria->addAssociation('stateMachineState');
        $criteria->addAssociation('transactions');
        $criteria->addAssociation('intoSymbioConnectorAddons');

        /** @var OrderEntity $orderEntity */
        $orderEntity = $this->getRepository(OrderDefinition::ENTITY_NAME)
            ->search($criteria, $context)
            ->first();

        if (!$orderEntity->hasExtension('intoSymbioConnectorAddons')) {
            $message = 'OrderDownstreamService - missing SymbioConnectorAddons, order number: ' .
                $orderEntity->getOrderNumber();
            $this->logger->error($message);
            throw new OrderDownstreamException($message);
        }

        /** @var OrderAddonsEntity $addons */
        $addons = $orderEntity->getExtension('intoSymbioConnectorAddons');

        if (!empty($addons->getSymbioOrderId())) {
            $message = 'OrderDownstreamService - duplicate order transfer candidate, skipped, order number: ' .
                $orderEntity->getOrderNumber() . ' - symbioOrderId: ' . $addons->getSymbioOrderId();
            $this->logger->error($message);
            throw new OrderDownstreamException($message);
        }

        $this->createDownstreamOrder($orderEntity, $context);
    }

    private function getSalesChannelApiSource(string $id): ?SalesChannelApiSource
    {
        if (strlen($id) === 0) {
            return null;
        }

        return new SalesChannelApiSource($id);
    }

    private function getRepository(string $entity): EntityRepositoryInterface
    {
        return $this->definitionInstanceRegistry->getRepository($entity);
    }

    private function createDownstreamOrder(OrderEntity $orderEntity, Context $context): void
    {
        $orderPlate = new OrderPlate();
        $this->composeOrder($orderPlate, $orderEntity, $context);
        $orderSummaryStruct = $this->createApiOrder($orderPlate);
        $this->validateCreatedApiOrder($orderPlate, $orderSummaryStruct);
        $this->linkApiOrder($orderPlate, $orderSummaryStruct, $context);
    }

    private function composeOrder(OrderPlate $orderPlate, OrderEntity $orderEntity, Context $context): void
    {
        $orderPlate->setEntityId($orderEntity->getId());

        /** @var OrderAddonsEntity $orderAddons */
        $orderAddons = $orderEntity->getExtension('intoSymbioConnectorAddons');
        $orderPlate->setAddonsId($orderAddons->getId());

        /** @var CustomerAddonsEntity $customerAddons */
        $customerAddons = $orderEntity->getOrderCustomer()?->getCustomer()?->getExtension('intoSymbioConnectorAddons');
        if (!$customerAddons instanceof CustomerAddonsEntity) {
            throw new OrderDownstreamException(sprintf('Missing customer addons entity, order id: %s',
                $orderEntity->getId()));
        }

        $symbioRecipientId = $customerAddons->getSymbioRecipientId();
        if (!is_int($symbioRecipientId)) {
            throw new OrderDownstreamException(sprintf('Missing Symbio recipient id, order id: %s',
                $orderEntity->getId()));
        }

        $orderPlate->setRecipientId($symbioRecipientId);

        $isoCode = $orderEntity->getCurrency()?->getIsoCode();
        if ($isoCode === null) {
            throw new OrderDownstreamException(sprintf('Missing currency, order id: %s', $orderEntity->getId()));
        }
        $orderPlate->setCurrency($isoCode);
        $orderPlate->setExternalShopReference($orderEntity->getOrderNumber());

        $externalPaymentReference = $this->getExternalPaymentReference($orderEntity);
        if (is_string($externalPaymentReference)) {
            $orderPlate->setExternalPaymentReference($externalPaymentReference);
        }

        $externalMarketplace = $this->magnalisterOrderRepository->getExternalMarketPlaceData($orderEntity->getOrderNumber());
        if (is_array($externalMarketplace)) {
            $orderPlate->setExternalMarketplaceReference($externalMarketplace['reference']);
        }

        if (is_string($orderEntity->getCustomerComment())) {
            $orderPlate->setRemark(substr($orderEntity->getCustomerComment(), 0, 512));
        }

        // set billing and shipping addresses

        if (!$orderEntity->getAddresses() instanceof OrderAddressCollection) {
            throw new OrderDownstreamException(sprintf('Missing order addresses, order id: %s', $orderEntity->getId()));
        }

        foreach ($orderEntity->getAddresses() as $orderAddressEntity) {
            if ($orderEntity->getBillingAddressId() === $orderAddressEntity->getId()) {
                $orderPlate->setBillingAddressId(
                    $this->getSymbioAddressIdFromOrderAddress(
                        AddressPlate::TYPE_MAIN,
                        $symbioRecipientId,
                        $orderAddressEntity,
                        $orderEntity
                    )
                );

                continue;
            }
            $orderPlate->setShippingAddressId(
                $this->getSymbioAddressIdFromOrderAddress(
                    AddressPlate::TYPE_OTHER,
                    $symbioRecipientId,
                    $orderAddressEntity,
                    $orderEntity
                )
            );
        }

        // default shipping address if none given - need to clone as Symbio would not accept a billing address for shipping

        if ($orderPlate->getShippingAddressId() === null) {
            $orderAddressEntity = $orderEntity->getAddresses()->first();
            $orderPlate->setShippingAddressId($this->createSymbioAddressFromOrderAddress(
                AddressPlate::TYPE_OTHER,
                $symbioRecipientId,
                $orderAddressEntity
            ));
        }

        //

        $orderPlate->setOrderOrigin($this->getOrderOrigin($orderEntity));

        foreach ($orderEntity->getLineItems() ?? [] as $lineItemEntity) {
            if ($lineItemEntity->getType() === 'product') {
                $orderPlate->addOrderItem($this->composeOrderItem($orderEntity, $lineItemEntity, $context));
                continue;
            }
            if ($lineItemEntity->getType() === 'promotion') {
                $this->addPromotionToOrder($orderPlate, $orderEntity, $lineItemEntity, $context);
            }
        }

        $this->addShippingCostsToOrder($orderPlate, $orderEntity);
    }

    private function composeAddress(
        OrderAddressEntity $orderAddressEntity,
        string             $type,
        int                $symbioRecipientId
    ): AddressPlate
    {
        $addressPlate = new AddressPlate();
        $addressPlate->setType($type);
        $addressPlate->setRecipientId($symbioRecipientId);

        $addressPlate->setAdditional(
            implode(
                ' / ',
                array_filter([
                    $orderAddressEntity->getAdditionalAddressLine1(),
                    $orderAddressEntity->getAdditionalAddressLine2(),
                ])
            )
        );
        $addressPlate->setCity($orderAddressEntity->getCity());
        $iso = $orderAddressEntity->getCountry()?->getIso();
        if ($iso === null) {
            throw new OrderDownstreamException(sprintf('Missing country, order address entity: %s',
                $orderAddressEntity->getId()));
        }
        $addressPlate->setCountry($iso);
        $addressPlate->setName(AddressHelper::composeOrderAddressName($orderAddressEntity));
        $addressPlate->setPhoneNumber($orderAddressEntity->getPhoneNumber() ?? '');
        $addressPlate->setZipCode($orderAddressEntity->getZipcode());

        $address = AddressHelper::guessStreetNumber($orderAddressEntity->getStreet());
        $addressPlate->setStreet($address['street'] ?? '');
        $addressPlate->setStreetNumber($address['number'] ?? '');

        return $addressPlate;
    }

    private function getOrderOrigin(OrderEntity $orderEntity): string
    {
        $customFields = $orderEntity->getOrderCustomer()?->getCustomer()?->getGroup()?->getCustomFields();
        if (is_array($customFields)) {
            return $customFields['migration_Shopware6_customer_group_sy_herkunft'] ?? '';
        }

        return '';
    }

    private function addShippingCostsToOrder(OrderPlate $orderPlate, OrderEntity $orderEntity): void
    {
        $orderPlate->addAdditionalOrderPayload(
            self::ERP_CONDITION_KEY_SHIPPING,
            $this->getNetPrice($orderEntity, $orderEntity->getShippingCosts())
        );
    }

    private function addPromotionToOrder(
        OrderPlate          $orderPlate,
        OrderEntity         $orderEntity,
        OrderLineItemEntity $lineItemEntity,
        Context             $context
    ): void
    {
        $payload = $lineItemEntity->getPayload();

        if (!is_array($payload) || $payload['discountScope'] === 'delivery') {
            return;
        }

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('id', $payload['discountId']));
        $criteria->addAssociation('intoSymbioConnectorAddons');
        $discountRepository = $this->definitionInstanceRegistry->getRepository(
            PromotionDiscountDefinition::ENTITY_NAME
        );
        /** @var PromotionDiscountEntity $discountEntity */
        $discountEntity = $discountRepository->search($criteria, $context)->first();

        $addons = $discountEntity->getExtension('intoSymbioConnectorAddons');
        if (!$addons instanceof PromotionDiscountAddonsEntity || empty($addons->getSymbioPromotionDiscountKey())) {
            return;
        }
        $netDiscount = $this->getNetPrice($orderEntity, $lineItemEntity->getPrice());
        $orderPlate->addAdditionalOrderPayload($addons->getSymbioPromotionDiscountKey(), -1.0 * $netDiscount);
    }

    private function getSymbioAddressIdFromOrderAddress(
        string             $type,
        int                $symbioRecipientId,
        OrderAddressEntity $orderAddressEntity,
        OrderEntity        $orderEntity
    ): int
    {
        $customerEntity = $orderEntity->getOrderCustomer()?->getCustomer();
        if ($customerEntity === null) {
            throw new OrderDownstreamException(sprintf('Missing customer entity, order id: %s', $orderEntity->getId()));
        }
        foreach ($customerEntity->getAddresses() ?? [] as $customerAddressEntity) {
            if (
                $this->getChecksum([
                    $orderAddressEntity->getCountryId(),
                    $orderAddressEntity->getZipcode(),
                    $orderAddressEntity->getCity(),
                    $orderAddressEntity->getStreet(),
                    $orderAddressEntity->getAdditionalAddressLine1(),
                    $orderAddressEntity->getAdditionalAddressLine2(),
                    $orderAddressEntity->getCompany(),
                    $orderAddressEntity->getFirstName(),
                    $orderAddressEntity->getLastName(),
                ]) ===
                $this->getChecksum([
                    $customerAddressEntity->getCountryId(),
                    $customerAddressEntity->getZipcode(),
                    $customerAddressEntity->getCity(),
                    $customerAddressEntity->getStreet(),
                    $customerAddressEntity->getAdditionalAddressLine1(),
                    $customerAddressEntity->getAdditionalAddressLine2(),
                    $customerAddressEntity->getCompany(),
                    $customerAddressEntity->getFirstName(),
                    $customerAddressEntity->getLastName(),
                ])
            ) {
                /** @var CustomerAddressAddonsEntity $addons */
                $addons = $customerAddressEntity->getExtension('intoSymbioConnectorAddons');
                if ($addons instanceof CustomerAddressAddonsEntity) {
                    if ($addons->getType() === $type || ($type === AddressPlate::TYPE_MAIN && $addons->getType() === AddressPlate::TYPE_BILLING)) {
                        return $addons->getSymbioAddressId();
                    }
                }
            }
        }

        return $this->createSymbioAddressFromOrderAddress($type, $symbioRecipientId, $orderAddressEntity);
    }

    private function createSymbioAddressFromOrderAddress(
        string             $type,
        int                $symbioRecipientId,
        OrderAddressEntity $orderAddressEntity
    ): int
    {
        // SymbioApi does not accept multiple MAIN addresses
        $type = $type === AddressPlate::TYPE_MAIN ? AddressPlate::TYPE_BILLING : $type;

        $orderAddressPlate = $this->composeAddress($orderAddressEntity, $type, $symbioRecipientId);

        return $this->createApiAddress($orderAddressPlate);
    }

    private function getChecksum(array $data): string
    {
        return md5(serialize($data));
    }

    private function composeOrderItem(
        OrderEntity         $orderEntity,
        OrderLineItemEntity $lineItemEntity,
        Context             $context
    ): OrderItemPlate
    {
        $orderItemPlate = new OrderItemPlate();

        $orderItemPlate->setOrderPosition($lineItemEntity->getPosition());

        $orderItemPlate->setOrderQuantity(
            $this->stockConverterService->convertStock2Symbio(
                $lineItemEntity->getProduct(),
                $lineItemEntity->getQuantity(),
                $context
            )
        );

        $orderItemPlate->setSalesPriceAbsolute(
            $this->stockConverterService->convertPrice2Symbio(
                $lineItemEntity->getProduct(),
                $this->getNetPrice($orderEntity, $lineItemEntity->getPrice()),
                $context
            )
        );

        /** @var ProductAddonsEntity $addons */
        $addons = $lineItemEntity->getProduct()?->getExtension('intoSymbioConnectorAddons');
        if (!$addons instanceof ProductAddonsEntity) {
            throw new OrderDownstreamException(sprintf('Missing product/product addons, line item id: %s',
                $lineItemEntity->getId()));
        }
        if ($addons->getSymbioArticleUid() === null) {
            throw new OrderDownstreamException(sprintf('Missing symbio article uid, line item id: %s',
                $lineItemEntity->getId()));
        }
        if ($addons->getSymbioArticleSalesId() === null) {
            throw new OrderDownstreamException(sprintf('Missing symbio article sales id, line item id: %s',
                $lineItemEntity->getId()));
        }
        $orderItemPlate->setUid($addons->getSymbioArticleUid());
        $orderItemPlate->setArticleSalesId($addons->getSymbioArticleSalesId());

        return $orderItemPlate;
    }

    private function getNetPrice(OrderEntity $orderEntity, ?CalculatedPrice $priceEntity): float
    {
        if ($priceEntity === null) {
            throw new OrderDownstreamException(sprintf('Missing price entity, order id: %s', $orderEntity->getId()));
        }
        if ($orderEntity->getPrice()->getTaxStatus() === 'gross') {
            $taxRule = $priceEntity->getTaxRules()->first();
            if ($taxRule === null) {
                throw new OrderDownstreamException(sprintf('Missing tax rule, order id: %s', $orderEntity->getId()));
            }
            $taxRate = 0.01 * (100.0 + $taxRule->getTaxRate());

            return $priceEntity->getUnitPrice() / $taxRate;
        }

        return $priceEntity->getUnitPrice();
    }

    private function createApiAddress(AddressPlate $orderAddressPlate): int
    {
        $credentials = $this->intoConfigService->getApiCredentials();
        $client = $this->symbioClientFactory->createSymbioClient($credentials);

        $uri = RequestUri::CREATE_ADDRESS;
        $body = $orderAddressPlate->jsonSerialize();

        $response = $client->sendPostRequest($uri, $body);
        if ($response->getStatusCode() !== Response::HTTP_CREATED) {
            $contents = $response->getBody()->getContents();
            $this->logger->error(
                'OrderDownstreamService - API error writing address',
                [
                    'status_code' => $response->getStatusCode(),
                    'body' => $body,
                ]
            );
            throw new ApiSymbioException([
                'error' => 'error submitting API address',
                'error_description' => 'request uri: ' . $uri .
                    ', request body: ' . \json_encode($body) .
                    ', response contents: ' . $contents,
            ], $response->getStatusCode());
        }

        $result = $client->decodeJsonResponse($response);

        if (!isset($result['id'])) {
            $contents = $response->getBody()->getContents();
            throw new ApiSymbioException([
                'error' => 'error submitting API address',
                'error_description' => 'request uri: ' . $uri .
                    ', request body: ' . \json_encode($body) .
                    ', response contents: ' . $contents,
            ], $response->getStatusCode());
        }

        return $result['id'];
    }

    private function createApiOrder(OrderPlate $orderPlate): OrderSummaryStruct
    {
        $credentials = $this->intoConfigService->getApiCredentials();
        $client = $this->symbioClientFactory->createSymbioClient($credentials);

        $uri = $this->createSubmitOrderUrl();
        $body = $orderPlate->jsonSerialize();

        $response = $client->sendPostRequest($uri, $body);

        if (!in_array($response->getStatusCode(), [
            Response::HTTP_OK,
            Response::HTTP_CREATED,
            Response::HTTP_CONFLICT,
        ])) {
            $contents = $response->getBody()->getContents();
            $this->logger->error(
                'OrderDownstreamService - API error, order number: ' . $orderPlate->getExternalShopReference(),
                [
                    'status_code' => $response->getStatusCode(),
                    'body' => $body,
                ]
            );
            throw new ApiSymbioException([
                'error' => 'error submitting API order, order number: ' . $orderPlate->getExternalShopReference(),
                'error_description' => 'order id: ' . $orderPlate->getEntityId() .
                    ', request uri: ' . $uri .
                    ', request body: ' . \json_encode($body) .
                    ', response contents: ' . $contents,
            ], $response->getStatusCode());
        }

//        if ($response->getStatusCode() === Response::HTTP_CONFLICT) {
//            $decodedResponse = $client->decodeJsonResponse($response);
//            $symbioOrderId = $decodedResponse['id'];
//            $symbioOrderStatus = $decodedResponse['id'];
//
//        }

        $summary = new OrderSummaryStruct();
        $summary->fromArray($client->decodeJsonResponse($response) ?? []);

        return $summary;
    }

    private function createSubmitOrderUrl(): string
    {
        return sprintf(
            RequestUri::SUBMIT_ORDER,
            $this->intoConfigService->getString('channelId'),
            $this->intoConfigService->getString('country'),
        );
    }

    private function validateCreatedApiOrder(OrderPlate $orderPlate, OrderSummaryStruct $orderSummaryStruct): void
    {
        if (count($orderSummaryStruct->getHeaderErrors() ?? []) > 0) {
            if (isset($orderSummaryStruct->getHeaderErrors()[409])) {
                $this->logger->warning('Order already exists in the system, order number: ' . $orderPlate->getExternalShopReference());
                return;
            }
                throw new ApiOrderDownstreamException([
                'error' => 'IntoSymbioConnector - Order Downstream error, order number: ' .
                    $orderPlate->getExternalShopReference(),
                'error_description' => 'Error Messages from API Headers: ' . json_encode(
                        $orderSummaryStruct->getHeaderErrors()
                    ) .
                    ' - Item error messages: ' . json_encode($orderSummaryStruct->getItemErrors()),
            ]);
        }
    }

    private function linkApiOrder(
        OrderPlate         $orderPlate,
        OrderSummaryStruct $orderSummaryStruct,
        Context            $context
    ): void
    {
        $data = [
            'id' => $orderPlate->getAddonsId(),
            'orderId' => $orderPlate->getEntityId(),
            'symbioOrderId' => $orderSummaryStruct->getId(),
            'symbioOrderStatus' => $orderSummaryStruct->getStatus(),
            'symbioOrderPlacement' => $orderPlate->jsonSerialize(),
            'symbioOrderSummary' => $orderSummaryStruct->jsonSerialize(),
            'symbioOrderTimestamp' => $orderSummaryStruct->getOrderDate(),
        ];
        $this->getRepository(OrderAddonsDefinition::ENTITY_NAME)->update([$data], $context);
    }

    private function getExternalPaymentReference(OrderEntity $orderEntity): ?string
    {
        $transactionEntity = $orderEntity->getTransactions()?->first();
        if ($transactionEntity instanceof OrderTransactionEntity) {
            $fields = $transactionEntity->getCustomFields();
            if (is_array($fields)) {
                foreach ($this->parameters['transaction_ids'] as $id) {
                    if (array_key_exists($id, $fields)) {
                        return $fields[$id];
                    }
                }
            }
        }

        return null;
    }
}
