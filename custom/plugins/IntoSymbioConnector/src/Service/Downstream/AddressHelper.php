<?php declare(strict_types=1);

namespace IntoSymbioConnector\Service\Downstream;

use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressEntity;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Checkout\Order\Aggregate\OrderAddress\OrderAddressEntity;

class AddressHelper
{
    public static function guessStreetNumber(string $address): array
    {
        $street = $address;
        $number = null;
        if (preg_match('/(.*)\s(\d{1,4}.{0,20})$/mi', $address, $matches) === 1) {
            $street = $matches[1];
            if (isset($matches[2])) {
                $number = $matches[2];
            }
        }

        return ['street' => $street, 'number' => $number];
    }

    public static function composeCustomerName(CustomerEntity $customerEntity): string
    {
        return self::composeName(
            $customerEntity->getCompany(),
            $customerEntity->getFirstName(),
            $customerEntity->getLastName()
        );
    }

    public static function composeCustomerAddressName(CustomerAddressEntity $customerAddressEntity): string
    {
        return self::composeName(
            $customerAddressEntity->getCompany(),
            $customerAddressEntity->getFirstName(),
            $customerAddressEntity->getLastName()
        );
    }

    public static function composeOrderAddressName(OrderAddressEntity $orderAddressEntity): string
    {
        return self::composeName(
            $orderAddressEntity->getCompany(),
            $orderAddressEntity->getFirstName(),
            $orderAddressEntity->getLastName()
        );
    }

    private static function composeName(?string $company, ?string $firstName, ?string $lastName): string
    {
        return implode(
            ' / ',
            array_filter([
                $company,
                implode(' ', [
                    $firstName,
                    $lastName,
                ]),
            ])
        );
    }
}
