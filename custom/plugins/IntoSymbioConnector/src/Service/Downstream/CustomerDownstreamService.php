<?php

declare(strict_types=1);


namespace IntoSymbioConnector\Service\Downstream;


use IntoSymbioConnector\Api\Client\SymbioClientFactory;
use IntoSymbioConnector\Api\Exception\ApiSymbioException;
use IntoSymbioConnector\Api\RequestUri;
use IntoSymbioConnector\DataAbstractionLayer\Entity\CustomerAddonsDefinition;
use IntoSymbioConnector\DataAbstractionLayer\Entity\CustomerAddonsEntity;
use IntoSymbioConnector\DataAbstractionLayer\Entity\CustomerAddressAddonsDefinition;
use IntoSymbioConnector\DataAbstractionLayer\Entity\CustomerAddressAddonsEntity;
use IntoSymbioConnector\Exception\CustomerDownstreamException;
use IntoSymbioConnector\Service\Common\IntoConfigService;
use IntoSymbioConnector\Struct\AddressPlate;
use IntoSymbioConnector\Struct\CustomerPlate;
use IntoSymbioConnector\Struct\DownstreamMessageStruct;
use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressCollection;
use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressEntity;
use Shopware\Core\Checkout\Customer\CustomerDefinition;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\Api\Context\SalesChannelApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\DefinitionInstanceRegistry;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\SalesChannelDefinition;
use Shopware\Core\System\SalesChannel\SalesChannelEntity;
use Symfony\Component\HttpFoundation\Response;

class CustomerDownstreamService implements DownstreamServiceInterface
{
    private IntoConfigService $intoConfigService;

    private SymbioClientFactory $symbioClientFactory;

    private DefinitionInstanceRegistry $definitionInstanceRegistry;

    public function __construct(
        IntoConfigService $intoConfigService,
        SymbioClientFactory $symbioClientFactory,
        DefinitionInstanceRegistry $definitionInstanceRegistry
    ) {
        $this->intoConfigService = $intoConfigService;
        $this->symbioClientFactory = $symbioClientFactory;
        $this->definitionInstanceRegistry = $definitionInstanceRegistry;
    }

    public function handle(DownstreamMessageStruct $downstream): void
    {
        $source = null;
        if ($this->isSalesChannelId($downstream->getSalesChannelId())) {
            $source = new SalesChannelApiSource($downstream->getSalesChannelId());
        }
        $context = Context::createDefaultContext($source);

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('id', $downstream->getId()))
            ->addAssociations(
                [
                    'addresses.country',
                    'defaultBillingAddress.country',
                    'intoSymbioConnectorAddons',
                    'intoSymbioConnectorAddons.mainAddress.country',
                ]
            );

        /** @var CustomerEntity $customerEntity */
        $customerEntity = $this->getRepository(CustomerDefinition::ENTITY_NAME)
            ->search($criteria, $context)
            ->first();

        if ($customerEntity->getExtension('intoSymbioConnectorAddons') === null) {
            $this->createDownstreamCustomer($customerEntity, $context);

            return;
        }

        $this->updateDownstreamCustomer($customerEntity, $context);
    }

    private function isSalesChannelId(string $id): bool
    {
        if (strlen($id) === 0) {
            return false;
        }

        return true;
    }

    private function getRepository(string $entity): EntityRepositoryInterface
    {
        return $this->definitionInstanceRegistry->getRepository($entity);
    }

    private function createDownstreamCustomer(CustomerEntity $customerEntity, Context $context): void
    {
        $customerPlate = new CustomerPlate();
        $this->composeCustomer($customerPlate, $customerEntity, $context);
        $created = $this->createApiCustomer($customerPlate);
        $this->linkApiCustomer($customerPlate, $created, $context);
    }

    private function composeCustomer(
        CustomerPlate $customerPlate,
        CustomerEntity $customerEntity,
        Context $context
    ): void {
        $customerPlate->setEntityId($customerEntity->getId());

        $customerPlate->setStatus(
            $customerEntity->getActive() ? CustomerPlate::ACTIVE_TRUE : CustomerPlate::ACTIVE_FALSE
        );
        $customerPlate->setName(AddressHelper::composeCustomerName($customerEntity));
        $customerPlate->setEmail($customerEntity->getEmail());
        if (is_array($customerEntity->getVatIds()) && is_string(current($customerEntity->getVatIds()))) {
            $customerPlate->setVatNumber(current($customerEntity->getVatIds()));
        }

        /** @var CustomerAddressEntity $mainCustomerAddress */
        $mainCustomerAddress = $customerEntity->getDefaultBillingAddress();
        if ($customerEntity->hasExtension('intoSymbioConnectorAddons')) {
            /** @var CustomerAddonsEntity $addons */
            $addons = $customerEntity->getExtension('intoSymbioConnectorAddons');
            if ($addons->getMainAddress() instanceof CustomerAddressEntity) {
                $mainCustomerAddress = $addons->getMainAddress();
            }
        }
        $mainAddress = new AddressPlate();
        $this->composeAddress($mainAddress, $mainCustomerAddress, $context);
        $mainAddress->setType(AddressPlate::TYPE_MAIN);
        $customerPlate->setMainAddress($mainAddress);

        $customerPlate->setBillingAddresses([]);
        $customerPlate->setShippingAddresses($this->getOtherAddresses($customerEntity, $mainCustomerAddress, $context));

        if (!$customerEntity->hasExtension('intoSymbioConnectorAddons')) {
            return;
        }
        /** @var CustomerAddonsEntity $addons */
        $addons = $customerEntity->getExtension('intoSymbioConnectorAddons');
        $customerPlate->setId($addons->getSymbioRecipientId());
        $customerPlate->setEntityId($addons->getCustomerId());
        $customerPlate->setAddonsId($addons->getId());
        $recipientIdentification = $addons->getSymbioRecipientIdentification();
        if (is_string($recipientIdentification)) {
            $customerPlate->setRecipientIdentification($recipientIdentification);
        }
    }

    private function composeAddress(
        AddressPlate $addressPlate,
        CustomerAddressEntity $customerAddressEntity,
        Context $context
    ): void {
        $addressPlate->setEntityId($customerAddressEntity->getId());

        $addressPlate->setAdditional(
            implode(
                ' / ',
                array_filter([
                    $customerAddressEntity->getAdditionalAddressLine1(),
                    $customerAddressEntity->getAdditionalAddressLine2(),
                ])
            )
        );
        $addressPlate->setCity($customerAddressEntity->getCity());
        $addressPlate->setCountry($this->getCountryIsoFromCustomerAddress($customerAddressEntity, $context));
        $addressPlate->setName(AddressHelper::composeCustomerAddressName($customerAddressEntity));
        $addressPlate->setPhoneNumber($customerAddressEntity->getPhoneNumber() ?? '');
        $addressPlate->setZipCode($customerAddressEntity->getZipcode());

        $address = AddressHelper::guessStreetNumber($customerAddressEntity->getStreet());
        $addressPlate->setStreet($address['street'] ?? '');
        $addressPlate->setStreetNumber($address['number'] ?? '');

        if (!$customerAddressEntity->hasExtension('intoSymbioConnectorAddons')) {
            return;
        }
        /** @var CustomerAddressAddonsEntity $addons */
        $addons = $customerAddressEntity->getExtension('intoSymbioConnectorAddons');
        $addressPlate->setId($addons->getSymbioAddressId());
        $addressPlate->setRecipientId($addons->getSymbioRecipientId());
        $addressPlate->setEntityId($addons->getCustomerAddressId());
        $addressPlate->setAddonsId($addons->getId());
    }

    private function getCountryIsoFromCustomerAddress(
        CustomerAddressEntity $customerAddressEntity,
        Context $context
    ): string {
        // get country from customer address - this should always be possiblie
        $iso = $customerAddressEntity->getCountry()?->getIso();
        if (is_string($iso)) {
            return $iso;
        }
        // fallback to sales channel
        $salesChannel = $this->getSalesChannelFromContext($context);
        if ($salesChannel instanceof SalesChannelEntity) {
            $iso = $salesChannel->getCountry()?->getIso();
            if (is_string($iso)) {
                return $iso;
            }
        }

        // last resort: configured Symbio API country
        return $this->intoConfigService->getString('country');
    }

    private function getSalesChannelFromContext(Context $context): ?SalesChannelEntity
    {
        if (!$context->getSource() instanceof SalesChannelApiSource) {
            return null;
        }
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('id', $context->getSource()->getSalesChannelId()))
            ->addAssociation('country');

        return $this->getRepository(SalesChannelDefinition::ENTITY_NAME)->search($criteria, $context)->first();
    }

    private function getOtherAddresses(
        CustomerEntity $customerEntity,
        CustomerAddressEntity $mainAddress,
        Context $context
    ): array {
        if (!$customerEntity->getAddresses() instanceof CustomerAddressCollection) {
            return [];
        }
        $addresses = [];
        foreach ($customerEntity->getAddresses() as $address) {
            if ($address->getId() === $mainAddress->getId()) {
                continue;
            }
            $addressPlate = new AddressPlate();
            $this->composeAddress($addressPlate, $address, $context);
            $addressPlate->setType(AddressPlate::TYPE_OTHER);
            $addresses[] = $addressPlate;
        }

        return $addresses;
    }

    private function createApiCustomer(CustomerPlate $customerPlate): array
    {
        $customerPlate->setRecipientIdentification(Uuid::randomHex());

        $credentials = $this->intoConfigService->getApiCredentials();
        $client = $this->symbioClientFactory->createSymbioClient($credentials);
        $response = $client->sendPostRequest(RequestUri::CREATE_RECIPIENT, $customerPlate->jsonSerialize());
        if (!in_array($response->getStatusCode(), [
            Response::HTTP_OK,
            Response::HTTP_CREATED,
        ])) {
            throw new ApiSymbioException([
                'error' => 'could not create API recipient',
                'error_description' => 'customer id: ' . $customerPlate->getEntityId(),
            ], $response->getStatusCode());
        }

        return $client->decodeJsonResponse($response) ?? [];
    }

    private function linkApiCustomer(CustomerPlate $customerPlate, array $created, Context $context): void
    {
        $customerPlate->setId($created['id']);

        // note important to link addresses first so that the symbio ids for addresses have been set in $customerPlate
        $this->linkApiAddress($customerPlate->getMainAddress(), $created['mainAddress'], $context);

        foreach ($created['shippingAddresses'] as $createdAddress) {
            $addressPlate = $this->matchAddressPlate($createdAddress, $customerPlate->getShippingAddresses());
            if (!$addressPlate instanceof AddressPlate) {
                throw new CustomerDownstreamException(
                    sprintf(
                        'IntoSymbioConnector - received non-matching customer address, symbio address id: %s',
                        $createdAddress['id']
                    )
                );
            }
            $this->linkApiAddress($addressPlate, $createdAddress, $context);
        }

        $mainAddressId = $customerPlate->getMainAddress()?->getEntityId();
        if ($mainAddressId === null) {
            throw new CustomerDownstreamException(sprintf('Missing main address - customer id: %s',
                $customerPlate->getId()));
        }

        $data = [
            'id' => $customerPlate->getAddonsId() ?? Uuid::randomHex(),
            'customerId' => $customerPlate->getEntityId(),
            'symbioRecipientId' => $customerPlate->getId(),
            'symbioRecipientIdentification' => $customerPlate->getRecipientIdentification(),
            'mainAddressId' => $mainAddressId,
            'hash' => $customerPlate->getChecksum(),
        ];
        $this->getRepository(CustomerAddonsDefinition::ENTITY_NAME)->upsert([$data], $context);
    }

    private function matchAddressPlate(array $createdAddress, array $addressPlates): ?AddressPlate
    {
        // match updated addresses
        /** @var AddressPlate $addressPlate */
        foreach ($addressPlates as $addressPlate) {
            if ($addressPlate->getId() === $createdAddress['id']) {
                return $addressPlate;
            }
        }

        // match new addresses
        /** @var AddressPlate $addressPlate */
        foreach ($addressPlates as $addressPlate) {
            if (!is_string($addressPlate->getAddonsId()) &&
                $addressPlate->getType() === $createdAddress['type'] &&
                $addressPlate->getName() === $createdAddress['name'] &&
                $addressPlate->getStreet() === $createdAddress['street'] &&
                $addressPlate->getStreetNumber() === $createdAddress['streetNumber'] &&
                $addressPlate->getAdditional() === $createdAddress['additional'] &&
                $addressPlate->getZipCode() === $createdAddress['zipCode'] &&
                $addressPlate->getCity() === $createdAddress['city'] &&
                $addressPlate->getCountry() === $createdAddress['country'] &&
                $addressPlate->getPhoneNumber() === $createdAddress['phoneNumber']
            ) {
                return $addressPlate;
            }
        }

        return null;
    }

    private function linkApiAddress(?AddressPlate $addressPlate, array $created, Context $context): void
    {
        if ($addressPlate === null) {
            throw new CustomerDownstreamException('Missing customer address plate.');
        }

        $addressPlate->setId($created['id']);
        $addressPlate->setRecipientId($created['recipientId']);

        $data = [
            'id' => $addressPlate->getAddonsId() ?? Uuid::randomHex(),
            'customerAddressId' => $addressPlate->getEntityId(),
            'symbioAddressId' => $addressPlate->getId(),
            'symbioRecipientId' => $addressPlate->getRecipientId(),
            'type' => $addressPlate->getType(),
        ];
        $this->getRepository(CustomerAddressAddonsDefinition::ENTITY_NAME)->upsert([$data], $context);
    }

    private function updateDownstreamCustomer(CustomerEntity $customerEntity, Context $context): void
    {
        $customerPlate = new CustomerPlate();
        $this->composeCustomer($customerPlate, $customerEntity, $context);

        /** @var CustomerAddonsEntity $addons */
        $addons = $customerEntity->getExtension('intoSymbioConnectorAddons');
        if ($customerPlate->getChecksum() === $addons->getHash()) {
            return;
        }
        $updated = $this->updateAPICustomer($customerPlate);
        $this->linkApiCustomer($customerPlate, $updated, $context);
    }

    private function updateAPICustomer(CustomerPlate $customerPlate): array
    {
        $credentials = $this->intoConfigService->getApiCredentials();
        $client = $this->symbioClientFactory->createSymbioClient($credentials);
        $response = $client->sendPutRequest(RequestUri::UPDATE_RECIPIENT, $customerPlate->jsonSerialize());
        if ($response->getStatusCode() === Response::HTTP_OK) {
            return $client->decodeJsonResponse($response) ?? [];
        }
        if (in_array($response->getStatusCode(), [
            Response::HTTP_BAD_REQUEST,
            Response::HTTP_NOT_FOUND,
        ])) {
            throw new ApiSymbioException([
                'error' => 'could not update API recipient',
                'error_description' => 'customer id: ' . $customerPlate->getEntityId() .
                    ' - validation body: ' . $response->getBody()->getContents(),
            ], $response->getStatusCode());
        }

        throw new ApiSymbioException([
            'error' => 'could not update API recipient',
            'error_description' => 'customer id: ' . $customerPlate->getEntityId() .
                ' - response code: ' . $response->getStatusCode(),
        ], $response->getStatusCode());
    }
}
