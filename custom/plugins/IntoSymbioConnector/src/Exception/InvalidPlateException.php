<?php declare(strict_types=1);

namespace IntoSymbioConnector\Exception;

class InvalidPlateException extends \Exception
{

    public function __construct(string $plate, array $errors)
    {
        parent::__construct(
            "Invalid data in plate: " . $plate . "\n" . implode(
                "\n",
                array_map(
                    function ($error) {
                        return '["' . $error[0] . '" => "' . $error[1] . '"] ' . $error[2];
                    },
                    $errors
                )
            )
        );
    }
}
