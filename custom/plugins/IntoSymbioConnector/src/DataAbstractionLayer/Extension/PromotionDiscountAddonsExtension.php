<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Extension;


use IntoSymbioConnector\DataAbstractionLayer\Entity\PromotionDiscountAddonsDefinition;
use Shopware\Core\Checkout\Promotion\Aggregate\PromotionDiscount\PromotionDiscountDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class PromotionDiscountAddonsExtension extends EntityExtension
{
    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            (new OneToOneAssociationField(
                'intoSymbioConnectorAddons',
                'id',
                'promotion_discount_id',
                PromotionDiscountAddonsDefinition::class
            ))->addFlags(new CascadeDelete())
        );
    }

    public function getDefinitionClass(): string
    {
        return PromotionDiscountDefinition::class;
    }
}
