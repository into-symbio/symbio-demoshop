<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Extension;


use IntoSymbioConnector\DataAbstractionLayer\Entity\OrderDeliveryPositionAddonsDefinition;
use Shopware\Core\Checkout\Order\Aggregate\OrderDeliveryPosition\OrderDeliveryPositionDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class OrderDeliveryPositionAddonsExtension extends EntityExtension
{
    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            (new OneToManyAssociationField(
                'intoSymbioConnectorAddons',
                OrderDeliveryPositionAddonsDefinition::class,
                'order_delivery_position_id',
                'id'
            ))->addFlags(new CascadeDelete())
        );
    }

    public function getDefinitionClass(): string
    {
        return OrderDeliveryPositionDefinition::class;
    }
}
