<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

class ProductAddonsCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return ProductAddonsEntity::class;
    }
}
