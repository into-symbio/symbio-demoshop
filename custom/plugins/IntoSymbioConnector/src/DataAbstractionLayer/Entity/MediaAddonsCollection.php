<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

class MediaAddonsCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return MediaAddonsEntity::class;
    }
}
