<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\JsonField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ReferenceVersionField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class ProductAddonsDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'into_symbio_connector_product_addons';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return ProductAddonsEntity::class;
    }

    public function getCollectionClass(): string
    {
        return ProductAddonsCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new FkField(
                'product_id',
                'productId',
                ProductDefinition::class
            ))->addFlags(new Required()),
            (new ReferenceVersionField(ProductDefinition::class))->addFlags(new Required()),
            (new IntField('symbio_product_id', 'symbioProductId')),
            (new IntField('symbio_article_id', 'symbioArticleId')),
            (new StringField('symbio_article_uid', 'symbioArticleUid')),
            (new StringField('symbio_article_sku', 'symbioArticleSku')),
            (new IntField('symbio_category_id', 'symbioCategoryId')),
            (new StringField('symbio_distributor_sku', 'symbioDistributorSku')),
            (new IntField('symbio_article_sales_id', 'symbioArticleSalesId')),
            (new StringField('hash', 'hash')),
            (new JsonField('videos', 'videos')),
            new OneToOneAssociationField(
                'product',
                'product_id',
                'id',
                ProductDefinition::class,
                false
            ),
        ]);
    }
}
