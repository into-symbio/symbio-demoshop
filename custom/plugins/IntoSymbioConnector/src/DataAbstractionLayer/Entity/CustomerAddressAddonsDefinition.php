<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class CustomerAddressAddonsDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'into_symbio_connector_customer_address_addons';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return CustomerAddressAddonsEntity::class;
    }

    public function getCollectionClass(): string
    {
        return CustomerAddressAddonsCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new FkField(
                'customer_address_id',
                'customerAddressId',
                CustomerAddressDefinition::class
            ))->addFlags(new Required()),
            (new IntField('symbio_address_id', 'symbioAddressId')),
            (new IntField('symbio_recipient_id', 'symbioRecipientId')),
            (new StringField('type', 'type')),
            new OneToOneAssociationField(
                'customerAddress',
                'customer_address_id',
                'id',
                CustomerAddressDefinition::class,
                false
            ),
        ]);
    }
}
