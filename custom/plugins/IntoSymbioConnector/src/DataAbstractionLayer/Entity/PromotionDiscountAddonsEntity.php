<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class PromotionDiscountAddonsEntity extends Entity
{
    use EntityIdTrait;

    protected string $promotionDiscountId;

    protected ?string $symbioPromotionDiscountKey;

    /**
     * @return string
     */
    public function getPromotionDiscountId(): string
    {
        return $this->promotionDiscountId;
    }

    /**
     * @param string $promotionDiscountId
     */
    public function setPromotionDiscountId(string $promotionDiscountId): void
    {
        $this->promotionDiscountId = $promotionDiscountId;
    }

    /**
     * @return string|null
     */
    public function getSymbioPromotionDiscountKey(): ?string
    {
        return $this->symbioPromotionDiscountKey;
    }

    /**
     * @param string|null $symbioPromotionDiscountKey
     */
    public function setSymbioPromotionDiscountKey(?string $symbioPromotionDiscountKey): void
    {
        $this->symbioPromotionDiscountKey = $symbioPromotionDiscountKey;
    }
}
