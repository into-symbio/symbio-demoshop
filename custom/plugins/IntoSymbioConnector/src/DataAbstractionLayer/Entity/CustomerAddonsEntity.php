<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class CustomerAddonsEntity extends Entity
{
    use EntityIdTrait;

    protected string $customerId;

    protected ?int $symbioRecipientId;

    protected ?string $symbioRecipientIdentification;

    // is set when Symbio recipient is created as Symbio does not allow change of main address
    protected ?string $mainAddressId;

    protected ?string $hash;

    protected ?CustomerAddressEntity $mainAddress;

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId(string $customerId): void
    {
        $this->customerId = $customerId;
    }

    /**
     * @return int|null
     */
    public function getSymbioRecipientId(): ?int
    {
        return $this->symbioRecipientId;
    }

    /**
     * @param int|null $symbioRecipientId
     */
    public function setSymbioRecipientId(?int $symbioRecipientId): void
    {
        $this->symbioRecipientId = $symbioRecipientId;
    }

    /**
     * @return string|null
     */
    public function getSymbioRecipientIdentification(): ?string
    {
        return $this->symbioRecipientIdentification;
    }

    /**
     * @param string|null $symbioRecipientIdentification
     */
    public function setSymbioRecipientIdentification(?string $symbioRecipientIdentification): void
    {
        $this->symbioRecipientIdentification = $symbioRecipientIdentification;
    }

    /**
     * @return string|null
     */
    public function getMainAddressId(): ?string
    {
        return $this->mainAddressId;
    }

    /**
     * @param string|null $mainAddressId
     */
    public function setMainAddressId(?string $mainAddressId): void
    {
        $this->mainAddressId = $mainAddressId;
    }

    /**
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string|null $hash
     */
    public function setHash(?string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return CustomerAddressEntity|null
     */
    public function getMainAddress(): ?CustomerAddressEntity
    {
        return $this->mainAddress;
    }

    /**
     * @param CustomerAddressEntity|null $mainAddress
     */
    public function setMainAddress(?CustomerAddressEntity $mainAddress): void
    {
        $this->mainAddress = $mainAddress;
    }
}
