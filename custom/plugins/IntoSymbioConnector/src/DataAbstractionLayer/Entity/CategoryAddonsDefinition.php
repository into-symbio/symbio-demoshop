<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Content\Category\CategoryDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ReferenceVersionField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class CategoryAddonsDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'into_symbio_connector_category_addons';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return CategoryAddonsEntity::class;
    }

    public function getCollectionClass(): string
    {
        return CategoryAddonsCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new FkField(
                'category_id',
                'categoryId',
                CategoryDefinition::class
            ))->addFlags(new Required()),
            (new ReferenceVersionField(CategoryDefinition::class))->addFlags(new Required()),
            (new IntField('symbio_id', 'symbioId')),
            (new StringField('hash', 'hash')),
            new OneToOneAssociationField(
                'category',
                'category_id',
                'id',
                CategoryDefinition::class,
                false
            ),
        ]);
    }
}
