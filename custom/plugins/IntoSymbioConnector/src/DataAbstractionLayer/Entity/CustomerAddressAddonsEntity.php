<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class CustomerAddressAddonsEntity extends Entity
{
    use EntityIdTrait;

    protected string $customerAddressId;

    protected ?int $symbioAddressId;

    protected ?int $symbioRecipientId;

    protected ?string $type;

    /**
     * @return string
     */
    public function getCustomerAddressId(): string
    {
        return $this->customerAddressId;
    }

    /**
     * @param string $customerAddressId
     */
    public function setCustomerAddressId(string $customerAddressId): void
    {
        $this->customerAddressId = $customerAddressId;
    }

    /**
     * @return int|null
     */
    public function getSymbioAddressId(): ?int
    {
        return $this->symbioAddressId;
    }

    /**
     * @param int|null $symbioAddressId
     */
    public function setSymbioAddressId(?int $symbioAddressId): void
    {
        $this->symbioAddressId = $symbioAddressId;
    }

    /**
     * @return int|null
     */
    public function getSymbioRecipientId(): ?int
    {
        return $this->symbioRecipientId;
    }

    /**
     * @param int|null $symbioRecipientId
     */
    public function setSymbioRecipientId(?int $symbioRecipientId): void
    {
        $this->symbioRecipientId = $symbioRecipientId;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }
}
