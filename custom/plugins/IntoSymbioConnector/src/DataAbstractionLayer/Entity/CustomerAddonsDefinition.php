<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressDefinition;
use Shopware\Core\Checkout\Customer\CustomerDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class CustomerAddonsDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'into_symbio_connector_customer_addons';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return CustomerAddonsEntity::class;
    }

    public function getCollectionClass(): string
    {
        return CustomerAddonsCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new FkField(
                'customer_id',
                'customerId',
                CustomerDefinition::class
            ))->addFlags(new Required()),
            (new IntField('symbio_recipient_id', 'symbioRecipientId')),
            (new StringField('symbio_recipient_identification', 'symbioRecipientIdentification')),
            (new FkField(
                'main_address_id',
                'mainAddressId',
                CustomerAddressDefinition::class
            ))->addFlags(new Required()),
            (new StringField('hash', 'hash')),
            new OneToOneAssociationField(
                'customer',
                'customer_id',
                'id',
                CustomerDefinition::class,
                false
            ),
            new OneToOneAssociationField(
                'mainAddress',
                'main_address_id',
                'id',
                CustomerAddressDefinition::class,
                false
            ),
        ]);
    }
}
