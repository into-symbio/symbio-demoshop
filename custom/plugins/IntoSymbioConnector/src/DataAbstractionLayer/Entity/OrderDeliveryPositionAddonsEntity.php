<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class OrderDeliveryPositionAddonsEntity extends Entity
{
    use EntityIdTrait;

    protected string $orderDeliveryPositionId;

    protected ?int $quantity;

    protected ?string $carrier;

    protected ?string $handlingUnit;

    protected ?string $trackingCode;

    /**
     * @return string
     */
    public function getOrderDeliveryPositionId(): string
    {
        return $this->orderDeliveryPositionId;
    }

    /**
     * @param string $orderDeliveryPositionId
     */
    public function setOrderDeliveryPositionId(string $orderDeliveryPositionId): void
    {
        $this->orderDeliveryPositionId = $orderDeliveryPositionId;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     */
    public function setQuantity(?int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string|null
     */
    public function getCarrier(): ?string
    {
        return $this->carrier;
    }

    /**
     * @param string|null $carrier
     */
    public function setCarrier(?string $carrier): void
    {
        $this->carrier = $carrier;
    }

    /**
     * @return string|null
     */
    public function getHandlingUnit(): ?string
    {
        return $this->handlingUnit;
    }

    /**
     * @param string|null $handlingUnit
     */
    public function setHandlingUnit(?string $handlingUnit): void
    {
        $this->handlingUnit = $handlingUnit;
    }

    /**
     * @return string|null
     */
    public function getTrackingCode(): ?string
    {
        return $this->trackingCode;
    }

    /**
     * @param string|null $trackingCode
     */
    public function setTrackingCode(?string $trackingCode): void
    {
        $this->trackingCode = $trackingCode;
    }
}
