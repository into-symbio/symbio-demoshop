<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class CategoryAddonsEntity extends Entity
{
    use EntityIdTrait;

    protected string $categoryId;

    protected ?int $symbioId;

    protected ?string $hash;

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return $this->categoryId;
    }

    /**
     * @param string $categoryId
     */
    public function setCategoryId(string $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return int|null
     */
    public function getSymbioId(): ?int
    {
        return $this->symbioId;
    }

    /**
     * @param int|null $symbioId
     */
    public function setSymbioId(?int $symbioId): void
    {
        $this->symbioId = $symbioId;
    }

    /**
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string|null $hash
     */
    public function setHash(?string $hash): void
    {
        $this->hash = $hash;
    }
}
