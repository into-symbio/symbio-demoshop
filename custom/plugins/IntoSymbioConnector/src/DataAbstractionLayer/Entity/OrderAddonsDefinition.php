<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Checkout\Order\OrderDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\JsonField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ReferenceVersionField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class OrderAddonsDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'into_symbio_connector_order_addons';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return OrderAddonsEntity::class;
    }

    public function getCollectionClass(): string
    {
        return OrderAddonsCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new FkField(
                'order_id',
                'orderId',
                OrderDefinition::class
            ))->addFlags(new Required()),
            (new ReferenceVersionField(OrderDefinition::class))->addFlags(new Required()),
            (new IntField('symbio_order_id', 'symbioOrderId')),
            (new StringField('symbio_order_status', 'symbioOrderStatus')),
            (new JsonField('symbio_order_placement', 'symbioOrderPlacement')),
            (new JsonField('symbio_order_summary', 'symbioOrderSummary')),
            (new DateTimeField('symbio_order_timestamp', 'symbioOrderTimestamp')),
            new OneToOneAssociationField(
                'order',
                'order_id',
                'id',
                OrderDefinition::class,
                false
            ),
        ]);
    }
}
