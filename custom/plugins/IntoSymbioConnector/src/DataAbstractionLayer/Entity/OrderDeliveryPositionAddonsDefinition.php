<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Checkout\Order\Aggregate\OrderDeliveryPosition\OrderDeliveryPositionDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ReferenceVersionField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class OrderDeliveryPositionAddonsDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'into_symbio_connector_order_delivery_position_addons';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return OrderDeliveryPositionAddonsEntity::class;
    }

    public function getCollectionClass(): string
    {
        return OrderDeliveryPositionAddonsCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new FkField(
                    'order_delivery_position_id',
                    'orderDeliverPositionId',
                    OrderDeliveryPositionDefinition::class
                ))->addFlags(new Required()),
                (new ReferenceVersionField(OrderDeliveryPositionDefinition::class))->addFlags(new Required()),
                new IntField('quantity', 'quantity'),
                new StringField('carrier', 'carrier'),
                new StringField('handling_unit', 'handlingUnit'),
                new StringField('tracking_code', 'trackingCode'),
                new ManyToOneAssociationField(
                    'orderDeliveryPosition',
                    'order_delivery_position_id',
                    OrderDeliveryPositionDefinition::class,
                ),
            ]
        );
    }
}
