<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class ProductAddonsEntity extends Entity
{
    use EntityIdTrait;

    protected string $productId;

    protected ?int $symbioProductId;

    protected ?int $symbioArticleId;

    protected ?string $symbioArticleUid;

    protected ?string $symbioArticleSku;

    protected ?int $symbioArticleSalesId;

    protected ?int $symbioCategoryId;

    protected ?string $symbioDistributorSku;

    protected ?string $hash;

    protected ?array $videos;

    protected ?ProductEntity $product;

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     */
    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return int|null
     */
    public function getSymbioProductId(): ?int
    {
        return $this->symbioProductId;
    }

    /**
     * @param int|null $symbioProductId
     */
    public function setSymbioProductId(?int $symbioProductId): void
    {
        $this->symbioProductId = $symbioProductId;
    }

    /**
     * @return int|null
     */
    public function getSymbioArticleId(): ?int
    {
        return $this->symbioArticleId;
    }

    /**
     * @param int|null $symbioArticleId
     */
    public function setSymbioArticleId(?int $symbioArticleId): void
    {
        $this->symbioArticleId = $symbioArticleId;
    }

    /**
     * @return string|null
     */
    public function getSymbioArticleUid(): ?string
    {
        return $this->symbioArticleUid;
    }

    /**
     * @param string|null $symbioArticleUid
     */
    public function setSymbioArticleUid(?string $symbioArticleUid): void
    {
        $this->symbioArticleUid = $symbioArticleUid;
    }

    /**
     * @return string|null
     */
    public function getSymbioArticleSku(): ?string
    {
        return $this->symbioArticleSku;
    }

    /**
     * @param string|null $symbioArticleSku
     */
    public function setSymbioArticleSku(?string $symbioArticleSku): void
    {
        $this->symbioArticleSku = $symbioArticleSku;
    }

    /**
     * @return int|null
     */
    public function getSymbioArticleSalesId(): ?int
    {
        return $this->symbioArticleSalesId;
    }

    /**
     * @param int|null $symbioArticleSalesId
     */
    public function setSymbioArticleSalesId(?int $symbioArticleSalesId): void
    {
        $this->symbioArticleSalesId = $symbioArticleSalesId;
    }

    /**
     * @return int|null
     */
    public function getSymbioCategoryId(): ?int
    {
        return $this->symbioCategoryId;
    }

    /**
     * @param int|null $symbioCategoryId
     */
    public function setSymbioCategoryId(?int $symbioCategoryId): void
    {
        $this->symbioCategoryId = $symbioCategoryId;
    }

    /**
     * @return string|null
     */
    public function getSymbioDistributorSku(): ?string
    {
        return $this->symbioDistributorSku;
    }

    /**
     * @param string|null $symbioDistributorSku
     */
    public function setSymbioDistributorSku(?string $symbioDistributorSku): void
    {
        $this->symbioDistributorSku = $symbioDistributorSku;
    }

    /**
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string|null $hash
     */
    public function setHash(?string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return array|null
     */
    public function getVideos(): ?array
    {
        return $this->videos;
    }

    /**
     * @param array|null $videos
     */
    public function setVideos(?array $videos): void
    {
        $this->videos = $videos;
    }

    /**
     * @return ProductEntity|null
     */
    public function getProduct(): ?ProductEntity
    {
        return $this->product;
    }

    /**
     * @param ProductEntity|null $product
     */
    public function setProduct(?ProductEntity $product): void
    {
        $this->product = $product;
    }
}
