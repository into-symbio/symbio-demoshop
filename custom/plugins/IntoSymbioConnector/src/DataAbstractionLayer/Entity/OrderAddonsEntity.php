<?php declare(strict_types=1);


namespace IntoSymbioConnector\DataAbstractionLayer\Entity;


use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class OrderAddonsEntity extends Entity
{
    use EntityIdTrait;

    protected string $orderId;

    protected ?int $symbioOrderId;

    protected ?string $symbioOrderStatus;

    protected ?string $symbioOrderPlacement;

    protected ?string $symbioOrderSummary;

    protected ?\DateTimeInterface $symbioOrderTimestamp;

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @param string $orderId
     */
    public function setOrderId(string $orderId): void
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int|null
     */
    public function getSymbioOrderId(): ?int
    {
        return $this->symbioOrderId;
    }

    /**
     * @param int|null $symbioOrderId
     */
    public function setSymbioOrderId(?int $symbioOrderId): void
    {
        $this->symbioOrderId = $symbioOrderId;
    }

    /**
     * @return string|null
     */
    public function getSymbioOrderStatus(): ?string
    {
        return $this->symbioOrderStatus;
    }

    /**
     * @param string|null $symbioOrderStatus
     */
    public function setSymbioOrderStatus(?string $symbioOrderStatus): void
    {
        $this->symbioOrderStatus = $symbioOrderStatus;
    }

    /**
     * @return string|null
     */
    public function getSymbioOrderPlacement(): ?string
    {
        return $this->symbioOrderPlacement;
    }

    /**
     * @param string|null $symbioOrderPlacement
     */
    public function setSymbioOrderPlacement(?string $symbioOrderPlacement): void
    {
        $this->symbioOrderPlacement = $symbioOrderPlacement;
    }

    /**
     * @return string|null
     */
    public function getSymbioOrderSummary(): ?string
    {
        return $this->symbioOrderSummary;
    }

    /**
     * @param string|null $symbioOrderSummary
     */
    public function setSymbioOrderSummary(?string $symbioOrderSummary): void
    {
        $this->symbioOrderSummary = $symbioOrderSummary;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getSymbioOrderTimestamp(): ?\DateTimeInterface
    {
        return $this->symbioOrderTimestamp;
    }

    /**
     * @param \DateTime|null $symbioOrderTimestamp
     */
    public function setSymbioOrderTimestamp(?\DateTime $symbioOrderTimestamp): void
    {
        $this->symbioOrderTimestamp = $symbioOrderTimestamp;
    }
}
