<?php

declare(strict_types=1);


namespace IntoSymbioConnector\Api\Client;


use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Stream\Stream;
use IntoSymbioConnector\Api\Authentication\OAuthCredentials;
use IntoSymbioConnector\Api\Authentication\TokenResource;
use IntoSymbioConnector\Api\RequestUri;
use League\Flysystem\FilesystemInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

class SymbioClientMock extends SymbioClient
{
    private FilesystemInterface $filesystem;

    public function __construct(
        TokenResource $tokenResource,
        OAuthCredentials $credentials,
        LoggerInterface $logger,
        FilesystemInterface $filesystem
    ) {
        parent::__construct($tokenResource, $credentials, $logger);

        $this->filesystem = $filesystem;
    }

    public function sendPostRequest(string $resourceUri, array $data): ResponseInterface
    {
        if (str_starts_with($resourceUri, RequestUri::CHECK_AVAILABILITY_OF_ARTICLES)) {
            return $this->mockStockListing();
        }
        if (str_starts_with($resourceUri, '/services/order/api/external/v2/order-placement/submit')) {
            return $this->mockSubmitOrder($data);
        }
        if (str_starts_with($resourceUri, RequestUri::CREATE_ADDRESS)) {
            return $this->mockCreateAddress($data);
        }
        if (str_starts_with($resourceUri, RequestUri::CREATE_RECIPIENT)) {
            return $this->mockMethodCustomer('post', $data);
        }

        return parent::sendPostRequest($resourceUri, $data);
    }

    public function sendDeleteRequest(string $resourceUri, ?string $query = null): ?ResponseInterface
    {
        return parent::sendDeleteRequest($resourceUri, $query);
    }

    public function sendPutRequest(string $resourceUri, array $data): ResponseInterface
    {
        if (str_starts_with($resourceUri, RequestUri::UPDATE_RECIPIENT)) {
            return $this->mockMethodCustomer('put', $data);
        }

        return parent::sendPutRequest($resourceUri, $data);
    }

    public function sendGetRequest(string $resourceUri, ?array $data = null, ?float $timeout = 0): ResponseInterface
    {
        if (str_starts_with($resourceUri, RequestUri::GET_LOCALIZED_CATEGORY_FOR_ACCOUNT)) {
            return $this->mockCategoryListing();
        }
        if (str_starts_with($resourceUri, RequestUri::GET_ALL_SELLING_PRODUCTS)) {
            return $this->mockGetAllSellingProducts();
        }
        if (str_starts_with($resourceUri, RequestUri::GET_SELLING_PRODUCT)) {
            return $this->mockGetSellingProduct($resourceUri);
        }

        return parent::sendGetRequest($resourceUri, $data, $timeout);
    }

    private function mockStockListing(): ResponseInterface
    {
        $json = $this->filesystem->read('/api_mock/upstream/stock.json');
        if ($json === false) {
            $json = '';
        }

        return new Response(200, [], $json);
    }

    private function mockCategoryListing(): ResponseInterface
    {
        $json = $this->filesystem->read('/api_mock/upstream/category.json');
        if ($json === false) {
            $json = '';
        }

        return new Response(200, [], $json);
    }

    private function mockGetAllSellingProducts(): ResponseInterface
    {
        $json = $this->filesystem->read('/api_mock/upstream/product.json');
        if ($json === false) {
            $json = '';
        }

        return new Response(200, [], $json);
    }

    private function mockGetSellingProduct(string $resourceUri): ResponseInterface
    {
        $json = $this->filesystem->read('/api_mock/upstream/product.json');
        if ($json === false) {
            $json = '';
        }

        $products = json_decode($json, true);
        if (!is_array($products)) {
            $products = [];
        }

        $product = null;
        if (preg_match('/^.*\/(\d{3,7})\/.*$/', $resourceUri, $matches) === 1) {
            $productId = $matches[1];
            $product = array_filter($products, function ($product) use ($productId) {
                return $product['id'] === (int)$productId;
            });
            $product = array_shift($product);
        }

        $json = json_encode($product ?? []);
        if ($json === false) {
            $json = '';
        }

        return new Response(200, [], $json);
    }

    /**
     * @param array|bool|float|int|object $data
     * @return ResponseInterface
     * @throws \Exception
     */
    private function mockSubmitOrder($data): ResponseInterface
    {
        $date = (new \DateTime())->format('Ymd_hisu');
        $dataString = \json_encode($data);
        if ($dataString === false) {
            throw new \Exception('Failure to create order.');
        }
        $this->filesystem->put('/api_mock/downstream/order_' . $date . '.json', $dataString);

        return new Response(
            200,
            [],
            /* @phpstan-ignore-next-line */
            Stream::factory(
            /* @phpstan-ignore-next-line */
                \json_encode([
                    'status' => 'ok',
                    'id' => random_int(9000000, 9999999),
                    'headerErrors' => [],
                    'itemErrors' => [],
                    'orderDate' => (new \DateTime())->format('Y-m-d'),
                ])
            )
        );
    }

    /**
     * @param string $method
     * @param array $data
     * @return ResponseInterface
     * @throws \Exception
     */
    private function mockMethodCustomer(string $method, array $data): ResponseInterface
    {
        $date = (new \DateTime())->format('Ymd_hisu');
        $dataString = \json_encode($data);
        if ($dataString === false) {
            throw new \Exception('Failure to create customer.');
        }
        $this->filesystem->put('/api_mock/downstream/customer_' . $method . '_' . $date . '.json', $dataString);

        $recipientId = random_int(9000000, 9999999);
        if (is_int($data['id'])) {
            $recipientId = $data['id'];
        }

        $mainAddress = $data['mainAddress'];
        $mainAddress['id'] = random_int(9000000, 9999999);
        if (isset($data['mainAddress'][0]) && is_int($data['mainAddress']['id'])) {
            $mainAddress['id'] = $data['mainAddress']['id'];
        }
        $mainAddress['recipientId'] = $recipientId;

        $shippingAddress = null;
        if (isset($data['shippingAddresses'][0])) {
            $shippingAddress = $data['shippingAddresses'][0];
            $shippingAddress['id'] = random_int(9000000, 9999999);
            if (is_int($data['shippingAddresses'][0]['id'])) {
                $shippingAddress['id'] = $data['shippingAddresses'][0]['id'];
            }
            $shippingAddress['recipientId'] = $recipientId;
        }

        $data = [
            'id' => $recipientId,
            'mainAddress' => $mainAddress,
            'shippingAddresses' => [],
        ];
        if (is_array($shippingAddress)) {
            $data['shippingAddresses'][0] = $shippingAddress;
        }

        return new Response(
            200,
            [],
            /* @phpstan-ignore-next-line */
            Stream::factory(\json_encode($data))
        );
    }

    private function mockCreateAddress($data): ResponseInterface
    {
        $date = (new \DateTime())->format('Ymd_hisu');
        $dataString = \json_encode($data);
        if ($dataString === false) {
            throw new \Exception('Failure to create address.');
        }
        $this->filesystem->put('/api_mock/downstream/address_' . $date . '.json', $dataString);

        $data['id'] = random_int(9000000, 9999999);

        return new Response(
            201,
            [],
            /* @phpstan-ignore-next-line */
            Stream::factory(\json_encode($data))
        );
    }
}
