<?php declare(strict_types=1);

namespace IntoSymbioConnector\Api\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use IntoSymbioConnector\Api\Exception\ApiException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractClient
{
    protected Client $client;

    protected LoggerInterface $logger;

    public function __construct(Client $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    protected function post(string $uri, array $options): ResponseInterface
    {
        try {
            $response = $this->client->post($uri, $options);
        } catch (RequestException $requestException) {
            throw $this->handleRequestException($requestException, $options);
        }

        return $response;
    }

    protected function get(string $uri, array $options = []): ResponseInterface
    {
        try {
            $response = $this->client->get($uri, $options);
        } catch (RequestException $requestException) {
            throw $this->handleRequestException($requestException, $options);
        }

        return $response;
    }

    protected function put(string $uri, array $options): ResponseInterface
    {
        try {
            $response = $this->client->put($uri, $options);
        } catch (RequestException $requestException) {
            throw $this->handleRequestException($requestException, $options);
        }

        return $response;
    }

    protected function delete(string $uri, array $options = []): ?ResponseInterface
    {
        try {
            $response = $this->client->delete($uri, $options);
        } catch (\Throwable $requestException) {
            if ($requestException instanceof RequestException) {
                if ($requestException->getResponse() instanceof ResponseInterface &&
                    $requestException->getResponse()->getStatusCode() != \Symfony\Component\HttpFoundation\Response::HTTP_CREATED) {
                    throw $this->handleRequestException($requestException, $options);
                }
                return $requestException->getResponse();
            }
            throw $requestException;
        }

        return $response;
    }

    protected function handleRequestException(RequestException $requestException, ?array $data): ApiException
    {
        $exceptionMessage = $requestException->getMessage();
        $exceptionResponse = $requestException->getResponse();

        if ($exceptionResponse === null) {
            $this->logger->error($exceptionMessage, [$data]);

            return new ApiException('General Error', $exceptionMessage, (int)$requestException->getCode());
        }

        $error = $this->decodeJsonResponse($exceptionResponse);
        if ($error === null) {
            throw $requestException;
        }

        return $this->handleError($requestException, $error);
    }

    public function decodeJsonResponse(ResponseInterface $response): ?array
    {
        $decoded = \json_decode($response->getBody()->getContents(), true);
        if (is_array($decoded)) {
            return $decoded;
        }

        return null;
    }

    abstract protected function handleError(RequestException $requestException, array $error): ApiException;
}
