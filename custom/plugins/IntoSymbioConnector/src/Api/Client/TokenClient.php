<?php declare(strict_types=1);


namespace IntoSymbioConnector\Api\Client;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use IntoSymbioConnector\Api\Authentication\OAuthCredentials;
use IntoSymbioConnector\Api\Exception\ApiException;
use IntoSymbioConnector\Api\Exception\ApiInvalidCredentialsException;
use IntoSymbioConnector\Api\Exception\ApiTokenException;
use IntoSymbioConnector\Api\RequestUri;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class TokenClient extends AbstractClient
{
    public function __construct(string $baseUrl, LoggerInterface $logger)
    {
        $client = new Client([
            'base_uri' => $baseUrl,
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
        ]);

        parent::__construct($client, $logger);
    }

    public function getToken(OAuthCredentials $credentials): array
    {
        $data = [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $credentials->getClientId(),
                'username' => $credentials->getUsername(),
                'password' => $credentials->getPassword(),
                'client_secret' => $credentials->getClientSecret(),
            ],
        ];

        $response = $this->post(RequestUri::OAUTH, $data);
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new ApiInvalidCredentialsException();
        }

        return $this->decodeJsonResponse($response) ?? [];
    }

    public function getTokenRefresh(OAuthCredentials $credentials, string $refreshToken): array
    {
        $data = [
            'form_params' => [
                'grant_type' => 'refresh_token',
                'client_id' => $credentials->getClientId(),
                'client_secret' => $credentials->getClientSecret(),
                'refresh_token' => $refreshToken,
            ],
        ];

        $response = $this->post(RequestUri::OAUTH, $data);
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new ApiInvalidCredentialsException();
        }

        return $this->decodeJsonResponse($response) ?? [];
    }

    protected function handleError(RequestException $requestException, array $error): ApiException
    {

        return new ApiTokenException($error, (int) $requestException->getCode());
    }
}
