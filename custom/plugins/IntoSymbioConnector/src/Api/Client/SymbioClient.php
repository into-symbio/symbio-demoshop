<?php declare(strict_types=1);

namespace IntoSymbioConnector\Api\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use IntoSymbioConnector\Api\Authentication\OAuthCredentials;
use IntoSymbioConnector\Api\Authentication\TokenResource;
use IntoSymbioConnector\Api\Exception\ApiException;
use IntoSymbioConnector\Api\Exception\ApiSymbioException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

class SymbioClient extends AbstractClient
{
    private TokenResource $tokenResource;

    public function __construct(
        TokenResource $tokenResource,
        OAuthCredentials $credentials,
        LoggerInterface $logger
    ) {
        $this->tokenResource = $tokenResource;

        $client = new Client([
            'base_uri' => $credentials->getBaseUrl(),
            RequestOptions::HTTP_ERRORS => false,
            RequestOptions::HEADERS => [
                'Authorization' => $this->createAuthorizationHeaderValue($credentials),
                'Accept' => '*/*',
                'Accept-Encoding' => 'gzip, deflate, br',
                'Content-Type' => 'application/json',
                'Connection' => 'keep-alive',
            ],
            RequestOptions::CONNECT_TIMEOUT => 5,
            RequestOptions::TIMEOUT => 40,
        ]);

        parent::__construct($client, $logger);
    }

    public function sendPostRequest(string $resourceUri, array $data): ResponseInterface
    {
        $options = [
            'json' => $data,
        ];

        return $this->post($resourceUri, $options);
    }

    public function sendDeleteRequest(string $resourceUri, ?string $query = null): ?ResponseInterface
    {
        if ($query === null) {
            return $this->delete($resourceUri);
        }

        $options = [
            'headers' => ['Content-Type' => 'application/json'],
            'query' => $query,
        ];

        return $this->delete($resourceUri, $options);
    }

    public function sendPutRequest(string $resourceUri, array $data): ResponseInterface
    {
        $options = [
            'json' => $data,
            'headers' => [
                'If-Match' => '*',
            ],
        ];

        return $this->put($resourceUri, $options);
    }

    public function sendGetRequest(string $resourceUri, ?array $data = null, ?float $timeout = null): ResponseInterface
    {
        $options = [];
        if (is_array($data)) {
            $options['json'] = $data;
        }
        if (is_float($timeout)) {
            $options[RequestOptions::TIMEOUT] = $timeout;
        }

        return $this->get($resourceUri, $options);
    }

    protected function handleError(RequestException $requestException, array $error): ApiException
    {
        return new ApiSymbioException($error, (int)$requestException->getCode());
    }

    private function createAuthorizationHeaderValue(OAuthCredentials $credentials): string
    {
        $token = $this->tokenResource->getToken($credentials);

        return 'Bearer ' . $token->getAccessToken();
    }
}
