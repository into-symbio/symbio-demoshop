<?php
declare(strict_types=1);

namespace IntoSymbioConnector\Api\Client;

use IntoSymbioConnector\Api\Authentication\OAuthCredentials;
use IntoSymbioConnector\Api\Authentication\TokenResource;
use League\Flysystem\FilesystemInterface;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;

class SymbioClientFactory
{
    private array $appConfig;

    private TokenResource $tokenResource;

    private LoggerInterface $logger;

    private FilesystemInterface $filesystem;

    public function __construct(
        array $appConfig,
        TokenResource $tokenResource,
        LoggerInterface $logger,
        FilesystemInterface $filesystem
    ) {
        $this->appConfig = array_replace_recursive([
            'symbio_client_mock' => false,
        ], $appConfig);
        $this->tokenResource = $tokenResource;
        $this->logger = $logger;
        $this->filesystem = $filesystem;
    }

    public function createSymbioClient(OAuthCredentials $credentials): SymbioClient
    {
        if ($this->appConfig['symbio_client_mock']) {
            return new SymbioClientMock($this->tokenResource, $credentials, $this->logger, $this->filesystem);
        }

        return new SymbioClient($this->tokenResource, $credentials, $this->logger);
    }
}
