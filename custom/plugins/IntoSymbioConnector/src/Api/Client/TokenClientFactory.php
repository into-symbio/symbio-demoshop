<?php declare(strict_types=1);

namespace IntoSymbioConnector\Api\Client;

use IntoSymbioConnector\Api\Authentication\OAuthCredentials;
use Psr\Log\LoggerInterface;

class TokenClientFactory
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function createTokenClient(OAuthCredentials $credentials): TokenClient
    {
        return new TokenClient($credentials->getBaseUrl(), $this->logger);
    }
}
