<?php declare(strict_types=1);

namespace IntoSymbioConnector\Api;

class RequestUri
{
    const OAUTH = '/auth/realms/SymbioCommerce/protocol/openid-connect/token';

    const GET_LOCALIZED_CATEGORY_FOR_ACCOUNT = '/services/product/api/external/v1/categories';
    const GET_ALL_SELLING_PRODUCTS = '/services/product/api/external/v1/selling-products/channels/';
    const GET_SELLING_PRODUCT = '/services/product/api/external/v1/selling-products/';

    const CREATE_ADDRESS = '/services/recipient/api/external/v1/addresses/';

    const CREATE_RECIPIENT = '/services/recipient/api/external/v1/recipients/';

    const UPDATE_RECIPIENT = '/services/recipient/api/external/v1/recipients/';

    const SUBMIT_ORDER = '/services/order/api/external/v2/order-placement/submit/channel/%s/countryIso/%s';

    const CHECK_AVAILABILITY_OF_ARTICLES = '/services/product/api/external/v1/resource-planning/article-availability';

    public function __construct()
    {
    }
}
