<?php declare(strict_types=1);

namespace IntoSymbioConnector\Api\Struct;

abstract class ApiStruct implements \JsonSerializable
{
    final public function __construct()
    {
    }

    public function assign(array $arrayData): ApiStruct
    {
        foreach ($arrayData as $key => $value) {
            $camelCaseKey = $this->toCamelCase($key);
            $setterMethod = 'set' . $camelCaseKey;
            if (!\method_exists($this, $setterMethod)) {

                continue;
            }

            if ($this->isScalar($value)) {
                if ($value !== null) {
                    $this->$setterMethod($value);
                }

                continue;
            }

            $namespace = $this->getNamespaceOfAssociation();
            if ($value !== [] && $this->isAssociativeArray($value)) {
                $className = $namespace . $camelCaseKey;
                if (!\class_exists($className)) {
                    continue;
                }

                $instance = $this->createNewAssociation($className, $value);
                $this->$setterMethod($instance);

                continue;
            }

            $className = $namespace . $this->getClassNameOfOneToManyAssociation($camelCaseKey);
            if (!\class_exists($className)) {
                $arrayData = \array_filter(
                    $value,
                    /** @param string|array|null $var */
                    static function ($var) {
                        return $var !== null;
                    }
                );
                $this->$setterMethod($arrayData);

                continue;
            }

            $arrayWithToManyAssociations = [];
            foreach ($value as $toManyAssociation) {
                if ($toManyAssociation === null) {
                    continue;
                }

                $instance = $this->createNewAssociation($className, $toManyAssociation);
                $arrayWithToManyAssociations[] = $instance;
            }
            $this->$setterMethod($arrayWithToManyAssociations);
        }

        return $this;
    }

    public function jsonSerialize(): array
    {
        $data = [];

        foreach (\get_object_vars($this) as $property => $value) {
            $data[$property] = $value;
        }

        return $data;
    }

    /**
     * @param array|bool|int|float|object|resource|string|null $value
     * @return bool
     */
    private function isScalar($value): bool
    {
        return !\is_array($value);
    }

    private function isAssociativeArray(array $value): bool
    {
        return \array_keys($value) !== \range(0, \count($value) - 1);
    }

    private function getNamespaceOfAssociation(): string
    {
        return static::class . '\\';
    }

    private function getClassNameOfOneToManyAssociation(string $camelCaseKey): string
    {
        return \rtrim($camelCaseKey, 's');
    }

    private function createNewAssociation(string $className, array $value): self
    {
        $instance = new $className();
        $instance->assign($value);

        return $instance;
    }

    private function toCamelCase(string $string): string
    {
        $string = \ucwords(\str_replace(['-', '_'], ' ', $string));

        return \str_replace(' ', '', $string);
    }
}
