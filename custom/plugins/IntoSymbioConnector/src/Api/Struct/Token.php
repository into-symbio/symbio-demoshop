<?php declare(strict_types=1);

namespace IntoSymbioConnector\Api\Struct;

class Token extends ApiStruct
{
    private string $accessToken;

    private string $refreshToken;

    private int $expiresIn;

    private int $refreshExpiresIn;

    private \DateTime $expireDateTime;

    private \DateTime $refreshExpireDateTime;

    public function assign(array $arrayData): Token
    {
        parent::assign($arrayData);

        $interval = \DateInterval::createFromDateString($this->getExpiresIn() . ' seconds');
        $this->setExpireDateTime((new \DateTime())->add($interval));

        $refreshInterval = \DateInterval::createFromDateString($this->getRefreshExpiresIn() . ' seconds');
        $this->setRefreshExpireDateTime((new \DateTime())->add($refreshInterval));

        return $this;
    }

    public function remaining(): int
    {

        return $this->getExpireDateTime()->getTimestamp() - (new \DateTime())->getTimestamp();
    }

    public function refreshRemaining(): int
    {

        return $this->getRefreshExpireDateTime()->getTimestamp() - (new \DateTime())->getTimestamp();
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken(string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return int
     */
    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }

    /**
     * @param int $expiresIn
     */
    public function setExpiresIn(int $expiresIn): void
    {
        $this->expiresIn = $expiresIn;
    }

    /**
     * @return int
     */
    public function getRefreshExpiresIn(): int
    {
        return $this->refreshExpiresIn;
    }

    /**
     * @param int $refreshExpiresIn
     */
    public function setRefreshExpiresIn(int $refreshExpiresIn): void
    {
        $this->refreshExpiresIn = $refreshExpiresIn;
    }

    /**
     * @return \DateTime
     */
    public function getExpireDateTime(): \DateTime
    {
        return $this->expireDateTime;
    }

    /**
     * @param \DateTime $expireDateTime
     */
    public function setExpireDateTime(\DateTime $expireDateTime): void
    {
        $this->expireDateTime = $expireDateTime;
    }

    /**
     * @return \DateTime
     */
    public function getRefreshExpireDateTime(): \DateTime
    {
        return $this->refreshExpireDateTime;
    }

    /**
     * @param \DateTime $refreshExpireDateTime
     */
    public function setRefreshExpireDateTime(\DateTime $refreshExpireDateTime): void
    {
        $this->refreshExpireDateTime = $refreshExpireDateTime;
    }
}
