<?php declare(strict_types=1);

namespace IntoSymbioConnector\Api\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use Symfony\Component\HttpFoundation\Response;

class ApiException extends ShopwareHttpException
{
    private ?int $apiStatusCode;

    public function __construct(
        string $name,
        string $message,
        int $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR
    ) {
        parent::__construct(
            'The error "{{ name }}" occurred with the following message: {{ message }}',
            ['name' => $name, 'message' => $message]
        );
        $this->apiStatusCode = $statusCode;
    }

    public function getStatusCode(): int
    {
        return $this->apiStatusCode ?? parent::getStatusCode();
    }

    public function getErrorCode(): string
    {
        return 'INTO_SYMBIO_CONNECTOR_API_EXCEPTION';
    }
}
