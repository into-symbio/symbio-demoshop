<?php declare(strict_types=1);

namespace IntoSymbioConnector\Api\Exception;

use Symfony\Component\HttpFoundation\Response;

class ApiOrderDownstreamException extends ApiException
{
    private array $error;

    public function __construct(
        array $error,
        int $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR
    ) {
        $this->error = $error;
        parent::__construct($this->error['error'], $this->error['error_description'], $statusCode);
    }

    public function getErrorCode(): string
    {

        return 'INTO_SYMBIO_CONNECTOR_ORDER_DOWNSTREAM_EXCEPTION_' . $this->error['error'];
    }
}
