<?php declare(strict_types=1);


namespace IntoSymbioConnector\Api\Exception;


use Shopware\Core\Framework\ShopwareHttpException;
use Symfony\Component\HttpFoundation\Response;

class ApiInvalidCredentialsException extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct('Invalid credentials for Symbio API provided');
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_UNAUTHORIZED;
    }

    public function getErrorCode(): string
    {
        return 'INTO_SYMBIO_CONNECTOR_INVALID_CREDENTIALS';
    }
}
