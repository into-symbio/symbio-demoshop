<?php declare(strict_types=1);

namespace IntoSymbioConnector\Api\Authentication;

use IntoSymbioConnector\Api\Client\TokenClientFactory;
use IntoSymbioConnector\Api\Struct\Token;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;

class TokenResource
{
    const CACHE_ID = 'INTO_SYMBIO_CONNECTOR_';
    const MIN_THRESHOLD = 10;
    const CACHE_EXPIRES_AFTER = 300;

    private CacheItemPoolInterface $cache;

    private TokenClientFactory $tokenClientFactory;

    public function __construct(CacheItemPoolInterface $cache, TokenClientFactory $tokenClientFactory)
    {
        $this->cache = $cache;
        $this->tokenClientFactory = $tokenClientFactory;
    }

    public function getToken(OAuthCredentials $credentials): Token
    {
        $cacheId = \md5(\serialize($credentials));
        $token = $this->getTokenFromCache($cacheId);
        if ($token === null) {
            $token = $this->getTokenFromApi($credentials);
            $this->cacheToken($token, $cacheId);

            return $token;
        }
        if ($token->remaining() < self::MIN_THRESHOLD) {
            $token = $this->getRefreshTokenFromApi($token, $credentials);
            $this->cacheToken($token, $cacheId);

            return $token;
        }

        return $token;
    }

    private function getTokenFromApi(OAuthCredentials $credentials): Token
    {
        $token = new Token();
        $tokenClient = $this->tokenClientFactory->createTokenClient($credentials);
        $token->assign($tokenClient->getToken($credentials));

        return $token;
    }

    private function getRefreshTokenFromApi(Token $token, OAuthCredentials $credentials): Token
    {
        if ($token->refreshRemaining() < self::MIN_THRESHOLD) {

            return $this->getTokenFromApi($credentials);
        }

        $refreshToken = new Token();
        $tokenClient = $this->tokenClientFactory->createTokenClient($credentials);
        $refreshToken->assign($tokenClient->getTokenRefresh($credentials, $token->getRefreshToken()));

        return $refreshToken;
    }

    private function getTokenFromCache(string $cacheId): ?Token
    {
        $token = $this->cache->getItem(self::CACHE_ID . $cacheId)->get();
        if (!is_string($token)) {
            return null;
        }

        $data = \unserialize($token, ['allowed_classes' => [Token::class, \DateTime::class]]);
        if (!$data instanceof Token) {
            return null;
        }

        return $data;
    }

    private function cacheToken(Token $token, string $cacheId): void
    {
        $item = $this->cache->getItem(self::CACHE_ID . $cacheId);
        $item->set(\serialize($token));
        $item->expiresAfter(self::CACHE_EXPIRES_AFTER);
        $this->cache->save($item);
    }
}
