<?php declare(strict_types=1);


namespace IntoSymbioConnector\Command;


use IntoSymbioConnector\Service\Sync\AbstractSyncService;
use Shopware\Core\Framework\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StockSyncCommand extends Command
{
    protected static $defaultName = 'into:stock:sync';

    protected AbstractSyncService $syncService;

    public function __construct(AbstractSyncService $syncService)
    {
        $this->syncService = $syncService;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $context = Context::createDefaultContext();
        return $this->syncService->run($context);
    }
}
