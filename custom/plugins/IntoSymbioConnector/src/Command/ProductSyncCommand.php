<?php declare(strict_types=1);


namespace IntoSymbioConnector\Command;


use IntoSymbioConnector\Service\Sync\ProductSyncService;
use Shopware\Core\Framework\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ProductSyncCommand extends Command
{
    protected static $defaultName = 'into:products:sync';

    protected ProductSyncService $syncService;

    public function __construct(ProductSyncService $syncService)
    {
        $this->syncService = $syncService;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption(
            'mode',
            'm',
            InputOption::VALUE_OPTIONAL,
            "Process mode: [stack|update|check]",
            'stack');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->syncService->run(Context::createDefaultContext(),
            match (strval($input->getOption('mode'))) {
                'update' => ProductSyncService::MODE_UPDATE,
                'check' => ProductSyncService::MODE_CHECK,
                default => ProductSyncService::MODE_STACK
            });

        return 0;
    }
}
