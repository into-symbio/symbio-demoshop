<?php declare(strict_types=1);

namespace IntoSymbioConnector\Command;

use IntoSymbioConnector\Service\Collect\OrderCollectService;
use Shopware\Core\Framework\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OrderCollectCommand extends Command
{
    protected static $defaultName = 'into:orders:collect';

    protected OrderCollectService $orderCollectService;

    public function __construct(OrderCollectService $orderCollectService)
    {
        $this->orderCollectService = $orderCollectService;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $context = Context::createDefaultContext();
        $this->orderCollectService->collect($context);

        return 0;
    }

}
