<?php declare(strict_types=1);


namespace IntoSymbioConnector\Command;


use IntoSymbioConnector\Service\Sync\ProductSyncService;
use Shopware\Core\Framework\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProductTouchedCommand extends Command
{
    protected static $defaultName = 'into:products:touched';

    protected ProductSyncService $syncService;

    public function __construct(ProductSyncService $syncService)
    {
        $this->syncService = $syncService;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(
            'path',
            InputOption::VALUE_REQUIRED,
            'Pfad/Name der Eingabedatei, relativ zu ../files/plugins/into_symbio_connector/touched_products/upload'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $path = $input->getArgument('path');
        if (!is_string($path)) {
            $io->error('Pfad der Eingabedatei fehlt oder ist fehlerhaft.');

            return 4;
        }

        $this->syncService->processTouchedProducts($path, Context::createDefaultContext());

        return 0;
    }
}
