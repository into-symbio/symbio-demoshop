<?php declare(strict_types=1);


namespace IntoSymbioConnector\Command;


use IntoSymbioConnector\Service\Import\DeliveryImportService;
use IntoSymbioConnector\Service\Sync\AbstractSyncService;
use Shopware\Core\Framework\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeliveryImportCommand extends Command
{
    protected static $defaultName = 'into:delivery:import';

    protected DeliveryImportService $deliveryImportService;

    public function __construct(DeliveryImportService $deliveryImportService)
    {
        $this->deliveryImportService = $deliveryImportService;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $context = Context::createDefaultContext();
        return $this->deliveryImportService->run($context);
    }
}
