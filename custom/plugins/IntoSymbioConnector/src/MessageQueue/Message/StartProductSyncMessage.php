<?php declare(strict_types=1);

namespace IntoSymbioConnector\MessageQueue\Message;

use IntoSymbioConnector\Service\Sync\ProductSyncService;

class StartProductSyncMessage
{
    private int $mode;

    private ?string $path;

    public function __construct(int $mode = ProductSyncService::MODE_STACK, ?string $path = null)
    {
        $this->mode = $mode;
        $this->path = $path;
    }

    /**
     * @return int
     */
    public function getMode(): int
    {
        return $this->mode;
    }

    /**
     * @param int $mode
     */
    public function setMode(int $mode): void
    {
        $this->mode = $mode;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string|null $path
     */
    public function setPath(?string $path): void
    {
        $this->path = $path;
    }
}