<?php declare(strict_types=1);


namespace IntoSymbioConnector\MessageQueue\Message;


abstract class AbstractSyncMessage
{
    private string $payload;

    /**
     * @return string
     */
    public function getPayload(): string
    {
        return $this->payload;
    }

    /**
     * @param string $payload
     */
    public function setPayload(string $payload): void
    {
        $this->payload = $payload;
    }
}
