<?php declare(strict_types=1);


namespace IntoSymbioConnector\MessageQueue\Handler;


use IntoSymbioConnector\MessageQueue\Message\CustomerUpdatedMessage;

class CustomerUpdatedMessageHandler extends AbstractUpdatedMessageHandler
{
    public static function getHandledMessages(): iterable
    {
        return [
            CustomerUpdatedMessage::class,
        ];
    }
}
