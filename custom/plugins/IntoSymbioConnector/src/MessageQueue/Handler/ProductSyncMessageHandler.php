<?php declare(strict_types=1);


namespace IntoSymbioConnector\MessageQueue\Handler;


use IntoSymbioConnector\MessageQueue\Message\ProductSyncMessage;
use IntoSymbioConnector\Struct\ProductPlate;

class ProductSyncMessageHandler extends AbstractSyncMessageHandler
{
    protected string $plateClass = ProductPlate::class;

    public static function getHandledMessages(): iterable
    {
        return [
            ProductSyncMessage::class,
        ];
    }
}
