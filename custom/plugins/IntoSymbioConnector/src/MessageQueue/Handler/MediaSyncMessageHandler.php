<?php declare(strict_types=1);


namespace IntoSymbioConnector\MessageQueue\Handler;


use IntoSymbioConnector\MessageQueue\Message\MediaSyncMessage;
use IntoSymbioConnector\Struct\MediaPlate;

class MediaSyncMessageHandler extends AbstractSyncMessageHandler
{
    protected string $plateClass = MediaPlate::class;

    public static function getHandledMessages(): iterable
    {
        return [
            MediaSyncMessage::class,
        ];
    }
}
