<?php declare(strict_types=1);

namespace IntoSymbioConnector\MessageQueue\Handler;

use IntoSymbioConnector\MessageQueue\Message\StartProductSyncMessage;
use IntoSymbioConnector\Service\Sync\ProductSyncService;
use Monolog\Logger;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\MessageQueue\Handler\AbstractMessageHandler;

class StartProductSyncMessageHandler extends AbstractMessageHandler
{
    private ProductSyncService $productSyncService;

    private Logger $logger;

    public function __construct(ProductSyncService $productSyncService, Logger $logger)
    {
        $this->productSyncService = $productSyncService;
        $this->logger = $logger;
    }

    /**
     * @param StartProductSyncMessage $message
     */
    public function handle($message): void
    {
        $context = Context::createDefaultContext();
        $mode = $message->getMode();

        try {
            if ($mode === ProductSyncService::MODE_UPDATE || $mode === ProductSyncService::MODE_CHECK) {
                $this->productSyncService->run($context, $mode);

                return;
            }
            if ($mode === ProductSyncService::MODE_TOUCHED) {
                $path = $message->getPath();
                if (is_string($path)) {
                    $this->productSyncService->processTouchedProducts($path, $context);
                }
            }
        } catch (\Throwable $throwable) {
            $this->logger->error(sprintf('StartProductSyncMessage in mode % failed.', $mode), [
                'message' => $throwable->getMessage(),
                'file' => $throwable->getFile(),
                'line' => $throwable->getLine(),
            ]);
        }
    }

    public static function getHandledMessages(): iterable
    {
        return [
            StartProductSyncMessage::class,
        ];
    }

}