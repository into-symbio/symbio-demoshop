<?php declare(strict_types=1);

namespace IntoSymbioConnector\MessageQueue\Handler;

use IntoSymbioConnector\MessageQueue\Message\AbstractSyncMessage;
use IntoSymbioConnector\Struct\AbstractPlate;
use IntoSymbioConnector\Service\Update\UpdateServiceInterface;
use Monolog\Logger;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\MessageQueue\Handler\AbstractMessageHandler;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractSyncMessageHandler extends AbstractMessageHandler
{
    protected string $plateClass = AbstractPlate::class;

    protected Logger $logger;

    protected Serializer $serializer;

    protected UpdateServiceInterface $updateService;

    public function __construct(Logger $logger, Serializer $serializer, UpdateServiceInterface $updateService)
    {
        $this->logger = $logger;
        $this->serializer = $serializer;
        $this->updateService = $updateService;
    }

    /**
     * @param AbstractSyncMessage $message
     */
    public function handle($message): void
    {
        try {
            /** @var AbstractPlate $plate */
            $plate = $this->serializer->deserialize($message->getPayload(), $this->plateClass, 'json');
            $this->updateService->handle($plate, Context::createDefaultContext());
        } catch (\Throwable $throwable) {
            $this->logger->error('IntoSymbioConnector - sync message handler exception', [
                'message' => $throwable->getMessage(),
                'file' => $throwable->getFile(),
                'line' => $throwable->getLine(),
                'payload' => $message->getPayload(),
            ]);
        }
    }
}
