<?php declare(strict_types=1);


namespace IntoSymbioConnector\MessageQueue\Handler;


use IntoSymbioConnector\MessageQueue\Message\OrderUpdatedMessage;

class OrderUpdatedMessageHandler extends AbstractUpdatedMessageHandler
{
    public static function getHandledMessages(): iterable
    {
        return [
            OrderUpdatedMessage::class,
        ];
    }
}
