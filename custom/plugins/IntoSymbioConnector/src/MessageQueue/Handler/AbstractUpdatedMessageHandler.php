<?php declare(strict_types=1);


namespace IntoSymbioConnector\MessageQueue\Handler;


use IntoSymbioConnector\Api\Exception\ApiInvalidCredentialsException;
use IntoSymbioConnector\Api\Exception\ApiTokenException;
use IntoSymbioConnector\MessageQueue\Message\AbstractSyncMessage;
use IntoSymbioConnector\Service\Downstream\DownstreamServiceInterface;
use IntoSymbioConnector\Struct\DownstreamMessageStruct;
use Monolog\Logger;
use Shopware\Core\Framework\MessageQueue\Handler\AbstractMessageHandler;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractUpdatedMessageHandler extends AbstractMessageHandler
{
    protected string $downstreamMessageStruct = DownstreamMessageStruct::class;

    protected Logger $logger;

    protected Serializer $serializer;

    protected DownstreamServiceInterface $downstreamService;

    public function __construct(Logger $logger, Serializer $serializer, DownstreamServiceInterface $updateService)
    {
        $this->logger = $logger;
        $this->serializer = $serializer;
        $this->downstreamService = $updateService;
    }

    /**
     * @param AbstractSyncMessage $message
     */
    public function handle($message): void
    {
        try {
            /** @var DownstreamMessageStruct $downstream */
            $downstream = $this->serializer->deserialize(
                $message->getPayload(),
                $this->downstreamMessageStruct,
                'json'
            );
            $this->downstreamService->handle($downstream);
        } catch (ApiInvalidCredentialsException | ApiTokenException $exception) {
            throw $exception;
        } catch (\Throwable $throwable) {
            $this->logger->error('IntoSymbioConnector - updated message handler exception', [
                'message' => $throwable->getMessage(),
                'file' => $throwable->getFile(),
                'line' => $throwable->getLine(),
                'payload' => $message->getPayload(),
            ]);
        }
    }
}
