<?php declare(strict_types=1);


namespace IntoSymbioConnector\MessageQueue\Handler;


use IntoSymbioConnector\MessageQueue\Message\CategorySyncMessage;
use IntoSymbioConnector\Struct\CategoryPlate;

class CategorySyncMessageHandler extends AbstractSyncMessageHandler
{
    protected string $plateClass = CategoryPlate::class;

    public static function getHandledMessages(): iterable
    {
        return [
            CategorySyncMessage::class,
        ];
    }
}
