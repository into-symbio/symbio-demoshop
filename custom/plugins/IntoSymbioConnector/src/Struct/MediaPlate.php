<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


class MediaPlate extends AbstractPlate
{
    public const VIDEO = 'VIDEO';

    public const IMAGE = 'IMAGE';

    public const DOCUMENT = 'DOCUMENT';

    public const TYPE = [
        self::VIDEO,
        self::IMAGE,
        self::DOCUMENT,
    ];

    private ?int $mediaId;

    private ?string $title;

    private string $uri;

    private string $type;

    private ?string $text = null;

    /**
     * @return int|null
     */
    public function getMediaId(): ?int
    {
        return $this->mediaId;
    }

    /**
     * @param int|null $mediaId
     */
    public function setMediaId(?int $mediaId): void
    {
        $this->mediaId = $mediaId;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     */
    public function setUri(string $uri): void
    {
        $this->uri = $uri;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    public function fromArray(array $array): AbstractStruct
    {
        parent::fromArray($array);
        $this->setMediaId($array['id']);

        if ($this->type === 'IMAGE' && isset($array['mediaUris']['original'])) {
            $this->uri = $array['mediaUris']['original'];
        }
        if ($this->type === 'DOCUMENT' && isset($array['mediaUris']['DOCUMENT'])) {
            $this->uri = $array['mediaUris']['DOCUMENT'];
        }
        if ($this->type === 'VIDEO' && isset($array['mediaUris']['VIDEO'])) {
            $this->uri = $array['mediaUris']['VIDEO'];
        }

        return $this;
    }

    public function getChecksum(): string
    {
        return md5(serialize($this->sigFields()));
    }

    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), $this->sigFields());
    }

    private function sigFields(): array
    {
        return [
            'mediaId' => $this->mediaId,
            'title' => $this->title,
            'uri' => $this->uri,
            'type' => $this->type,
            'text' => $this->text,
        ];
    }
}
