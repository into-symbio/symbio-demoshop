<?php

declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


use IntoSymbioConnector\Exception\InvalidPlateException;

class CustomerPlate extends AbstractPlate
{
    private const NAME = 'Customer';

    public const ACTIVE_TRUE = 'ACTIVE';

    public const ACTIVE_FALSE = 'INACTIVE';

    /** @var AddressPlate[] */
    private array $billingAddresses = [];

    private string $email = '';

    private string $externalGroupReference = '';

    private ?int $id = null;

    private ?AddressPlate $mainAddress = null;

    private string $name = '';

    private string $recipientIdentification = '';

    /** @var AddressPlate[] */
    private array $shippingAddresses = [];

    private string $status = '';

    private string $vatNumber = '';

    /**
     * @return AddressPlate[]
     */
    public function getBillingAddresses(): array
    {
        return $this->billingAddresses;
    }

    /**
     * @param AddressPlate[] $billingAddresses
     */
    public function setBillingAddresses(array $billingAddresses): void
    {
        $this->billingAddresses = $billingAddresses;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getExternalGroupReference(): string
    {
        return $this->externalGroupReference;
    }

    /**
     * @param string $externalGroupReference
     */
    public function setExternalGroupReference(string $externalGroupReference): void
    {
        $this->externalGroupReference = $externalGroupReference;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return AddressPlate|null
     */
    public function getMainAddress(): ?AddressPlate
    {
        return $this->mainAddress;
    }

    /**
     * @param AddressPlate|null $mainAddress
     */
    public function setMainAddress(?AddressPlate $mainAddress): void
    {
        $this->mainAddress = $mainAddress;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getRecipientIdentification(): string
    {
        return $this->recipientIdentification;
    }

    /**
     * @param string $recipientIdentification
     */
    public function setRecipientIdentification(string $recipientIdentification): void
    {
        $this->recipientIdentification = $recipientIdentification;
    }

    /**
     * @return AddressPlate[]
     */
    public function getShippingAddresses(): array
    {
        return $this->shippingAddresses;
    }

    /**
     * @param AddressPlate[] $shippingAddresses
     */
    public function setShippingAddresses(array $shippingAddresses): void
    {
        $this->shippingAddresses = $shippingAddresses;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getVatNumber(): string
    {
        return $this->vatNumber;
    }

    /**
     * @param string $vatNumber
     */
    public function setVatNumber(string $vatNumber): void
    {
        $this->vatNumber = $vatNumber;
    }

    public function validate(): void
    {
        $errors = [];
        if (!in_array($this->status, ['ACTIVE', 'INACTIVE'])) {
            $errors[] = ['status', $this->status, ' must be one of ACTIVE, INACTIVE'];
        }
        if (strlen($this->name) > 255) {
            $errors[] = ['name', $this->name, ' required, 0 to 255 chars'];
        }
        if (count($errors) > 0) {
            throw new InvalidPlateException(self::NAME, $errors);
        }
    }

    public function jsonSerialize(): array
    {
        $this->validate();

        return [
            'billingAddresses' => array_map(function ($address) {
                return $address->jsonSerialize();
            }, $this->billingAddresses),
            'email' => $this->email,
            'externalGroupReference' => $this->externalGroupReference,
            'id' => $this->id,
            'mainAddress' => ($this->mainAddress ? $this->mainAddress->jsonSerialize() : ''),
            'name' => $this->name,
            'recipientIdentification' => $this->recipientIdentification,
            'shippingAddresses' => array_map(function ($address) {
                return $address->jsonSerialize();
            }, $this->shippingAddresses),
            'status' => $this->status,
            'vatNumber' => $this->vatNumber,
        ];
    }
}
