<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


class CategoryPlate extends AbstractPlate
{
    public ?int $categoryId = null;

    public ?int $parentId = null;

    public ?string $name = null;

    public ?string $description = null;

    public ?string $imageBase64 = null;

    public ?int $siblingId = null;

    public bool $root = false;

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    /**
     * @param int|null $categoryId
     */
    public function setCategoryId(?int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parentId
     */
    public function setParentId(?int $parentId): void
    {
        $this->parentId = $parentId;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getImageBase64(): ?string
    {
        return $this->imageBase64;
    }

    /**
     * @param string|null $imageBase64
     */
    public function setImageBase64(?string $imageBase64): void
    {
        $this->imageBase64 = $imageBase64;
    }

    /**
     * @return int|null
     */
    public function getSiblingId(): ?int
    {
        return $this->siblingId;
    }

    /**
     * @param int|null $siblingId
     */
    public function setSiblingId(?int $siblingId): void
    {
        $this->siblingId = $siblingId;
    }

    /**
     * @return bool
     */
    public function isRoot(): bool
    {
        return $this->root;
    }

    /**
     * @param bool $root
     */
    public function setRoot(bool $root): void
    {
        $this->root = $root;
    }

    public function fromArray(array $array): AbstractStruct
    {
        parent::fromArray($array);
        $this->setCategoryId($array['id']);

        return $this;
    }

    public function getChecksum(): string
    {
        return md5(serialize($this->sigFields()));
    }

    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), $this->sigFields());
    }

    private function sigFields(): array
    {
        return [
            'categoryId' => $this->categoryId,
            'parentId' => $this->parentId,
            'name' => $this->name,
            'description' => $this->description,
            'imageBase64' => $this->imageBase64,
            'siblingId' => $this->siblingId,
            'root' => $this->root,
        ];
    }
}
