<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


abstract class AbstractStruct implements \JsonSerializable
{
    public function fromArray(array $array): AbstractStruct
    {
        foreach ($array as $property => $value) {
            if ($value === null) {
                continue;
            }
            $setter = 'set' . ucfirst($property);
            if (method_exists($this, $setter)) {
                $this->$setter($value);
            }
        }

        return $this;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
