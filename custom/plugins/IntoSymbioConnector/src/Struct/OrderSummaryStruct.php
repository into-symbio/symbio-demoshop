<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


class OrderSummaryStruct extends AbstractStruct
{
    public string $status;

    public ?int $id;

    public ?array $headerErrors;

    public ?array $itemErrors;

    public string $orderDate;

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array|null
     */
    public function getHeaderErrors(): ?array
    {
        return $this->headerErrors;
    }

    /**
     * @param array|null $headerErrors
     */
    public function setHeaderErrors(?array $headerErrors): void
    {
        $this->headerErrors = $headerErrors;
    }

    /**
     * @return array|null
     */
    public function getItemErrors(): ?array
    {
        return $this->itemErrors;
    }

    /**
     * @param array|null $itemErrors
     */
    public function setItemErrors(?array $itemErrors): void
    {
        $this->itemErrors = $itemErrors;
    }

    /**
     * @return string
     */
    public function getOrderDate(): string
    {
        return $this->orderDate;
    }

    /**
     * @param string $orderDate
     */
    public function setOrderDate(string $orderDate): void
    {
        $this->orderDate = $orderDate;
    }
}
