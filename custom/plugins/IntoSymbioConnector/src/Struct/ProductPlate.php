<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


class ProductPlate extends AbstractPlate
{
    private bool $dirty = false;

    private ?int $productId = null;

    private string $uid = '';

    private ?int $categoryId = null;

    private ?string $name = null;

    private array $mediaIds = [];

    private array $videos = [];

    /**
     * @var ArticlePlate[]
     */
    private array $articles = [];

    /**
     * @return bool
     */
    public function isDirty(): bool
    {
        return $this->dirty;
    }

    /**
     * @param bool $dirty
     */
    public function setDirty(bool $dirty): void
    {
        $this->dirty = $dirty;
    }

    /**
     * @return int|null
     */
    public function getProductId(): ?int
    {
        return $this->productId;
    }

    /**
     * @param int|null $productId
     */
    public function setProductId(?int $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    /**
     * @param int|null $categoryId
     */
    public function setCategoryId(?int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getMediaIds(): array
    {
        return $this->mediaIds;
    }

    /**
     * @param array $ids
     */
    public function setMediaIds(array $ids): void
    {
        $this->mediaIds = $ids;
    }

    /**
     * @param int $id
     */
    public function addMediaId(int $id): void
    {
        $this->mediaIds[] = $id;
    }

    /**
     * @return array
     */
    public function getVideos(): array
    {
        return $this->videos;
    }

    /**
     * @param array $videos
     */
    public function setVideos(array $videos): void
    {
        $this->videos = $videos;
    }

    /**
     * @param array $video
     */
    public function addVideo(array $video): void
    {
        $this->videos[] = $video;
    }

    /**
     * @return ArticlePlate[]
     */
    public function getArticles(): array
    {
        return $this->articles;
    }

    /**
     * @param ArticlePlate[] $articles
     */
    public function setArticles(array $articles): void
    {
        $this->articles = $articles;
    }

    public function addArticle(ArticlePlate $articlePlate): void
    {
        $this->articles[] = $articlePlate;
    }

    public function fromArray(array $array): AbstractStruct
    {
        parent::fromArray($array);
        $this->setProductId($array['id']);
        $this->setCategoryId($array['category']['id']);
        foreach ($array['media'] as $media) {
            if ($media['type'] === MediaPlate::VIDEO) {
                $this->addVideo($media);
                continue;
            }
            $this->addMediaId($media['id']);
        }

        return $this;
    }

    public function getChecksum(): string
    {
        // note this construct for bc w/ hashes generated pre-v3
        return md5(serialize(array_merge(
            [
                'entityId' => null,
                'addonsId' => null,
            ],
            $this->sigFields()
        )));
    }

    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), $this->sigFields());
    }

    private function sigFields(): array
    {
        return [
            'productId' => $this->productId,
            'uid' => $this->uid,
            'categoryId' => $this->categoryId,
            'name' => $this->name,
            'mediaIds' => $this->mediaIds,
            'videos' => $this->videos,
            'articles' => array_map(function ($articlePlate) {
                return $articlePlate->jsonSerialize();
            }, $this->articles),
        ];
    }
}
