<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


use phpDocumentor\Reflection\Types\Mixed_;

class AttributeStruct extends AbstractStruct
{
    public ?string $type = null;

    public ?string $key = null;

    public ?string $value = null;

    public ?string $unit = null;

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getKey(): ?string
    {
        return $this->key;
    }

    /**
     * @param string|null $key
     */
    public function setKey(?string $key): void
    {
        $this->key = $key;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     */
    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return string|null
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }

    /**
     * @param string|null $unit
     */
    public function setUnit(?string $unit): void
    {
        $this->unit = $unit;
    }

    public function jsonSerialize(): array
    {
        $data = [
            'type' => $this->type,
            'key' => $this->key,
            'value' => $this->value,
        ];
        // note this is for bc of hashes generated with versions prior to v3
        if (!empty($this->unit)) {
            $data['unit'] = $this->unit;
        }

        return $data;
    }
}
