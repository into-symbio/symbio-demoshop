<?php declare(strict_types=1);

namespace IntoSymbioConnector\Struct;

abstract class AbstractPlate extends AbstractStruct
{
    const CREATE = 1;
    const UPDATE = 2;
    const DELETE = 3;

    public int $method;

    public ?string $entityId = null; // Shopware UUID for synced entities

    public ?string $addonsId = null; // Shopware UUID for the entity addons record

    /**
     * @return int
     */
    public function getMethod(): int
    {
        return $this->method;
    }

    /**
     * @param int $method
     */
    public function setMethod(int $method): void
    {
        $this->method = $method;
    }

    /**
     * @return string|null
     */
    public function getEntityId(): ?string
    {
        return $this->entityId;
    }

    /**
     * @param string|null $entityId
     */
    public function setEntityId(?string $entityId): void
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string|null
     */
    public function getAddonsId(): ?string
    {
        return $this->addonsId;
    }

    /**
     * @param string|null $addonsId
     */
    public function setAddonsId(?string $addonsId): void
    {
        $this->addonsId = $addonsId;
    }

    public function getChecksum(): string
    {
        $clone = clone($this);
        unset($clone->method);

        return md5(serialize($clone->jsonSerialize()));
    }

    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), get_object_vars($this));
    }

    public function validate(): void
    {
    }
}
