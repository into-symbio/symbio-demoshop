<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


abstract class AbstractPlatesCollection implements \IteratorAggregate
{
    /**
     * @var AbstractPlate[]
     */
    protected array $elements = [];

    abstract public function add(AbstractPlate $element): void;

    /**
     * @param int|string $key
     * @return AbstractPlate|null
     */
    public function get($key): ?AbstractPlate
    {
        if (array_key_exists($key, $this->elements)) {
            return $this->elements[$key];
        }

        return null;
    }

    /**
     * @param int|string $key
     * @return bool
     */
    public function has($key): bool
    {
        return \array_key_exists($key, $this->elements);
    }

    public function count(): int
    {
        return \count($this->elements);
    }

    public function getIterator(): \Generator
    {
        yield from $this->elements;
    }
}
