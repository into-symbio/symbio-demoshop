<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


class ProductPlatesCollection extends AbstractPlatesCollection
{
    /**
     * @param ProductPlate $element
     */
    public function add($element): void
    {
        $this->elements[$element->getProductId()] = $element;
    }
}
