<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


use IntoSymbioConnector\Exception\InvalidPlateException;

class OrderPlate extends AbstractPlate
{
    private array $additionalOrderPayload = [];

    private ?int $billingAddressId = null;

    private string $currency = '';

    private ?string $externalMarketplaceReference = null;

    private ?string $externalPaymentReference = null;

    private ?string $externalShopReference = null;

    /**
     * @var OrderItemPlate[]
     */
    private array $orderItems = [];

    private string $orderOrigin = '';

    private ?int $recipientId = null;

    private ?string $remark = null;

    private ?string $requestedDeliveryDate = null;

    private ?int $shippingAddressId = null;

    /**
     * @return array
     */
    public function getAdditionalOrderPayload(): array
    {
        return $this->additionalOrderPayload;
    }

    /**
     * @param array $additionalOrderPayload
     */
    public function setAdditionalOrderPayload(array $additionalOrderPayload): void
    {
        $this->additionalOrderPayload = $additionalOrderPayload;
    }

    /**
     * @param string $key
     * @param float|int|string $value
     */
    public function addAdditionalOrderPayload(string $key, $value): void
    {
        $this->additionalOrderPayload[$key] = $value;
    }

    /**
     * @return int|null
     */
    public function getBillingAddressId(): ?int
    {
        return $this->billingAddressId;
    }

    /**
     * @param int|null $billingAddressId
     */
    public function setBillingAddressId(?int $billingAddressId): void
    {
        $this->billingAddressId = $billingAddressId;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string|null
     */
    public function getExternalMarketplaceReference(): ?string
    {
        return $this->externalMarketplaceReference;
    }

    /**
     * @param string|null $externalMarketplaceReference
     */
    public function setExternalMarketplaceReference(?string $externalMarketplaceReference): void
    {
        $this->externalMarketplaceReference = $externalMarketplaceReference;
    }

    /**
     * @return string|null
     */
    public function getExternalPaymentReference(): ?string
    {
        return $this->externalPaymentReference;
    }

    /**
     * @param string|null $externalPaymentReference
     */
    public function setExternalPaymentReference(?string $externalPaymentReference): void
    {
        $this->externalPaymentReference = $externalPaymentReference;
    }

    /**
     * @return string|null
     */
    public function getExternalShopReference(): ?string
    {
        return $this->externalShopReference;
    }

    /**
     * @param string|null $externalShopReference
     */
    public function setExternalShopReference(?string $externalShopReference): void
    {
        $this->externalShopReference = $externalShopReference;
    }

    /**
     * @return OrderItemPlate[]
     */
    public function getOrderItems(): array
    {
        return $this->orderItems;
    }

    /**
     * @param OrderItemPlate[] $orderItems
     */
    public function setOrderItems(array $orderItems): void
    {
        $this->orderItems = $orderItems;
    }

    /**
     * @param OrderItemPlate $orderItemPlate
     */
    public function addOrderItem(OrderItemPlate $orderItemPlate): void
    {
        $this->orderItems[] = $orderItemPlate;
    }

    /**
     * @return string
     */
    public function getOrderOrigin(): string
    {
        return $this->orderOrigin;
    }

    /**
     * @param string $orderOrigin
     */
    public function setOrderOrigin(string $orderOrigin): void
    {
        $this->orderOrigin = $orderOrigin;
    }

    /**
     * @return int|null
     */
    public function getRecipientId(): ?int
    {
        return $this->recipientId;
    }

    /**
     * @param int|null $recipientId
     */
    public function setRecipientId(?int $recipientId): void
    {
        $this->recipientId = $recipientId;
    }

    /**
     * @return string|null
     */
    public function getRemark(): ?string
    {
        return $this->remark;
    }

    /**
     * @param string|null $remark
     */
    public function setRemark(?string $remark): void
    {
        $this->remark = $remark;
    }

    /**
     * @return string|null
     */
    public function getRequestedDeliveryDate(): ?string
    {
        return $this->requestedDeliveryDate;
    }

    /**
     * @param string|null $requestedDeliveryDate
     */
    public function setRequestedDeliveryDate(?string $requestedDeliveryDate): void
    {
        $this->requestedDeliveryDate = $requestedDeliveryDate;
    }

    /**
     * @return int|null
     */
    public function getShippingAddressId(): ?int
    {
        return $this->shippingAddressId;
    }

    /**
     * @param int|null $shippingAddressId
     */
    public function setShippingAddressId(?int $shippingAddressId): void
    {
        $this->shippingAddressId = $shippingAddressId;
    }

    /**
     * @return array
     * @throws InvalidPlateException
     */
    public function jsonSerialize(): array
    {
        $this->validate();

        return [
            'additionalOrderPayload' => \json_encode($this->additionalOrderPayload),
            'billingAddressId' => $this->billingAddressId,
            'shippingAddressId' => $this->shippingAddressId,
            'currency' => $this->currency,
            'externalMarketplaceReference' => $this->externalMarketplaceReference,
            'externalPaymentReference' => $this->externalPaymentReference,
            'externalShopReference' => $this->externalShopReference,
            'orderItems' => array_map(function ($orderItem) {
                return $orderItem->jsonSerialize();
            }, $this->orderItems),
            'recipientId' => $this->recipientId,
            'remark' => $this->remark,
            'orderOrigin' => $this->orderOrigin,
        ];
    }
}
