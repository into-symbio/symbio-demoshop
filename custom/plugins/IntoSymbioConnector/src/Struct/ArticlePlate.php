<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


class ArticlePlate extends AbstractPlate
{
    private bool $dirty = false;

    private ?int $productId = null;

    private ?int $articleId = null;

    private string $uid = '';

    private string $sku = '';

    private ?string $name = null;

    private ?string $description = null;

    private ?string $gtin = null;

    private ?int $articleQuantityScale = null;

    private ?float $basePriceFactor = null;

    private ?int $minimumArticleQuantity = null;

    private ?float $netWeight = null;

    private ?string $netWeightUnit = null;

    private ?string $countryOfOrigin = null;

    private ?string $customTariffNumber = null;

    /**
     * @var AttributeStruct[]
     */
    private array $attributeStructs = [];

    private array $mediaIds = [];

    private array $videos = [];

    /**
     * @var PriceStruct[];
     */
    private array $priceStructs = [];

    /**
     * @return bool
     */
    public function isDirty(): bool
    {
        return $this->dirty;
    }

    /**
     * @param bool $dirty
     */
    public function setDirty(bool $dirty): void
    {
        $this->dirty = $dirty;
    }

    /**
     * @return int|null
     */
    public function getProductId(): ?int
    {
        return $this->productId;
    }

    /**
     * @param int|null $productId
     */
    public function setProductId(?int $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return int|null
     */
    public function getArticleId(): ?int
    {
        return $this->articleId;
    }

    /**
     * @param int|null $articleId
     */
    public function setArticleId(?int $articleId): void
    {
        $this->articleId = $articleId;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku(string $sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getGtin(): ?string
    {
        return $this->gtin;
    }

    /**
     * @param string|null $gtin
     */
    public function setGtin(?string $gtin): void
    {
        $this->gtin = $gtin;
    }

    /**
     * @return int|null
     */
    public function getArticleQuantityScale(): ?int
    {
        return $this->articleQuantityScale;
    }

    /**
     * @param int|null $articleQuantityScale
     */
    public function setArticleQuantityScale(?int $articleQuantityScale): void
    {
        $this->articleQuantityScale = $articleQuantityScale;
    }

    /**
     * @return float|null
     */
    public function getBasePriceFactor(): ?float
    {
        return $this->basePriceFactor;
    }

    /**
     * @param float|null $basePriceFactor
     */
    public function setBasePriceFactor(?float $basePriceFactor): void
    {
        $this->basePriceFactor = $basePriceFactor;
    }

    /**
     * @return int|null
     */
    public function getMinimumArticleQuantity(): ?int
    {
        return $this->minimumArticleQuantity;
    }

    /**
     * @param int|null $minimumArticleQuantity
     */
    public function setMinimumArticleQuantity(?int $minimumArticleQuantity): void
    {
        $this->minimumArticleQuantity = $minimumArticleQuantity;
    }

    /**
     * @return float|null
     */
    public function getNetWeight(): ?float
    {
        return $this->netWeight;
    }

    /**
     * @param float|null $netWeight
     */
    public function setNetWeight(?float $netWeight): void
    {
        $this->netWeight = $netWeight;
    }

    /**
     * @return string|null
     */
    public function getNetWeightUnit(): ?string
    {
        return $this->netWeightUnit;
    }

    /**
     * @param string|null $netWeightUnit
     */
    public function setNetWeightUnit(?string $netWeightUnit): void
    {
        $this->netWeightUnit = $netWeightUnit;
    }

    /**
     * @return string|null
     */
    public function getCountryOfOrigin(): ?string
    {
        return $this->countryOfOrigin;
    }

    /**
     * @param string|null $countryOfOrigin
     */
    public function setCountryOfOrigin(?string $countryOfOrigin): void
    {
        $this->countryOfOrigin = $countryOfOrigin;
    }

    /**
     * @return string|null
     */
    public function getCustomTariffNumber(): ?string
    {
        return $this->customTariffNumber;
    }

    /**
     * @param string|null $customTariffNumber
     */
    public function setCustomTariffNumber(?string $customTariffNumber): void
    {
        $this->customTariffNumber = $customTariffNumber;
    }

    /**
     * @return AttributeStruct[]
     */
    public function getAttributeStructs(): array
    {
        return $this->attributeStructs;
    }

    /**
     * @param AttributeStruct[] $attributeStructs
     */
    public function setAttributeStructs(array $attributeStructs): void
    {
        $this->attributeStructs = $attributeStructs;
    }

    /**
     * @param AttributeStruct $attributeStruct
     */
    public function addAttributeStruct(AttributeStruct $attributeStruct): void
    {
        $this->attributeStructs[] = $attributeStruct;
    }

    /**
     * @return array
     */
    public function getMediaIds(): array
    {
        return $this->mediaIds;
    }

    /**
     * @param array $mediaIds
     */
    public function setMediaIds(array $mediaIds): void
    {
        $this->mediaIds = $mediaIds;
    }

    /**
     * @param int $id
     */
    public function addMediaId(int $id): void
    {
        $this->mediaIds[] = $id;
    }

    /**
     * @return array
     */
    public function getVideos(): array
    {
        return $this->videos;
    }

    /**
     * @param array $videos
     */
    public function setVideos(array $videos): void
    {
        $this->videos = $videos;
    }

    /**
     * @param array $video
     */
    public function addVideo(array $video): void
    {
        $this->videos[] = $video;
    }

    /**
     * @return PriceStruct[]
     */
    public function getPriceStructs(): array
    {
        return $this->priceStructs;
    }

    /**
     * @param PriceStruct[] $priceStructs
     */
    public function setPriceStructs(array $priceStructs): void
    {
        $this->priceStructs = $priceStructs;
    }

    public function addPriceStruct(PriceStruct $priceStruct): void
    {
        $this->priceStructs[] = $priceStruct;
    }

    public function getFirstPriceStruct(): ?PriceStruct
    {
        if (empty($this->priceStructs)) {

            return null;
        }

        return $this->priceStructs[0];
    }

    public function fromArray(array $array): AbstractStruct
    {
        parent::fromArray($array);
        foreach ($array['media'] as $media) {
            if ($media['type'] === MediaPlate::VIDEO) {
                $this->addVideo($media);
                continue;
            }
            $this->addMediaId($media['id']);
        }
        foreach ($array['distributionInfo'] as $price) {
            $priceStruct = new PriceStruct();
            $priceStruct->fromArray($price);
            $this->addPriceStruct($priceStruct);
        }
        foreach ($array['attributes'] as $attribute) {
            $attributeStruct = new AttributeStruct();
            $attributeStruct->fromArray($attribute);
            $this->addAttributeStruct($attributeStruct);
        }

        return $this;
    }

    public function jsonSerialize(): array
    {

        return [
            'productId' => $this->productId,
            'articleId' => $this->articleId,
            'uid' => $this->uid,
            'sku' => $this->sku,
            'name' => $this->name,
            'description' => $this->description,
            'gtin' => $this->gtin,
            'articleQuantityScale' => $this->articleQuantityScale,
            'basePriceFactor' => $this->basePriceFactor,
            'minimumArticleQuantity' => $this->minimumArticleQuantity,
            'netWeight' => $this->netWeight,
            'netWeightUnit' => $this->netWeightUnit,
            'countryOfOrigin' => $this->countryOfOrigin,
            'customTariffNumber' => $this->customTariffNumber,
            'attributeStructs' => array_map(function ($attributeStruct) {
                return $attributeStruct->jsonSerialize();
            }, $this->attributeStructs),
            'mediaIds' => $this->mediaIds,
            'videos' => $this->videos,
            'priceStructs' => array_map(function ($priceStruct) {
                return $priceStruct->jsonSerialize();
            }, $this->priceStructs),
        ];
    }
}
