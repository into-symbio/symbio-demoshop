<?php declare(strict_types=1);

namespace IntoSymbioConnector\Struct;

use IntoSymbioConnector\Exception\InvalidPlateException;

class AddressPlate extends AbstractPlate
{
    private const NAME = 'Address';

    public const TYPE_MAIN = 'MAIN';
    public const TYPE_BILLING = 'BILLING';
    public const TYPE_OTHER = 'SHIPPING';

    private ?int $id = null;

    private ?int $recipientId = null;

    private string $type = '';

    private string $name = '';

    private string $street = '';

    private string $streetNumber = '';

    private string $additional = '';

    private string $zipCode = '';

    private string $city = '';

    private string $country = '';

    private string $phoneNumber = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getRecipientId(): ?int
    {
        return $this->recipientId;
    }

    /**
     * @param int|null $recipientId
     */
    public function setRecipientId(?int $recipientId): void
    {
        $this->recipientId = $recipientId;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getStreetNumber(): string
    {
        return $this->streetNumber;
    }

    /**
     * @param string $streetNumber
     */
    public function setStreetNumber(string $streetNumber): void
    {
        $this->streetNumber = $streetNumber;
    }

    /**
     * @return string
     */
    public function getAdditional(): string
    {
        return $this->additional;
    }

    /**
     * @param string $additional
     */
    public function setAdditional(string $additional): void
    {
        $this->additional = $additional;
    }

    /**
     * @return string
     */
    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode(string $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @inheritDoc
     */
    public function validate(): void
    {
        $errors = [];
        if (!in_array($this->type, [self::TYPE_BILLING, self::TYPE_MAIN, self::TYPE_OTHER])) {
            $errors[] = ['type', $this->type, ' must be one of BILLING, MAIN, SHIPPING'];
        }
        if (strlen($this->name) > 255) {
            $errors[] = ['name', $this->name, ' is required and must not exceed 255 chars.'];
        }
        if (count($errors) > 0) {

            throw new InvalidPlateException(self::NAME, $errors);
        }
    }

    public function jsonSerialize(): array
    {
        $this->validate();

        return [
            'id' => $this->id,
            'recipientId' => $this->recipientId,
            'type' => $this->type,
            'name' => $this->name,
            'street' => $this->street,
            'streetNumber' => $this->streetNumber,
            'additional' => $this->additional,
            'zipCode' => $this->zipCode,
            'city' => $this->city,
            'country' => $this->country,
            'phoneNumber' => $this->phoneNumber,
        ];
    }
}
