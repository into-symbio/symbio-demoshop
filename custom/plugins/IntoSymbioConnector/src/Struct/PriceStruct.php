<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


class PriceStruct extends AbstractStruct
{
    private ?string $countryIso2 = null;

    private ?string $currency = null;

    private ?float $price = null;

    private ?float $singleUnitPrice = null;

    private ?int $articleQuantity = null;

    private ?string $articleUnit = null;

    private ?float $minPricePct = null;

    private ?string $distributorSku = null;

    private ?int $articleSalesId = null;

    /**
     * @return string|null
     */
    public function getCountryIso2(): ?string
    {
        return $this->countryIso2;
    }

    /**
     * @param string|null $countryIso2
     */
    public function setCountryIso2(?string $countryIso2): void
    {
        $this->countryIso2 = $countryIso2;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     */
    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return float|null
     */
    public function getSingleUnitPrice(): ?float
    {
        return $this->singleUnitPrice;
    }

    /**
     * @param float|null $singleUnitPrice
     */
    public function setSingleUnitPrice(?float $singleUnitPrice): void
    {
        $this->singleUnitPrice = $singleUnitPrice;
    }

    /**
     * @return int|null
     */
    public function getArticleQuantity(): ?int
    {
        return $this->articleQuantity;
    }

    /**
     * @param int|null $articleQuantity
     */
    public function setArticleQuantity(?int $articleQuantity): void
    {
        $this->articleQuantity = $articleQuantity;
    }

    /**
     * @return string|null
     */
    public function getArticleUnit(): ?string
    {
        return $this->articleUnit;
    }

    /**
     * @param string|null $articleUnit
     */
    public function setArticleUnit(?string $articleUnit): void
    {
        $this->articleUnit = $articleUnit;
    }

    /**
     * @return float|null
     */
    public function getMinPricePct(): ?float
    {
        return $this->minPricePct;
    }

    /**
     * @param float|null $minPricePct
     */
    public function setMinPricePct(?float $minPricePct): void
    {
        $this->minPricePct = $minPricePct;
    }

    /**
     * @return string|null
     */
    public function getDistributorSku(): ?string
    {
        return $this->distributorSku;
    }

    /**
     * @param string|null $distributorSku
     */
    public function setDistributorSku(?string $distributorSku): void
    {
        $this->distributorSku = $distributorSku;
    }

    /**
     * @return int|null
     */
    public function getArticleSalesId(): ?int
    {
        return $this->articleSalesId;
    }

    /**
     * @param int|null $articleSalesId
     */
    public function setArticleSalesId(?int $articleSalesId): void
    {
        $this->articleSalesId = $articleSalesId;
    }

    public function jsonSerialize(): array
    {

        return array_merge(parent::jsonSerialize(), [
            'countryIso2' => $this->countryIso2,
            'currency' => $this->currency,
            'price' => $this->price,
            'singleUnitPrice' => $this->singleUnitPrice,
            'articleQuantity' => $this->articleQuantity,
            'articleUnit' => $this->articleUnit,
            'minPricePct' => $this->minPricePct,
            'distributorSku' => $this->distributorSku,
            'articleSalesId' => $this->articleSalesId,
        ]);
    }
}
