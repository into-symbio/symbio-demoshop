<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


use Shopware\Core\Framework\Api\Context\ContextSource;

class DownstreamMessageStruct extends AbstractStruct
{
    private string $event;

    private string $entity;

    private string $id;

    private string $operation;

    private string $salesChannelId = '';

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @param string $event
     */
    public function setEvent(string $event): void
    {
        $this->event = $event;
    }

    /**
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity(string $entity): void
    {
        $this->entity = $entity;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOperation(): string
    {
        return $this->operation;
    }

    /**
     * @param string $operation
     */
    public function setOperation(string $operation): void
    {
        $this->operation = $operation;
    }

    /**
     * @return string
     */
    public function getSalesChannelId(): string
    {
        return $this->salesChannelId;
    }

    /**
     * @param string $salesChannelId
     */
    public function setSalesChannelId(string $salesChannelId): void
    {
        $this->salesChannelId = $salesChannelId;
    }

    public function jsonSerialize(): array
    {
        return [
            'event' => $this->event,
            'entity' => $this->entity,
            'id' => $this->id,
            'operation' => $this->operation,
            'salesChannelId' => $this->salesChannelId,
        ];
    }
}
