<?php declare(strict_types=1);


namespace IntoSymbioConnector\Struct;


class OrderItemPlate extends AbstractPlate
{
    private int $orderPosition = 0;

    private int $orderQuantity;

    private int $articleSalesId = 0;

    private string $remark = '';

    private float $salesPriceAbsolute = 0.0;

    private string $uid = '';

    /**
     * @return int
     */
    public function getOrderPosition(): int
    {
        return $this->orderPosition;
    }

    /**
     * @param int $orderPosition
     */
    public function setOrderPosition(int $orderPosition): void
    {
        $this->orderPosition = $orderPosition;
    }

    /**
     * @return int
     */
    public function getOrderQuantity(): int
    {
        return $this->orderQuantity;
    }

    /**
     * @param int $orderQuantity
     */
    public function setOrderQuantity(int $orderQuantity): void
    {
        $this->orderQuantity = $orderQuantity;
    }

    /**
     * @return int
     */
    public function getArticleSalesId(): int
    {
        return $this->articleSalesId;
    }

    /**
     * @param int $articleSalesId
     */
    public function setArticleSalesId(int $articleSalesId): void
    {
        $this->articleSalesId = $articleSalesId;
    }

    /**
     * @return string
     */
    public function getRemark(): string
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark(string $remark): void
    {
        $this->remark = $remark;
    }

    /**
     * @return float
     */
    public function getSalesPriceAbsolute(): float
    {
        return $this->salesPriceAbsolute;
    }

    /**
     * @param float $salesPriceAbsolute
     */
    public function setSalesPriceAbsolute(float $salesPriceAbsolute): void
    {
        $this->salesPriceAbsolute = $salesPriceAbsolute;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid): void
    {
        $this->uid = $uid;
    }

    public function jsonSerialize(): array
    {
        $this->validate();

        return [
            'orderPosition' => $this->orderPosition,
            'orderQuantity' => $this->orderQuantity,
            'articleSalesId' => $this->articleSalesId,
            'remark' => $this->remark,
            'salesPriceAbsolute' => $this->salesPriceAbsolute,
            'uid' => $this->uid,
        ];
    }
}
