<?php declare(strict_types=1);

namespace Into\IntoOrderFilter\Subscriber;
use Shopware\Core\Checkout\Order\OrderEvents;
use Shopware\Core\Checkout\Payment\PaymentMethodEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class Order implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return [
              OrderEvents::ORDER_LOADED_EVENT => 'addDistinguishablePaymentName',
        ];
    }

	public function addDistinguishablePaymentName(EntityLoadedEvent $event): void
    {
        foreach ($event->getEntities() as $payment) {
            if($payment->hasExtension('intoSymbioConnectorAddons')) {
                $payment->setCustomFields(['symbioOrderId' => $payment->getExtensions()['intoSymbioConnectorAddons']->getSymbioOrderId()]);
            }
        }
    }
}
