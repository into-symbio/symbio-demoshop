// import template from './sw-order-list.html.twig';
// ~ import './sw-order-list.scss';
import deDE from "./snippet/en-GB.json";
import enGB from "./snippet/de-DE.json";
const { Component, Mixin } = Shopware;
const { Criteria } = Shopware.Data;

Component.override('sw-order-list', {
    inject: ['repositoryFactory', 'filterFactory','MagnalisterOrderService'],

    computed: {
        // listFilters() {
        //     const filters = this.$super('listFilters');
        //     const intoSymbioFilter = this.filterFactory.create('order',
        //     {
        //         'line-item-filter-testorder':
        //         {
        //             property: 'intoSymbioConnectorAddons',
        //             label: this.$tc('into-order-filter.general.title'),
        //             placeholder: this.$tc('into-order-filter.general.selectption'),
        //             optionHasCriteria: this.$tc('into-order-filter.general.onlylabel'),
        //             optionNoCriteria: this.$tc('into-order-filter.general.withoutlabel'),
        //         }
        //     } ).pop();
        //
        //     filters.push(intoSymbioFilter);
        //
        //     return filters;
        // },
    },
    methods: {
        createdComponent() {
        //     this.defaultFilters.push('line-item-filter-testorder');
            return this.$super('createdComponent');
        },
        getOrderColumns() {
            const columns = this.$super('getOrderColumns');
            columns.push(
                {
                    property: 'customFields.symbioOrderId',
                    inlineEdit: 'string',
                    label: 'Symbio Order-Id',
                    allowResize: true,
                    visible: false,
                });
            return columns;
        },
    },


});
