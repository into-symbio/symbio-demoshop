import template from './sw-cms-preview-category-listing.html.twig';
import './sw-cms-preview-category-listing.scss';

Shopware.Component.register('sw-cms-preview-category-listing', {
    template,
});
