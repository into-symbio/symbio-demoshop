import template from './sw-cms-el-preview-category-listing.html.twig';
import './sw-cms-el-preview-category-listing.scss';

Shopware.Component.register('sw-cms-el-preview-category-listing', {
    template,
});
