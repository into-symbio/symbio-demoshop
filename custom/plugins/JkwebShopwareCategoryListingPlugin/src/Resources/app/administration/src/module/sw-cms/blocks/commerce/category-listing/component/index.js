import template from './sw-cms-block-category-listing.html.twig';
import './sw-cms-block-category-listing.scss';

Shopware.Component.register('sw-cms-block-category-listing', {
    template,
});
