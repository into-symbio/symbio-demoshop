# 1.0.7
- Fix category links

# 1.0.6
- Compatibility 6.4.10.0

# 1.0.5
- Compatibility 6.4.0.0
