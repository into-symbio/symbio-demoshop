<?php
declare(strict_types=1);
/**
 * LeopardSearchExtension
 * Copyright (c) Die Leoparden GmbH
 */

namespace LeopardSearchExtension\Subscriber;

use LeopardSearchExtension\LeopardSearchExtension;
use Shopware\Core\Content\Product\Events\ProductSearchCriteriaEvent;
use Shopware\Core\Content\Product\ProductEntity;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ProductSearchCriteriaSubscriber
 */
class ProductSearchCriteriaSubscriber extends AbstractSubscriber
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ProductSearchCriteriaEvent::class => 'onProductSearchCriteria',
        ];
    }

    /**
     * @param ProductSearchCriteriaEvent $event
     *
     * @return void
     */
    public function onProductSearchCriteria(ProductSearchCriteriaEvent $event): void
    {
        /** @var ProductEntity|null $productEntity */
        $productEntity = $this->onSearchCriteria($event);

        if (!$productEntity instanceof ProductEntity) {
            return;
        }
        $event
            ->getRequest()
            ->attributes
            ->set(LeopardSearchExtension::KERNEL_RESPONSE_ATTRIBUTE, true);

        (new RedirectResponse($this->getRouter()
            ->generate('frontend.detail.page', ['productId' => $productEntity->getId()])
        ))->send();
    }
}
