<?php
declare(strict_types=1);
/**
 * LeopardSearchExtension
 * Copyright (c) Die Leoparden GmbH
 */

namespace LeopardSearchExtension\Subscriber;

use Shopware\Core\Content\Product\Events\ProductListingCriteriaEvent;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\OrFilter;
use Shopware\Core\System\CustomField\CustomFieldEntity;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class AbstractSubscriber
 */
abstract class AbstractSubscriber implements EventSubscriberInterface
{
    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    /**
     * @var EntityRepository
     */
    private $productRepository;

    /**
     * @var EntityRepository
     */
    private $customFieldRepository;
    /**
     * @var RouterInterface
     */
    private RouterInterface $router;

    /**
     * AbstractSubscriber constructor.
     *
     * @param SystemConfigService $systemConfigService
     * @param EntityRepository    $productRepository
     * @param EntityRepository    $customFieldRepository
     * @param RouterInterface     $router
     */
    public function __construct(SystemConfigService $systemConfigService, EntityRepository $productRepository, EntityRepository $customFieldRepository, RouterInterface $router)
    {
        $this->systemConfigService = $systemConfigService;
        $this->productRepository = $productRepository;
        $this->customFieldRepository = $customFieldRepository;
        $this->router = $router;
    }

    /**
     * @param ProductListingCriteriaEvent $event
     *
     * @return ProductEntity|null
     */
    public function onSearchCriteria(ProductListingCriteriaEvent $event): ?ProductEntity
    {
        /** @var mixed[] $config */
        $config = $this->getConfig($event->getSalesChannelContext());
        if (empty($config['activated'])) {
            return null;
        }

        /** @var Request $request */
        $request = $event->getRequest();

        /** @var string $searchTerm */
        $searchTerm = $request->get('search');
        if (!is_string($searchTerm)) {
            return null;
        }
        $searchTerm = trim($searchTerm);
        if (empty($searchTerm)) {
            return null;
        }

        /** @var string[] $filterTargets */
        $filterTargets = $config['filterTargets'];
        if (!is_array($filterTargets)) {
            $filterTargets = [];
        }

        /** @var string[] $filterTargetsCustomFieldsIds */
        $filterTargetsCustomFieldsIds = $config['filterTargetsCustomFieldsIds'] ?? null;

        if (is_array($filterTargetsCustomFieldsIds) && count($filterTargetsCustomFieldsIds) >= 1) {
            $filterTargets = [
                ...$filterTargets,
                ...array_map(
                    function ($customFieldEntity) {
                        /* @var CustomFieldEntity $customFieldEntity */
                        return "product.translations.customFields.{$customFieldEntity->getName()}";
                    },
                    array_values($this->loadCustomFieldsEntity($filterTargetsCustomFieldsIds, $event->getContext()))
                ),
            ];
        }

        if (empty($filterTargets)) {
            return null;
        }

        /** @var string[] $filterValues */
        $filterValues = [$searchTerm];
        if (!empty($config['alphanumericOnlySearch'])) {
            $alphanumericOnlySearchTerm = preg_replace("/[^A-Za-z\d]/", '', $searchTerm);
            if (is_string($alphanumericOnlySearchTerm)) {
                $filterValues[] = $alphanumericOnlySearchTerm;
            }
        }

        /** @var ProductEntity|null $productEntity */
        $productEntity = $this->loadProductEntity($filterTargets, $filterValues, $event->getContext(), $config['findParent']);

        if (!$productEntity instanceof ProductEntity) {
            return null;
        }

        return $productEntity;
    }

    /**
     * @return EntityRepository
     */
    public function getCustomFieldRepository(): EntityRepository
    {
        return $this->customFieldRepository;
    }

    /**
     * @param SalesChannelContext $salesChannelContext
     *
     * @return mixed[]
     */
    protected function getConfig(SalesChannelContext $salesChannelContext): array
    {
        /** @var mixed[]|bool|float|int|string|null $config */
        $config = $this->getSystemConfigService()->get('LeopardSearchExtension.config', $salesChannelContext->getSalesChannel()->getId());

        return is_array($config) ? $config : [];
    }

    /**
     * @return SystemConfigService
     */
    protected function getSystemConfigService(): SystemConfigService
    {
        return $this->systemConfigService;
    }

    /**
     * @param string[] $customFieldsIds
     * @param Context  $context
     *
     * @return CustomFieldEntity[]
     */
    protected function loadCustomFieldsEntity(array $customFieldsIds, Context $context): array
    {
        return $this
            ->getCustomFieldRepository()
            ->search(
                (new Criteria())
                    ->addFilter(new EqualsAnyFilter('id', $customFieldsIds)),
                $context)
            ->getElements();
    }

    /**
     * @param string[] $filterFields
     * @param string[] $filterValues
     * @param Context  $context
     * @param bool     $findParent
     *
     * @return ProductEntity|null
     */
    protected function loadProductEntity(array $filterFields, array $filterValues, Context $context, bool $findParent): ?ProductEntity
    {
        /** @var OrFilter $filter */
        $filter = new OrFilter(array_map(function (string $field) use ($filterValues) {
            return new EqualsAnyFilter($field, $filterValues);
        }, $filterFields));

        /** @var Criteria $criteria */
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('product.active', true))
            ->addFilter($filter)
            ->setLimit(2);

        if (!$findParent) {
            $criteria
                ->addFilter(new OrFilter([
                        new EqualsFilter('childCount', null),
                        new EqualsFilter('childCount', 0),
                    ]
                ));
        }

        /** @var EntitySearchResult $entitySearchResult */
        $entitySearchResult = $this->getProductRepository()->search($criteria, $context);

        return $entitySearchResult->count() === 1 ? $entitySearchResult->first() : null;
    }

    /**
     * @return EntityRepository
     */
    protected function getProductRepository(): EntityRepository
    {
        return $this->productRepository;
    }

    /**
     * @return RouterInterface
     */
    protected function getRouter(): RouterInterface
    {
        return $this->router;
    }
}
