<?php
declare(strict_types=1);
/**
 * LeopardSearchExtension
 * Copyright (c) Die Leoparden GmbH
 */

namespace LeopardSearchExtension\Subscriber;

use LeopardSearchExtension\LeopardSearchExtension;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ProductSuggestCriteriaSubscriber
 */
class KernelResponseSubscriber extends AbstractSubscriber
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        if ($event->getRequest()->attributes->has(LeopardSearchExtension::KERNEL_RESPONSE_ATTRIBUTE)) {
            $event
                ->getResponse()
                ->headers
                ->add(['Cache-Control' => 'no-store, private']);
        }
    }
}
