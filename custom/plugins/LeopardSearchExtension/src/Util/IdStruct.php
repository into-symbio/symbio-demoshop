<?php
/**
 * LeopardSearchExtension
 * Copyright (c) Die Leoparden GmbH
 */

namespace LeopardSearchExtension\Util;

use Shopware\Core\Framework\Struct\Struct;

class IdStruct extends Struct
{
    /**
     * @var string
     */
    public $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }
}
