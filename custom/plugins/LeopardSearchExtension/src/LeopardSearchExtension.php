<?php
declare(strict_types=1);
/**
 * LeopardSearchExtension
 * Copyright (c) Die Leoparden GmbH
 */

namespace LeopardSearchExtension;

use Shopware\Core\Framework\Plugin;

/**
 * Class LeopardSearchExtension
 */
class LeopardSearchExtension extends Plugin
{
    public const KERNEL_RESPONSE_ATTRIBUTE = 'LEOPARD_SEARCH_EXTENSION';
    public const SUGGEST_EXTENSION_PRODUCT_ID = 'LEOPARD_SUGGEST_EXTENSION_PRODUCT_ID';
}
