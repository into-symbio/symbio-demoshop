<?php

namespace IntoMainVariant\Command;

use IntoMainVariant\Service\ProductService;
use IntoMainVariant\Service\ProductTranslationService;
use Shopware\Core\Content\Product\ProductEntity;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class IntoMainVariantCommand extends Command
{
    protected static $defaultName = 'into:set-main-variant';

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var ProductTranslationService
     */
    private $productTranslationService;

    public function __construct(
        ProductService $productService,
        ProductTranslationService  $productTranslationService
    )
    {
        parent::__construct();
        $this->productService = $productService;
        $this->productTranslationService = $productTranslationService;
    }

    public function configure()
    {
        $this->setDescription('Populate column main_variant_id');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $masterProducts = $this->productService->getMasterProducts();
        foreach ($masterProducts as $masterProduct) {
            /**
             * @var ProductEntity $masterProduct
             */
            $masterProductId = $masterProduct->getId();
            $output->writeln('Set custom_field in table product_translation for product:' . $masterProduct->getProductNumber());
            $this->productTranslationService->updateCustomFieldsByProductId($masterProduct);
            $output->writeln('Set main_variant_id for product:' . $masterProduct->getProductNumber());
            $productVariants = $this->productService->getProductVariantsByParentId($masterProductId);
            if ($productVariants->count() != 0) {
                /**
                 * @var ProductEntity $masterProduct
                 */
                $firstProductVariant = $productVariants->first();
                $firstProductVariantId = $firstProductVariant->getId();

                $this->productService->updateProductMasterMainVariantIdByVariantId($masterProduct, $firstProductVariantId);
            }
        }
        return 0;
    }
}