<?php

namespace IntoMainVariant\Service;

use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;

class ProductService
{
    /**
     * @var EntityRepositoryInterface
     */
    private $productRepository;

    public function __construct(
        EntityRepositoryInterface $productRepository
    )
    {
        $this->productRepository = $productRepository;
    }

    public function getMasterProducts()
    {
        $context = Context::createDefaultContext();
        $criteria = new Criteria();
        $criteria->addFilter(
            new ContainsFilter('productNumber', '_Master')
        );

        return $this->productRepository->search($criteria, $context);
    }

    public function getProductVariantsByParentId($id)
    {
        $context = Context::createDefaultContext();
        $criteria = new Criteria();
        $criteria->addFilter(
            new EqualsFilter('parentId', $id)
        );

        return $this->productRepository->search($criteria, $context);
    }

    public function updateProductMasterMainVariantIdByVariantId(ProductEntity $productEntity, $variantId): void
    {
        $context = Context::createDefaultContext();
        $this->productRepository->update([
            [
                'id' => $productEntity->getId(),
                'mainVariantId' => $variantId
            ]
        ], $context);
    }

}