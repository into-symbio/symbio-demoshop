<?php

namespace IntoMainVariant\Service;

use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;

class ProductTranslationService
{
    /**
     * @var EntityRepositoryInterface
     */
    private $productTranslationRepository;

    public function __construct(
        EntityRepositoryInterface $productTranslationRepository
    )
    {
        $this->productTranslationRepository = $productTranslationRepository;
    }

    public function updateCustomFieldsByProductId(ProductEntity $productEntity)
    {
        $customFields = $productEntity->getCustomFields();
        $context = Context::createDefaultContext();
        $customFields['mkx_better_variants_hide_variants'] = true;
        $customFields['mkx_better_variants_use_parent_data'] = true;

        $this->productTranslationRepository->update([
            [
                'productId' => $productEntity->getId(),
                'customFields' => $customFields
            ]
        ], $context);
    }
}