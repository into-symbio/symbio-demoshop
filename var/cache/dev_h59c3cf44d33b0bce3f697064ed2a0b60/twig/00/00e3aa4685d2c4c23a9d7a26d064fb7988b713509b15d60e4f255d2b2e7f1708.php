<?php

use Twig\Environment;
use function Shopware\Core\Framework\Adapter\Twig\sw_get_attribute;
use function Shopware\Core\Framework\Adapter\Twig\sw_escape_filter;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Storefront/storefront/component/review/point.html.twig */
class __TwigTemplate_88ef87c8f36512ad10fa46444112d3db28a4e36be0e8ad1efbb2eea419f916d9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'component_review_point' => [$this, 'block_component_review_point'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/component/review/point.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/component/review/point.html.twig"));

        // line 1
        $this->displayBlock('component_review_point', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_component_review_point($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_review_point"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_review_point"));

        // line 2
        echo "    ";
        if ( !array_key_exists("size", $context)) {
            // line 3
            echo "        ";
            $context["size"] = "xs";
            // line 4
            echo "    ";
        }
        // line 5
        echo "
    ";
        // line 7
        echo "    ";
        if ($this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\FeatureFlagExtension']->feature("v6.5.0.0")) {
            // line 8
            echo "        <div class=\"product-review-point\">
    ";
        } else {
            // line 10
            echo "        <span class=\"product-review-point\">
    ";
        }
        // line 12
        echo "        ";
        if ((0 === twig_compare(($context["type"] ?? null), "half"))) {
            // line 13
            echo "            <div class=\"point-container\">
                <div class=\"point-rating point-partial-placeholder\">
                    ";
            // line 15
            ((function () use ($context, $blocks) {
                $finder = $this->env->getExtension('Shopware\Core\Framework\Adapter\Twig\Extension\NodeExtension')->getFinder();

                $includeTemplate = $finder->find("@Storefront/storefront/utilities/icon.html.twig");

                return $this->loadTemplate($includeTemplate ?? null, "@Storefront/storefront/component/review/point.html.twig", 15);
            })())->display(twig_array_merge($context, ["color" => "light", "size" => ($context["size"] ?? null), "pack" => "solid", "name" => "star"]));
            // line 16
            echo "                </div>
                <div class=\"point-rating point-partial\" style=\"clip-path: inset(0 ";
            // line 17
            echo sw_escape_filter($this->env, ((1 - ($context["left"] ?? null)) * 100), "html", null, true);
            echo "% 0 0)\">
                    ";
            // line 18
            ((function () use ($context, $blocks) {
                $finder = $this->env->getExtension('Shopware\Core\Framework\Adapter\Twig\Extension\NodeExtension')->getFinder();

                $includeTemplate = $finder->find("@Storefront/storefront/utilities/icon.html.twig");

                return $this->loadTemplate($includeTemplate ?? null, "@Storefront/storefront/component/review/point.html.twig", 18);
            })())->display(twig_array_merge($context, ["color" => "review", "size" => ($context["size"] ?? null), "pack" => "solid", "name" => "star"]));
            // line 19
            echo "                </div>
            </div>
        ";
        } elseif ((0 === twig_compare(        // line 21
($context["type"] ?? null), "blank"))) {
            // line 22
            echo "            <div class=\"point-container\">
                <div class=\"point-rating point-blank\">
                    ";
            // line 24
            ((function () use ($context, $blocks) {
                $finder = $this->env->getExtension('Shopware\Core\Framework\Adapter\Twig\Extension\NodeExtension')->getFinder();

                $includeTemplate = $finder->find("@Storefront/storefront/utilities/icon.html.twig");

                return $this->loadTemplate($includeTemplate ?? null, "@Storefront/storefront/component/review/point.html.twig", 24);
            })())->display(twig_array_merge($context, ["size" => ($context["size"] ?? null), "pack" => "solid", "name" => "star"]));
            // line 25
            echo "                </div>
            </div>
        ";
        } else {
            // line 28
            echo "            <div class=\"point-container\">
                <div class=\"point-rating point-full\">
                    ";
            // line 30
            ((function () use ($context, $blocks) {
                $finder = $this->env->getExtension('Shopware\Core\Framework\Adapter\Twig\Extension\NodeExtension')->getFinder();

                $includeTemplate = $finder->find("@Storefront/storefront/utilities/icon.html.twig");

                return $this->loadTemplate($includeTemplate ?? null, "@Storefront/storefront/component/review/point.html.twig", 30);
            })())->display(twig_array_merge($context, ["color" => "review", "size" => ($context["size"] ?? null), "pack" => "solid", "name" => "star"]));
            // line 31
            echo "                </div>
            </div>
        ";
        }
        // line 34
        echo "    ";
        // line 35
        echo "    ";
        if ($this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\FeatureFlagExtension']->feature("v6.5.0.0")) {
            // line 36
            echo "        </div>
    ";
        } else {
            // line 38
            echo "        </span>
        ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Storefront/storefront/component/review/point.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  165 => 38,  161 => 36,  158 => 35,  156 => 34,  151 => 31,  143 => 30,  139 => 28,  134 => 25,  126 => 24,  122 => 22,  120 => 21,  116 => 19,  108 => 18,  104 => 17,  101 => 16,  93 => 15,  89 => 13,  86 => 12,  82 => 10,  78 => 8,  75 => 7,  72 => 5,  69 => 4,  66 => 3,  63 => 2,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block component_review_point %}
    {% if size is not defined %}
        {% set size = 'xs' %}
    {% endif %}

    {# @deprecated tag:v6.5.0 - span will be a div #}
    {% if feature('v6.5.0.0') %}
        <div class=\"product-review-point\">
    {% else %}
        <span class=\"product-review-point\">
    {% endif %}
        {% if type == 'half' %}
            <div class=\"point-container\">
                <div class=\"point-rating point-partial-placeholder\">
                    {% sw_icon 'star' style { 'color': 'light', 'size': size, 'pack': 'solid' } %}
                </div>
                <div class=\"point-rating point-partial\" style=\"clip-path: inset(0 {{ (1 - left) * 100 }}% 0 0)\">
                    {% sw_icon 'star' style { 'color': 'review', 'size': size, 'pack': 'solid' } %}
                </div>
            </div>
        {% elseif type == 'blank' %}
            <div class=\"point-container\">
                <div class=\"point-rating point-blank\">
                    {% sw_icon 'star' style { 'size': size, 'pack': 'solid' } %}
                </div>
            </div>
        {% else %}
            <div class=\"point-container\">
                <div class=\"point-rating point-full\">
                    {% sw_icon 'star' style { 'color': 'review', 'size': size, 'pack': 'solid' } %}
                </div>
            </div>
        {% endif %}
    {# @deprecated tag:v6.5.0 - span will be a div #}
    {% if feature('v6.5.0.0') %}
        </div>
    {% else %}
        </span>
        {% endif %}
{% endblock %}
", "@Storefront/storefront/component/review/point.html.twig", "/var/www/html/vendor/shopware/storefront/Resources/views/storefront/component/review/point.html.twig");
    }
}
