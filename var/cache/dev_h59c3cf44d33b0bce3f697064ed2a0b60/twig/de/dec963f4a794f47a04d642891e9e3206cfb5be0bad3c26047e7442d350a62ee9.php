<?php

use Twig\Environment;
use function Shopware\Core\Framework\Adapter\Twig\sw_get_attribute;
use function Shopware\Core\Framework\Adapter\Twig\sw_escape_filter;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Storefront/storefront/component/product/card/price-unit.html.twig */
class __TwigTemplate_df0203e9508e45334b90b804b8bf715b5822bf0ebecc7cd41be99516899d4223 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'component_product_box_price_info' => [$this, 'block_component_product_box_price_info'],
            'component_product_box_price_unit' => [$this, 'block_component_product_box_price_unit'],
            'component_product_box_price_purchase_unit' => [$this, 'block_component_product_box_price_purchase_unit'],
            'component_product_box_price_reference_unit' => [$this, 'block_component_product_box_price_reference_unit'],
            'component_product_box_price' => [$this, 'block_component_product_box_price'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/component/product/card/price-unit.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/component/product/card/price-unit.html.twig"));

        // line 1
        $this->displayBlock('component_product_box_price_info', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_component_product_box_price_info($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_product_box_price_info"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_product_box_price_info"));

        // line 2
        echo "    ";
        // line 3
        echo "    ";
        $context["purchaseUnit"] = sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "purchaseUnit", [], "any", false, false, false, 3);
        // line 4
        echo "
    ";
        // line 6
        echo "    ";
        $context["listingPrice"] = sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "calculatedListingPrice", [], "any", false, false, false, 6);
        // line 7
        echo "
    ";
        // line 9
        echo "    ";
        $context["fromPrice"] = sw_get_attribute($this->env, $this->source, ($context["listingPrice"] ?? null), "from", [], "any", false, false, false, 9);
        // line 10
        echo "
    ";
        // line 11
        $context["cheapest"] = sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "calculatedCheapestPrice", [], "any", false, false, false, 11);
        // line 12
        echo "
    ";
        // line 13
        $context["real"] = sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "calculatedPrice", [], "any", false, false, false, 13);
        // line 14
        echo "    ";
        if ((1 === twig_compare(sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "calculatedPrices", [], "any", false, false, false, 14), "count", [], "any", false, false, false, 14), 0))) {
            // line 15
            echo "        ";
            $context["real"] = sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "calculatedPrices", [], "any", false, false, false, 15), "last", [], "any", false, false, false, 15);
            // line 16
            echo "    ";
        }
        // line 17
        echo "
    ";
        // line 18
        $context["referencePrice"] = sw_get_attribute($this->env, $this->source, ($context["real"] ?? null), "referencePrice", [], "any", false, false, false, 18);
        // line 19
        echo "
    ";
        // line 20
        $context["displayFrom"] = (1 === twig_compare(sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "calculatedPrices", [], "any", false, false, false, 20), "count", [], "any", false, false, false, 20), 1));
        // line 21
        echo "    ";
        $context["displayParent"] = (sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "variantListingConfig", [], "any", false, false, false, 21), "displayParent", [], "any", false, false, false, 21) && (sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "parentId", [], "any", false, false, false, 21) === null));
        // line 22
        echo "
    ";
        // line 23
        if (($context["displayParent"] ?? null)) {
            // line 24
            echo "        ";
            $context["displayFromVariants"] = (($context["displayParent"] ?? null) && (sw_get_attribute($this->env, $this->source, ($context["real"] ?? null), "unitPrice", [], "any", false, false, false, 24) !== sw_get_attribute($this->env, $this->source, ($context["cheapest"] ?? null), "unitPrice", [], "any", false, false, false, 24)));
            // line 25
            echo "        ";
            $context["real"] = ($context["cheapest"] ?? null);
            // line 26
            echo "    ";
        }
        // line 27
        echo "
    <div class=\"product-price-info\">
        ";
        // line 29
        $this->displayBlock('component_product_box_price_unit', $context, $blocks);
        // line 53
        echo "
        ";
        // line 54
        $this->displayBlock('component_product_box_price', $context, $blocks);
        // line 96
        echo "    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 29
    public function block_component_product_box_price_unit($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_product_box_price_unit"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_product_box_price_unit"));

        // line 30
        echo "            <p class=\"product-price-unit\">
                ";
        // line 32
        echo "                ";
        $this->displayBlock('component_product_box_price_purchase_unit', $context, $blocks);
        // line 42
        echo "
                ";
        // line 44
        echo "                ";
        $this->displayBlock('component_product_box_price_reference_unit', $context, $blocks);
        // line 51
        echo "            </p>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 32
    public function block_component_product_box_price_purchase_unit($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_product_box_price_purchase_unit"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_product_box_price_purchase_unit"));

        // line 33
        echo "                    ";
        if ((($context["referencePrice"] ?? null) && sw_get_attribute($this->env, $this->source, ($context["referencePrice"] ?? null), "unitName", [], "any", false, false, false, 33))) {
            // line 34
            echo "                        <span class=\"product-unit-label\">
                            ";
            // line 35
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("listing.boxUnitLabel"));
            echo "
                        </span>
                        <span class=\"price-unit-content\">
                            ";
            // line 38
            echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["referencePrice"] ?? null), "purchaseUnit", [], "any", false, false, false, 38), "html", null, true);
            echo " ";
            echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["referencePrice"] ?? null), "unitName", [], "any", false, false, false, 38), "html", null, true);
            echo "
                        </span>
                    ";
        }
        // line 41
        echo "                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 44
    public function block_component_product_box_price_reference_unit($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_product_box_price_reference_unit"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_product_box_price_reference_unit"));

        // line 45
        echo "                    ";
        if ( !(null === ($context["referencePrice"] ?? null))) {
            // line 46
            echo "                        <span class=\"price-unit-reference\">
                            (";
            // line 47
            echo sw_escape_filter($this->env, $this->extensions['Shopware\Core\Framework\Adapter\Twig\Filter\CurrencyFilter']->formatCurrency($context, sw_get_attribute($this->env, $this->source, ($context["referencePrice"] ?? null), "price", [], "any", false, false, false, 47)), "html", null, true);
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("general.star"));
            echo " / ";
            echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["referencePrice"] ?? null), "referenceUnit", [], "any", false, false, false, 47), "html", null, true);
            echo " ";
            echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["referencePrice"] ?? null), "unitName", [], "any", false, false, false, 47), "html", null, true);
            echo ")
                        </span>
                    ";
        }
        // line 50
        echo "                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 54
    public function block_component_product_box_price($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_product_box_price"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_product_box_price"));

        // line 55
        echo "            <div class=\"product-price-wrapper\">
                ";
        // line 56
        $context["price"] = ($context["real"] ?? null);
        // line 57
        echo "                ";
        $context["isListPrice"] = (1 === twig_compare(sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["price"] ?? null), "listPrice", [], "any", false, false, false, 57), "percentage", [], "any", false, false, false, 57), 0));
        // line 58
        echo "                ";
        $context["isRegulationPrice"] = (0 !== twig_compare(sw_get_attribute($this->env, $this->source, ($context["price"] ?? null), "regulationPrice", [], "any", false, false, false, 58), null));
        // line 59
        echo "
                <div class=\"product-cheapest-price";
        // line 60
        if ((((($context["isListPrice"] ?? null) && ($context["isRegulationPrice"] ?? null)) &&  !($context["displayFrom"] ?? null)) &&  !($context["displayFromVariants"] ?? null))) {
            echo " with-list-price";
        }
        if (((($context["isRegulationPrice"] ?? null) &&  !($context["displayFrom"] ?? null)) && ($context["displayFromVariants"] ?? null))) {
            echo " with-regulation-price";
        }
        if ((($context["displayFrom"] ?? null) && ($context["isRegulationPrice"] ?? null))) {
            echo " with-from-price";
        }
        echo "\">
                    ";
        // line 61
        if ((0 !== twig_compare(sw_get_attribute($this->env, $this->source, ($context["cheapest"] ?? null), "unitPrice", [], "any", false, false, false, 61), sw_get_attribute($this->env, $this->source, ($context["real"] ?? null), "unitPrice", [], "any", false, false, false, 61)))) {
            // line 62
            echo "                        <div>";
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("listing.cheapestPriceLabel"));
            echo "<span class=\"product-cheapest-price-price\"> ";
            echo sw_escape_filter($this->env, $this->extensions['Shopware\Core\Framework\Adapter\Twig\Filter\CurrencyFilter']->formatCurrency($context, sw_get_attribute($this->env, $this->source, ($context["cheapest"] ?? null), "unitPrice", [], "any", false, false, false, 62)), "html", null, true);
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("general.star"));
            echo "</span></div>
                    ";
        }
        // line 64
        echo "                </div>

                ";
        // line 66
        if ((($context["displayFrom"] ?? null) || (($context["displayParent"] ?? null) && ($context["displayFromVariants"] ?? null)))) {
            // line 67
            echo "                    ";
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("listing.listingTextFrom"));
            echo "
                ";
        }
        // line 69
        echo "
                <span class=\"product-price";
        // line 70
        if (((($context["isListPrice"] ?? null) &&  !($context["displayFrom"] ?? null)) &&  !($context["displayFromVariants"] ?? null))) {
            echo " with-list-price";
        }
        echo "\">
                    ";
        // line 71
        echo sw_escape_filter($this->env, $this->extensions['Shopware\Core\Framework\Adapter\Twig\Filter\CurrencyFilter']->formatCurrency($context, sw_get_attribute($this->env, $this->source, ($context["price"] ?? null), "unitPrice", [], "any", false, false, false, 71)), "html", null, true);
        echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("general.star"));
        echo "

                    ";
        // line 73
        if (((($context["isListPrice"] ?? null) &&  !($context["displayFrom"] ?? null)) &&  !($context["displayFromVariants"] ?? null))) {
            // line 74
            echo "                        ";
            $context["afterListPriceSnippetExists"] = (1 === twig_compare(twig_length_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("listing.afterListPrice")), 0));
            // line 75
            echo "                        ";
            $context["beforeListPriceSnippetExists"] = (1 === twig_compare(twig_length_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("listing.beforeListPrice")), 0));
            // line 76
            echo "                        ";
            $context["hideStrikeTrough"] = (($context["beforeListPriceSnippetExists"] ?? null) || ($context["afterListPriceSnippetExists"] ?? null));
            // line 77
            echo "
                        <span class=\"list-price";
            // line 78
            if (($context["hideStrikeTrough"] ?? null)) {
                echo " list-price-no-line-through";
            }
            echo "\">
                            ";
            // line 79
            if (($context["beforeListPriceSnippetExists"] ?? null)) {
                echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize(twig_trim_filter($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("listing.beforeListPrice")));
            }
            // line 80
            echo "
                            <span class=\"list-price-price\">";
            // line 81
            echo sw_escape_filter($this->env, $this->extensions['Shopware\Core\Framework\Adapter\Twig\Filter\CurrencyFilter']->formatCurrency($context, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["price"] ?? null), "listPrice", [], "any", false, false, false, 81), "price", [], "any", false, false, false, 81)), "html", null, true);
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("general.star"));
            echo "</span>

                            ";
            // line 83
            if (($context["afterListPriceSnippetExists"] ?? null)) {
                echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize(twig_trim_filter($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("listing.afterListPrice")));
            }
            // line 84
            echo "
                            <span class=\"list-price-percentage\">";
            // line 85
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.listPricePercentage", ["%price%" => sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["price"] ?? null), "listPrice", [], "any", false, false, false, 85), "percentage", [], "any", false, false, false, 85)]));
            echo "</span>
                        </span>
                    ";
        }
        // line 88
        echo "                </span>
                ";
        // line 89
        if (($context["isRegulationPrice"] ?? null)) {
            // line 90
            echo "                    <span class=\"product-price with-regulation-price\">
                        ";
            // line 91
            if (($context["isListPrice"] ?? null)) {
                echo "<br/>";
            }
            echo "<span class=\"regulation-price\">";
            echo sw_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("general.listPricePreviously", ["%price%" => $this->extensions['Shopware\Core\Framework\Adapter\Twig\Filter\CurrencyFilter']->formatCurrency($context, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["price"] ?? null), "regulationPrice", [], "any", false, false, false, 91), "price", [], "any", false, false, false, 91))]), "html", null, true);
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("general.star"));
            echo "</span>
                    </span>
                ";
        }
        // line 94
        echo "            </div>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Storefront/storefront/component/product/card/price-unit.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  401 => 94,  390 => 91,  387 => 90,  385 => 89,  382 => 88,  376 => 85,  373 => 84,  369 => 83,  363 => 81,  360 => 80,  356 => 79,  350 => 78,  347 => 77,  344 => 76,  341 => 75,  338 => 74,  336 => 73,  330 => 71,  324 => 70,  321 => 69,  315 => 67,  313 => 66,  309 => 64,  300 => 62,  298 => 61,  286 => 60,  283 => 59,  280 => 58,  277 => 57,  275 => 56,  272 => 55,  262 => 54,  252 => 50,  241 => 47,  238 => 46,  235 => 45,  225 => 44,  215 => 41,  207 => 38,  201 => 35,  198 => 34,  195 => 33,  185 => 32,  174 => 51,  171 => 44,  168 => 42,  165 => 32,  162 => 30,  152 => 29,  141 => 96,  139 => 54,  136 => 53,  134 => 29,  130 => 27,  127 => 26,  124 => 25,  121 => 24,  119 => 23,  116 => 22,  113 => 21,  111 => 20,  108 => 19,  106 => 18,  103 => 17,  100 => 16,  97 => 15,  94 => 14,  92 => 13,  89 => 12,  87 => 11,  84 => 10,  81 => 9,  78 => 7,  75 => 6,  72 => 4,  69 => 3,  67 => 2,  48 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block component_product_box_price_info %}
    {# @deprecated tag:v6.5.0 - purchaseUnit will be removed, use product.purchaseUnit if needed #}
    {% set purchaseUnit = product.purchaseUnit %}

    {# @deprecated tag:v6.5.0 - listingPrice will be removed without replacement, since it was removed from the product struct #}
    {% set listingPrice = product.calculatedListingPrice %}

    {# @deprecated tag:v6.5.0 - fromPrice will be removed without replacement #}
    {% set fromPrice = listingPrice.from %}

    {% set cheapest = product.calculatedCheapestPrice %}

    {% set real = product.calculatedPrice %}
    {% if product.calculatedPrices.count > 0 %}
        {% set real = product.calculatedPrices.last %}
    {% endif %}

    {% set referencePrice = real.referencePrice %}

    {% set displayFrom = product.calculatedPrices.count > 1 %}
    {% set displayParent = product.variantListingConfig.displayParent and product.parentId === null %}

    {% if displayParent %}
        {% set displayFromVariants = displayParent and real.unitPrice !== cheapest.unitPrice %}
        {% set real = cheapest %}
    {% endif %}

    <div class=\"product-price-info\">
        {% block component_product_box_price_unit %}
            <p class=\"product-price-unit\">
                {# Price is based on the purchase unit #}
                {% block component_product_box_price_purchase_unit %}
                    {% if referencePrice and referencePrice.unitName %}
                        <span class=\"product-unit-label\">
                            {{ \"listing.boxUnitLabel\"|trans|sw_sanitize }}
                        </span>
                        <span class=\"price-unit-content\">
                            {{ referencePrice.purchaseUnit }} {{ referencePrice.unitName }}
                        </span>
                    {% endif %}
                {% endblock %}

                {# Item price is based on a reference unit #}
                {% block component_product_box_price_reference_unit %}
                    {% if referencePrice is not null %}
                        <span class=\"price-unit-reference\">
                            ({{ referencePrice.price|currency }}{{ \"general.star\"|trans|sw_sanitize }} / {{ referencePrice.referenceUnit }} {{ referencePrice.unitName }})
                        </span>
                    {% endif %}
                {% endblock %}
            </p>
        {% endblock %}

        {% block component_product_box_price %}
            <div class=\"product-price-wrapper\">
                {% set price = real %}
                {% set isListPrice = price.listPrice.percentage > 0 %}
                {% set isRegulationPrice = price.regulationPrice != null %}

                <div class=\"product-cheapest-price{% if isListPrice and isRegulationPrice and not displayFrom and not displayFromVariants %} with-list-price{% endif %}{% if isRegulationPrice and not displayFrom and displayFromVariants %} with-regulation-price{% endif %}{% if displayFrom and isRegulationPrice %} with-from-price{% endif %}\">
                    {% if cheapest.unitPrice != real.unitPrice %}
                        <div>{{ \"listing.cheapestPriceLabel\"|trans|sw_sanitize }}<span class=\"product-cheapest-price-price\"> {{ cheapest.unitPrice|currency }}{{ \"general.star\"|trans|sw_sanitize }}</span></div>
                    {% endif %}
                </div>

                {% if displayFrom or (displayParent and displayFromVariants) %}
                    {{ \"listing.listingTextFrom\"|trans|sw_sanitize }}
                {% endif %}

                <span class=\"product-price{% if isListPrice and not displayFrom and not displayFromVariants %} with-list-price{% endif %}\">
                    {{ price.unitPrice|currency }}{{ \"general.star\"|trans|sw_sanitize }}

                    {% if isListPrice and not displayFrom and not displayFromVariants %}
                        {% set afterListPriceSnippetExists = \"listing.afterListPrice\"|trans|length > 0 %}
                        {% set beforeListPriceSnippetExists = \"listing.beforeListPrice\"|trans|length > 0 %}
                        {% set hideStrikeTrough = beforeListPriceSnippetExists or afterListPriceSnippetExists %}

                        <span class=\"list-price{% if hideStrikeTrough %} list-price-no-line-through{% endif %}\">
                            {% if beforeListPriceSnippetExists %}{{ \"listing.beforeListPrice\"|trans|trim|sw_sanitize }}{% endif %}

                            <span class=\"list-price-price\">{{ price.listPrice.price|currency }}{{ \"general.star\"|trans|sw_sanitize }}</span>

                            {% if afterListPriceSnippetExists %}{{ \"listing.afterListPrice\"|trans|trim|sw_sanitize }}{% endif %}

                            <span class=\"list-price-percentage\">{{ \"detail.listPricePercentage\"|trans({'%price%': price.listPrice.percentage })|sw_sanitize }}</span>
                        </span>
                    {% endif %}
                </span>
                {% if isRegulationPrice %}
                    <span class=\"product-price with-regulation-price\">
                        {% if isListPrice %}<br/>{% endif %}<span class=\"regulation-price\">{{ \"general.listPricePreviously\"|trans({'%price%': price.regulationPrice.price|currency }) }}{{ \"general.star\"|trans|sw_sanitize }}</span>
                    </span>
                {% endif %}
            </div>
        {% endblock %}
    </div>
{% endblock %}
", "@Storefront/storefront/component/product/card/price-unit.html.twig", "/var/www/html/vendor/shopware/storefront/Resources/views/storefront/component/product/card/price-unit.html.twig");
    }
}
