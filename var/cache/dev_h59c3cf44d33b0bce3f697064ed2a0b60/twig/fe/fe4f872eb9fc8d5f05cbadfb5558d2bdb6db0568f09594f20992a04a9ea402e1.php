<?php

use Twig\Environment;
use function Shopware\Core\Framework\Adapter\Twig\sw_get_attribute;
use function Shopware\Core\Framework\Adapter\Twig\sw_escape_filter;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Storefront/storefront/component/pagination.html.twig */
class __TwigTemplate_6a52da5848382fedfc8bd47a4434722bf480128593f19300679ec015e55ed732 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'component_pagination_nav' => [$this, 'block_component_pagination_nav'],
            'component_pagination' => [$this, 'block_component_pagination'],
            'component_pagination_first' => [$this, 'block_component_pagination_first'],
            'component_pagination_first_input' => [$this, 'block_component_pagination_first_input'],
            'component_pagination_first_label' => [$this, 'block_component_pagination_first_label'],
            'component_pagination_first_link' => [$this, 'block_component_pagination_first_link'],
            'component_pagination_prev' => [$this, 'block_component_pagination_prev'],
            'component_pagination_prev_input' => [$this, 'block_component_pagination_prev_input'],
            'component_pagination_prev_label' => [$this, 'block_component_pagination_prev_label'],
            'component_pagination_prev_link' => [$this, 'block_component_pagination_prev_link'],
            'component_pagination_prev_icon' => [$this, 'block_component_pagination_prev_icon'],
            'component_pagination_loop' => [$this, 'block_component_pagination_loop'],
            'component_pagination_item' => [$this, 'block_component_pagination_item'],
            'component_pagination_item_input' => [$this, 'block_component_pagination_item_input'],
            'component_pagination_item_label' => [$this, 'block_component_pagination_item_label'],
            'component_pagination_item_link' => [$this, 'block_component_pagination_item_link'],
            'component_pagination_item_text' => [$this, 'block_component_pagination_item_text'],
            'component_pagination_next' => [$this, 'block_component_pagination_next'],
            'component_pagination_next_input' => [$this, 'block_component_pagination_next_input'],
            'component_pagination_next_label' => [$this, 'block_component_pagination_next_label'],
            'component_pagination_next_link' => [$this, 'block_component_pagination_next_link'],
            'component_pagination_next_icon' => [$this, 'block_component_pagination_next_icon'],
            'component_pagination_last' => [$this, 'block_component_pagination_last'],
            'component_pagination_last_input' => [$this, 'block_component_pagination_last_input'],
            'component_pagination_last_label' => [$this, 'block_component_pagination_last_label'],
            'component_pagination_last_link' => [$this, 'block_component_pagination_last_link'],
            'component_pagination_last_icon' => [$this, 'block_component_pagination_last_icon'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/component/pagination.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/component/pagination.html.twig"));

        // line 1
        $this->displayBlock('component_pagination_nav', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_component_pagination_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_nav"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_nav"));

        // line 2
        echo "    ";
        $context["currentPage"] = twig_round(((sw_get_attribute($this->env, $this->source, ($context["criteria"] ?? null), "offset", [], "any", false, false, false, 2) + 1) / sw_get_attribute($this->env, $this->source, ($context["criteria"] ?? null), "limit", [], "any", false, false, false, 2)), 0, "ceil");
        // line 3
        echo "    ";
        $context["totalEntities"] = ((($context["total"] ?? null)) ? (($context["total"] ?? null)) : (sw_get_attribute($this->env, $this->source, ($context["entities"] ?? null), "total", [], "any", false, false, false, 3)));
        // line 4
        echo "    ";
        $context["totalPages"] = twig_round((($context["totalEntities"] ?? null) / sw_get_attribute($this->env, $this->source, ($context["criteria"] ?? null), "limit", [], "any", false, false, false, 4)), 0, "ceil");
        // line 5
        echo "
    ";
        // line 6
        $context["paginationSuffix"] = "";
        // line 7
        echo "    ";
        // line 8
        echo "    ";
        if (($this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\FeatureFlagExtension']->feature("v6.5.0.0") && ($context["paginationLocation"] ?? null))) {
            // line 9
            echo "        ";
            $context["paginationSuffix"] = ("-" . ($context["paginationLocation"] ?? null));
            // line 10
            echo "    ";
        }
        // line 11
        echo "
    ";
        // line 12
        if ((1 === twig_compare(($context["totalPages"] ?? null), 1))) {
            // line 13
            echo "        <nav aria-label=\"pagination\" class=\"pagination-nav\">
            ";
            // line 14
            $this->displayBlock('component_pagination', $context, $blocks);
            // line 161
            echo "        </nav>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_component_pagination($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination"));

        // line 15
        echo "            <ul class=\"pagination\">
                ";
        // line 16
        $this->displayBlock('component_pagination_first', $context, $blocks);
        // line 37
        echo "
                ";
        // line 38
        $this->displayBlock('component_pagination_prev', $context, $blocks);
        // line 61
        echo "
                ";
        // line 62
        $this->displayBlock('component_pagination_loop', $context, $blocks);
        // line 108
        echo "
                ";
        // line 109
        $this->displayBlock('component_pagination_next', $context, $blocks);
        // line 132
        echo "
                ";
        // line 133
        $this->displayBlock('component_pagination_last', $context, $blocks);
        // line 159
        echo "            </ul>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_component_pagination_first($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_first"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_first"));

        // line 17
        echo "                    <li class=\"page-item page-first";
        if ((0 === twig_compare(($context["currentPage"] ?? null), 1))) {
            echo " disabled";
        }
        echo "\">
                        ";
        // line 18
        $this->displayBlock('component_pagination_first_input', $context, $blocks);
        // line 27
        echo "
                        ";
        // line 28
        $this->displayBlock('component_pagination_first_label', $context, $blocks);
        // line 35
        echo "                    </li>
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 18
    public function block_component_pagination_first_input($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_first_input"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_first_input"));

        // line 19
        echo "                            <input type=\"radio\"
                                   ";
        // line 20
        if ((0 === twig_compare(($context["currentPage"] ?? null), 1))) {
            echo "disabled=\"disabled\"";
        }
        // line 21
        echo "                                   name=\"p\"
                                   id=\"p-first";
        // line 22
        echo sw_escape_filter($this->env, ($context["paginationSuffix"] ?? null), "html", null, true);
        echo "\"
                                   value=\"1\"
                                   class=\"d-none\"
                                   title=\"pagination\">
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 28
    public function block_component_pagination_first_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_first_label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_first_label"));

        // line 29
        echo "                            <label class=\"page-link\" for=\"p-first";
        echo sw_escape_filter($this->env, ($context["paginationSuffix"] ?? null), "html", null, true);
        echo "\">
                                ";
        // line 30
        $this->displayBlock('component_pagination_first_link', $context, $blocks);
        // line 33
        echo "                            </label>
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 30
    public function block_component_pagination_first_link($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_first_link"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_first_link"));

        // line 31
        echo "                                    ";
        ((function () use ($context, $blocks) {
            $finder = $this->env->getExtension('Shopware\Core\Framework\Adapter\Twig\Extension\NodeExtension')->getFinder();

            $includeTemplate = $finder->find("@Storefront/storefront/utilities/icon.html.twig");

            return $this->loadTemplate($includeTemplate ?? null, "@Storefront/storefront/component/pagination.html.twig", 31);
        })())->display(twig_array_merge($context, ["size" => "fluid", "pack" => "solid", "name" => "arrow-medium-double-left"]));
        // line 32
        echo "                                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 38
    public function block_component_pagination_prev($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_prev"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_prev"));

        // line 39
        echo "                    <li class=\"page-item page-prev";
        if ((0 === twig_compare(($context["currentPage"] ?? null), 1))) {
            echo " disabled";
        }
        echo "\">
                        ";
        // line 40
        $this->displayBlock('component_pagination_prev_input', $context, $blocks);
        // line 49
        echo "
                        ";
        // line 50
        $this->displayBlock('component_pagination_prev_label', $context, $blocks);
        // line 59
        echo "                    </li>
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 40
    public function block_component_pagination_prev_input($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_prev_input"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_prev_input"));

        // line 41
        echo "                            <input type=\"radio\"
                                   ";
        // line 42
        if ((0 === twig_compare(($context["currentPage"] ?? null), 1))) {
            echo "disabled=\"disabled\"";
        }
        // line 43
        echo "                                   name=\"p\"
                                   id=\"p-prev";
        // line 44
        echo sw_escape_filter($this->env, ($context["paginationSuffix"] ?? null), "html", null, true);
        echo "\"
                                   value=\"";
        // line 45
        echo sw_escape_filter($this->env, (($context["currentPage"] ?? null) - 1), "html", null, true);
        echo "\"
                                   class=\"d-none\"
                                   title=\"pagination\">
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 50
    public function block_component_pagination_prev_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_prev_label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_prev_label"));

        // line 51
        echo "                            <label class=\"page-link\" for=\"p-prev";
        echo sw_escape_filter($this->env, ($context["paginationSuffix"] ?? null), "html", null, true);
        echo "\">
                                ";
        // line 52
        $this->displayBlock('component_pagination_prev_link', $context, $blocks);
        // line 57
        echo "                            </label>
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 52
    public function block_component_pagination_prev_link($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_prev_link"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_prev_link"));

        // line 53
        echo "                                    ";
        $this->displayBlock('component_pagination_prev_icon', $context, $blocks);
        // line 56
        echo "                                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 53
    public function block_component_pagination_prev_icon($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_prev_icon"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_prev_icon"));

        // line 54
        echo "                                        ";
        ((function () use ($context, $blocks) {
            $finder = $this->env->getExtension('Shopware\Core\Framework\Adapter\Twig\Extension\NodeExtension')->getFinder();

            $includeTemplate = $finder->find("@Storefront/storefront/utilities/icon.html.twig");

            return $this->loadTemplate($includeTemplate ?? null, "@Storefront/storefront/component/pagination.html.twig", 54);
        })())->display(twig_array_merge($context, ["size" => "fluid", "pack" => "solid", "name" => "arrow-medium-left"]));
        // line 55
        echo "                                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 62
    public function block_component_pagination_loop($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_loop"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_loop"));

        // line 63
        echo "                    ";
        $context["start"] = (($context["currentPage"] ?? null) - 2);
        // line 64
        echo "                    ";
        if ((0 >= twig_compare(($context["start"] ?? null), 0))) {
            // line 65
            echo "                        ";
            $context["start"] = (($context["currentPage"] ?? null) - 1);
            // line 66
            echo "                        ";
            if ((0 >= twig_compare(($context["start"] ?? null), 0))) {
                // line 67
                echo "                            ";
                $context["start"] = ($context["currentPage"] ?? null);
                // line 68
                echo "                        ";
            }
            // line 69
            echo "                    ";
        }
        // line 70
        echo "
                    ";
        // line 71
        $context["end"] = (($context["start"] ?? null) + 4);
        // line 72
        echo "
                    ";
        // line 73
        if ((1 === twig_compare(($context["end"] ?? null), ($context["totalPages"] ?? null)))) {
            // line 74
            echo "                        ";
            $context["end"] = ($context["totalPages"] ?? null);
            // line 75
            echo "                    ";
        }
        // line 76
        echo "
                    ";
        // line 77
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(($context["start"] ?? null), ($context["end"] ?? null)));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 78
            echo "
                        ";
            // line 79
            $context["isActive"] = (0 === twig_compare(($context["currentPage"] ?? null), $context["page"]));
            // line 80
            echo "
                        ";
            // line 81
            $this->displayBlock('component_pagination_item', $context, $blocks);
            // line 105
            echo "
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 81
    public function block_component_pagination_item($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_item"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_item"));

        // line 82
        echo "                            <li class=\"page-item";
        if (($context["isActive"] ?? null)) {
            echo " active";
        }
        echo "\">
                                ";
        // line 83
        $this->displayBlock('component_pagination_item_input', $context, $blocks);
        // line 92
        echo "
                                ";
        // line 93
        $this->displayBlock('component_pagination_item_label', $context, $blocks);
        // line 103
        echo "                            </li>
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 83
    public function block_component_pagination_item_input($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_item_input"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_item_input"));

        // line 84
        echo "                                    <input type=\"radio\"
                                           name=\"p\"
                                           id=\"p";
        // line 86
        echo sw_escape_filter($this->env, ($context["page"] ?? null), "html", null, true);
        echo sw_escape_filter($this->env, ($context["paginationSuffix"] ?? null), "html", null, true);
        echo "\"
                                           value=\"";
        // line 87
        echo sw_escape_filter($this->env, ($context["page"] ?? null), "html", null, true);
        echo "\"
                                           class=\"d-none\"
                                           title=\"pagination\"
                                           ";
        // line 90
        if (($context["isActive"] ?? null)) {
            echo "checked=\"checked\"";
        }
        echo ">
                                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 93
    public function block_component_pagination_item_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_item_label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_item_label"));

        // line 94
        echo "                                    <label class=\"page-link\"
                                           for=\"p";
        // line 95
        echo sw_escape_filter($this->env, ($context["page"] ?? null), "html", null, true);
        echo sw_escape_filter($this->env, ($context["paginationSuffix"] ?? null), "html", null, true);
        echo "\">
                                        ";
        // line 96
        $this->displayBlock('component_pagination_item_link', $context, $blocks);
        // line 101
        echo "                                    </label>
                                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 96
    public function block_component_pagination_item_link($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_item_link"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_item_link"));

        // line 97
        echo "                                            ";
        $this->displayBlock('component_pagination_item_text', $context, $blocks);
        // line 100
        echo "                                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 97
    public function block_component_pagination_item_text($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_item_text"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_item_text"));

        // line 98
        echo "                                                ";
        echo sw_escape_filter($this->env, ($context["page"] ?? null), "html", null, true);
        echo "
                                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 109
    public function block_component_pagination_next($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_next"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_next"));

        // line 110
        echo "                    <li class=\"page-item page-next";
        if ((0 === twig_compare(($context["currentPage"] ?? null), ($context["totalPages"] ?? null)))) {
            echo " disabled";
        }
        echo "\">
                        ";
        // line 111
        $this->displayBlock('component_pagination_next_input', $context, $blocks);
        // line 120
        echo "
                        ";
        // line 121
        $this->displayBlock('component_pagination_next_label', $context, $blocks);
        // line 130
        echo "                    </li>
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 111
    public function block_component_pagination_next_input($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_next_input"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_next_input"));

        // line 112
        echo "                            <input type=\"radio\"
                                   ";
        // line 113
        if ((0 === twig_compare(($context["currentPage"] ?? null), ($context["totalPages"] ?? null)))) {
            echo "disabled=\"disabled\"";
        }
        // line 114
        echo "                                   name=\"p\"
                                   id=\"p-next";
        // line 115
        echo sw_escape_filter($this->env, ($context["paginationSuffix"] ?? null), "html", null, true);
        echo "\"
                                   value=\"";
        // line 116
        echo sw_escape_filter($this->env, (($context["currentPage"] ?? null) + 1), "html", null, true);
        echo "\"
                                   class=\"d-none\"
                                   title=\"pagination\">
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 121
    public function block_component_pagination_next_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_next_label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_next_label"));

        // line 122
        echo "                            <label class=\"page-link\" for=\"p-next";
        echo sw_escape_filter($this->env, ($context["paginationSuffix"] ?? null), "html", null, true);
        echo "\">
                                ";
        // line 123
        $this->displayBlock('component_pagination_next_link', $context, $blocks);
        // line 128
        echo "                            </label>
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 123
    public function block_component_pagination_next_link($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_next_link"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_next_link"));

        // line 124
        echo "                                    ";
        $this->displayBlock('component_pagination_next_icon', $context, $blocks);
        // line 127
        echo "                                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 124
    public function block_component_pagination_next_icon($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_next_icon"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_next_icon"));

        // line 125
        echo "                                        ";
        ((function () use ($context, $blocks) {
            $finder = $this->env->getExtension('Shopware\Core\Framework\Adapter\Twig\Extension\NodeExtension')->getFinder();

            $includeTemplate = $finder->find("@Storefront/storefront/utilities/icon.html.twig");

            return $this->loadTemplate($includeTemplate ?? null, "@Storefront/storefront/component/pagination.html.twig", 125);
        })())->display(twig_array_merge($context, ["size" => "fluid", "pack" => "solid", "name" => "arrow-medium-right"]));
        // line 126
        echo "                                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 133
    public function block_component_pagination_last($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_last"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_last"));

        // line 134
        echo "                    <li class=\"page-item page-last";
        if ((0 === twig_compare(($context["currentPage"] ?? null), ($context["totalPages"] ?? null)))) {
            echo " disabled";
        }
        echo "\">
                        ";
        // line 135
        $this->displayBlock('component_pagination_last_input', $context, $blocks);
        // line 144
        echo "
                        ";
        // line 145
        $this->displayBlock('component_pagination_last_label', $context, $blocks);
        // line 157
        echo "                    </li>
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 135
    public function block_component_pagination_last_input($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_last_input"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_last_input"));

        // line 136
        echo "                            <input type=\"radio\"
                                   ";
        // line 137
        if ((0 === twig_compare(($context["currentPage"] ?? null), ($context["totalPages"] ?? null)))) {
            echo "disabled=\"disabled\"";
        }
        // line 138
        echo "                                   name=\"p\"
                                   id=\"p-last";
        // line 139
        echo sw_escape_filter($this->env, ($context["paginationSuffix"] ?? null), "html", null, true);
        echo "\"
                                   value=\"";
        // line 140
        echo sw_escape_filter($this->env, ($context["totalPages"] ?? null), "html", null, true);
        echo "\"
                                   class=\"d-none\"
                                   title=\"pagination\">
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 145
    public function block_component_pagination_last_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_last_label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_last_label"));

        // line 146
        echo "                            <label class=\"page-link\" for=\"p-last";
        echo sw_escape_filter($this->env, ($context["paginationSuffix"] ?? null), "html", null, true);
        echo "\">
                                ";
        // line 147
        $this->displayBlock('component_pagination_last_link', $context, $blocks);
        // line 155
        echo "                            </label>
                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 147
    public function block_component_pagination_last_link($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_last_link"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_last_link"));

        // line 148
        echo "                                    ";
        $this->displayBlock('component_pagination_last_icon', $context, $blocks);
        // line 154
        echo "                                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 148
    public function block_component_pagination_last_icon($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_last_icon"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "component_pagination_last_icon"));

        // line 149
        echo "                                        ";
        ((function () use ($context, $blocks) {
            $finder = $this->env->getExtension('Shopware\Core\Framework\Adapter\Twig\Extension\NodeExtension')->getFinder();

            $includeTemplate = $finder->find("@Storefront/storefront/utilities/icon.html.twig");

            return $this->loadTemplate($includeTemplate ?? null, "@Storefront/storefront/component/pagination.html.twig", 149);
        })())->display(twig_array_merge($context, ["size" => "fluid", "pack" => "solid", "name" => "arrow-medium-double-right"]));
        // line 153
        echo "                                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Storefront/storefront/component/pagination.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1011 => 153,  1002 => 149,  992 => 148,  982 => 154,  979 => 148,  969 => 147,  958 => 155,  956 => 147,  951 => 146,  941 => 145,  927 => 140,  923 => 139,  920 => 138,  916 => 137,  913 => 136,  903 => 135,  892 => 157,  890 => 145,  887 => 144,  885 => 135,  878 => 134,  868 => 133,  858 => 126,  849 => 125,  839 => 124,  829 => 127,  826 => 124,  816 => 123,  805 => 128,  803 => 123,  798 => 122,  788 => 121,  774 => 116,  770 => 115,  767 => 114,  763 => 113,  760 => 112,  750 => 111,  739 => 130,  737 => 121,  734 => 120,  732 => 111,  725 => 110,  715 => 109,  702 => 98,  692 => 97,  682 => 100,  679 => 97,  669 => 96,  658 => 101,  656 => 96,  651 => 95,  648 => 94,  638 => 93,  624 => 90,  618 => 87,  613 => 86,  609 => 84,  599 => 83,  588 => 103,  586 => 93,  583 => 92,  581 => 83,  574 => 82,  564 => 81,  554 => 107,  539 => 105,  537 => 81,  534 => 80,  532 => 79,  529 => 78,  512 => 77,  509 => 76,  506 => 75,  503 => 74,  501 => 73,  498 => 72,  496 => 71,  493 => 70,  490 => 69,  487 => 68,  484 => 67,  481 => 66,  478 => 65,  475 => 64,  472 => 63,  462 => 62,  452 => 55,  443 => 54,  433 => 53,  423 => 56,  420 => 53,  410 => 52,  399 => 57,  397 => 52,  392 => 51,  382 => 50,  368 => 45,  364 => 44,  361 => 43,  357 => 42,  354 => 41,  344 => 40,  333 => 59,  331 => 50,  328 => 49,  326 => 40,  319 => 39,  309 => 38,  299 => 32,  290 => 31,  280 => 30,  269 => 33,  267 => 30,  262 => 29,  252 => 28,  237 => 22,  234 => 21,  230 => 20,  227 => 19,  217 => 18,  206 => 35,  204 => 28,  201 => 27,  199 => 18,  192 => 17,  182 => 16,  171 => 159,  169 => 133,  166 => 132,  164 => 109,  161 => 108,  159 => 62,  156 => 61,  154 => 38,  151 => 37,  149 => 16,  146 => 15,  136 => 14,  124 => 161,  122 => 14,  119 => 13,  117 => 12,  114 => 11,  111 => 10,  108 => 9,  105 => 8,  103 => 7,  101 => 6,  98 => 5,  95 => 4,  92 => 3,  89 => 2,  70 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block component_pagination_nav %}
    {% set currentPage = ((criteria.offset + 1) / criteria.limit)|round(0, 'ceil') %}
    {% set totalEntities = total ? total : entities.total %}
    {% set totalPages = (totalEntities / criteria.limit)|round(0, 'ceil') %}

    {% set paginationSuffix = '' %}
    {# @deprecated tag:v6.5.0 - pagination template will append `paginationSuffix` for input and label ids to avoid duplicate ids. #}
    {% if feature('v6.5.0.0') and paginationLocation %}
        {% set paginationSuffix = '-' ~ paginationLocation %}
    {% endif %}

    {% if totalPages > 1 %}
        <nav aria-label=\"pagination\" class=\"pagination-nav\">
            {% block component_pagination %}
            <ul class=\"pagination\">
                {% block component_pagination_first %}
                    <li class=\"page-item page-first{% if currentPage == 1 %} disabled{% endif %}\">
                        {% block component_pagination_first_input %}
                            <input type=\"radio\"
                                   {% if currentPage == 1 %}disabled=\"disabled\"{% endif %}
                                   name=\"p\"
                                   id=\"p-first{{ paginationSuffix }}\"
                                   value=\"1\"
                                   class=\"d-none\"
                                   title=\"pagination\">
                        {% endblock %}

                        {% block component_pagination_first_label %}
                            <label class=\"page-link\" for=\"p-first{{ paginationSuffix }}\">
                                {% block component_pagination_first_link %}
                                    {% sw_icon 'arrow-medium-double-left' style { 'size': 'fluid', 'pack': 'solid'} %}
                                {% endblock %}
                            </label>
                        {% endblock %}
                    </li>
                {% endblock %}

                {% block component_pagination_prev %}
                    <li class=\"page-item page-prev{% if currentPage == 1 %} disabled{% endif %}\">
                        {% block component_pagination_prev_input %}
                            <input type=\"radio\"
                                   {% if currentPage == 1 %}disabled=\"disabled\"{% endif %}
                                   name=\"p\"
                                   id=\"p-prev{{ paginationSuffix }}\"
                                   value=\"{{ currentPage - 1 }}\"
                                   class=\"d-none\"
                                   title=\"pagination\">
                        {% endblock %}

                        {% block component_pagination_prev_label %}
                            <label class=\"page-link\" for=\"p-prev{{ paginationSuffix }}\">
                                {% block component_pagination_prev_link %}
                                    {% block component_pagination_prev_icon %}
                                        {% sw_icon 'arrow-medium-left' style {'size': 'fluid', 'pack': 'solid'} %}
                                    {% endblock %}
                                {% endblock %}
                            </label>
                        {% endblock %}
                    </li>
                {% endblock %}

                {% block component_pagination_loop %}
                    {% set start = currentPage - 2 %}
                    {% if start <= 0 %}
                        {% set start = currentPage - 1 %}
                        {% if start <= 0 %}
                            {% set start = currentPage %}
                        {% endif %}
                    {% endif %}

                    {% set end = start + 4 %}

                    {% if end > totalPages %}
                        {% set end = totalPages %}
                    {% endif %}

                    {% for page in start..end %}

                        {% set isActive = (currentPage == page) %}

                        {% block component_pagination_item %}
                            <li class=\"page-item{% if isActive %} active{% endif %}\">
                                {% block component_pagination_item_input %}
                                    <input type=\"radio\"
                                           name=\"p\"
                                           id=\"p{{ page }}{{ paginationSuffix }}\"
                                           value=\"{{ page }}\"
                                           class=\"d-none\"
                                           title=\"pagination\"
                                           {% if isActive %}checked=\"checked\"{% endif %}>
                                {% endblock %}

                                {% block component_pagination_item_label %}
                                    <label class=\"page-link\"
                                           for=\"p{{ page }}{{ paginationSuffix }}\">
                                        {% block component_pagination_item_link %}
                                            {% block component_pagination_item_text %}
                                                {{ page }}
                                            {% endblock %}
                                        {% endblock %}
                                    </label>
                                {% endblock %}
                            </li>
                        {% endblock %}

                    {% endfor %}
                {% endblock %}

                {% block component_pagination_next %}
                    <li class=\"page-item page-next{% if currentPage == totalPages %} disabled{% endif %}\">
                        {% block component_pagination_next_input %}
                            <input type=\"radio\"
                                   {% if currentPage == totalPages %}disabled=\"disabled\"{% endif %}
                                   name=\"p\"
                                   id=\"p-next{{ paginationSuffix }}\"
                                   value=\"{{ currentPage + 1 }}\"
                                   class=\"d-none\"
                                   title=\"pagination\">
                        {% endblock %}

                        {% block component_pagination_next_label %}
                            <label class=\"page-link\" for=\"p-next{{ paginationSuffix }}\">
                                {% block component_pagination_next_link %}
                                    {% block component_pagination_next_icon %}
                                        {% sw_icon 'arrow-medium-right' style { 'size': 'fluid', 'pack': 'solid'} %}
                                    {% endblock %}
                                {% endblock %}
                            </label>
                        {% endblock %}
                    </li>
                {% endblock %}

                {% block component_pagination_last %}
                    <li class=\"page-item page-last{% if currentPage == totalPages %} disabled{% endif %}\">
                        {% block component_pagination_last_input %}
                            <input type=\"radio\"
                                   {% if currentPage == totalPages %}disabled=\"disabled\"{% endif %}
                                   name=\"p\"
                                   id=\"p-last{{ paginationSuffix }}\"
                                   value=\"{{ totalPages }}\"
                                   class=\"d-none\"
                                   title=\"pagination\">
                        {% endblock %}

                        {% block component_pagination_last_label %}
                            <label class=\"page-link\" for=\"p-last{{ paginationSuffix }}\">
                                {% block component_pagination_last_link %}
                                    {% block component_pagination_last_icon %}
                                        {% sw_icon 'arrow-medium-double-right' style {
                                            'size': 'fluid',
                                            'pack': 'solid'
                                        } %}
                                    {% endblock %}
                                {% endblock %}
                            </label>
                        {% endblock %}
                    </li>
                {% endblock %}
            </ul>
        {% endblock %}
        </nav>
    {% endif %}
{% endblock %}
", "@Storefront/storefront/component/pagination.html.twig", "/var/www/html/vendor/shopware/storefront/Resources/views/storefront/component/pagination.html.twig");
    }
}
