<?php

use Twig\Environment;
use function Shopware\Core\Framework\Adapter\Twig\sw_get_attribute;
use function Shopware\Core\Framework\Adapter\Twig\sw_escape_filter;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Storefront/storefront/utilities/thumbnail.html.twig */
class __TwigTemplate_94f71bef7e1dfbf7cfa28c15b926e1bc3b1b70d71e3ef680b54d488b9eb593c3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'thumbnail_utility' => [$this, 'block_thumbnail_utility'],
            'thumbnail_utility_img' => [$this, 'block_thumbnail_utility_img'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/utilities/thumbnail.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/utilities/thumbnail.html.twig"));

        // line 1
        $this->displayBlock('thumbnail_utility', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_thumbnail_utility($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "thumbnail_utility"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "thumbnail_utility"));

        // line 2
        echo "    ";
        // line 3
        echo "    ";
        if ( !array_key_exists("load", $context)) {
            // line 4
            echo "        ";
            $context["load"] = true;
            // line 5
            echo "    ";
        }
        // line 6
        echo "
    ";
        // line 8
        echo "    ";
        // line 9
        echo "    ";
        if ( !array_key_exists("loadOriginalImage", $context)) {
            // line 10
            echo "        ";
            $context["loadOriginalImage"] = false;
            // line 11
            echo "    ";
        }
        // line 12
        echo "
    ";
        // line 14
        echo "    ";
        // line 15
        echo "    ";
        if ( !array_key_exists("autoColumnSizes", $context)) {
            // line 16
            echo "        ";
            $context["autoColumnSizes"] = true;
            // line 17
            echo "    ";
        }
        // line 18
        echo "
    ";
        // line 19
        if ( !array_key_exists("attributes", $context)) {
            // line 20
            echo "        ";
            $context["attributes"] = [];
            // line 21
            echo "    ";
        }
        // line 22
        echo "
    ";
        // line 23
        if (( !sw_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "alt", [], "any", true, true, false, 23) && sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["media"] ?? null), "translated", [], "any", false, true, false, 23), "alt", [], "any", true, true, false, 23))) {
            // line 24
            echo "        ";
            $context["attributes"] = twig_array_merge(($context["attributes"] ?? null), ["alt" => sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["media"] ?? null), "translated", [], "any", false, false, false, 24), "alt", [], "any", false, false, false, 24)]);
            // line 25
            echo "    ";
        }
        // line 26
        echo "
    ";
        // line 27
        if (( !sw_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "title", [], "any", true, true, false, 27) && sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["media"] ?? null), "translated", [], "any", false, true, false, 27), "title", [], "any", true, true, false, 27))) {
            // line 28
            echo "        ";
            $context["attributes"] = twig_array_merge(($context["attributes"] ?? null), ["title" => sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["media"] ?? null), "translated", [], "any", false, false, false, 28), "title", [], "any", false, false, false, 28)]);
            // line 29
            echo "    ";
        }
        // line 30
        echo "
    ";
        // line 32
        echo "    ";
        if ((1 === twig_compare(twig_length_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["media"] ?? null), "thumbnails", [], "any", false, false, false, 32)), 0))) {
            // line 33
            echo "        ";
            if (((($context["autoColumnSizes"] ?? null) && ($context["columns"] ?? null)) &&  !array_key_exists("sizes", $context))) {
                // line 34
                echo "            ";
                // line 35
                echo "            ";
                $context["sizes"] = ["xs" => (($this->extensions['Shopware\Storefront\Framework\Twig\Extension\ConfigExtension']->theme($context, "breakpoint.sm") - 1) . "px"), "sm" => (($this->extensions['Shopware\Storefront\Framework\Twig\Extension\ConfigExtension']->theme($context, "breakpoint.md") - 1) . "px"), "md" => (twig_round((($this->extensions['Shopware\Storefront\Framework\Twig\Extension\ConfigExtension']->theme($context, "breakpoint.lg") - 1) /                 // line 38
($context["columns"] ?? null)), 0, "ceil") . "px"), "lg" => (twig_round((($this->extensions['Shopware\Storefront\Framework\Twig\Extension\ConfigExtension']->theme($context, "breakpoint.xl") - 1) /                 // line 39
($context["columns"] ?? null)), 0, "ceil") . "px")];
                // line 41
                echo "
            ";
                // line 43
                echo "            ";
                if ((0 === twig_compare(($context["layout"] ?? null), "full-width"))) {
                    // line 44
                    echo "                ";
                    $context["container"] = 100;
                    // line 45
                    echo "                ";
                    $context["sizes"] = twig_array_merge(($context["sizes"] ?? null), ["xl" => (twig_round((($context["container"] ?? null) / ($context["columns"] ?? null)), 0, "ceil") . "vw")]);
                    // line 46
                    echo "            ";
                } else {
                    // line 47
                    echo "                ";
                    $context["container"] = 1360;
                    // line 48
                    echo "                ";
                    $context["sizes"] = twig_array_merge(($context["sizes"] ?? null), ["xl" => (twig_round((($context["container"] ?? null) / ($context["columns"] ?? null)), 0, "ceil") . "px")]);
                    // line 49
                    echo "            ";
                }
                // line 50
                echo "        ";
            }
            // line 51
            echo "
        ";
            // line 52
            $context["thumbnails"] = twig_reverse_filter($this->env, $this->extensions['Shopware\Core\Framework\Adapter\Twig\SecurityExtension']->sort(sw_get_attribute($this->env, $this->source, ($context["media"] ?? null), "thumbnails", [], "any", false, false, false, 52)));
            // line 53
            echo "
        ";
            // line 55
            echo "        ";
            ob_start();
            ob_start();
            // line 56
            echo "            ";
            if (($context["loadOriginalImage"] ?? null)) {
                echo sw_escape_filter($this->env, $this->extensions['Shopware\Storefront\Framework\Twig\Extension\UrlEncodingTwigFilter']->encodeMediaUrl(($context["media"] ?? null)), "html", null, true);
                echo " ";
                echo sw_escape_filter($this->env, (sw_get_attribute($this->env, $this->source, twig_first($this->env, ($context["thumbnails"] ?? null)), "width", [], "any", false, false, false, 56) + 1), "html", null, true);
                echo "w, ";
            }
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["thumbnails"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["thumbnail"]) {
                echo sw_escape_filter($this->env, $this->extensions['Shopware\Storefront\Framework\Twig\Extension\UrlEncodingTwigFilter']->encodeUrl(sw_get_attribute($this->env, $this->source, $context["thumbnail"], "url", [], "any", false, false, false, 56)), "html", null, true);
                echo " ";
                echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, $context["thumbnail"], "width", [], "any", false, false, false, 56), "html", null, true);
                echo "w";
                if ( !sw_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 56)) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['thumbnail'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "        ";
            $___internal_parse_1_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 55
            echo twig_spaceless($___internal_parse_1_);
            $context["srcsetValue"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 58
            echo "
        ";
            // line 60
            echo "        ";
            ob_start();
            ob_start();
            // line 61
            echo "            ";
            $context["sizeFallback"] = 100;
            // line 62
            echo "
            ";
            // line 64
            echo "            ";
            if ((($context["autoColumnSizes"] ?? null) && ($context["columns"] ?? null))) {
                // line 65
                echo "                ";
                $context["sizeFallback"] = twig_round((($context["sizeFallback"] ?? null) / ($context["columns"] ?? null)), 0, "ceil");
                // line 66
                echo "            ";
            }
            // line 67
            echo "
            ";
            // line 68
            $context["breakpoint"] = ["xs" => $this->extensions['Shopware\Storefront\Framework\Twig\Extension\ConfigExtension']->theme($context, "breakpoint.xs"), "sm" => $this->extensions['Shopware\Storefront\Framework\Twig\Extension\ConfigExtension']->theme($context, "breakpoint.sm"), "md" => $this->extensions['Shopware\Storefront\Framework\Twig\Extension\ConfigExtension']->theme($context, "breakpoint.md"), "lg" => $this->extensions['Shopware\Storefront\Framework\Twig\Extension\ConfigExtension']->theme($context, "breakpoint.lg"), "xl" => $this->extensions['Shopware\Storefront\Framework\Twig\Extension\ConfigExtension']->theme($context, "breakpoint.xl")];
            // line 75
            echo "
            ";
            // line 76
            if ((1 === twig_compare(sw_get_attribute($this->env, $this->source, twig_first($this->env, ($context["thumbnails"] ?? null)), "width", [], "any", false, false, false, 76), twig_first($this->env, twig_reverse_filter($this->env, ($context["breakpoint"] ?? null)))))) {
                // line 77
                echo "                ";
                // line 78
                echo "                ";
                $context["maxWidth"] = sw_get_attribute($this->env, $this->source, twig_first($this->env, ($context["thumbnails"] ?? null)), "width", [], "any", false, false, false, 78);
                // line 79
                echo "            ";
            }
            // line 80
            echo "
            ";
            // line 81
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_reverse_filter($this->env, ($context["breakpoint"] ?? null)));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                echo "(min-width: ";
                echo sw_escape_filter($this->env, $context["value"], "html", null, true);
                echo "px) ";
                echo sw_escape_filter($this->env, (($__internal_compile_0 = ($context["sizes"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[$context["key"]] ?? null) : null), "html", null, true);
                if ( !sw_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 81)) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ", ";
            echo sw_escape_filter($this->env, ($context["sizeFallback"] ?? null), "html", null, true);
            echo "vw
        ";
            $___internal_parse_2_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 60
            echo twig_spaceless($___internal_parse_2_);
            $context["sizesValue"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 83
            echo "    ";
        }
        // line 84
        echo "
    ";
        // line 85
        $this->displayBlock('thumbnail_utility_img', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_thumbnail_utility_img($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "thumbnail_utility_img"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "thumbnail_utility_img"));

        // line 86
        echo "        <img ";
        if (($context["load"] ?? null)) {
            echo "src=\"";
            echo sw_escape_filter($this->env, $this->extensions['Shopware\Storefront\Framework\Twig\Extension\UrlEncodingTwigFilter']->encodeMediaUrl(($context["media"] ?? null)), "html", null, true);
            echo "\" ";
        } else {
            echo "data-src=\"";
            echo sw_escape_filter($this->env, $this->extensions['Shopware\Storefront\Framework\Twig\Extension\UrlEncodingTwigFilter']->encodeMediaUrl(($context["media"] ?? null)), "html", null, true);
            echo "\" ";
        }
        // line 87
        echo "            ";
        if ((1 === twig_compare(twig_length_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["media"] ?? null), "thumbnails", [], "any", false, false, false, 87)), 0))) {
            // line 88
            echo "                ";
            if (($context["load"] ?? null)) {
                echo "srcset=\"";
                echo sw_escape_filter($this->env, ($context["srcsetValue"] ?? null), "html", null, true);
                echo "\" ";
            } else {
                echo "data-srcset=\"";
                echo sw_escape_filter($this->env, ($context["srcsetValue"] ?? null), "html", null, true);
                echo "\" ";
            }
            // line 89
            echo "                ";
            if ((($__internal_compile_1 = ($context["sizes"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1["default"] ?? null) : null)) {
                // line 90
                echo "                sizes=\"";
                echo sw_escape_filter($this->env, (($__internal_compile_2 = ($context["sizes"] ?? null)) && is_array($__internal_compile_2) || $__internal_compile_2 instanceof ArrayAccess ? ($__internal_compile_2["default"] ?? null) : null), "html", null, true);
                echo "\"
                ";
            } elseif ((1 === twig_compare(twig_length_filter($this->env,             // line 91
($context["sizes"] ?? null)), 0))) {
                // line 92
                echo "                sizes=\"";
                echo sw_escape_filter($this->env, ($context["sizesValue"] ?? null), "html", null, true);
                echo "\"
                ";
            }
            // line 94
            echo "            ";
        }
        // line 95
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["attributes"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            if ((0 !== twig_compare($context["value"], ""))) {
                echo " ";
                echo sw_escape_filter($this->env, $context["key"], "html", null, true);
                echo "=\"";
                echo sw_escape_filter($this->env, $context["value"], "html", null, true);
                echo "\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 96
        echo "        />
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Storefront/storefront/utilities/thumbnail.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  412 => 96,  396 => 95,  393 => 94,  387 => 92,  385 => 91,  380 => 90,  377 => 89,  366 => 88,  363 => 87,  352 => 86,  333 => 85,  330 => 84,  327 => 83,  324 => 60,  283 => 81,  280 => 80,  277 => 79,  274 => 78,  272 => 77,  270 => 76,  267 => 75,  265 => 68,  262 => 67,  259 => 66,  256 => 65,  253 => 64,  250 => 62,  247 => 61,  243 => 60,  240 => 58,  237 => 55,  234 => 57,  191 => 56,  187 => 55,  184 => 53,  182 => 52,  179 => 51,  176 => 50,  173 => 49,  170 => 48,  167 => 47,  164 => 46,  161 => 45,  158 => 44,  155 => 43,  152 => 41,  150 => 39,  149 => 38,  147 => 35,  145 => 34,  142 => 33,  139 => 32,  136 => 30,  133 => 29,  130 => 28,  128 => 27,  125 => 26,  122 => 25,  119 => 24,  117 => 23,  114 => 22,  111 => 21,  108 => 20,  106 => 19,  103 => 18,  100 => 17,  97 => 16,  94 => 15,  92 => 14,  89 => 12,  86 => 11,  83 => 10,  80 => 9,  78 => 8,  75 => 6,  72 => 5,  69 => 4,  66 => 3,  64 => 2,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block thumbnail_utility %}
    {# activate load per default. If it is not activated only a data-src is set instead of the src tag. #}
    {% if load is not defined %}
        {% set load = true %}
    {% endif %}

    {# By default no original image will be loaded as soon as thumbnails are available. #}
    {# When set to true the orginal image will be loaded when the viewport is greater than the largest available thumbnail. #}
    {% if loadOriginalImage is not defined %}
        {% set loadOriginalImage = false %}
    {% endif %}

    {# By default the srcset sizes will be calculated automatically if `columns` are present and no `sizes` are configured. #}
    {# When set to false the sizes attribute will not be generated automatically. #}
    {% if autoColumnSizes is not defined %}
        {% set autoColumnSizes = true %}
    {% endif %}

    {% if attributes is not defined %}
        {% set attributes = {} %}
    {% endif %}

    {% if attributes.alt is not defined and media.translated.alt is defined %}
        {% set attributes = attributes|merge({'alt': media.translated.alt}) %}
    {% endif %}

    {% if attributes.title is not defined and media.translated.title is defined %}
        {% set attributes = attributes|merge({'title': media.translated.title}) %}
    {% endif %}

    {# uses cms block column count and all available thumbnails to determine the correct image size for the current viewport #}
    {% if media.thumbnails|length > 0 %}
        {% if autoColumnSizes and columns and sizes is not defined %}
            {# set image size for every viewport #}
            {% set sizes = {
                'xs': (theme_config('breakpoint.sm') - 1) ~'px',
                'sm': (theme_config('breakpoint.md') - 1) ~'px',
                'md': ((theme_config('breakpoint.lg') - 1) / columns)|round(0, 'ceil') ~'px',
                'lg': ((theme_config('breakpoint.xl') - 1) / columns)|round(0, 'ceil') ~'px'
            } %}

            {# set image size for largest viewport depending on the cms block sizing mode (boxed or full-width) #}
            {% if layout == 'full-width' %}
                {% set container = 100 %}
                {% set sizes = sizes|merge({ 'xl': (container / columns)|round(0, 'ceil') ~'vw'}) %}
            {% else %}
                {% set container = 1360 %}
                {% set sizes = sizes|merge({ 'xl': (container / columns)|round(0, 'ceil') ~'px'}) %}
            {% endif %}
        {% endif %}

        {% set thumbnails = media.thumbnails|sort|reverse %}

        {# generate srcset with all available thumbnails #}
        {% set srcsetValue %}{% apply spaceless %}
            {% if loadOriginalImage %}{{ media|sw_encode_media_url }} {{ thumbnails|first.width + 1 }}w, {% endif %}{% for thumbnail in thumbnails %}{{ thumbnail.url | sw_encode_url }} {{ thumbnail.width }}w{% if not loop.last %}, {% endif %}{% endfor %}
        {% endapply %}{% endset %}

        {# generate sizes #}
        {% set sizesValue %}{% apply spaceless %}
            {% set sizeFallback = 100 %}

            {# set largest size depending on column count of cms block #}
            {% if autoColumnSizes and columns %}
                {% set sizeFallback = (sizeFallback / columns)|round(0, 'ceil') %}
            {% endif %}

            {% set breakpoint = {
                'xs': theme_config('breakpoint.xs'),
                'sm': theme_config('breakpoint.sm'),
                'md': theme_config('breakpoint.md'),
                'lg': theme_config('breakpoint.lg'),
                'xl': theme_config('breakpoint.xl')
            } %}

            {% if thumbnails|first.width > breakpoint|reverse|first %}
                {# @deprecated tag:v6.5.0 - Variable `maxWidth` and parent condition will be removed #}
                {% set maxWidth = thumbnails|first.width %}
            {% endif %}

            {% for key, value in breakpoint|reverse %}(min-width: {{ value }}px) {{ sizes[key] }}{% if not loop.last %}, {% endif %}{% endfor %}, {{ sizeFallback }}vw
        {% endapply %}{% endset %}
    {% endif %}

    {% block thumbnail_utility_img %}
        <img {% if load %}src=\"{{ media|sw_encode_media_url }}\" {% else %}data-src=\"{{ media|sw_encode_media_url }}\" {% endif %}
            {% if media.thumbnails|length > 0 %}
                {% if load %}srcset=\"{{ srcsetValue }}\" {% else %}data-srcset=\"{{ srcsetValue }}\" {% endif %}
                {% if sizes['default'] %}
                sizes=\"{{ sizes['default'] }}\"
                {% elseif sizes|length > 0 %}
                sizes=\"{{ sizesValue }}\"
                {% endif %}
            {% endif %}
            {% for key, value in attributes %}{% if value != '' %} {{ key }}=\"{{ value }}\"{% endif %}{% endfor %}
        />
    {% endblock %}
{% endblock %}
", "@Storefront/storefront/utilities/thumbnail.html.twig", "/var/www/html/vendor/shopware/storefront/Resources/views/storefront/utilities/thumbnail.html.twig");
    }
}
