<?php

use Twig\Environment;
use function Shopware\Core\Framework\Adapter\Twig\sw_get_attribute;
use function Shopware\Core\Framework\Adapter\Twig\sw_escape_filter;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Storefront/storefront/page/product-detail/buy-widget-form.html.twig */
class __TwigTemplate_f109137b981f748b893f05b81990a13cba25f007564fa0d67df07dc5d70c3c42 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'page_product_detail_buy_form_inner' => [$this, 'block_page_product_detail_buy_form_inner'],
            'page_product_detail_buy_form_action' => [$this, 'block_page_product_detail_buy_form_action'],
            'page_product_detail_buy_form_inner_csrf' => [$this, 'block_page_product_detail_buy_form_inner_csrf'],
            'page_product_detail_buy_container' => [$this, 'block_page_product_detail_buy_container'],
            'page_product_detail_buy_quantity_container' => [$this, 'block_page_product_detail_buy_quantity_container'],
            'page_product_detail_buy_quantity' => [$this, 'block_page_product_detail_buy_quantity'],
            'page_product_detail_buy_quantity_input_group' => [$this, 'block_page_product_detail_buy_quantity_input_group'],
            'page_product_detail_buy_quantity_input' => [$this, 'block_page_product_detail_buy_quantity_input'],
            'page_product_detail_buy_quantity_input_unit' => [$this, 'block_page_product_detail_buy_quantity_input_unit'],
            'page_product_detail_buy_quantity_select' => [$this, 'block_page_product_detail_buy_quantity_select'],
            'page_product_detail_buy_redirect_input' => [$this, 'block_page_product_detail_buy_redirect_input'],
            'page_product_detail_buy_product_buy_info' => [$this, 'block_page_product_detail_buy_product_buy_info'],
            'page_product_detail_product_buy_meta' => [$this, 'block_page_product_detail_product_buy_meta'],
            'page_product_detail_buy_button_container' => [$this, 'block_page_product_detail_buy_button_container'],
            'page_product_detail_buy_button' => [$this, 'block_page_product_detail_buy_button'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/page/product-detail/buy-widget-form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/page/product-detail/buy-widget-form.html.twig"));

        // line 1
        $this->displayBlock('page_product_detail_buy_form_inner', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_page_product_detail_buy_form_inner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_form_inner"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_form_inner"));

        // line 2
        echo "    ";
        // line 3
        echo "
    ";
        // line 5
        echo "    ";
        $context["product"] = sw_get_attribute($this->env, $this->source, ($context["page"] ?? null), "product", [], "any", false, false, false, 5);
        // line 6
        echo "
    <form
        id=\"productDetailPageBuyProductForm\"
        action=\"";
        // line 9
        $this->displayBlock('page_product_detail_buy_form_action', $context, $blocks);
        echo "\"
        method=\"post\"
        class=\"buy-widget\"
        data-add-to-cart=\"true\">

        ";
        // line 15
        echo "        ";
        $this->displayBlock('page_product_detail_buy_form_inner_csrf', $context, $blocks);
        // line 18
        echo "
        ";
        // line 19
        $context["DOWNLOAD_STATE"] = twig_constant("Shopware\\Core\\Content\\Product\\State::IS_DOWNLOAD");
        // line 20
        echo "        ";
        $context["showQuantitySelect"] = (( !sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "states", [], "any", true, true, false, 20) || !twig_in_filter(($context["DOWNLOAD_STATE"] ?? null), sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "states", [], "any", false, false, false, 20))) || (twig_in_filter(($context["DOWNLOAD_STATE"] ?? null), sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "states", [], "any", false, false, false, 20)) && (sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "maxPurchase", [], "any", false, false, false, 20) !== 1)));
        // line 21
        echo "
        ";
        // line 22
        $context["buyable"] = ((sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "available", [], "any", false, false, false, 22) && (0 >= twig_compare(sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "childCount", [], "any", false, false, false, 22), 0))) && (1 === twig_compare(sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "calculatedMaxPurchase", [], "any", false, false, false, 22), 0)));
        // line 23
        echo "
        ";
        // line 24
        $this->displayBlock('page_product_detail_buy_container', $context, $blocks);
        // line 157
        echo "    </form>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_page_product_detail_buy_form_action($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_form_action"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_form_action"));

        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("frontend.checkout.line-item.add");
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_page_product_detail_buy_form_inner_csrf($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_form_inner_csrf"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_form_inner_csrf"));

        // line 16
        echo "            ";
        echo $this->extensions['Shopware\Storefront\Framework\Twig\Extension\CsrfFunctionExtension']->createCsrfPlaceholder("frontend.checkout.line-item.add");
        echo "
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 24
    public function block_page_product_detail_buy_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_container"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_container"));

        // line 25
        echo "            ";
        if (($context["buyable"] ?? null)) {
            // line 26
            echo "                <div class=\"";
            echo sw_escape_filter($this->env, ($context["formRowClass"] ?? null), "html", null, true);
            echo " buy-widget-container\">
                    ";
            // line 27
            $this->displayBlock('page_product_detail_buy_quantity_container', $context, $blocks);
            // line 92
            echo "
                    ";
            // line 93
            $this->displayBlock('page_product_detail_buy_redirect_input', $context, $blocks);
            // line 104
            echo "
                    ";
            // line 105
            $this->displayBlock('page_product_detail_buy_product_buy_info', $context, $blocks);
            // line 122
            echo "
                    ";
            // line 123
            $this->displayBlock('page_product_detail_product_buy_meta', $context, $blocks);
            // line 131
            echo "
                    ";
            // line 132
            $this->displayBlock('page_product_detail_buy_button_container', $context, $blocks);
            // line 154
            echo "                </div>
            ";
        }
        // line 156
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 27
    public function block_page_product_detail_buy_quantity_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity_container"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity_container"));

        // line 28
        echo "                        ";
        if (($context["showQuantitySelect"] ?? null)) {
            // line 29
            echo "                            <div class=\"col-4\">
                                ";
            // line 30
            $context["selectQuantityThreshold"] = 100;
            // line 31
            echo "                                ";
            $this->displayBlock('page_product_detail_buy_quantity', $context, $blocks);
            // line 89
            echo "                            </div>
                        ";
        }
        // line 91
        echo "                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 31
    public function block_page_product_detail_buy_quantity($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity"));

        // line 32
        echo "                                    ";
        // line 33
        echo "                                    ";
        if ((1 === twig_compare(((sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "calculatedMaxPurchase", [], "any", false, false, false, 33) - sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "minPurchase", [], "any", false, false, false, 33)) / sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "purchaseSteps", [], "any", false, false, false, 33)), ($context["selectQuantityThreshold"] ?? null)))) {
            // line 34
            echo "                                        ";
            $this->displayBlock('page_product_detail_buy_quantity_input_group', $context, $blocks);
            // line 67
            echo "                                    ";
        } else {
            // line 68
            echo "                                        ";
            $this->displayBlock('page_product_detail_buy_quantity_select', $context, $blocks);
            // line 87
            echo "                                    ";
        }
        // line 88
        echo "                                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 34
    public function block_page_product_detail_buy_quantity_input_group($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity_input_group"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity_input_group"));

        // line 35
        echo "                                            <div class=\"input-group\">
                                            ";
        // line 36
        $this->displayBlock('page_product_detail_buy_quantity_input', $context, $blocks);
        // line 47
        echo "                                                        ";
        $this->displayBlock('page_product_detail_buy_quantity_input_unit', $context, $blocks);
        // line 65
        echo "                                        </div>
                                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 36
    public function block_page_product_detail_buy_quantity_input($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity_input"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity_input"));

        // line 37
        echo "                                                <input
                                                    type=\"number\"
                                                    name=\"lineItems[";
        // line 39
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "id", [], "any", false, false, false, 39), "html", null, true);
        echo "][quantity]\"
                                                    class=\"form-control product-detail-quantity-input\"
                                                    min=\"";
        // line 41
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "minPurchase", [], "any", false, false, false, 41), "html", null, true);
        echo "\"
                                                    max=\"";
        // line 42
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "calculatedMaxPurchase", [], "any", false, false, false, 42), "html", null, true);
        echo "\"
                                                    step=\"";
        // line 43
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "purchaseSteps", [], "any", false, false, false, 43), "html", null, true);
        echo "\"
                                                    value=\"";
        // line 44
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "minPurchase", [], "any", false, false, false, 44), "html", null, true);
        echo "\"
                                                />
                                                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 47
    public function block_page_product_detail_buy_quantity_input_unit($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity_input_unit"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity_input_unit"));

        // line 48
        echo "                                                            ";
        if (sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translated", [], "any", false, false, false, 48), "packUnit", [], "any", false, false, false, 48)) {
            // line 49
            echo "                                                                ";
            if ( !$this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\FeatureFlagExtension']->feature("v6.5.0.0")) {
                // line 50
                echo "                                                                    <div class=\"input-group-append\">
                                                                ";
            }
            // line 52
            echo "
                                                                <span class=\"input-group-text\">
                                                                    ";
            // line 54
            if (((1 === twig_compare(sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "minPurchase", [], "any", false, false, false, 54), 1)) && sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translated", [], "any", false, false, false, 54), "packUnitPlural", [], "any", false, false, false, 54))) {
                // line 55
                echo "                                                                        ";
                echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translated", [], "any", false, false, false, 55), "packUnitPlural", [], "any", false, false, false, 55), "html", null, true);
                echo "
                                                                    ";
            } elseif (sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source,             // line 56
($context["product"] ?? null), "translated", [], "any", false, false, false, 56), "packUnit", [], "any", false, false, false, 56)) {
                // line 57
                echo "                                                                        ";
                echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translated", [], "any", false, false, false, 57), "packUnit", [], "any", false, false, false, 57), "html", null, true);
                echo "
                                                                    ";
            }
            // line 59
            echo "                                                                </span>
                                                            ";
            // line 60
            if ( !$this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\FeatureFlagExtension']->feature("v6.5.0.0")) {
                // line 61
                echo "                                                        </div>
                                                    ";
            }
            // line 63
            echo "                                                ";
        }
        // line 64
        echo "                                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 68
    public function block_page_product_detail_buy_quantity_select($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity_select"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_quantity_select"));

        // line 69
        echo "                                            <select name=\"lineItems[";
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "id", [], "any", false, false, false, 69), "html", null, true);
        echo "][quantity]\"
                                                class=\"";
        // line 70
        echo sw_escape_filter($this->env, ($context["formSelectClass"] ?? null), "html", null, true);
        echo " product-detail-quantity-select\">
                                                    ";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "minPurchase", [], "any", false, false, false, 71), sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "calculatedMaxPurchase", [], "any", false, false, false, 71), sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "purchaseSteps", [], "any", false, false, false, 71)));
        foreach ($context['_seq'] as $context["_key"] => $context["quantity"]) {
            // line 72
            echo "                                                        <option value=\"";
            echo sw_escape_filter($this->env, $context["quantity"], "html", null, true);
            echo "\">
                                                            ";
            // line 73
            echo sw_escape_filter($this->env, $context["quantity"], "html", null, true);
            echo "
                                                        ";
            // line 74
            if ((0 === twig_compare($context["quantity"], 1))) {
                // line 75
                echo "                                                            ";
                if (sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translated", [], "any", false, false, false, 75), "packUnit", [], "any", false, false, false, 75)) {
                    echo " ";
                    echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translated", [], "any", false, false, false, 75), "packUnit", [], "any", false, false, false, 75), "html", null, true);
                }
                // line 76
                echo "                                                        ";
            } else {
                // line 77
                echo "                                                            ";
                if (sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translated", [], "any", false, false, false, 77), "packUnitPlural", [], "any", false, false, false, 77)) {
                    // line 78
                    echo "                                                                ";
                    echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translated", [], "any", false, false, false, 78), "packUnitPlural", [], "any", false, false, false, 78), "html", null, true);
                    echo "
                                                            ";
                } elseif (sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source,                 // line 79
($context["product"] ?? null), "translated", [], "any", false, false, false, 79), "packUnit", [], "any", false, false, false, 79)) {
                    // line 80
                    echo "                                                                ";
                    echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translated", [], "any", false, false, false, 80), "packUnit", [], "any", false, false, false, 80), "html", null, true);
                    echo "
                                                            ";
                }
                // line 82
                echo "                                                        ";
            }
            // line 83
            echo "                                                    </option>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quantity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "                                            </select>
                                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 93
    public function block_page_product_detail_buy_redirect_input($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_redirect_input"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_redirect_input"));

        // line 94
        echo "                        ";
        // line 95
        echo "                        <input type=\"hidden\"
                               name=\"redirectTo\"
                               value=\"frontend.detail.page\">

                        <input type=\"hidden\"
                               name=\"redirectParameters\"
                               data-redirect-parameters=\"true\"
                               value='{\"productId\": \"";
        // line 102
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "id", [], "any", false, false, false, 102), "html", null, true);
        echo "\"}'>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 105
    public function block_page_product_detail_buy_product_buy_info($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_product_buy_info"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_product_buy_info"));

        // line 106
        echo "                        <input type=\"hidden\"
                               name=\"lineItems[";
        // line 107
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "id", [], "any", false, false, false, 107), "html", null, true);
        echo "][id]\"
                               value=\"";
        // line 108
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "id", [], "any", false, false, false, 108), "html", null, true);
        echo "\">
                        <input type=\"hidden\"
                               name=\"lineItems[";
        // line 110
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "id", [], "any", false, false, false, 110), "html", null, true);
        echo "][type]\"
                               value=\"product\">
                        <input type=\"hidden\"
                               name=\"lineItems[";
        // line 113
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "id", [], "any", false, false, false, 113), "html", null, true);
        echo "][referencedId]\"
                               value=\"";
        // line 114
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "id", [], "any", false, false, false, 114), "html", null, true);
        echo "\">
                        <input type=\"hidden\"
                               name=\"lineItems[";
        // line 116
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "id", [], "any", false, false, false, 116), "html", null, true);
        echo "][stackable]\"
                               value=\"1\">
                        <input type=\"hidden\"
                               name=\"lineItems[";
        // line 119
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "id", [], "any", false, false, false, 119), "html", null, true);
        echo "][removable]\"
                               value=\"1\">
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 123
    public function block_page_product_detail_product_buy_meta($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_product_buy_meta"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_product_buy_meta"));

        // line 124
        echo "                        <input type=\"hidden\"
                               name=\"product-name\"
                               value=\"";
        // line 126
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translated", [], "any", false, false, false, 126), "name", [], "any", false, false, false, 126), "html", null, true);
        echo "\">
                        <input type=\"hidden\"
                               name=\"brand-name\"
                               value=\"";
        // line 129
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["product"] ?? null), "manufacturer", [], "any", false, false, false, 129), "getName", [], "method", false, false, false, 129), "html", null, true);
        echo "\">
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 132
    public function block_page_product_detail_buy_button_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_button_container"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_button_container"));

        // line 133
        echo "                        <div class=\"";
        if (($context["showQuantitySelect"] ?? null)) {
            echo "col-8";
        } else {
            echo "col-12";
        }
        echo "\">
                            ";
        // line 134
        $this->displayBlock('page_product_detail_buy_button', $context, $blocks);
        // line 152
        echo "                        </div>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 134
    public function block_page_product_detail_buy_button($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_button"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_buy_button"));

        // line 135
        echo "                                ";
        // line 136
        echo "                                ";
        if ($this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\FeatureFlagExtension']->feature("v6.5.0.0")) {
            // line 137
            echo "                                    <div class=\"d-grid\">
                                        <button class=\"btn btn-primary btn-buy\"
                                                title=\"";
            // line 139
            echo sw_escape_filter($this->env, twig_striptags($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.addProduct")), "html", null, true);
            echo "\"
                                                aria-label=\"";
            // line 140
            echo sw_escape_filter($this->env, twig_striptags($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.addProduct")), "html", null, true);
            echo "\">
                                            ";
            // line 141
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.addProduct"));
            echo "
                                        </button>
                                    </div>
                                ";
        } else {
            // line 145
            echo "                                    <button class=\"btn btn-primary btn-block btn-buy\"
                                            title=\"";
            // line 146
            echo sw_escape_filter($this->env, twig_striptags($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.addProduct")), "html", null, true);
            echo "\"
                                            aria-label=\"";
            // line 147
            echo sw_escape_filter($this->env, twig_striptags($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.addProduct")), "html", null, true);
            echo "\">
                                        ";
            // line 148
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.addProduct"));
            echo "
                                    </button>
                                ";
        }
        // line 151
        echo "                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Storefront/storefront/page/product-detail/buy-widget-form.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  716 => 151,  710 => 148,  706 => 147,  702 => 146,  699 => 145,  692 => 141,  688 => 140,  684 => 139,  680 => 137,  677 => 136,  675 => 135,  665 => 134,  654 => 152,  652 => 134,  643 => 133,  633 => 132,  621 => 129,  615 => 126,  611 => 124,  601 => 123,  588 => 119,  582 => 116,  577 => 114,  573 => 113,  567 => 110,  562 => 108,  558 => 107,  555 => 106,  545 => 105,  533 => 102,  524 => 95,  522 => 94,  512 => 93,  501 => 85,  494 => 83,  491 => 82,  485 => 80,  483 => 79,  478 => 78,  475 => 77,  472 => 76,  466 => 75,  464 => 74,  460 => 73,  455 => 72,  451 => 71,  447 => 70,  442 => 69,  432 => 68,  422 => 64,  419 => 63,  415 => 61,  413 => 60,  410 => 59,  404 => 57,  402 => 56,  397 => 55,  395 => 54,  391 => 52,  387 => 50,  384 => 49,  381 => 48,  371 => 47,  358 => 44,  354 => 43,  350 => 42,  346 => 41,  341 => 39,  337 => 37,  327 => 36,  316 => 65,  313 => 47,  311 => 36,  308 => 35,  298 => 34,  288 => 88,  285 => 87,  282 => 68,  279 => 67,  276 => 34,  273 => 33,  271 => 32,  261 => 31,  251 => 91,  247 => 89,  244 => 31,  242 => 30,  239 => 29,  236 => 28,  226 => 27,  216 => 156,  212 => 154,  210 => 132,  207 => 131,  205 => 123,  202 => 122,  200 => 105,  197 => 104,  195 => 93,  192 => 92,  190 => 27,  185 => 26,  182 => 25,  172 => 24,  159 => 16,  149 => 15,  130 => 9,  119 => 157,  117 => 24,  114 => 23,  112 => 22,  109 => 21,  106 => 20,  104 => 19,  101 => 18,  98 => 15,  90 => 9,  85 => 6,  82 => 5,  79 => 3,  77 => 2,  58 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block page_product_detail_buy_form_inner %}
    {# @var page \\Shopware\\Storefront\\Page\\Product\\ProductPage #}

    {# @var product \\Shopware\\Core\\Content\\Product\\SalesChannel\\SalesChannelProductEntity #}
    {% set product = page.product %}

    <form
        id=\"productDetailPageBuyProductForm\"
        action=\"{% block page_product_detail_buy_form_action %}{{ path('frontend.checkout.line-item.add') }}{% endblock %}\"
        method=\"post\"
        class=\"buy-widget\"
        data-add-to-cart=\"true\">

        {# @deprecated tag:v6.5.0 - Block page_product_detail_buy_form_inner_csrf will be removed. #}
        {% block page_product_detail_buy_form_inner_csrf %}
            {{ sw_csrf('frontend.checkout.line-item.add') }}
        {% endblock %}

        {% set DOWNLOAD_STATE = constant('Shopware\\\\Core\\\\Content\\\\Product\\\\State::IS_DOWNLOAD') %}
        {% set showQuantitySelect = not product.states is defined or DOWNLOAD_STATE not in product.states or (DOWNLOAD_STATE in product.states and product.maxPurchase !== 1) %}

        {% set buyable = product.available and product.childCount <= 0 and product.calculatedMaxPurchase > 0 %}

        {% block page_product_detail_buy_container %}
            {% if buyable %}
                <div class=\"{{ formRowClass }} buy-widget-container\">
                    {% block page_product_detail_buy_quantity_container %}
                        {% if showQuantitySelect %}
                            <div class=\"col-4\">
                                {% set selectQuantityThreshold = 100 %}
                                {% block page_product_detail_buy_quantity %}
                                    {# @deprecated tag:v6.5.0 - Numeric quantity input with additional plus/minus controls will be the default. #}
                                    {% if (product.calculatedMaxPurchase - product.minPurchase) / product.purchaseSteps > selectQuantityThreshold %}
                                        {% block page_product_detail_buy_quantity_input_group %}
                                            <div class=\"input-group\">
                                            {% block page_product_detail_buy_quantity_input %}
                                                <input
                                                    type=\"number\"
                                                    name=\"lineItems[{{ product.id }}][quantity]\"
                                                    class=\"form-control product-detail-quantity-input\"
                                                    min=\"{{ product.minPurchase }}\"
                                                    max=\"{{ product.calculatedMaxPurchase }}\"
                                                    step=\"{{ product.purchaseSteps }}\"
                                                    value=\"{{ product.minPurchase }}\"
                                                />
                                                    {% endblock %}
                                                        {% block page_product_detail_buy_quantity_input_unit %}
                                                            {% if product.translated.packUnit %}
                                                                {% if not feature('v6.5.0.0') %}
                                                                    <div class=\"input-group-append\">
                                                                {% endif %}

                                                                <span class=\"input-group-text\">
                                                                    {% if product.minPurchase > 1 and product.translated.packUnitPlural %}
                                                                        {{ product.translated.packUnitPlural }}
                                                                    {% elseif product.translated.packUnit %}
                                                                        {{ product.translated.packUnit }}
                                                                    {% endif %}
                                                                </span>
                                                            {% if not feature('v6.5.0.0') %}
                                                        </div>
                                                    {% endif %}
                                                {% endif %}
                                            {% endblock %}
                                        </div>
                                    {% endblock %}
                                    {% else %}
                                        {% block page_product_detail_buy_quantity_select %}
                                            <select name=\"lineItems[{{ product.id }}][quantity]\"
                                                class=\"{{ formSelectClass }} product-detail-quantity-select\">
                                                    {% for quantity in range(product.minPurchase, product.calculatedMaxPurchase, product.purchaseSteps) %}
                                                        <option value=\"{{ quantity }}\">
                                                            {{ quantity }}
                                                        {% if quantity == 1 %}
                                                            {% if product.translated.packUnit %} {{ product.translated.packUnit }}{% endif %}
                                                        {% else %}
                                                            {% if product.translated.packUnitPlural %}
                                                                {{ product.translated.packUnitPlural }}
                                                            {% elseif product.translated.packUnit %}
                                                                {{ product.translated.packUnit }}
                                                            {% endif %}
                                                        {% endif %}
                                                    </option>
                                                {% endfor %}
                                            </select>
                                        {% endblock %}
                                    {% endif %}
                                {% endblock %}
                            </div>
                        {% endif %}
                    {% endblock %}

                    {% block page_product_detail_buy_redirect_input %}
                        {# fallback redirect back to detail page is deactivated via js #}
                        <input type=\"hidden\"
                               name=\"redirectTo\"
                               value=\"frontend.detail.page\">

                        <input type=\"hidden\"
                               name=\"redirectParameters\"
                               data-redirect-parameters=\"true\"
                               value='{\"productId\": \"{{ product.id }}\"}'>
                    {% endblock %}

                    {% block page_product_detail_buy_product_buy_info %}
                        <input type=\"hidden\"
                               name=\"lineItems[{{ product.id }}][id]\"
                               value=\"{{ product.id }}\">
                        <input type=\"hidden\"
                               name=\"lineItems[{{ product.id }}][type]\"
                               value=\"product\">
                        <input type=\"hidden\"
                               name=\"lineItems[{{ product.id }}][referencedId]\"
                               value=\"{{ product.id }}\">
                        <input type=\"hidden\"
                               name=\"lineItems[{{ product.id }}][stackable]\"
                               value=\"1\">
                        <input type=\"hidden\"
                               name=\"lineItems[{{ product.id }}][removable]\"
                               value=\"1\">
                    {% endblock %}

                    {% block page_product_detail_product_buy_meta %}
                        <input type=\"hidden\"
                               name=\"product-name\"
                               value=\"{{ product.translated.name }}\">
                        <input type=\"hidden\"
                               name=\"brand-name\"
                               value=\"{{ product.manufacturer.getName() }}\">
                    {% endblock %}

                    {% block page_product_detail_buy_button_container %}
                        <div class=\"{% if showQuantitySelect %}col-8{% else %}col-12{% endif %}\">
                            {% block page_product_detail_buy_button %}
                                {# @deprecated tag:v6.5.0 - Bootstrap v5 removes `btn-block` class, use `d-grid` wrapper instead #}
                                {% if feature('v6.5.0.0') %}
                                    <div class=\"d-grid\">
                                        <button class=\"btn btn-primary btn-buy\"
                                                title=\"{{ \"detail.addProduct\"|trans|striptags }}\"
                                                aria-label=\"{{ \"detail.addProduct\"|trans|striptags }}\">
                                            {{ \"detail.addProduct\"|trans|sw_sanitize }}
                                        </button>
                                    </div>
                                {% else %}
                                    <button class=\"btn btn-primary btn-block btn-buy\"
                                            title=\"{{ \"detail.addProduct\"|trans|striptags }}\"
                                            aria-label=\"{{ \"detail.addProduct\"|trans|striptags }}\">
                                        {{ \"detail.addProduct\"|trans|sw_sanitize }}
                                    </button>
                                {% endif %}
                            {% endblock %}
                        </div>
                    {% endblock %}
                </div>
            {% endif %}
        {% endblock %}
    </form>
{% endblock %}
", "@Storefront/storefront/page/product-detail/buy-widget-form.html.twig", "/var/www/html/vendor/shopware/storefront/Resources/views/storefront/page/product-detail/buy-widget-form.html.twig");
    }
}
