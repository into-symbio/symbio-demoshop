<?php

use Twig\Environment;
use function Shopware\Core\Framework\Adapter\Twig\sw_get_attribute;
use function Shopware\Core\Framework\Adapter\Twig\sw_escape_filter;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Storefront/storefront/page/product-detail/review/review-widget.html.twig */
class __TwigTemplate_39703289a70e3272f35af1b05f1a6a6df80f92152365c6ad9b83cda1bb489dec extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'page_product_detail_review_widget' => [$this, 'block_page_product_detail_review_widget'],
            'page_product_detail_review_info_container' => [$this, 'block_page_product_detail_review_info_container'],
            'page_product_detail_review_info' => [$this, 'block_page_product_detail_review_info'],
            'page_product_detail_review_title' => [$this, 'block_page_product_detail_review_title'],
            'page_product_detail_review_overview' => [$this, 'block_page_product_detail_review_overview'],
            'page_product_detail_review_filter' => [$this, 'block_page_product_detail_review_filter'],
            'page_product_detail_review_filter_csrf' => [$this, 'block_page_product_detail_review_filter_csrf'],
            'page_product_detail_review_filter_box' => [$this, 'block_page_product_detail_review_filter_box'],
            'page_product_detail_review_filter_checkbox' => [$this, 'block_page_product_detail_review_filter_checkbox'],
            'page_product_detail_review_filter_checkbox_input' => [$this, 'block_page_product_detail_review_filter_checkbox_input'],
            'page_product_detail_review_filter_checkbox_label' => [$this, 'block_page_product_detail_review_filter_checkbox_label'],
            'page_product_detail_review_filter_progressbar' => [$this, 'block_page_product_detail_review_filter_progressbar'],
            'page_product_detail_review_filter_share' => [$this, 'block_page_product_detail_review_filter_share'],
            'page_product_detail_review_filter_divider' => [$this, 'block_page_product_detail_review_filter_divider'],
            'page_product_detail_review_form_teaser' => [$this, 'block_page_product_detail_review_form_teaser'],
            'page_product_detail_review_form_teaser_header' => [$this, 'block_page_product_detail_review_form_teaser_header'],
            'page_product_detail_review_form_teaser_text' => [$this, 'block_page_product_detail_review_form_teaser_text'],
            'page_product_detail_review_form_teaser_button' => [$this, 'block_page_product_detail_review_form_teaser_button'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/page/product-detail/review/review-widget.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Storefront/storefront/page/product-detail/review/review-widget.html.twig"));

        // line 1
        $this->displayBlock('page_product_detail_review_widget', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_page_product_detail_review_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_widget"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_widget"));

        // line 2
        echo "    <div class=\"sticky-top product-detail-review-widget\">

        ";
        // line 4
        $this->displayBlock('page_product_detail_review_info_container', $context, $blocks);
        // line 141
        echo "
        ";
        // line 142
        $this->displayBlock('page_product_detail_review_form_teaser', $context, $blocks);
        // line 186
        echo "    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_page_product_detail_review_info_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_info_container"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_info_container"));

        // line 5
        echo "            ";
        $this->displayBlock('page_product_detail_review_info', $context, $blocks);
        // line 40
        echo "
            ";
        // line 41
        if ((1 === twig_compare(($context["productReviewCount"] ?? null), 0))) {
            // line 42
            echo "                <hr>
            ";
        }
        // line 44
        echo "
            ";
        // line 45
        $context["formAjaxSubmitOptions"] = ["replaceSelectors" => [0 => ".js-review-container"], "submitOnChange" => true];
        // line 49
        echo "
            ";
        // line 50
        $this->displayBlock('page_product_detail_review_filter', $context, $blocks);
        // line 140
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_page_product_detail_review_info($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_info"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_info"));

        // line 6
        echo "                <div class=\"product-detail-review-info js-review-info\">

                    ";
        // line 8
        $this->displayBlock('page_product_detail_review_title', $context, $blocks);
        // line 13
        echo "
                    ";
        // line 14
        $this->displayBlock('page_product_detail_review_overview', $context, $blocks);
        // line 38
        echo "                </div>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 8
    public function block_page_product_detail_review_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_title"));

        // line 9
        echo "                        <p class=\"product-detail-review-title h5\">
                            ";
        // line 10
        echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.reviewTitle", ["%count%" => sw_get_attribute($this->env, $this->source, ($context["reviews"] ?? null), "total", [], "any", false, false, false, 10), "%total%" => sw_get_attribute($this->env, $this->source, ($context["reviews"] ?? null), "totalReviews", [], "any", false, false, false, 10)]));
        echo "
                        </p>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_page_product_detail_review_overview($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_overview"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_overview"));

        // line 15
        echo "                        <div class=\"product-detail-review-rating\"
                            ";
        // line 16
        if ((1 === twig_compare(($context["productReviewCount"] ?? null), 0))) {
            // line 17
            echo "                                itemprop=\"aggregateRating\" itemscope itemtype=\"https://schema.org/AggregateRating\"
                            ";
        }
        // line 18
        echo ">

                            ";
        // line 20
        if ((1 === twig_compare(($context["productReviewCount"] ?? null), 0))) {
            // line 21
            echo "                                <meta itemprop=\"bestRating\" content=\"5\">
                                <meta itemprop=\"ratingCount\" content=\"";
            // line 22
            echo sw_escape_filter($this->env, ($context["productReviewCount"] ?? null), "html", null, true);
            echo "\">
                                <meta itemprop=\"ratingValue\" content=\"";
            // line 23
            echo sw_escape_filter($this->env, ($context["productAvgRating"] ?? null), "html", null, true);
            echo "\">
                            ";
        }
        // line 25
        echo "
                            ";
        // line 26
        $this->loadTemplate("@Storefront/storefront/component/review/rating.html.twig", "@Storefront/storefront/page/product-detail/review/review-widget.html.twig", 26)->display(twig_array_merge($context, ["points" =>         // line 27
($context["productAvgRating"] ?? null), "style" => "text-primary"]));
        // line 30
        echo "
                            ";
        // line 31
        if ((1 === twig_compare(($context["productReviewCount"] ?? null), 0))) {
            // line 32
            echo "                                <p class=\"h6\">
                                    ";
            // line 33
            echo sw_escape_filter($this->env, ($context["productAvgRating"] ?? null), "html", null, true);
            echo " ";
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.reviewAvgRate"));
            echo " ";
            echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["reviews"] ?? null), "matrix", [], "any", false, false, false, 33), "maxPoints", [], "any", false, false, false, 33), "html", null, true);
            echo " ";
            echo sw_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.reviewAvgRateElements"), "html", null, true);
            echo "
                                </p>
                            ";
        }
        // line 36
        echo "                        </div>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 50
    public function block_page_product_detail_review_filter($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter"));

        // line 51
        echo "                ";
        if ((1 === twig_compare(($context["productReviewCount"] ?? null), 0))) {
            // line 52
            echo "                    <div class=\"js-review-filter\">
                        <form class=\"review-filter-form\"
                              action=\"";
            // line 54
            echo sw_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("frontend.product.reviews", ["productId" => sw_get_attribute($this->env, $this->source, ($context["reviews"] ?? null), "productId", [], "any", false, false, false, 54), "parentId" => sw_get_attribute($this->env, $this->source, ($context["reviews"] ?? null), "parentId", [], "any", false, false, false, 54)]), "html", null, true);
            echo "\"
                              method=\"post\"
                              data-form-ajax-submit=\"true\"
                              data-form-ajax-submit-options='";
            // line 57
            echo sw_escape_filter($this->env, $this->env->getFilter('json_encode')->getCallable()(($context["formAjaxSubmitOptions"] ?? null)), "html", null, true);
            echo "'>

                            ";
            // line 60
            echo "                            ";
            $this->displayBlock('page_product_detail_review_filter_csrf', $context, $blocks);
            // line 63
            echo "
                            ";
            // line 64
            if (sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 64), "get", [0 => "limit"], "method", false, false, false, 64)) {
                // line 65
                echo "                                <input type=\"hidden\" name=\"limit\" value=\"";
                echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 65), "get", [0 => "limit"], "method", false, false, false, 65), "html", null, true);
                echo "\">
                            ";
            }
            // line 67
            echo "
                            ";
            // line 68
            if (sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 68), "get", [0 => "language"], "method", false, false, false, 68)) {
                // line 69
                echo "                                <input type=\"hidden\" name=\"language\" value=\"";
                echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 69), "get", [0 => "language"], "method", false, false, false, 69), "html", null, true);
                echo "\">
                            ";
            }
            // line 71
            echo "
                            ";
            // line 72
            if (sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 72), "get", [0 => "sort"], "method", false, false, false, 72)) {
                // line 73
                echo "                                <input type=\"hidden\" name=\"sort\" value=\"";
                echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 73), "get", [0 => "sort"], "method", false, false, false, 73), "html", null, true);
                echo "\">
                            ";
            }
            // line 75
            echo "
                            ";
            // line 76
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["reviews"] ?? null), "matrix", [], "any", false, false, false, 76), "matrix", [], "any", false, false, false, 76));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["matrix"]) {
                // line 77
                echo "                                ";
                $this->displayBlock('page_product_detail_review_filter_box', $context, $blocks);
                // line 131
                echo "                            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['matrix'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 132
            echo "                        </form>
                    </div>

                    ";
            // line 135
            $this->displayBlock('page_product_detail_review_filter_divider', $context, $blocks);
            // line 138
            echo "                ";
        }
        // line 139
        echo "            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 60
    public function block_page_product_detail_review_filter_csrf($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_csrf"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_csrf"));

        // line 61
        echo "                                ";
        echo $this->extensions['Shopware\Storefront\Framework\Twig\Extension\CsrfFunctionExtension']->createCsrfPlaceholder("frontend.product.reviews");
        echo "
                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 77
    public function block_page_product_detail_review_filter_box($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_box"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_box"));

        // line 78
        echo "                                    <div class=\"row product-detail-review-filter\">

                                        ";
        // line 80
        $this->displayBlock('page_product_detail_review_filter_checkbox', $context, $blocks);
        // line 110
        echo "
                                        ";
        // line 111
        $this->displayBlock('page_product_detail_review_filter_progressbar', $context, $blocks);
        // line 123
        echo "
                                        ";
        // line 124
        $this->displayBlock('page_product_detail_review_filter_share', $context, $blocks);
        // line 129
        echo "                                    </div>
                                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 80
    public function block_page_product_detail_review_filter_checkbox($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_checkbox"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_checkbox"));

        // line 81
        echo "                                            <div class=\"col-md-8 col-lg-5 product-detail-review-checkbox\">
                                                <div class=\"";
        // line 82
        echo sw_escape_filter($this->env, ($context["formCheckboxWrapperClass"] ?? null), "html", null, true);
        echo "\">

                                                    ";
        // line 84
        $this->displayBlock('page_product_detail_review_filter_checkbox_input', $context, $blocks);
        // line 99
        echo "
                                                    ";
        // line 100
        $this->displayBlock('page_product_detail_review_filter_checkbox_label', $context, $blocks);
        // line 107
        echo "                                                </div>
                                            </div>
                                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 84
    public function block_page_product_detail_review_filter_checkbox_input($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_checkbox_input"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_checkbox_input"));

        // line 85
        echo "                                                        <input type=\"checkbox\"
                                                               class=\"";
        // line 86
        echo sw_escape_filter($this->env, ($context["formCheckInputClass"] ?? null), "html", null, true);
        echo "\"
                                                               id=\"reviewRating";
        // line 87
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["matrix"] ?? null), "points", [], "any", false, false, false, 87), "html", null, true);
        echo "\"
                                                               name=\"points[]\"
                                                            ";
        // line 89
        if (sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 89), "get", [0 => "points"], "method", false, false, false, 89)) {
            // line 90
            echo "                                                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(sw_get_attribute($this->env, $this->source, sw_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 90), "get", [0 => "points"], "method", false, false, false, 90));
            foreach ($context['_seq'] as $context["_key"] => $context["points"]) {
                // line 91
                echo "                                                                    ";
                if ((0 === twig_compare($context["points"], sw_get_attribute($this->env, $this->source, ($context["matrix"] ?? null), "points", [], "any", false, false, false, 91)))) {
                    // line 92
                    echo "                                                                        checked=\"checked\"
                                                                    ";
                }
                // line 94
                echo "                                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['points'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 95
            echo "                                                            ";
        }
        // line 96
        echo "                                                               value=\"";
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["matrix"] ?? null), "points", [], "any", false, false, false, 96), "html", null, true);
        echo "\"
                                                            ";
        // line 97
        if ((-1 === twig_compare(sw_get_attribute($this->env, $this->source, ($context["matrix"] ?? null), "count", [], "any", false, false, false, 97), 1))) {
            echo "disabled";
        }
        echo ">
                                                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 100
    public function block_page_product_detail_review_filter_checkbox_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_checkbox_label"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_checkbox_label"));

        // line 101
        echo "                                                        <label class=\"custom-control-label text-nowrap\"
                                                               for=\"reviewRating";
        // line 102
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["matrix"] ?? null), "points", [], "any", false, false, false, 102), "html", null, true);
        echo "\">
                                                            <small>";
        // line 103
        echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((("detail.review" . sw_get_attribute($this->env, $this->source, ($context["matrix"] ?? null), "points", [], "any", false, false, false, 103)) . "PointRatingText")));
        echo "
                                                                (";
        // line 104
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["matrix"] ?? null), "count", [], "any", false, false, false, 104), "html", null, true);
        echo ")</small>
                                                        </label>
                                                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 111
    public function block_page_product_detail_review_filter_progressbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_progressbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_progressbar"));

        // line 112
        echo "                                            <div class=\"col d-none d-lg-block product-detail-review-progressbar-col\">
                                                <div class=\"progress product-detail-review-progressbar-container\">
                                                    <div class=\"progress-bar product-detail-review-progressbar-bar\"
                                                         role=\"progressbar\"
                                                         style=\"width: ";
        // line 116
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["matrix"] ?? null), "percent", [], "any", false, false, false, 116), "html", null, true);
        echo "%;\"
                                                         aria-valuenow=\"";
        // line 117
        echo sw_escape_filter($this->env, sw_get_attribute($this->env, $this->source, ($context["matrix"] ?? null), "percent", [], "any", false, false, false, 117), "html", null, true);
        echo "\"
                                                         aria-valuemin=\"0\"
                                                         aria-valuemax=\"100\"></div>
                                                </div>
                                            </div>
                                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 124
    public function block_page_product_detail_review_filter_share($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_share"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_share"));

        // line 125
        echo "                                            <div class=\"col-12 col-md-3 product-detail-review-share\">
                                                <p><small>";
        // line 126
        echo sw_escape_filter($this->env, twig_round(sw_get_attribute($this->env, $this->source, ($context["matrix"] ?? null), "percent", [], "any", false, false, false, 126)), "html", null, true);
        echo "%</small></p>
                                            </div>
                                        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 135
    public function block_page_product_detail_review_filter_divider($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_divider"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_filter_divider"));

        // line 136
        echo "                        <hr/>
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 142
    public function block_page_product_detail_review_form_teaser($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_form_teaser"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_form_teaser"));

        // line 143
        echo "            <div class=\"product-detail-review-teaser js-review-teaser\">

                ";
        // line 145
        $this->displayBlock('page_product_detail_review_form_teaser_header', $context, $blocks);
        // line 154
        echo "
                ";
        // line 155
        $this->displayBlock('page_product_detail_review_form_teaser_text', $context, $blocks);
        // line 164
        echo "
                ";
        // line 165
        $this->displayBlock('page_product_detail_review_form_teaser_button', $context, $blocks);
        // line 184
        echo "            </div>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 145
    public function block_page_product_detail_review_form_teaser_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_form_teaser_header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_form_teaser_header"));

        // line 146
        echo "                    <p class=\"h4\">
                        ";
        // line 147
        if ( !sw_get_attribute($this->env, $this->source, ($context["reviews"] ?? null), "customerReview", [], "any", false, false, false, 147)) {
            // line 148
            echo "                            ";
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.reviewTeaserTitle"));
            echo "
                        ";
        } else {
            // line 150
            echo "                            ";
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.reviewExistsTeaserTitle"));
            echo "
                        ";
        }
        // line 152
        echo "                    </p>
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 155
    public function block_page_product_detail_review_form_teaser_text($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_form_teaser_text"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_form_teaser_text"));

        // line 156
        echo "                    <p>
                        ";
        // line 157
        if ( !sw_get_attribute($this->env, $this->source, ($context["page"] ?? null), "customerReview", [], "any", false, false, false, 157)) {
            // line 158
            echo "                            ";
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.reviewTeaserText"));
            echo "
                        ";
        } else {
            // line 160
            echo "                            ";
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.reviewExistsTeaserText"));
            echo "
                        ";
        }
        // line 162
        echo "                    </p>
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 165
    public function block_page_product_detail_review_form_teaser_button($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_form_teaser_button"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_product_detail_review_form_teaser_button"));

        // line 166
        echo "                    <button class=\"btn btn-primary product-detail-review-teaser-btn\"
                            type=\"button\"
                            ";
        // line 168
        echo sw_escape_filter($this->env, ($context["dataBsToggleAttr"] ?? null), "html", null, true);
        echo "=\"collapse\"
                            ";
        // line 169
        echo sw_escape_filter($this->env, ($context["dataBsTargetAttr"] ?? null), "html", null, true);
        echo "=\".multi-collapse\"
                            aria-expanded=\"false\"
                            aria-controls=\"review-form review-list\">
                        <span class=\"product-detail-review-teaser-show\">
\t\t\t\t\t\t\t";
        // line 173
        if ( !sw_get_attribute($this->env, $this->source, ($context["reviews"] ?? null), "customerReview", [], "any", false, false, false, 173)) {
            // line 174
            echo "                                ";
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.reviewTeaserButton"));
            echo "
                            ";
        } else {
            // line 176
            echo "                                ";
            echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.reviewExistsTeaserButton"));
            echo "
                            ";
        }
        // line 178
        echo "\t\t\t\t\t\t</span>
                        <span class=\"product-detail-review-teaser-hide\">
\t\t\t\t\t\t\t";
        // line 180
        echo $this->extensions['Shopware\Core\Framework\Adapter\Twig\Extension\SwSanitizeTwigFilter']->sanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("detail.reviewTeaserButtonHide"));
        echo "
\t\t\t\t\t\t</span>
                    </button>
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Storefront/storefront/page/product-detail/review/review-widget.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  831 => 180,  827 => 178,  821 => 176,  815 => 174,  813 => 173,  806 => 169,  802 => 168,  798 => 166,  788 => 165,  777 => 162,  771 => 160,  765 => 158,  763 => 157,  760 => 156,  750 => 155,  739 => 152,  733 => 150,  727 => 148,  725 => 147,  722 => 146,  712 => 145,  701 => 184,  699 => 165,  696 => 164,  694 => 155,  691 => 154,  689 => 145,  685 => 143,  675 => 142,  664 => 136,  654 => 135,  641 => 126,  638 => 125,  628 => 124,  612 => 117,  608 => 116,  602 => 112,  592 => 111,  579 => 104,  575 => 103,  571 => 102,  568 => 101,  558 => 100,  544 => 97,  539 => 96,  536 => 95,  530 => 94,  526 => 92,  523 => 91,  518 => 90,  516 => 89,  511 => 87,  507 => 86,  504 => 85,  494 => 84,  482 => 107,  480 => 100,  477 => 99,  475 => 84,  470 => 82,  467 => 81,  457 => 80,  446 => 129,  444 => 124,  441 => 123,  439 => 111,  436 => 110,  434 => 80,  430 => 78,  420 => 77,  407 => 61,  397 => 60,  387 => 139,  384 => 138,  382 => 135,  377 => 132,  363 => 131,  360 => 77,  343 => 76,  340 => 75,  334 => 73,  332 => 72,  329 => 71,  323 => 69,  321 => 68,  318 => 67,  312 => 65,  310 => 64,  307 => 63,  304 => 60,  299 => 57,  293 => 54,  289 => 52,  286 => 51,  276 => 50,  265 => 36,  253 => 33,  250 => 32,  248 => 31,  245 => 30,  243 => 27,  242 => 26,  239 => 25,  234 => 23,  230 => 22,  227 => 21,  225 => 20,  221 => 18,  217 => 17,  215 => 16,  212 => 15,  202 => 14,  189 => 10,  186 => 9,  176 => 8,  165 => 38,  163 => 14,  160 => 13,  158 => 8,  154 => 6,  144 => 5,  134 => 140,  132 => 50,  129 => 49,  127 => 45,  124 => 44,  120 => 42,  118 => 41,  115 => 40,  112 => 5,  102 => 4,  91 => 186,  89 => 142,  86 => 141,  84 => 4,  80 => 2,  61 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block page_product_detail_review_widget %}
    <div class=\"sticky-top product-detail-review-widget\">

        {% block page_product_detail_review_info_container %}
            {% block page_product_detail_review_info %}
                <div class=\"product-detail-review-info js-review-info\">

                    {% block page_product_detail_review_title %}
                        <p class=\"product-detail-review-title h5\">
                            {{ \"detail.reviewTitle\"|trans({'%count%': reviews.total, '%total%':reviews.totalReviews })|sw_sanitize }}
                        </p>
                    {% endblock %}

                    {% block page_product_detail_review_overview %}
                        <div class=\"product-detail-review-rating\"
                            {% if productReviewCount > 0 %}
                                itemprop=\"aggregateRating\" itemscope itemtype=\"https://schema.org/AggregateRating\"
                            {% endif %}>

                            {% if productReviewCount > 0 %}
                                <meta itemprop=\"bestRating\" content=\"5\">
                                <meta itemprop=\"ratingCount\" content=\"{{ productReviewCount }}\">
                                <meta itemprop=\"ratingValue\" content=\"{{ productAvgRating }}\">
                            {% endif %}

                            {% sw_include '@Storefront/storefront/component/review/rating.html.twig' with {
                                points: productAvgRating,
                                style: 'text-primary'
                            } %}

                            {% if productReviewCount > 0 %}
                                <p class=\"h6\">
                                    {{ productAvgRating }} {{ \"detail.reviewAvgRate\"|trans|sw_sanitize }} {{ reviews.matrix.maxPoints }} {{ \"detail.reviewAvgRateElements\"|trans }}
                                </p>
                            {% endif %}
                        </div>
                    {% endblock %}
                </div>
            {% endblock %}

            {% if productReviewCount > 0 %}
                <hr>
            {% endif %}

            {% set formAjaxSubmitOptions = {
                replaceSelectors: [\".js-review-container\"],
                submitOnChange: true
            } %}

            {% block page_product_detail_review_filter %}
                {% if productReviewCount > 0 %}
                    <div class=\"js-review-filter\">
                        <form class=\"review-filter-form\"
                              action=\"{{ path('frontend.product.reviews', { productId: reviews.productId, parentId: reviews.parentId }) }}\"
                              method=\"post\"
                              data-form-ajax-submit=\"true\"
                              data-form-ajax-submit-options='{{ formAjaxSubmitOptions|json_encode }}'>

                            {# @deprecated tag:v6.5.0 - Block page_product_detail_review_filter_csrf will be removed. #}
                            {% block page_product_detail_review_filter_csrf %}
                                {{ sw_csrf('frontend.product.reviews') }}
                            {% endblock %}

                            {% if app.request.get('limit') %}
                                <input type=\"hidden\" name=\"limit\" value=\"{{ app.request.get('limit') }}\">
                            {% endif %}

                            {% if app.request.get('language') %}
                                <input type=\"hidden\" name=\"language\" value=\"{{ app.request.get('language') }}\">
                            {% endif %}

                            {% if app.request.get('sort') %}
                                <input type=\"hidden\" name=\"sort\" value=\"{{ app.request.get('sort') }}\">
                            {% endif %}

                            {% for matrix in reviews.matrix.matrix %}
                                {% block page_product_detail_review_filter_box %}
                                    <div class=\"row product-detail-review-filter\">

                                        {% block page_product_detail_review_filter_checkbox %}
                                            <div class=\"col-md-8 col-lg-5 product-detail-review-checkbox\">
                                                <div class=\"{{ formCheckboxWrapperClass }}\">

                                                    {% block page_product_detail_review_filter_checkbox_input %}
                                                        <input type=\"checkbox\"
                                                               class=\"{{ formCheckInputClass }}\"
                                                               id=\"reviewRating{{ matrix.points }}\"
                                                               name=\"points[]\"
                                                            {% if app.request.get('points') %}
                                                                {% for points in app.request.get('points') %}
                                                                    {% if points == matrix.points %}
                                                                        checked=\"checked\"
                                                                    {% endif %}
                                                                {% endfor %}
                                                            {% endif %}
                                                               value=\"{{ matrix.points }}\"
                                                            {% if matrix.count < 1 %}disabled{% endif %}>
                                                    {% endblock %}

                                                    {% block page_product_detail_review_filter_checkbox_label %}
                                                        <label class=\"custom-control-label text-nowrap\"
                                                               for=\"reviewRating{{ matrix.points }}\">
                                                            <small>{{ \"detail.review#{matrix.points}PointRatingText\"|trans|sw_sanitize }}
                                                                ({{ matrix.count }})</small>
                                                        </label>
                                                    {% endblock %}
                                                </div>
                                            </div>
                                        {% endblock %}

                                        {% block page_product_detail_review_filter_progressbar %}
                                            <div class=\"col d-none d-lg-block product-detail-review-progressbar-col\">
                                                <div class=\"progress product-detail-review-progressbar-container\">
                                                    <div class=\"progress-bar product-detail-review-progressbar-bar\"
                                                         role=\"progressbar\"
                                                         style=\"width: {{ matrix.percent }}%;\"
                                                         aria-valuenow=\"{{ matrix.percent }}\"
                                                         aria-valuemin=\"0\"
                                                         aria-valuemax=\"100\"></div>
                                                </div>
                                            </div>
                                        {% endblock %}

                                        {% block page_product_detail_review_filter_share %}
                                            <div class=\"col-12 col-md-3 product-detail-review-share\">
                                                <p><small>{{ matrix.percent|round }}%</small></p>
                                            </div>
                                        {% endblock %}
                                    </div>
                                {% endblock %}
                            {% endfor %}
                        </form>
                    </div>

                    {% block page_product_detail_review_filter_divider %}
                        <hr/>
                    {% endblock %}
                {% endif %}
            {% endblock %}
        {% endblock %}

        {% block page_product_detail_review_form_teaser %}
            <div class=\"product-detail-review-teaser js-review-teaser\">

                {% block page_product_detail_review_form_teaser_header %}
                    <p class=\"h4\">
                        {% if not reviews.customerReview %}
                            {{ \"detail.reviewTeaserTitle\"|trans|sw_sanitize }}
                        {% else %}
                            {{ \"detail.reviewExistsTeaserTitle\"|trans|sw_sanitize }}
                        {% endif %}
                    </p>
                {% endblock %}

                {% block page_product_detail_review_form_teaser_text %}
                    <p>
                        {% if not page.customerReview %}
                            {{ \"detail.reviewTeaserText\"|trans|sw_sanitize }}
                        {% else %}
                            {{ \"detail.reviewExistsTeaserText\"|trans|sw_sanitize }}
                        {% endif %}
                    </p>
                {% endblock %}

                {% block page_product_detail_review_form_teaser_button %}
                    <button class=\"btn btn-primary product-detail-review-teaser-btn\"
                            type=\"button\"
                            {{ dataBsToggleAttr }}=\"collapse\"
                            {{ dataBsTargetAttr }}=\".multi-collapse\"
                            aria-expanded=\"false\"
                            aria-controls=\"review-form review-list\">
                        <span class=\"product-detail-review-teaser-show\">
\t\t\t\t\t\t\t{% if not reviews.customerReview %}
                                {{ \"detail.reviewTeaserButton\"|trans|sw_sanitize }}
                            {% else %}
                                {{ \"detail.reviewExistsTeaserButton\"|trans|sw_sanitize }}
                            {% endif %}
\t\t\t\t\t\t</span>
                        <span class=\"product-detail-review-teaser-hide\">
\t\t\t\t\t\t\t{{ \"detail.reviewTeaserButtonHide\"|trans|sw_sanitize }}
\t\t\t\t\t\t</span>
                    </button>
                {% endblock %}
            </div>
        {% endblock %}
    </div>
{% endblock %}
", "@Storefront/storefront/page/product-detail/review/review-widget.html.twig", "/var/www/html/vendor/shopware/storefront/Resources/views/storefront/page/product-detail/review/review-widget.html.twig");
    }
}
