<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerCYabdBm\Shopware_Core_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerCYabdBm/Shopware_Core_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerCYabdBm.legacy');

    return;
}

if (!\class_exists(Shopware_Core_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerCYabdBm\Shopware_Core_KernelDevDebugContainer::class, Shopware_Core_KernelDevDebugContainer::class, false);
}

return new \ContainerCYabdBm\Shopware_Core_KernelDevDebugContainer([
    'container.build_hash' => 'CYabdBm',
    'container.build_id' => 'ec120162',
    'container.build_time' => 1689660044,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerCYabdBm');
